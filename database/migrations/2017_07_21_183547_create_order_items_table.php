<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_id')->nullable();
            $table->string('product_id')->nullable();
            $table->decimal('cost_unit',25,2)->default('0.00')->nullable();
            $table->string('qty')->nullable();
            $table->decimal('final_cost',25,2)->default('0.00')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}
