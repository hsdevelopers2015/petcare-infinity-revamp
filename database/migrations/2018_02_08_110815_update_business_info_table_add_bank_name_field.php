<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBusinessInfoTableAddBankNameField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function($table)
        {
            $table->string("last_subscription")->nullable()->after("remarks");
            $table->string("subscription_status")->nullable()->after("last_subscription");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function($table)
        {
            $table->dropColumn(array('last_subscription','subscription_status'));
        });
    }
}
