<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStockInTableAddSupplierFieldAndExpirationDateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_in', function($table)
        {
            $table->longText("supplier_name")->nullable()->after("status");
            $table->dateTime("expiration_date")->nullable()->after("status");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_in', function($table)
        {
            $table->dropColumn(array('supplier_name','expiration_date'));
        });
    }
}
