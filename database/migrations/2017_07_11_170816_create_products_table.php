<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('product_code')->nullable();
            $table->string('product_name')->nullable();
            $table->string('brand_partner')->nullable();
            $table->string('category')->nullable();
            $table->decimal('cost',25,2)->default('0.00')->nullable();
            $table->decimal('discount',25,2)->default('0')->nullable();

            $table->string('filename',150)->nullable();
            $table->text('directory')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
