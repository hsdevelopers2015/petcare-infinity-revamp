<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableProductsAddIsFocusFocusTypeAndFocusValueFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table)
        {
            $table->enum("focus_type",['amount','qty'])->nullable()->after("discount");
            $table->decimal("focus_value")->default('0.00')->nullable()->after("discount");
            $table->enum("is_focus",['no','yes'])->nullable()->after("discount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table)
        {
            $table->dropColumn(array('is_focus','focus_type','focus_value',));
        });
    }
}
