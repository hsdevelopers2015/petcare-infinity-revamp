<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBusinessInfoTableAddPrcNumberPurchaserAndAuthorizedPersonnelField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_info', function($table)
        {
            $table->longText("owners",150)->nullable()->after("user_id");
            $table->string("prc_number")->nullable()->after("user_id");
            $table->string("purchaser",150)->nullable()->after("user_id");
            $table->string("purchaser_number",150)->nullable()->after("user_id");
            $table->string("authorized_recipient",150)->nullable()->after("user_id");
            $table->string("authorized_recipient_number",150)->nullable()->after("user_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_info', function($table)
        {
            $table->dropColumn(array('prc_number','purchaser','purchaser_number','authorized_recipient','authorized_recipient_number','owners'));
        });
    }
}
