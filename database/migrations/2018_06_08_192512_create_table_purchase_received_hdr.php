<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePurchaseReceivedHdr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_received_hdr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchasehdr_code');
            $table->string('supplier_code');
            $table->string('invoice_number');
            $table->string('description');
            $table->integer('qty')->default('0')->nullable();
            $table->decimal('cost_price',9,2)->default("0.00");
            $table->decimal('selling_price',9,2)->default("0.00");
            $table->string('status')->default('posted');
            $table->timestamps();
            $table->softDeletes();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_received_hdr');
    }
}
