<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDeliveryReceiptNumberAddSiNumberField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_receipt', function($table)
        {
            $table->string("si_number")->nullable()->after("dr_number");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_receipt', function($table)
        {
            $table->dropColumn(array('si_number'));
        });
    }
}
