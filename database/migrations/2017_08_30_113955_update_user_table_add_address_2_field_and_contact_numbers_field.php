<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableAddAddress2FieldAndContactNumbersField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function($table)
        {
            $table->string("address_2")->nullable()->after("address");
            $table->string("contact_2",150)->nullable()->after("address");
            $table->string("contact_3",150)->nullable()->after("address");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function($table)
        {
            $table->dropColumn(array('address_2','contact_2','contact_3'));
        });
    }
}
