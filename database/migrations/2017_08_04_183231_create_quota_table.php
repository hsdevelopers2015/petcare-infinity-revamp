<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quota_configuration', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sales_quota')->nullable();
            $table->string('mother_quota')->nullable();
            $table->string('special_quota')->nullable();
            $table->string('group_quota')->nullable();
            $table->string('month')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quota_configuration');
    }
}
