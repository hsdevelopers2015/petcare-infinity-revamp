<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClustersAddQuotaAmountField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clusters', function($table)
        {
            $table->string("area_quota")->default('0')->nullable()->after("area");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clusters', function($table)
        {
            $table->dropColumn(array('area_quota'));
        });
    }
}
