<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExpensesTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_items', function($table)
        {
            $table->string("type")->nullable()->after("amount");
            $table->string("sub_type")->nullable()->after("amount");
            $table->integer("invoice_no")->default('0')->nullable()->after("amount");
            $table->integer("tin_number")->default('0')->nullable()->after("amount");
            $table->decimal("net_amount")->default('0')->nullable()->after("amount");
            $table->decimal("vat")->default('0')->nullable()->after("amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_items', function($table)
        {
            $table->dropColumn(array('type','sub_type','invoice_no','tin_number','net_amount','vat',));
        });
    }
}
