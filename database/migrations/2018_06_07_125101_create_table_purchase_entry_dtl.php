<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePurchaseEntryDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_entry_dtl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchasehdr_id');
            $table->string('purchasehdr_code');
            $table->string('supplier_code');
            $table->string('product_code');
            $table->string('product_name');
            $table->integer('qty')->default('0');
            $table->decimal('cost_price',9,2)->default("0.00");
            $table->decimal('selling_price',9,2)->default("0.00");
            $table->date('expiration_date')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_entry_dtl');
    }
}
