<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablePurchaseReceivedDtlAddColumnQuantityType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_received_dtl', function($table)
        {
            $table->string("quantity_type")->default('inwards_quantity')->after("product_name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_received_dtl', function($table)
        {
            $table->dropColumn(array('quantity_type'));
        });
    }
}
