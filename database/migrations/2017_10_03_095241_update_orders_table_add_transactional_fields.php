<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTableAddTransactionalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table)
        {
            $table->decimal("vat_sales",25,2)->nullable()->default('0.00')->after("mode_of_payment");
            $table->decimal("vat_ex",25,2)->nullable()->default('0.00')->after("mode_of_payment");
            $table->decimal("zero",25,2)->nullable()->default('0.00')->after("mode_of_payment");
            $table->string("vat_amount",25,2)->nullable()->default('12')->after("mode_of_payment");
            $table->string("dr_number")->nullable()->after("mode_of_payment");
            $table->string("prepared_by")->nullable()->after("mode_of_payment");
            $table->string("approved_by")->nullable()->after("mode_of_payment");
            $table->enum("invoice_is_created",['no','yes'])->nullable()->default('no')->after("mode_of_payment");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table)
        {
            $table->dropColumn(array('dr_number','vat_sales','vat_ex','zero','vat_amount','prepared_by','approved_by','invoice_is_created'));
        });
    }
}
