<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('clients', function (Blueprint $table) {
             $table->increments('id');
             $table->string('name')->nullable();
             $table->date('birthday')->nullable();             
             $table->string('email')->nullable();
             $table->string('contact_no')->nullable();
             $table->string('client_address')->nullable();
             $table->string('business_name')->nullable();
             $table->string('business_address')->nullable();

             $table->string('cluster_id')->nullable();
             $table->string('agent_id')->nullable();
             $table->string('status')->default('active');

   
             $table->timestamps();
             $table->softDeletes();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
