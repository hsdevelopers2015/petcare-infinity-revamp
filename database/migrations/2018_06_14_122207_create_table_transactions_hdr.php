<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionsHdr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_hdr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_code')->nullable();
            $table->string('account_type')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('supplier_code')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('description')->nullable();
            $table->decimal('cost',9,2)->default('0.00');
            $table->decimal('selling',9,2)->default('0.00');
            $table->string('status')->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions_hdr');
    }
}



