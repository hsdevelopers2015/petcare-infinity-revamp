<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTableAddQuotaTypeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table)
        {
            $table->enum("quota_type",['mother_quota','special_quota','group_quota'])->default('mother_quota')->nullable()->after("category");
            $table->string("unit")->nullable()->after("category");
            $table->string("replenish")->default(10)->nullable()->after("category");
            $table->string("deal_incentive")->nullable()->after("category");
            $table->string("deal_condition")->nullable()->after("category");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table)
        {
            $table->dropColumn(array('quota_type','unit','replenish','deal_incentive','deal_condition'));
        });
    }
}
