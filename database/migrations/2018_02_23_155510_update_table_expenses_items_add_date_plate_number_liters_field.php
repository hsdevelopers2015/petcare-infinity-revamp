<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableExpensesItemsAddDatePlateNumberLitersField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_items', function($table)
        {
            $table->date("expense_date")->nullable()->after("type");
            $table->string("plate_number")->nullable()->after("type");
            $table->string("liters")->nullable()->after("type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_items', function($table)
        {
            $table->dropColumn(array('expense_date','plate_number','liters',));
        });
    }
}
