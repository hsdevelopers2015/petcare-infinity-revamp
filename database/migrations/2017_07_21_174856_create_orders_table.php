<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_code')->nullable();
            $table->string('client_id')->nullable();
            $table->string('total_qty',25,2)->default('0')->nullable();
            $table->decimal('total_amount',25,2)->default('0')->nullable();
            $table->enum('status',['draft','cancelled','order_confirmation','shipped','delivered'])->default('draft')->nullable();
            $table->enum('mode_of_payment',['cash_on_delivery','dated_checks','post_dated_cheque'])->default('cash_on_delivery')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
