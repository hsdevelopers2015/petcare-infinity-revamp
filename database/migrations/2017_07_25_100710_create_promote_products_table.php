<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoteProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promote_products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sa_id')->nullable();
            $table->string('product_id')->nullable();
            $table->text('description')->nullable();
            
            $table->string('filename',150)->nullable();
            $table->text('directory')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promote_products');
    }
}
