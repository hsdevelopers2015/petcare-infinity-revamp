<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionsDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_dtl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_code')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('account_type')->nullable();
            $table->string('supplier_code')->nullable();
            $table->string('brand_code')->nullable();
            $table->string('product_code')->nullable();
            $table->string('product_name')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('stock_trasfer_in')->default('no');
            $table->string('stock_trasfer_out')->default('no');
            $table->string('unit')->nullable();
            $table->string('quantity')->nullable();
            $table->string('cost')->nullable();
            $table->string('selling')->nullable();
            $table->date('expiration_date')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions_dtl');
    }
}

   
