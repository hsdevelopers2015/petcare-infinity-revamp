<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');

            $table->string('fname');
            $table->string('lname');
            $table->string('sa_id')->nullable();
            $table->string('fb_id');
            $table->string('email');
            $table->string('username')->unique();
            $table->string('password');
            $table->date('birthdate');
            $table->string('cluster_id')->nullable();
            $table->enum('gender',['male','female'])->default('male');
            $table->string('contact',150)->nullable();
            $table->text('address');
            $table->string('type',50)->default('user');
            $table->string('position',150)->nullable();
            $table->string('segment')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->enum("is_lock",['yes','no'])->default('no');
            $table->string('filename')->nullable();
            $table->text('directory')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
