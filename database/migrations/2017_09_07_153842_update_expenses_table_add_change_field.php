<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExpensesTableAddChangeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function($table)
        {
            $table->decimal("change")->default('0.00')->nullable()->after("requested_amount");
            $table->decimal("reimbursement_amount")->default('0.00')->nullable()->after("requested_amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function($table)
        {
            $table->dropColumn(array('change','reimbursement_amount'));
        });
    }
}
