<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_code')->nullable();
            $table->string('client_id')->nullable();
            $table->string('sa_id')->nullable();
            $table->decimal('total_qty',25,2)->default('0')->nullable();
            $table->decimal('total_amount',25,2)->default('0.00')->nullable();
            $table->enum('status',['posted','draft','cancelled','posted_cancelled','received','for_delivery'])->default('draft')->nullable();
            $table->enum('mode_of_payment',['cash_on_delivery','dated_checks','post_dated_cheque'])->default('cash_on_delivery')->nullable();
            $table->string('payment_term')->nullable();
            $table->string('payment_status')->default('pending')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->longText('remarks')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
