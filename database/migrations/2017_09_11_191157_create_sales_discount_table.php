<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_discount', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sales_id')->nullable();
            $table->enum('discount_type',['amount','percentage'])->nullable();
            $table->string('discount_amount')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_discount');
    }
}
