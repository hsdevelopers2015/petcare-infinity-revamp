<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTransactionHdrAddColumnPurchaseReturnPo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions_hdr', function($table)
        {
            $table->string("purchase_return_po")->nullable()->after("supplier_code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions_hdr', function($table)
        {
            $table->dropColumn(array('purchase_return_po'));
        });
    }
}
