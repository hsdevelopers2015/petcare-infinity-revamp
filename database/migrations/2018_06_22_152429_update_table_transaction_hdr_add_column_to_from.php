<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTransactionHdrAddColumnToFrom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions_hdr', function($table)
        {
            $table->string("from")->nullable()->after("transaction_code");
            $table->string("to")->nullable()->after("from");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions_hdr', function($table)
        {
            $table->dropColumn(array('from','to'));
        });
    }
}
