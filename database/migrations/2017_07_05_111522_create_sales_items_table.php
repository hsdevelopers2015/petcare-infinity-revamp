<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_items', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_id')->nullable();
            $table->string('product_id')->nullable();
            $table->decimal('cost_unit',25,2)->default('0.00')->nullable();
            $table->string('qty')->default('0')->nullable();
            $table->decimal('discount',25,2)->default('0.00')->nullable();
            $table->string('discount_type')->nullable();
            $table->decimal('final_cost',25,2)->default('0.00')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_items');
    }
}
