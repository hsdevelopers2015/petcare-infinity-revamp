<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdtaeSalesDiscountTableAddDiscountNameField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_discount', function($table)
        {
            $table->string("discount_name")->nullable()->after("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_discount', function($table)
        {
            $table->dropColumn(array('discount_name'));
        });
    }
}
