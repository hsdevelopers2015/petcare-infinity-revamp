<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->string('business_name');
            $table->string('business_type')->nullable();
            $table->string('business_location')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('directory')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('business_info');
    }
}
