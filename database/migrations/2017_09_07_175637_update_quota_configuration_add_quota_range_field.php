<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuotaConfigurationAddQuotaRangeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quota_configuration', function($table)
        {
            $table->enum("date_range",['month','year'])->default('month')->nullable()->after("month");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quota_configuration', function($table)
        {
            $table->dropColumn(array('date_range'));
        });
    }
}
