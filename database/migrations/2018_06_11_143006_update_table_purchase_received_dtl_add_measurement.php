<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablePurchaseReceivedDtlAddMeasurement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('purchase_received_dtl', function($table)
        {
            $table->string("measurement_code")->nullable()->after("qty");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_received_dtl', function($table)
        {
            $table->dropColumn(array('measurement_code'));
        });
    }
}
