<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_returns', function (Blueprint $table) {
            $table->increments('id');

            $table->string('stock_id')->nullable();
            $table->string('transaction_code')->nullable();
            $table->string('product_id')->nullable();
            $table->string('qty')->nullable();
            $table->enum('status',['draft','approved'])->default('draft')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_returns');
    }
}
