<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTransactionDtlAddColumnPoOrderReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions_dtl', function($table)
        {
            $table->string("purchase_return_po")->nullable()->after("brand_code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions_dtl', function($table)
        {
            $table->dropColumn(array('purchase_return_po'));
        });
    }
}
