<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('expense_id')->nullable();
            $table->string('sa_id')->nullable();
            $table->string('request_code')->nullable();
            $table->string('description')->nullable();
            $table->decimal('requested_amount')->default('0.00')->nullable();
            $table->enum('type',['liquidation','reimbursement'])->nullable();
            $table->enum('status',['submitted','denied','approved'])->default('submitted')->nullable();
            
            $table->string('filename',150)->nullable();
            $table->text('directory')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
    }
}
