<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sa_id')->nullable();
            $table->string('request_id')->nullable();
            $table->string('description')->nullable();
            $table->enum('type',['liquidation','reimbursement'])->nullable();
            $table->decimal('total_amount')->default('0.00')->nullable();
            $table->decimal('requested_amount')->default('0.00')->nullable();
            $table->enum('status',['draft','submitted','denied','cancelled','approved'])->default('draft')->nullable();
            
            $table->string('filename',150)->nullable();
            $table->text('directory')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses');
    }
}
