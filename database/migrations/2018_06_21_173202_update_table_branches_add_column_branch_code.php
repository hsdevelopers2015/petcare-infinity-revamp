<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableBranchesAddColumnBranchCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function($table)
        {
            $table->string("branch_code")->nullable()->after("id");
            $table->string("branch_email")->nullable()->after("branch_contact");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('branches', function($table)
        {
            $table->dropColumn(array('branch_code','branch_email'));
        });
    }
}
