<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuotaConfigurationTableAddLuzonVisayasMindanaoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quota_configuration', function($table)
        {
            $table->string("mindanao")->default('0')->nullable()->after("mother_quota");
            $table->string("visayas")->default('0')->nullable()->after("mother_quota");
            $table->string("luzon")->default('0')->nullable()->after("mother_quota");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quota_configuration', function($table)
        {
            $table->dropColumn(array('luzon','visayas','mindanao'));
        });
    }
}
