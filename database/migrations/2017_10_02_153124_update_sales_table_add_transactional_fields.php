<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSalesTableAddTransactionalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function($table)
        {
            $table->decimal("vat_sales",25,2)->nullable()->default('0.00')->after("status");
            $table->decimal("vat_ex",25,2)->nullable()->default('0.00')->after("status");
            $table->decimal("zero",25,2)->nullable()->default('0.00')->after("status");
            $table->string("vat_amount",25,2)->nullable()->default('12')->after("status");
            $table->string("dr_number")->nullable()->after("status");
            $table->string("approved_by")->nullable()->after("status");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function($table)
        {
            $table->dropColumn(array('dr_number','vat_sales','vat_ex','zero','vat_amount','approved_by',));
        });
    }
}
