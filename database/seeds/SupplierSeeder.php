<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\Supplier;

class SupplierSeeder extends Seeder{
	public function run(){

		$supplier = Supplier::find(1);

		if($supplier){
			$input = ['supplier_code' => "warehouse", 
			'supplier_name' => "Warehouse", 
			'type' => "warehouse",
			'supplier_address' => 'Central',
			'supplier_email' => "warehouse@yahoo.com",
			'supplier_contact_number' => "123456789",
			'supplier_contact_person' => "John Doe",
			'status' => "active"];
			$supplier->fill($input);
			$supplier->save();
		}else{
				Supplier::create(
			    	['id'=> 1,
			    	'supplier_code' => "warehouse", 
			    	'supplier_name' => "Warehouse",
					'type' => "warehouse",
					'supplier_address' => 'Central',
					'supplier_email' => "warehouse@yahoo.com",
					'supplier_contact_number' => "123456789",
					'supplier_contact_person' => "John Doe",
					'status' => "active"]
			    );
		}
	   
	}
}