<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\User;

class AdminSeeder extends Seeder{
	public function run(){

		$first_user = User::find(1);

		if($first_user){
			$input = ['username' => "admin", 'email' => "admin@domain.com",'password' => bcrypt('admin'),'fname' => "Super",'lname' => "Admin","type" => "super_user"];
			$first_user->fill($input);
			$first_user->save();
		}else{
				User::create(
			    	['id'=> 1,'username' => "admin", 'email' => "admin@domain.com",'password' => bcrypt('admin'),'fname' => "Super",'lname' => "Admin","type" => "super_user"]
			    );
		}
	   
	}
}