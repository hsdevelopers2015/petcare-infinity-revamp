<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\AccessControl;

class AccessControlModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	AccessControl::truncate();

        AccessControl::create(
            [
                'module_name' => "request_form",
            ]
        );

        AccessControl::create(
            [
                'module_name' => "quota",
            ]
        );

        AccessControl::create(
            [
                'module_name' => "clients",
            ]
        );

        AccessControl::create(
            [
                'module_name' => "product_returns",
            ]
        );

        AccessControl::create(
            [
                'module_name' => "clustering",
            ]
        );

        AccessControl::create(
            [
                'module_name' => "reporting",
            ]
        );


        AccessControl::create(
            [
                'module_name' => "products",
            ]
        );
    }
}
