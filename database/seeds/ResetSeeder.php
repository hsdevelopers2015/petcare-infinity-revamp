<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\User;

class ResetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('products')->truncate();
    	// DB::table('user_timeline')->truncate();
    	// DB::table('transaction_logs')->truncate();
    	// DB::table('stock_in')->truncate();
    	// DB::table('sent_contracts')->truncate();
    	// DB::table('branches')->truncate();
    	// DB::table('clusters')->truncate();
    	// DB::table('contracts')->truncate();
    	// DB::table('delivery_receipt')->truncate();
    	// DB::table('expenses')->truncate();
    	// DB::table('expenses_items')->truncate();
    	// DB::table('inventory')->truncate();
    	// DB::table('order_discount')->truncate();
    	// DB::table('order_items')->truncate();
    	// DB::table('orders')->truncate();
    	// DB::table('product_returns')->truncate();
    	// DB::table('promote_products')->truncate();
    	// DB::table('quota_configuration')->truncate();
    	// DB::table('requests')->truncate();
    	// DB::table('sales')->truncate();
    	// DB::table('sales_discount')->truncate();
    	// DB::table('sales_items')->truncate();
    	

    	// $users = User::where('type','client')->where('cluster_id','6')->get();

    	// foreach($users as $index => $user){
    	// 	$user->forceDelete();
    	// }
    }
}
