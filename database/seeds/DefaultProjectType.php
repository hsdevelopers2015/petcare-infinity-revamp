<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\ProjectType;

class DefaultProjectType extends Seeder{
	public function run(){
		ProjectType::truncate();
		ProjectType::create(
	    	['code' => "web_wordpress",
	    	 'name' => "Website (Wordpress CMS)"]
	    );

	    ProjectType::create(
	    	['code' => "web_laravel",
	    	 'name' => "Website (Laravel CMS)"]
	    );

	    ProjectType::create(
	    	['code' => "web_app_laravel",
	    	 'name' => "Webapp (Laravel Framework)"]
	    );

	    ProjectType::create(
	    	['code' => "web_app_mobile",
	    	 'name' => "Webapp + Mobile App (iOS and Android)"]
	    );

	    ProjectType::create(
	    	['code' => "web_app_android",
	    	 'name' => "Webapp + Android Mobile App"]
	    );

	    ProjectType::create(
	    	['code' => "web_app_ios",
	    	 'name' => "Webapp + iOS Mobile App"]
	    );

	    ProjectType::create(
	    	['code' => "android_app",
	    	 'name' => "Android Mobile App"]
	    );

	    ProjectType::create(
	    	['code' => "ios_app",
	    	 'name' => "iOS Mobile App"]
	    );

	    ProjectType::create(
	    	['code' => "poster_design",
	    	 'name' => "Poster Design"]
	    );

	    ProjectType::create(
	    	['code' => "logo_branding",
	    	 'name' => "Logo / Branding"]
	    );

	    ProjectType::create(
	    	['code' => "app_assets",
	    	 'name' => "Application Assets"]
	    );

	    ProjectType::create(
	    	['code' => "web_design",
	    	 'name' => "Website Design"]
	    );

	    ProjectType::create(
	    	['code' => "char_design",
	    	 'name' => "Character Designs"]
	    );

	    ProjectType::create(
	    	['code' => "emoji_assets",
	    	 'name' => "Emoji Assets"]
	    );

	    ProjectType::create(
	    	['code' => "tshirt_design",
	    	 'name' => "T-shirt Design"]
	    );

	}
}