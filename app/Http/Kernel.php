<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        // \GeneaLabs\LaravelCaffeine\Http\Middleware\LaravelCaffeineDripMiddleware::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            // \App\Http\Middleware\VerifyCsrfToken::class,
        ],
        'web-no-crsf' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'backoffice.auth' => \App\Laravel\Middleware\Backoffice\Authenticate::class,
        'backoffice.guest' => \App\Laravel\Middleware\Backoffice\RedirectIfAuthenticated::class,
        'backoffice.lock' => \App\Laravel\Middleware\Backoffice\Lock::class,
        'backoffice.super_user_only' => \App\Laravel\Middleware\Backoffice\SuperUserOnly::class,

        'api.tokenizer' => \App\Laravel\Middleware\Api\ApiTokenizer::class,
        'api.auth' => \App\Laravel\Middleware\Api\Authenticate::class,

        'frontend.auth' => \App\Laravel\Middleware\Frontend\Authenticate::class,
        'frontend.guest' => \App\Laravel\Middleware\Frontend\RedirectIfAuthenticated::class,
        'frontend.lock' => \App\Laravel\Middleware\Frontend\Lock::class,

    ];
}
