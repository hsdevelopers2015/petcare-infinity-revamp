<?php 

namespace App\Laravel\Services;

/*
*
* Models used for this class
*/
use App\Laravel\Models\ImageDirectory;

/*
*
* Classes used for this class
*/
use App\Http\Requests\Request;
use Carbon, Session, Str, File, Image;

class ImageUploader {

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	public static function upload(Request $request, $directory = "uploads", array $thumbnail = ['height' => 250, 'width' => 250]){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",95);
		Image::make("{$path_directory}/{$filename}")->crop($thumbnail['width'],$thumbnail['height'])->save("{$thumb_directory}/{$filename}",95);

		$new_image = new ImageDirectory;
		$new_image->directory = $path_directory;
		$new_image->filename = $filename;
		$new_image->save();
		
		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}