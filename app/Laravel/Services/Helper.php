<?php 

namespace App\Laravel\Services;

use App\Laravel\Models\Invoice;
use App\Laravel\Models\Cluster;
use App\Laravel\Models\SupportLogs;
use App\Laravel\Models\UserTimeline;
use App\Laravel\Models\BusinessInfo;
use App\Laravel\Models\SupportThread;
use App\Laravel\Models\TransactionLog;
use App\Laravel\Models\BillingHistory;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\Branch;
use Route,Str,Carbon,Input,DB,Curl;

class Helper{

  	/**
	 * Parse date to the specified format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_format($time,$format = "M d, Y @ h:i a") {
		return $time == "0000-00-00 00:00:00" ? "" : date($format,strtotime($time));
	}

	/**
	 * Parse date to the 'date only' format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_only($time) {
		return Self::date_format($time, "F d, Y");
	}

	/**
	 * Parse date to the 'time only' format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function time_only($time) {
		return Self::date_format($time, "g:i A");
	}

	/**
	 * Parse date to the standard sql date format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */

	
	public static function date_db($time) {
		return $time == "0000-00-00 00:00:00" ? "" : date(env('DATE_DB',"Y-m-d"),strtotime($time));
	}

	/**
	 * Parse date to the standard sql datetime format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return date
	 */
	public static function datetime_db($time) {
		return $time == "0000-00-00 00:00:00" ? "" : date(env('DATETIME_DB',"Y-m-d H:i:s"),strtotime($time));
	}

	/**
	 * Parse date to a greeting
	 *
	 * @param date $time
	 *
	 * @return string
	 */
	public static function greet($time = NULL) {
		if(!$time) $time = Carbon::now();
		$hour = date("G",strtotime($time));
		
		if($hour < 5) {
			$greeting = "You woke up early";
		}elseif($hour < 10){
			$greeting = "Good morning";
		}elseif($hour <= 12){
			$greeting = "It's almost lunch";
		}elseif($hour < 18){
			$greeting = "Good afternoon";
		}elseif($hour <= 22){
			$greeting = "Good evening";
		}else{
			$greeting = "You must be working really hard";
		}

		return $greeting;
	}

	public static function convert_to_hr_min($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	/**
	 * Shows time passed
	 *
	 * @param date $time
	 *
	 * @return string
	 */
	public static function time_passed($time){
		$current_date = Carbon::now();
		$secsago = strtotime($current_date) - strtotime($time);
		$nice_date = 1;
		if ($secsago < 60):
			if($secsago < 30){ return "Just Now.";}
		    $period = $secsago == 1 ? '1 second'     : $secsago . ' seconds';
		elseif ($secsago < 3600) :
		    $period    =   floor($secsago/60);
		    $period    =   $period == 1 ? '1 minute' : $period . ' minutes';
		elseif ($secsago < 86400):
		    $period    =   floor($secsago/3600);
		    $period    =   $period == 1 ? '1 hour'   : $period . ' hours';
		elseif ($secsago < 432000):
		    $period    =   floor($secsago/86400);
		    $period    =   $period == 1 ? '1 day'    : $period . ' days';
		else:
		   $nice_date = 0;
		   $period = date("M d, Y",strtotime($time));
		endif;
		if($nice_date == 1)
			return $period." ago";
		else
			return $period;
	}

	public static function next_sequence($id){

			$initial_counter = 0000;			
			$current_sequence = TransactionHeader::orderBy('transaction_code',"DESC")->first();
			// dd($current_sequence->transaction_code);
			return $current_sequence? str_pad(($current_sequence->transaction_code)+1, 5, "0", STR_PAD_LEFT) : str_pad(($initial_counter)+1, 5, "0", STR_PAD_LEFT);
	}

	public static function branch_next_sequence($id){

			$initial_counter = 0000;			
			$current_sequence = Branch::orderBy('branch_code',"DESC")->first();
			// dd($current_sequence->transaction_code);
			return $current_sequence? str_pad(($current_sequence->branch_code)+1, 5, "0", STR_PAD_LEFT) : str_pad(($initial_counter)+1, 5, "0", STR_PAD_LEFT);
	}

	/**
	 * Checks if route is active
	 *
	 * @param array $routes
	 * @param string $class
	 *
	 * @return string
	 */
	public static function is_active(array $routes, $class = "active") {
		return  in_array(Route::currentRouteName(), $routes) ? $class : NULL;
	}
	
	/**
	 * Generate seven uppercase characters randomly
	 *
	 * @return string
	 */
	public static function create_ucode(){
		return Str::upper(Str::random(7));
	}

	/**
	 * Create a filename
	 *
	 * @param string $extension
	 *
	 * @return string
	 */
	public static function create_filename($extension){
		return Str::lower(Str::random(10).date("mdYhs")).".".$extension;
	}

	/**
	 * Gets the excerpt of a string
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function get_excerpt($str){
		$paragraphs = explode("<br>", $str);
	    return Str::words(strip_tags($paragraphs[0]),20);
	}

	/**
	 * Improved array diff method
	 *
	 * @param array $a
	 * @param array $b
	 *
	 * @return array
	 */
	public static function array_diff($a, $b) {
	    $map = $out = array();
	    foreach($a as $val) $map[$val] = 1;
	    foreach($b as $val) if(isset($map[$val])) $map[$val] = 0;
	    foreach($map as $val => $ok) if($ok) $out[] = $val;
	    return $out;
	}

	/**
	 * Gets the slug of a string
	 *
	 * @param string $str
	 * @param string $tbl
	 * @param string $col
	 *
	 * @return string
	 */
	public static function get_slug($tbl, $col, $val){
		$slug = Str::slug($val);
		$check_slug = DB::table($tbl)->where("{$col}",'like',"%{$val}%")->count();
		if($check_slug != 0) $slug .= ++$check_slug;
		return $slug;
	}

	/**
	 * Translates a number to a short alhanumeric version
	 *
	 * Translated any number up to 9007199254740992
	 * to a shorter version in letters e.g.:
	 * 9007199254740989 --> PpQXn7COf
	 *
	 * specifiying the second argument true, it will
	 * translate back e.g.:
	 * PpQXn7COf --> 9007199254740989
	 *
	 * @param mixed   $in	  String or long input to translate
	 * @param boolean $to_num  Reverses translation when true
	 * @param mixed   $pad_up  Number or boolean padds the result up to a specified length
	 * @param string  $pass_key Supplying a password makes it harder to calculate the original ID
	 *
	 * @return mixed string or long
	 */
	public static function alphaID($in, $to_num = false, $pad_up = false, $pass_key = null) {
		$out   =   '';
		$index = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$base  = strlen($index);

		if ($pass_key !== null) {
			// Although this function's purpose is to just make the
			// ID short - and not so much secure,
			// with this patch by Simon Franz (http://blog.snaky.org/)
			// you can optionally supply a password to make it harder
			// to calculate the corresponding numeric ID

			for ($n = 0; $n < strlen($index); $n++) {
				$i[] = substr($index, $n, 1);
			}

			$pass_hash = hash('sha256',$pass_key);
			$pass_hash = (strlen($pass_hash) < strlen($index) ? hash('sha512', $pass_key) : $pass_hash);

			for ($n = 0; $n < strlen($index); $n++) {
				$p[] =  substr($pass_hash, $n, 1);
			}

			array_multisort($p, SORT_DESC, $i);
			$index = implode($i);
		}

		if ($to_num) {
			$len = strlen($in) - 1;
			for ($t = $len; $t >= 0; $t--) {
				$bcp = bcpow($base, $len - $t);
				$out = $out + strpos($index, substr($in, $t, 1)) * $bcp;
			}
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$out -= pow($base, $pad_up);
				}
			}
		} else {
			// Digital number  -->>  alphabet letter code
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$in += pow($base, $pad_up);
				}
			}
			for ($t = ($in != 0 ? floor(log($in, $base)) : 0); $t >= 0; $t--) {
				$bcp = bcpow($base, $t);
				$a   = floor($in / $bcp) % $base;
				$out = $out . substr($index, $a, 1);
				$in  = $in - ($a * $bcp);
			}
		}
		return $out;
	}

	/**
	 * Explode the string to an array then explode each values per element
	 *
	 * @var char/string $delimeter1
	 * @var char/string $delimeter2
	 * @var string $srt
	 *
	 * @return string
	 */
	public static function parse_prizes($delimeter1, $delimeter2, $str){
		$response = explode($delimeter1, $str);
		foreach ($response as $key => $param) {
			$response["{$key}"] = explode($delimeter2, $param);
		}
		return $response;
	}

	/**
	* Compute the total wins of an array
	*
	* @var array $arr
	*
	* @return int
	*/
	public static function count_total_wins($arr){
		$total = 0;
		foreach ($arr as $value) {
			$total += $value[1];
		}
		return $total;
	}

	/**
	* Get prize description
	*
	* @var array $arr
	*
	* @return int
	*/
	public static function get_prize_desc($arr, $index){
		$response = NULL;
		$count = 0;
		foreach ($arr as $value) {
			$count += $value[1];
			if($index <= $count){
				$response = $value[0];
				break;
			}
		}
		return $response;
	}

	public static function status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'no': $badge = "<span class='label label-block label-striped border-left-grey'>" . Str::upper(str_replace("_", " ", "For Approval")) . "</span>"; break;
			case 'yes': $badge = "<span class='label label-block label-striped border-left-success'>" . Str::upper(str_replace("_", " ", "Approved")) . "</span>"; break;
			case 'active': $badge = "<span class='notice notice-blue label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'expired': $badge = "<span class='notice notice-danger label label-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>";  break;
			case 'paid': $badge = "<span class='notice notice-blue label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'pending': $badge = "<span class='notice notice-warning label label-warning'>" . Str::upper(str_replace("_", " ", $status)) . "</span>";  break;
			case 'overdue': $badge = "<span class='notice notice-danger label label-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'open': $badge = "<span class='notice notice-blue label label-info'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'closed': $badge = "<span class='notice notice-default label label-default'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'low': $badge = "<span class='notice notice-blue label label-sold'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'medium': $badge = "<span class='notice notice-warning label label-warning'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'high': $badge = "<span class='notice notice-danger label label-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='label label-block label-striped border-left-default'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function leads_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'closed_lost': $badge = "<span class='notice notice-danger label label-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'stale': $badge = "<span class='notice notice-default label label-default'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='notice notice-green label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function support_logs($user_id,$client_id,$support_id,$action){
		$new_support_logs = new SupportLogs;
		$new_support_logs->user_id = $user_id;
		$new_support_logs->client_id = $client_id;
		$new_support_logs->support_id = $support_id;
		$new_support_logs->action = $action;
		$new_support_logs->save();
	}

	public static function billing_history($user_id,$client_id,$invoice_id,$action,$type){
		$new_billing_history = new BillingHistory;
		$new_billing_history->user_id = $user_id;
		$new_billing_history->client_id = $client_id;
		$new_billing_history->invoice_id = $invoice_id;
		$new_billing_history->action = $action;
		$new_billing_history->type = $type;
		$new_billing_history->save();
	}

	public static function count_thread_message($support_id,$type){
		if($type=='client'){
			$count = SupportThread::where('support_id',$support_id)->where('client_is_read','no')->count();
			if($count>0){
				return '<span class="bubble visible-lg-inline">'.$count.'</span>';
			}
		}else{
			$count = SupportThread::where('support_id',$support_id)->where('admin_is_read','no')->count();
			if($count>0){
				return $count.' unread '.Str::plural('message',$count);
			}else{
				return "Read";
			}
		}
	}

	public static function count_message($support_id,$type){
		if($type=='client'){
			$count = SupportThread::where('support_id',$support_id)->where('client_is_read','no')->count();
			if($count>0){
				return $count." new ".Str::plural('message',$count);
			}else{
				return "Read";
			}
		}
	}

	public static function weather($weather){
		if($weather=='Rain'){
			return "rain";
		}elseif($weather=='Clouds'){
			return "cloudy";
		}else{
			return "clear-day";
		}
	}

	public static function bg_image($type){
		switch ($type) {
			case 'apps':
				return asset('crm/services/apps.png');
				break;

			case 'custom-apps':
				return asset('crm/services/custom-apps.png');
				break;

			case 'marketing':
				return asset('crm/services/marketing.png');
				break;

			case 'designs':
				return asset('crm/services/design.png');
				break;

			case 'cloud-apps':
				return asset('crm/services/cloud.png');
				break;

			case 'web-apps':
				return asset('crm/services/web.png');
				break;
			
			default:
				return asset('crm/services/apps.png');
				break;
		}
	}

	public static function type_description($type){
		switch ($type) {
				
			case 'apps':
				return "Mobile Applications";
				break;

			case 'custom-apps':
				return "Custom Applications";
				break;

			case 'marketing':
				return "Digital Marketing";
				break;

			case 'designs':
				return "Multimedia Designs";
				break;

			case 'cloud-apps':
				return "Cloud Applications";
				break;

			case 'web-apps':
				return "Web Applications";
				break;
			
			default:
				return "Mobile Applications";
				break;
		}
	}

	public static function type_price($type){
		switch ($type) {
				
			case 'apps':
				return "PHP&nbsp;150,000";
				break;

			case 'custom-apps':
				return "PHP&nbsp;150,000";
				break;

			case 'marketing':
				return "PHP&nbsp;150,000";
				break;

			case 'designs':
				return "PHP&nbsp;5,000";
				break;

			case 'cloud-apps':
				return "PHP&nbsp;150,000";
				break;

			case 'web-apps':
				return "PHP&nbsp;10,000&nbsp;to&nbsp;65,000";
				break;
			
			default:
				return "PHP&nbsp;150,000";
				break;
		}
	}

	public static function timeline_status($status){
		switch ($status) {
				
			case 'activated':
				return "success";
				break;

			case 'inquiry':
				return "success";
				break;
			
			default:
				return "info";
				break;
		}
	}

	public static function timeline_status_icon($status){
		switch ($status) {
				
			case 'activated':
				return "check";
				break;

			case 'inquiry':
				return "info";
				break;
			
			default:
				return "flag";
				break;
		}
	}

	public static function avail_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'inquiry': $badge = "<span class='notice notice-success label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='notice notice-info label label-info'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function commission_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'released': $badge = "<span class='notice notice-success label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='notice notice-info label label-info'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function amount($number,$sepator = ","){
		return number_format($number,2,".",$sepator);
	}

	public static function currency_converter($currency,$amount) {

	$api_currency = Curl::to('http://www.apilayer.net/api/live?access_key=dba41a84eb5446337402545615f01c2e')
		// ->withData( ['access_key' => "dba41a84eb5446337402545615f01c2e",'currencies' => "USD",'format'=>"1"])
		->asJson()
		->get();
		// dd($currency->quotes->USDPHP);
		// dd($currency->quotes->USDSGD);
		switch ($currency) {
			case 'php':
			$peso = $amount;
			$us_dollar = $amount/$api_currency->quotes->USDPHP;
			$sin_dollar = ($amount/$api_currency->quotes->USDPHP)*$api_currency->quotes->USDSGD;
				break;

			case 'usd':
			$us_dollar = $amount;
			$peso = $amount*$api_currency->quotes->USDPHP;
			$sin_dollar = $amount*$api_currency->quotes->USDSGD;
				break;

			case 'sgd':
			$sin_dollar = $amount;
			$us_dollar = $amount/$api_currency->quotes->USDSGD;
			$peso = $amount/$api_currency->quotes->USDSGD*$api_currency->quotes->USDPHP;
				break;
			
			default:
			$peso = $amount;
			$us_dollar = $amount/$api_currency->quotes->USDPHP;
			$sin_dollar = ($amount/$api_currency->quotes->USDPHP)*$api_currency->quotes->USDSGD;
				break;
		}
		
		$amounts = ['peso' => $peso,'us_dollar' => $us_dollar,'sin_dollar' => $sin_dollar,];

		return $amounts;
	}

	public static function invoice($id) {
		$invoice = Invoice::find($id);

		return $invoice;
	}

	public static function progress($value) {
		$progress = 0;
		switch ($value) {
			case $value>=75:
				$progress = 'success';
				break;

			case $value>50 AND $value<75:
				$progress = 'info';
				break;

			case $value<=50 AND $value>=25:
				$progress = 'warning';
				break;

			case $value<25:
				$progress = 'danger';
				break;
			
			default:
				$progress = 'info';
				break;
		}

		return $progress;
	}

	public static function type_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'encoder': $badge = "<span class='tag tag-default tag-primary tag-default'>" . Str::upper(str_replace("_", " ", 'Inventory Officer')) . "</span>"; break;
			case 'sales_agent': $badge = "<span class='tag tag-default tag-info tag-default'>" . Str::upper(str_replace("_", " ", $col)) . "</span>"; break;
			default: $badge = "<span class='tag tag-default tag-primary tag-default'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function stockin_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'posted': $badge = "<span class='tag tag-success label label-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-info label label-info'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function sales_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'cash_on_delivery': $badge = "<span class='tag tag-info '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'dated_checks': $badge = "<span class='tag tag-primary '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function payment_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'pending': $badge = "<span class='tag tag-warning '>".Str::upper('Collectibles')."</span>"; break;
			case 'partially_paid': $badge = "<span class='tag tag-info '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function order_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'draft': $badge = "<span class='tag tag-default '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'cancelled': $badge = "<span class='tag tag-danger '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'shipped': $badge = "<span class='tag tag-primary '>SHIPPING</span>"; break;
			case 'order_confirmation': $badge = "<span class='tag tag-info '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-success'>RECEIVED</span>"; break;
		}

		return $badge;
	}

	public static function request_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'liquidation': $badge = "<span class='tag tag-primary '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-info'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function expense_status_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'draft': $badge = "<span class='tag tag-default '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'approved': $badge = "<span class='tag tag-success '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'submitted': $badge = "<span class='tag tag-info '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'cancelled': $badge = "<span class='tag tag-warning '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

	public static function user_timeline($user_id,$reference_id,$reference_name,$action,$action_type) {
		$user_timeline = new UserTimeline;

		$user_timeline->user_id = $user_id;
		$user_timeline->reference_id = $reference_id;
		$user_timeline->reference_name = $reference_name;
		$user_timeline->action = $action;
		$user_timeline->action_type = $action_type;

		$user_timeline->save();

	}

	public static function action_icon($type){
		switch ($type) {
			case 'create':
				return "fa fa-plus-circle";
				break;

			case 'delete':
				return "fa fa-trash";
				break;

			case 'update':
				return "fa fa-pencil";
				break;

			case 'ship':
				return "icon-basket-loaded";
				break;

			case 'sent':
				return "fa fa-envelope";
				break;

			case 'promote':
				return "fa fa-check";
				break;

			case 'receive':
				return "ft-box";
				break;

			case 'submit':
				return "fa fa-check";
				break;
			
			default:
				return "fa fa-plus-circle";
				break;
		}
	}

	public static function action_color($type){
		switch ($type) {
			case 'create':
				return "success";
				break;

			case 'delete':
				return "danger";
				break;

			case 'update':
				return "info";
				break;

			case 'ship':
				return "primary";
				break;

			case 'sent':
				return "warning";
				break;

			case 'promote':
				return "primary";
				break;

			case 'receive':
				return "warning";
				break;

			case 'submit':
				return "blue";
				break;
			
			default:
				return "teal";
				break;
		}
	}

	public static function sales_badge($col) {
		$status = Str::lower($col);
		switch ($status) {
			case 'draft': $badge = "<span class='tag tag-danger '>NEW</span>"; break;
			case 'posted': $badge = "<span class='tag tag-success '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'received': $badge = "<span class='tag tag-primary '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'for_delivery': $badge = "<span class='tag tag-success '>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='tag tag-danger'>CANCELLED</span>"; break;
		}

		return $badge;
	}

	public static function cluster($cluster_id){
		$cluster = Cluster::find($cluster_id);

		return $cluster;
	}

	public static function get_business($id){
		$business = BusinessInfo::find($id);

		return $business;
	}

	public static function transaction_log($user_id,$reference_id,$reference,$status,$action,$description){
		$transaction_log = new TransactionLog;
		$transaction_log->user_id = $user_id;
		$transaction_log->reference_id = $reference_id;
		$transaction_log->reference = $reference;
		$transaction_log->status = $status;
		$transaction_log->action = $action;
		$transaction_log->description = $description;
		$transaction_log->save();
	}
}

