<?php namespace App\Laravel\Services;

use Illuminate\Validation\Validator;
use Auth,Hash,Str,Input;

use App\Laravel\Models\IPAddress;
use App\Laravel\Models\User;
use App\Laravel\Models\EventRegistrant;
use App\Laravel\Models\Quota;
use App\Laravel\Models\Commission;
use App\Laravel\Models\Inventory;

// use App\Laravel\Models\Masterfile\User;
// use App\Laravel\Models\Referral\Referral as MyReferral;
// use App\Laravel\Models\Masterfile\ShippingMethod;
// use App\Laravel\Models\Transaction\Product;
// use App\Laravel\Models\Masterfile\ProductList;
// use App\Laravel\Models\UserDelivery;
// use App\Laravel\Models\Masterfile\Currency;
// use App\Laravel\Models\Masterfile\UOM;
// use App\Laravel\Models\Masterfile\UOMType;
// use App\Laravel\Models\Language;
// use App\Laravel\Models\Country;

class CustomValidator extends Validator {

    public function validateEmailRegistrant($attribute,$value,$parameters){
        $event_id = 0;
        if(isset($parameters[0])){
            $event_id = $parameters[0];
        }

        $email = Str::lower($value);
        
        $is_unique = EventRegistrant::where('event_id',$event_id)->where('email',$email)->first();
        return $is_unique ? FALSE : TRUE;

    }

    public function validateCheckQuantity($attribute,$value,$parameters){
        
        $check = Inventory::where('product_id',$parameters[0])->first();
        if($parameters[0]){
            if($value > $check->quantity){
                $is_valid_amount = FALSE;
            }else{
                $is_valid_amount = TRUE;
            }
        }else{
            $is_valid_amount= FALSE;
        }

        return $is_valid_amount;
    }

    public function validateCheckPassword($attribute,$value,$parameters){
        
        $check = User::where('id',$parameters[0])->first();
        if($parameters[0]){
            // dd(bcrypt($value)." - ".$check->password);
            if(Hash::check($value,$check->password)){
                $is_valid_password = TRUE;
            }else{
                $is_valid_password = FALSE;
            }
        }else{
            $is_valid_password = FALSE;
        }

        return $is_valid_password;
    }

    public function validateUniqueUsername($attribute,$value,$parameters){
        $username = Str::lower($value);
        $user_id = FALSE;
        if($parameters){
            $user_id = $parameters[0];
        }

        if($user_id){
            $is_unique = User::where('id','<>',$user_id)->whereRaw("LOWER(username) = '{$username}'")->first();
        }else{
            $is_unique = User::whereRaw("LOWER(username) = '{$username}'")->first();
        }

        return $is_unique ? FALSE : TRUE;

    }

    public function validateIsEmailExist($attribute,$value,$parameters){
        $email = Str::lower($value);

        $is_email_exist = User::where('email',$email)->first();

        if($is_email_exist){
            return TRUE;
        }

        return FALSE;
    }

    public function validatePhoneRegistrant($attribute,$value,$parameters){
        $event_id = 0;
        if(isset($parameters[0])){
            $event_id = $parameters[0];
        }

        $contact_number = Str::lower($value);
        
        $is_unique = EventRegistrant::where('event_id',$event_id)->where('contact_number',$contact_number)->first();
        return $is_unique ? FALSE : TRUE;

    }

    public function validateUniqueIp($attribute,$value,$parameters){
        $record_id = FALSE;
        if(isset($parameters[0])){
            $record_id = $parameters[0];
        }

        if($record_id){
            $is_unique = IPAddress::where('id','<>',$record_id)->where('ip',$value)->first();
        }else{
            $is_unique = IPAddress::where('ip',$value)->first();
        }

        return $is_unique ? FALSE : TRUE;
    }

    public function validateUniqueFb($attribute,$value,$parameters){
        $fb_id = Str::lower($value);
        $is_unique = User::whereRaw("LOWER(fb_id) = '{$fb_id}'")->whereIn('type',['buyer','seller'])->first();

        return $is_unique ? FALSE : TRUE;
    }

    public function validateMore($attribute,$value,$parameters){
        $less_value = isset($parameters[0])? $parameters[0] : 0;

        return $value >= $less_value ? TRUE : FALSE;
    }

    public function validateLess($attribute,$value,$parameters){
        $more_value = isset($parameters[0])? $parameters[0] : 0;

        return $more_value >= $value ? TRUE : FALSE;
    }

    public function validateValidReferral($attribute,$value,$parameters){
        return User::where('referral_code',$value)->count() == 1 ? TRUE : FALSE;
    }

    public function validateCheckReferral($attribute,$value,$parameters){
        return MyReferral::where('referral_code',$value)->count() == 0 ? TRUE : FALSE;
    }

    public function validateShippingMethod($attribute,$value,$parameters){
        return ShippingMethod::where('id',$value)->count() == 1 ? TRUE :FALSE;
    }

    public function validateUniqueEmail($attribute,$value,$parameters){
        $email = Str::lower($value);
        $user_id = FALSE;
        if($parameters){
            $user_id = $parameters[0];
        }

        if($user_id){
            $is_unique = User::where('id','<>',$user_id)->where('email',$email)->whereIn('type',['buyer','seller'])->first();
        }else{
            $is_unique = User::where('email',$email)->whereIn('type',['buyer','seller'])->first();
        }

        return $is_unique ? FALSE : TRUE;
    }

    public function validateOldPassword($attribute,$value,$parameters){
        // $user = User::find(Input::get('user_id',0));
        if($parameters){
            $user_id = $parameters[0];
            $user = User::find($user_id);
            return Hash::check($value,$user->password);
        }

        return FALSE;
    }

    public function validatePasswordFormat($attribute,$value,$parameters){
        return preg_match(("/^(?=.*)[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{2,25}$/"), $value);
    }

    public function validateValidUser($attribute,$value,$parameters){
        $value = Str::lower($value);
        $valid_users = ["buyer","seller"];

        return in_array($value, $valid_users);
    }

    public function validateValidGender($attribute,$value,$parameters){
        $gender = Str::lower($value);
        $valid_genders = ["male","female"];

        return in_array($gender, $valid_genders);
    }

    public function validateValidListingType($attribute,$value,$parameters){
        $value = Str::lower($value);
        $valid_types = ["buying","selling"];

        return in_array($value, $valid_types);
    }

    public function validateValidDefOption($attribute,$value,$parameters){
        $value = Str::lower($value);
        $valid_options = ["yes","no"];

        return in_array($value, $valid_options);
    }

    public function validateInStock($attribute,$value,$parameters){
        $product = Product::find($parameters[0]);

        if($product->always_available == "yes"){
            return TRUE;
        }else{
            if($product->stock_available AND $product->stock_available >= $value){
                return TRUE;
            }
            return FALSE;
        }
    }

    public function validateValidUom($attribute,$value,$parameters){
        $code = Str::lower($value);

        $uom = UOM::where('code',$code)->first();

        if($uom){
            return TRUE;
        }

        return FALSE;
    }

    public function validateValidUomType($attribute,$value,$parameters){
        $code = Str::lower($value);

        $type = UOMType::where('code',$code)->first();

        if($type){
            return TRUE;
        }

        return FALSE;
    }

    public function validateValidCurrency($attribute,$value,$parameters){
        $code = Str::lower($value);
        $currency = Currency::where('code',$code)->first();

        if($currency){
            return TRUE;
        }

        return FALSE;
    }

    public function validateValidMobileType($attribute,$value,$parameters){
        $value = Str::lower($value);
        $valid_mobile = ["telephone","mobile"];

        return in_array($value, $valid_mobile);
    }

    public function validateMyShipping($attribute,$value,$parameters){
        $check_delivery_info = UserDelivery::where('user_id',$parameters[0])->where('id',$value)->first();

        if($check_delivery_info){
            return TRUE;
        }

        return FALSE;
    }

    public function validateValidDateRange($attribute,$value,$parameters){
        $from = Input::get($parameters[0]);
        $to = Input::get($parameters[1]);

        $bool = 1;

        if($from AND $to){
            $reference_date = date_create(Helper::date_db($from));
            $comparative_date = date_create(Helper::date_db($to));
            $date_difference = date_diff($reference_date,$comparative_date)->format("%R%a") + 0;
            $bool = ( $date_difference >= 0) ? TRUE : FALSE;
        }

        return $bool;
    }

    public function validateValidAccountType($attribute,$value,$parameters){
        $type = Str::lower($value);
        $valid_types = ["bancnet","gcash"];

        return in_array($type, $valid_types);
    }

    public function validateValidSettingValue($attribute,$value,$parameters){
        $type = Input::get($parameters[0]);
        $value = Input::get($parameters[1]);

        $valid_value = 0;
        switch(Str::lower($type)) {
            case 'language':
                $value = Language::where("id",$value)->count();
            break;
            
            case 'country':
                $value = Country::where("id",$value)->count();
            break;

            case 'currency':
                $value = Currency::where("id",$value)->count();
            break;

            default:
                $value = 0;
            break;
        }

        return $value;
    }

    public function validateValidProduct($attribute,$value,$parameters){
        // $title = Str::lower($value);
        // $list = ProductList::whereRaw("LOWER(title) = '?'",[$title])->first();
        $is_exist = ProductList::where('title',$value)->first();
        if($is_exist){
            return TRUE;
        }

        return FALSE;
    }

    public function validateValidThreshold($attribute,$value,$parameters){
        $total_commission = [];
        $quota = Quota::orderBy('created_at','DESC')->first();
        $commission_threshold = $quota->commission_threshold;
        $commissions = Commission::where('invoice_id',$parameters[0])->get();
        foreach($commissions as $index => $info){
            array_push($total_commission,$info->commission);
        }
        $total_commission = array_sum($total_commission);

        $total_commissions = $total_commission + $value;

        if($total_commissions>$commission_threshold){
            $is_valid = FALSE;
        }else{
            $is_valid = TRUE;
        }

        return  $is_valid;
    }

    public function validateValidPartner($attribute,$value,$parameters){
        $partner = Commission::where('partner_id',$parameters[0])->where('invoice_id',$parameters[1])->first();

        return  $partner ?  FALSE: TRUE;
    }



    public function validateComputeMotherquota($attribute,$value,$parameters){

        // $check = Inventory::where('product_id',$parameters[0])->first();
        // if($parameters[0]){
        //     if($value > $check->quantity){
        //         $is_valid_amount = FALSE;
        //     }else{
        //         $is_valid_amount = TRUE;
        //     }
        // }else{
        //     $is_valid_amount= FALSE;
        // }

        // return $is_valid_amount;
    }

} 