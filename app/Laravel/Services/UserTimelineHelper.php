<?php 

namespace App\Laravel\Services;

use App\Laravel\Models\SupportLogs;
use App\Laravel\Models\Invoice;
use App\Laravel\Models\SupportThread;
use App\Laravel\Models\BillingHistory;
use Route,Str,Carbon,Input,DB,Curl;

class  UserTimelineHelper{

  	/**
	 * Parse date to the specified format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function timeline($time,$format = "M d, Y @ h:i a") {
		return $time == "0000-00-00 00:00:00" ? "" : date($format,strtotime($time));
	}
}

