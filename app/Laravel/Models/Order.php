<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class Order extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_code','client_id','total_qty','total_amount','status','mode_of_payment','vat_sales','at_ex','zero','vat_amount','dr_number','prepared_by','approved_by','invoice_is_created'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function client_info($id){
		return User::where('id',$id)->withTrashed()->first();
	}

	public function sales_agent_info($id){
        return User::where('id',$id)->withTrashed()->first();

    }

    public function check_dr($id,$reference){
		$check = DeliveryReceipt::where('reference_id',$id)->where('reference',$reference)->count();

		if($check == 1){
			return 1;
		}else{
			return 0;
		}
	}

	public function users() {
		return $this->hasOne('App\Laravel\Models\User', 'id', 'client_id')->withTrashed();
	}

	public function scopeCheckUser($query, $mark = "yes"){
		return $query->has('users')->withTrashed();
	}

	public function cluster_info($user_id){
		$check = User::where('id',$user_id)->withTrashed()->first();
	
		$cluster = Cluster::where('id',$check->cluster_id)->first();

		return $cluster;
	}

	public function cluster_agent($id){
		$user = User::where('id',$id)->withTrashed()->first();

		$cluster_id = $user->cluster_id;

		$sales_agent = User::where('type','sales_agent')->where('cluster_id',$cluster_id)->withTrashed()->first();

		return $sales_agent->fname.' '.$sales_agent->lname;
	}

	public function dr() {
		return $this->hasOne('App\Laravel\Models\DeliveryReceipt', 'reference_id', 'id')->where('reference','orders');
	}

}