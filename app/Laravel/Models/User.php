<?php

namespace App\Laravel\Models;

use App\Laravel\Models\User;
use App\Laravel\Models\Commission;
use App\Laravel\Models\Quota;
use App\Laravel\Models\QuotaConfiguration;
use App\Laravel\Models\Sales;
use App\Laravel\Models\Cluster;
use App\Laravel\Models\Order;
use App\Laravel\Models\Branch;
use App\Laravel\Models\AccessControl;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Models\Attendance;
use App\Laravel\Traits\DateFormatterTrait;

use Carbon,Helper,Str;


class User extends Authenticatable
{
    use SoftDeletes, DateFormatterTrait;
    
    /**
     * Enable soft delete in table
     * @var boolean
     */
    protected $softDelete = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'fname','lname','email','birthdate','gender','contact','address','type','fb_id','sa_id','position','username','time_in','grace_period_min','status','directory','filename','password','theme','layout','ao_id','quota_amount','cluster_id','segment','remarks','address_2','contact_2','contact_3','area','client_type','last_subscription'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    protected $appends = ['is_login','attendance_id'];


    // public function getIsLoginAttribute(){
    //     $date_today = Helper::date_db(Carbon::now());

    //     $is_login = Attendance::where('user_id',$this->id)->whereRaw("DATE(login) = '{$date_today}'")->first();
    //     $result = FALSE;
    //     if($is_login){
    //         $result = TRUE;
    //     }

    //     return $result;

    // }

    public function getAttendanceIdAttribute(){
        $date_today = Helper::date_db(Carbon::now());

        $is_login = Attendance::where('user_id',$this->id)->whereRaw("DATE(login) = '{$date_today}'")->first();
        $result = 0;
        if($is_login){
            $result = $is_login->id;
        }

        return $result;
    }

    public function settings(){
        $my_settings =  $this->hasMany('App\Laravel\Models\UserSetting','user_id','id')->get();

        $results  = [];
        foreach($my_settings as $index => $row){
            switch($row->type){
                case 'file':
                $data = ["type" => $row->type , "filename" => $row->filename,"directory" => $row->directory];
                break;
                default:
                $data = ["type" => $row->type,"value" => $row->value];

            }
            $results[Str::lower($row->code)] =$data;
            
        }


        return $results;
    }

    public function business_info(){
        return $this->hasOne("App\Laravel\Models\BusinessInfo",'user_id','id')->withTrashed();
    }

    public function salesagent_info(){
        return $this->hasOne("App\Laravel\Models\User",'id','sa_id')->withTrashed();
    }

    public function account_officer(){
        return $this->hasOne('App\Laravel\Models\User','id','ao_id')->withTrashed();
        
    }

    public function orders(){
        return $this->hasOne('App\Laravel\Models\Order','client_id','id');
        
    }

    // public function cluster_info(){
    //     return $this->belongsTo('App\Laravel\Models\Cluster','cluster_id','id');
    // }

    public function client_count($id){

        $count = User::where('cluster_id',$id)->where('type','client')->withTrashed()->get();
        return $count;

    }

    public function clients_count($id){

        $count = User::where('cluster_id',$id)->where('type','client')->withTrashed()->get();
        return $count;

    }

    public function partner_commission($id){

        $quota_config = Quota::orderBy('created_at','DESC')->first();
        
        $from = date(env('DATE_DB',"Y-m-d"),strtotime($quota_config->from));
        $to = date(env('DATE_DB',"Y-m-d"),strtotime($quota_config->to));

        
        $my_total_amount = [];
        $commission = Commission::where('partner_id',$id)->get();
        foreach($commission as $index => $info){
            $invoice_id = $info->invoice_id;
            $my_invoice = Invoice::whereRaw("DATE(updated_at) >= '{$from}'")->whereRaw("DATE(updated_at) <= '{$to}'")->where('status','paid')
            ->where('id',$invoice_id)->get();

            $my_current_amount = 0;
            foreach ($my_invoice as $index => $info) {
                $discount =  $info->discount_exact_amount;
                $vatable = $info->amount - $discount;

                if($info->wh_tax=="yes"){
                    $wh_tax = $vatable*($info->wh_tax/100);
                }else{
                    $wh_tax = "";
                }

                if($info->type=="exclusive"){
                    $vatable = $vatable;
                    $vat = $vatable * ($info->vat/100);
                    $my_current_amount = $vatable+$vat-$wh_tax;
                }else{
                    $vatable = $vatable/(1+($info->vat/100));
                    $vat = $vatable * ($info->vat/100);
                    $my_current_amount = $vatable+$vat-$wh_tax;
                }
                array_push($my_total_amount, $my_current_amount);
            }

        }

        $my_total_amount = array_sum($my_total_amount);

        return $my_total_amount;
    }

    public function count_client_income($id){
        $clients = User::where('sa_id',$id)->withTrashed()->get();

        $total = [];

        foreach($clients as $index => $info){
            $sales = Sales::where('client_id',$info->id)->first();
            if($sales){
               array_push($total, $sales->total_amount);
           }else{
            $total = [];
        }
    }

    return array_sum($total);
}

public function get_clients($id){
    $clients = User::where('cluster_id',$id)->where('type','client')->withTrashed()->get();

    $client_ids = [];

    foreach($clients as $index => $info){
        // $clients = User::where('sa_id',$info->id)->get();

        // foreach($clients as $index=>$info){
        //     array_push($all_clients,$info->id);
        // }

        array_push($client_ids,$info->id);
    }

    return $client_ids;
}

public function cluster_id($id){
    return User::where('cluster_id',$id)->where('type','sales_agent')->orWhere('type','sales_head')->withTrashed()->get();
}

public function team_member($id){
    $head = User::find($id);
    $cluster_id = $head->cluster_id;
    $team_members = User::where('cluster_id',$cluster_id)->where('id','!=',$id)->where('type','sales_agent')->withTrashed()->get();

    return $team_members;

}

public function cluster($id){
    return Cluster::where('id',$id)->first();
}

public function unpaid_bills($id){
    $date_today = date(env('DATE_DB',"Y-m-d"),strtotime(Carbon::now()));

    $sales = Sales::where('client_id',$id)->where('payment_status','pending')->where('status','posted')->whereRaw("DATE(due_date) <= '{$date_today}'")->get();

    $total = [];

    foreach ($sales as $index => $info) {
        array_push($total,$info->total_amount);
    }

    return array_sum($total);
}

public function collectible_bills($id){
    $date_today = date(env('DATE_DB',"Y-m-d"),strtotime(Carbon::now()));

    $sales = Sales::where('client_id',$id)->where('payment_status','pending')->where('status','posted')->whereRaw("DATE(due_date) > '{$date_today}'")->get();

    $total = [];

    foreach ($sales as $index => $info) {
        array_push($total,$info->total_amount);
    }

    return array_sum($total);
}

public function paid_bills($id){
    $sales = Sales::where('client_id',$id)->where('payment_status','!=','pending')->where('status','posted')->get();

    $total = [];

    foreach ($sales as $index => $info) {
        array_push($total,$info->total_amount);
    }

    return array_sum($total);
}

public function total_purchased($id){
    
    $date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
    if(!$date_range){
        $from = date('Y-m-1');
        $to = date('Y-m-t');
    }else{
        if($date_range->date_range == 'month'){
            $from = date('Y-m-1');
            $to = date('Y-m-t');
        }else{
            $from = date('Y-1-1');
            $to = date('Y-12-31');
        }
    }

    $order = Order::where('client_id',$id)->where('status','delivered')->where('created_at','<=',$to)->where('created_at','>=',$from)->get();

    return $order;
}

public function total_orders($id){
    $order = Order::where('client_id',$id)->whereIn('status',['shipped','order_confirmation'])->get();

    return $order;
}


public function total_ordering_customers($id){

    $customers = User::where('cluster_id',$id)->where('type','client')->withTrashed()->get();

    $client_id = [];

    foreach($customers as $index => $info){
        array_push($client_id,$info->id);
    }

    $ordering_customers = Order::whereIn('client_id',$client_id)->whereIn('status',['shipped','order_confirmation'])->get();

    return $ordering_customers;

}

public function on_going_sales($id,$user_id,$sales_type){

    $customers = User::where('cluster_id',$id)->withTrashed()->get();

    $client_id = [];

    foreach($customers as $index => $info){
        array_push($client_id,$info->id);
    }

    $sales = Sales::whereIn('client_id',$client_id)->where('sa_id',$user_id)->where('status','posted')->where('payment_status','pending')->get();

    $total_amount = [];
    foreach($sales as $index => $info){
        array_push($total_amount,$info->total_amount);
    }

    if($sales_type == "sales"){
        return $sales;
    }else{
        return array_sum($total_amount);
    }

}

public function total_sales($id,$span){

    $date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
    if(!$date_range){
        $from = date('Y-m-1');
        $to = date('Y-m-t');
    }else{
        if($date_range->date_range == 'month'){
            $from = date('Y-m-1');
            $to = date('Y-m-t');
        }else{
            $from = date('Y-1-1');
            $to = date('Y-12-31');
        }
    }

    if($span=="all"){
        $sales = Sales::where('sa_id',$id)->where('status','posted')->whereIn('payment_status',['fully_paid','partially_paid'])->get();
    }elseif($span=="month"){
        $sales = Sales::where('sa_id',$id)->where('status','posted')->whereIn('payment_status',['fully_paid','partially_paid'])->where('created_at','<=',$to)->where('created_at','>=',$from)->get();
    }else{
        $sales = Sales::where('sa_id',$id)->where('status','posted')->where('payment_status','pending')->get();
    }

    $total_amount = [];

    foreach($sales as $index => $info){
        array_push($total_amount,$info->total_amount);
    }

    return array_sum($total_amount);
}

public function invoice_count($id){
    $date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
    if(!$date_range){
        $from = date('Y-m-1');
        $to = date('Y-m-t');
    }else{
        if($date_range->date_range == 'month'){
            $from = date('Y-m-1');
            $to = date('Y-m-t');
        }else{
            $from = date('Y-1-1');
            $to = date('Y-12-31');
        }
    }
    $invoice = Sales::where('client_id',$id)->where('status','posted')->where('created_at','<=',$to)->where('created_at','>=',$from)->get();

    return $invoice;
}

public function scopeCheckOrder($query, $mark = "yes"){
    return $query->has('orders');
}

public function check_order($id){
    $date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
    if(!$date_range){
        $from = date('Y-m-1');
        $to = date('Y-m-t');
    }else{
        if($date_range->date_range == 'month'){
            $from = date('Y-m-1');
            $to = date('Y-m-t');
        }else{
            $from = date('Y-1-1');
            $to = date('Y-12-31');
        }
    }
    $total = Order::whereIn('status',['order_confirmation','shipped','delivered'])->where('created_at','<=',$to)->where('created_at','>=',$from)->get();

    $total_quantity = [];

    foreach ($total as $index => $info) {
        array_push($total_quantity,$info->total_qty);
    }

    $total_quantity = array_sum($total_quantity);

    $qty = [];

    $client = Order::where('client_id',$id)->whereIn('status',['order_confirmation','shipped','delivered'])->where('created_at','<=',$to)->where('created_at','>=',$from)->get();

    foreach ($client as $index => $info) {
        array_push($qty,$info->total_qty);
    }

    $qty = array_sum($qty);

    if($qty == 0){
        return 0;
    }else{
        return round(($qty/$total_quantity)*100,2);
    }

}

public function branch_count($id){
    return Branch::where('user_id',$id)->count();
}

public function check_user_access($id){
    return AccessControl::where('user_ids','like','% '.$id.',%')->count();
}

}
