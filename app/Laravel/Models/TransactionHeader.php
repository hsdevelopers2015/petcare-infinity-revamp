<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;

class TransactionHeader extends Model{
	
	use DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transactions_hdr';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_code','account_type','transaction_type','supplier_code','purchase_return_po','invoice_number','description','cost','selling','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function sa_lists($id){
		return User::where('cluster_id',$id)->whereIn('type',['sales_agent'])->orderBy('area')->get();
	}

	public function supplier(){
		return $this->belongsTo('App\Laravel\Models\Supplier','supplier_code','supplier_code')->withTrashed();
	}

	public function purchase_entry_dtl(){
		return $this->hasMany('App\Laravel\Models\PurchaseEntryDetail','purchasehdr_code','purchase_code')->orderBy('created_at');
	}

	public function transaction_dtl(){
		return $this->hasMany('App\Laravel\Models\TransactionDetail','transaction_code','transaction_code')->orderBy('created_at');
	}

	public function details()
	{
		return $this->hasMany(TransactionDetail::class, 'transaction_code', 'transaction_code')->orderBy('created_at');
	}

}