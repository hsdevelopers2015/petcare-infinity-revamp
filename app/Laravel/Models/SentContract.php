<?php namespace App\Laravel\Models;

use App\Laravel\Models\Contract;
use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class SentContract extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sent_contracts';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sa_id','client_id','contract_id'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function contract_info($id){
		return Contract::where('id',$id)->first();
	}

	public function client_info($id){
		return User::where('id',$id)->withTrashed()->first();
	}

	public function sa_info($id){
		return User::where('id',$id)->withTrashed()->first();
	}

	public function client(){
        return $this->hasOne("App\Laravel\Models\User",'id','client_id')->withTrashed();
    }

	public function sales_agent(){
        return $this->hasOne("App\Laravel\Models\User",'id','sa_id')->withTrashed();
    }

	public function contract(){
        return $this->hasOne("App\Laravel\Models\Contract",'id','contract_id')->withTrashed();
    }

    public function scopeCheckClient($query, $mark = "yes"){
		return $query->has('client');
	}

	public function scopeCheckSalesagent($query, $mark = "yes"){
		return $query->has('sales_agent');
	}

	public function scopeCheckContract($query, $mark = "yes"){
		return $query->has('contract');
	}

}