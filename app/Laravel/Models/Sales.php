<?php namespace App\Laravel\Models;

use App\Laravel\Models\Inventory;
use App\Laravel\Models\SalesItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Cluster;
use App\Laravel\Models\DeliveryReceipt;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

use Carbon;

class Sales extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_code','client_id','sa_id','total_qty','total_amount','balance','status','due_date','due_day','vat_sales','vat_ex','zero','vat_amount','dr_number','approved_by'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = ['aging'];

	public function getAgingAttribute() {
		// if($this->due_day AND $this->status == "received") {
		// 	$due_day = $this->due_day * 24;
		// 	$received_at = Carbon::parse($this->updated_at);
		// 	$divisor = $due_day * 60;
		// 	$diff = $received_at->diffInMinutes(Carbon::now(), false);
		// 	$aging = 100 - round($diff / $divisor * 100);
		// 	return $aging > 0 ? $aging : 0;
		// }
		// return 100;
	}

	public function sales_items($id){
		$items = SalesItem::where('transaction_id',$id)->get();

		foreach($items as $index => $info){
			$product_id = $info->product_id;
			$product_qty = $info->qty;
			$product_free = $info->free? : 0;

			$inventory = Inventory::where('product_id',$product_id)->first();

			$inventory->quantity = $inventory->quantity - $product_qty - $product_free;

			$inventory->save();
		}
	}

	public function client_info($id){
		return User::where('id',$id)->withTrashed()->first();
	}

	public function sales_agent_info($id){
		return User::where('id',$id)->withTrashed()->first();
	}

	public function check_dr($id,$reference){
		$check = DeliveryReceipt::where('reference_id',$id)->where('reference','sales')->count();

		if($check >= 1){
			return 1;
		}else{
			return 0;
		}
	}

	public function users() {
		return $this->hasOne('App\Laravel\Models\User', 'id', 'client_id')->withTrashed();
	}

	public function scopeCheckUser($query, $mark = "yes"){
		return $query->has('users')->withTrashed();
	}

	public function cluster_info($user_id){
		$check = User::where('id',$user_id)->withTrashed()->first();
	
		$cluster = Cluster::where('id',$check->cluster_id)->first();

		return $cluster;
	}

	public function cluster_agent($id){
		$user = User::where('id',$id)->withTrashed()->first();

		$cluster_id = $user->cluster_id;

		$sales_agent = User::where('type','sales_agent')->where('cluster_id',$cluster_id)->withTrashed()->first();

		if($sales_agent){
			return $sales_agent->fname.' '.$sales_agent->lname;
		}else{
			return "No sales agent yet";
		}
	}

	public function dr() {
		return $this->hasOne('App\Laravel\Models\DeliveryReceipt', 'reference_id', 'id')->where('reference','sales');
	}
}