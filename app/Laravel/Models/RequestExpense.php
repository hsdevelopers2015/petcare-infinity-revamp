<?php namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class RequestExpense extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'requests';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['expense_id','sa_id','description','filename','directory','type','request_code'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function expenses(){
		return $this->hasOne("App\Laravel\Models\Expense",'request_id','id')->withTrashed();
	}

	public function scopeRequestExpense($query, $mark = "yes"){
        return $query->has('expenses');
    }

    public function sa_info($id){
    	return User::where('id',$id)->withTrashed()->first();
    }

}