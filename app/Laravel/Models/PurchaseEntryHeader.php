<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class PurchaseEntryHeader extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'purchase_entry_hdr';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['purchase_code','supplier_code','description','qty','cost_price','selling_price','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function sa_lists($id){
		return User::where('cluster_id',$id)->whereIn('type',['sales_agent'])->orderBy('area')->get();
	}

	public function supplier(){
		return $this->belongsTo('App\Laravel\Models\Supplier','supplier_code','supplier_code')->withTrashed();
	}

	public function purchase_entry_dtl(){
		return $this->hasMany('App\Laravel\Models\PurchaseEntryDetail','purchasehdr_code','purchase_code')->orderBy('created_at');
	}

}