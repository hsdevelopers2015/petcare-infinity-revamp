<?php namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use App\Laravel\Models\SalesItem;

class SalesDiscount extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales_discount';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sales_id','discount_name','discount_type','discount_amount'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function sales(){
        return $this->hasOne("App\Laravel\Models\Sales",'id','sales_id');
    }

    public function sales_items($id){
    	$items = SalesItem::where('transaction_id',$id)->get();

    	$total_amount = [];
    	foreach($items as $index => $info){
    		array_push($total_amount,$info->final_cost);
    	}

    	return array_sum($total_amount);
    }

}