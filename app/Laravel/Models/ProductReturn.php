<?php namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class ProductReturn extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_returns';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['stock_id','transaction_code','product_id','qty','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function stock_in(){
        return $this->hasOne("App\Laravel\Models\StockIn",'id','stock_id');
    }

    public function scopeCheckStock($query, $mark = "yes"){
		return $query->has('stock_in');
	}
}