<?php namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class Branch extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'branches';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['client_id','name','birthday','email','contact_no','client_address','business_name','business_address','cluster_id','agent_id','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function business_info(){
        return $this->hasOne("App\Laravel\Models\BusinessInfo",'id','business_id');
    }

    public function user_info(){
        return $this->hasOne("App\Laravel\Models\User",'id','user_id')->withTrashed();
    }

    public function scopeCheckUser($query, $mark = "yes"){
		return $query->has('user_info');
	}

}