<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class PurchaseReceivedDetail extends Model{
	
	// use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'purchase_received_dtl';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['purchasehdr_code','supplier_code','product_code','product_name','qty','measurement_code','cost_price','selling_price','expiration_date','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function supplier(){
		return $this->belongsTo('App\Laravel\Models\Supplier','supplier_code','supplier_code');
	}
	public function product(){
		return $this->belongsTo('App\Laravel\Models\Product','product_code','product_code');
	}



}