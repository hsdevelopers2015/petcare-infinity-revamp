<?php namespace App\Laravel\Models;

use App\Laravel\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class StockIn extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stock_in';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_code','product_id','qty','status','acquisition_cost','supplier_name','expiration_date'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function product_info($id){
		return Product::where('id',$id)->first();
	}

	public function product(){
		return $this->hasOne('App\Laravel\Models\Product','id','product_id')->withTrashed();
	}

	public function scopeCheckProduct($query, $mark = "yes"){
		return $query->has('product');
	}

}