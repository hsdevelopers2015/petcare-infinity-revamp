<?php namespace App\Laravel\Models;

use App\Laravel\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class OrderItem extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_items';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_id','product_id','cost_unit','qty','discount','discount_type','final_cost'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function product_info($id){
        return Product::where('id',$id)->first();
    }

    public function orders() {
		return $this->hasOne('App\Laravel\Models\Order', 'id', 'transaction_id')->where('status','delivered');
	}

	public function scopeCheckDelivered($query, $mark = "yes"){
		return $query->has('orders');
	}

	

}