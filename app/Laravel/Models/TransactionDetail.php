<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;

class TransactionDetail extends Model{
	
	use DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transactions_dtl';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_code','from','to','account_type','supplier_code','brand_code','purchase_return_po','product_code','product_name','transaction_type','stock_trasfer_in','stock_trasfer_out','unit','quantity','cost','selling','expiration_date','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function sa_lists($id){
		return User::where('cluster_id',$id)->whereIn('type',['sales_agent'])->orderBy('area')->get();
	}

	public function supplier(){
		return $this->belongsTo('App\Laravel\Models\Supplier','supplier_code','supplier_code')->withTrashed();
	}

	public function brand(){
		return $this->belongsTo('App\Laravel\Models\Brand','brand_code','brand_code')->withTrashed();
	}

	public function purchase_entry_dtl(){
		return $this->hasMany('App\Laravel\Models\PurchaseEntryDetail','purchasehdr_code','purchase_code')->orderBy('created_at');
	}

	public function product(){
		return $this->belongsTo('App\Laravel\Models\Product','product_code','product_code')->get();
	}

	public function prod()
	{
		return $this->belongsTo(Product::class, 'product_code', 'product_code');
	}

}