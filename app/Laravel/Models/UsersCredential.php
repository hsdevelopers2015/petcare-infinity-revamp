<?php namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class UsersCredential extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_credentials';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function user_info(){
        return $this->hasOne("App\Laravel\Models\User",'id','user_id')->withTrashed();
    }

}