<?php namespace App\Laravel\Models;

use App\Laravel\Models\OrderItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class OrderDiscount extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_discount';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_id','discount_name','discount_type','discount_amount'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function order(){
        return $this->hasOne("App\Laravel\Models\Order",'id','order_id');
    }

    public function order_items($id){
    	$order_item = OrderItem::where('transaction_id',$id)->get();

    	$total_amount = [];
    	foreach ($order_item as $index => $info) {
    		$amount = $info->qty * $info->product_info($info->product_id)? $info->product_info($info->product_id)->cost: 0;

    		array_push($total_amount,$amount);
    	}

    	return array_sum($total_amount);
    }

}