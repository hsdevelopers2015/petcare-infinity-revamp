<?php namespace App\Laravel\Models;

use App\Laravel\Models\User;
use App\Laravel\Models\RequestExpense;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class Expense extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'expenses';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sa_id','description','total_amount','status','type','filename','directory','change','reimbursement_amount'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function request(){
		return $this->hasOne("App\Laravel\Models\RequestExpense",'id','request_id');
	}

	public function sa_info($id){
		return User::where('id',$id)->first();
	}

	public function request_info($id){
		return RequestExpense::where('expense_id',$id)->first();
	}

	public function total_amount($id){
		$expenses = ExpensesItem::where('expense_id',$id)->get();

		$total = [];

		foreach($expenses as $index => $info){
			array_push($total,$info->amount);
		}

		return array_sum($total);
	}

	public function item_count($id){
		return ExpensesItem::where('expense_id',$id)->get();
	}



}