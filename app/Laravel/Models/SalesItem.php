<?php namespace App\Laravel\Models;

use App\Laravel\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class SalesItem extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales_items';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['free','transaction_id','product_id','cost_unit','qty','discount','discount_type','final_cost'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function sales() {
		return $this->hasOne('App\Laravel\Models\Sales', 'id', 'transaction_id')->where('payment_status','!=','pending');
	}

	public function product_info($id){
        return Product::where('id',$id)->first();
    }

    public function scopeCheckPaid($query, $mark = "yes"){
		return $query->has('sales');
	}

}