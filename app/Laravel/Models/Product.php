<?php namespace App\Laravel\Models;

use App\Laravel\Models\Inventory;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\QuotaConfiguration;
use App\Laravel\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

use Auth;

class Product extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['product_code','product_name','brand_partner','category','cost','selling','discount','directory','filename','quota_type','unit','replenish','deal_incentive','deal_condition','is_focus','focus_type','focus_value','product_description'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function inventory() {
		return $this->hasOne('App\Laravel\Models\Inventory', 'product_id', 'id');
	}

	public function orders() {
		return $this->hasOne('App\Laravel\Models\OrderItem', 'product_id', 'id');
	}
	public function purchase_received_dtl() {
		return $this->hasMany('App\Laravel\Models\TransactionDetail', 'product_code', 'product_code')->where('account_type','purchase');
	}

	public function stock_transfer_received() {
		return $this->hasMany('App\Laravel\Models\TransactionDetail', 'product_code', 'product_code')->where('account_type','stock_transfer');
	}
	public function purchase_return() {
		return $this->hasMany('App\Laravel\Models\TransactionDetail', 'product_code', 'product_code')->where('account_type','purchase_return');
	}
	public function measurement_code() {
		return $this->belongsTo('App\Laravel\Models\PurchaseReceivedDetail', 'product_code', 'product_code');
	}

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function physical_count($id){
		$inventory_check = Inventory::where('product_id',$id)->first();

		if($inventory_check){
			return $inventory_check;
		}else{
			return 0;
		}
	}

	public function scopeCheckInventory($query, $mark = "yes"){
		return $query->has('inventory');
	}

	public function scopeCheckOrder($query, $mark = "yes"){
		return $query->has('orders');
	}

	public function check_order($id){
		$date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
		if(!$date_range){
			$from = date('Y-m-1');
			$to = date('Y-m-t');
		}else{
			if($date_range->date_range == 'month'){
				$from = date('Y-m-1');
				$to = date('Y-m-t');
			}else{
				$from = date('Y-1-1');
				$to = date('Y-12-31');
			}
		}


		$total = OrderItem::where('created_at','<=',$to)->where('created_at','>=',$from)->checkDelivered()->get();

		$total_quantity = [];

		foreach ($total as $index => $info) {
			array_push($total_quantity,$info->qty);
		}

		$total_quantity = array_sum($total_quantity);

		$product = OrderItem::where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkDelivered()->get();

		$product_quantity = [];

		foreach($product as $index => $info){
			array_push($product_quantity,$info->qty);
		}

		$product_quantity = array_sum($product_quantity);

		if($product_quantity == 0){
			return 0;
		}else{
			return round(($product_quantity/$total_quantity)*100,2);
		}
	}

	public function check_order_count($id){
		$date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
		if(!$date_range){
			$from = date('Y-m-1');
			$to = date('Y-m-t');
		}else{
			if($date_range->date_range == 'month'){
				$from = date('Y-m-1');
				$to = date('Y-m-t');
			}else{
				$from = date('Y-1-1');
				$to = date('Y-12-31');
			}
		}
		$qty = OrderItem::where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkDelivered()->get();

		$total_quantity = [];

		foreach($qty as $index => $info){
			array_push($total_quantity,$info->qty);
		}

		return array_sum($total_quantity);
	}

	public function check_sales_order_count($id){
		$date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
		if(!$date_range){
			$from = date('Y-m-1');
			$to = date('Y-m-t');
		}else{
			if($date_range->date_range == 'month'){
				$from = date('Y-m-1');
				$to = date('Y-m-t');
			}else{
				$from = date('Y-1-1');
				$to = date('Y-12-31');
			}
		}
		
		$order_qty = OrderItem::where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkDelivered()->get();
		$sales_qty = SalesItem::where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkPaid()->get();

		$total_order_quantity = [];
		$total_sales_quantity = [];

		foreach($order_qty as $index => $info){
			array_push($total_order_quantity,$info->qty);
		}

		foreach($sales_qty as $index => $info){
			array_push($total_sales_quantity,$info->qty);
		}

		$total_order_qty = array_sum($total_order_quantity);
		$total_sales_qty = array_sum($total_sales_quantity);

		return $total_order_qty + $total_sales_qty;
	}



	public function check_sales_agent_sales_order_count($id){
		$date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();
		if(!$date_range){
			$from = date('Y-m-1');
			$to = date('Y-m-t');
		}else{
			if($date_range->date_range == 'month'){
				$from = date('Y-m-1');
				$to = date('Y-m-t');
			}else{
				$from = date('Y-1-1');
				$to = date('Y-12-31');
			}
		}

		$users = User::where('cluster_id',Auth::user()->cluster_id)->withTrashed()->get();

		$user_ids = [];
		
		foreach($users as $index => $info){
			array_push($user_ids,$info->id);
		}

		$orders = Order::whereIn('client_id',$user_ids)->get();

		$order_ids = [];

		foreach($orders as $index => $info){
			array_push($order_ids,$info->id);
		}

		$sales = Sales::whereIn('client_id',$user_ids)->get();

		$sales_ids = [];

		foreach($sales as $index => $info){
			array_push($sales_ids,$info->id);
		}

		$order_qty = OrderItem::whereIn('transaction_id',$order_ids)->where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkDelivered()->get();
		$sales_qty = SalesItem::whereIn('transaction_id',$sales_ids)->where('product_id',$id)->where('created_at','<=',$to)->where('created_at','>=',$from)->checkPaid()->get();

		$total_order_quantity = [];
		$total_sales_quantity = [];

		foreach($order_qty as $index => $info){
			array_push($total_order_quantity,$info->qty);
		}

		foreach($sales_qty as $index => $info){
			array_push($total_sales_quantity,$info->qty);
		}

		$total_order_qty = array_sum($total_order_quantity);
		$total_sales_qty = array_sum($total_sales_quantity);

		return $total_order_qty + $total_sales_qty;
	}

}