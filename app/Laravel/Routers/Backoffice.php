<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Backoffice", 
	'as' => "backoffice.", 
	// 'domain' => env('BACKOFFICE_URL',"backoffice.vetcare.localhost.com"), 
	// 'prefix'	=> "old",

	], function(){

		
	/**
	*
	* Routes for guests
	*/
	$this->group(['middleware' => ["web","backoffice.guest"]], function(){
		$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login',['uses' => "AuthController@authenticate"]);
	});

	$this->group(['middleware' => ["web-no-crsf","backoffice.auth"]],function(){
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
	});

	/**
	*
	* Routes for authenticated users
	*/
	$this->group(['middleware' => ["web","backoffice.auth"]], function(){

		$this->group(['prefix' => "ajax", 'as' => "ajax."], function() {
			$this->get('transaction', ['as' => "transaction", 'uses' => "AjaxController@transaction"]);
			$this->get('products', ['as' => "products", 'uses' => "AjaxController@products"]);
			$this->get('brands', ['as' => "brands", 'uses' => "AjaxController@brands"]);
		});
		
		$this->get('icons',['as' => "icons", 'uses' => "DashboardController@icons"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);
		

		$this->group(['middleware' => "backoffice.lock"], function(){
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "profile", 'as' => "profile."], function () {
				$this->get('setting',['as' => "settings", 'uses' => "ProfileController@setting"]);
				$this->post('setting',['uses' => "ProfileController@update_setting"]);
				$this->get('user/{username?}',['as' => "user_profile" ,'uses' => "ProfileController@view"]);

				$this->get('password-setting',['as' => "password" ,'uses' => "ProfileController@password_setting"]);
				$this->post('password-setting',['uses' => "ProfileController@update_password"]);
				
			});

			//Categories 

			$this->group(['prefix' => "category", 'as' => "category."], function () {
				$this->get('/',['as' => "index", 'uses' => "CategoryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "CategoryController@create"]);
				$this->post('create',['uses' => "CategoryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "CategoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "CategoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CategoryController@destroy"]);

				$this->any('view/{id?}',['as' => "view", 'uses' => "CategoryController@view"]);;

				
			});

			//Brands 

			$this->group(['prefix' => "brand", 'as' => "brand."], function () {
				$this->get('/',['as' => "index", 'uses' => "BrandController@index"]);
				$this->get('create',['as' => "create", 'uses' => "BrandController@create"]);
				$this->post('create',['uses' => "BrandController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "BrandController@edit"]);
				$this->post('edit/{id?}',['uses' => "BrandController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BrandController@destroy"]);

				$this->any('view/{id?}',['as' => "view", 'uses' => "BrandController@view"]);;

				
			});

			//supplier 

			$this->group(['prefix' => "supplier", 'as' => "supplier."], function () {
				$this->get('/',['as' => "index", 'uses' => "SupplierController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SupplierController@create"]);
				$this->post('create',['uses' => "SupplierController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SupplierController@edit"]);
				$this->post('edit/{id?}',['uses' => "SupplierController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SupplierController@destroy"]);
			});

			//Purchase Entry Header

			$this->group(['prefix' => "purchase_entry_hdr", 'as' => "purchase_entry_hdr."], function () {
				$this->get('/',['as' => "index", 'uses' => "PurchaseEntryHeaderController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PurchaseEntryHeaderController@create"]);
				$this->post('create',['uses' => "PurchaseEntryHeaderController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PurchaseEntryHeaderController@edit"]);
				$this->post('edit/{id?}',['uses' => "PurchaseEntryHeaderController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PurchaseEntryHeaderController@destroy"]);
			});

			//stock transfer in

			$this->group(['prefix' => "stock_transfer_in", 'as' => "stock_transfer_in."], function () {
				$this->get('/',['as' => "index", 'uses' => "StockTransferInController@index"]);
				$this->get('create',['as' => "create", 'uses' => "StockTransferInController@create"]);
				$this->post('create',['as' => "create", 'uses' => "StockTransferInController@store"]);
				$this->get('show/{transaction_code?}',['as' => "show", 'uses' => "StockTransferInController@show"]);
				$this->post('show/{transaction_code?}',['uses' => "StockTransferInController@received"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "StockTransferInController@edit"]);
				$this->post('edit/{id?}',['uses' => "StockTransferInController@update"]);				
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "StockTransferInController@destroy"]);
				
			});

			//stock transfer out

			$this->group(['prefix' => "stock_transfer_out", 'as' => "stock_transfer_out."], function () {
				$this->get('/',['as' => "index", 'uses' => "StockTransferOutController@index"]);
				$this->get('create',['as' => "create", 'uses' => "StockTransferOutController@create"]);
				$this->post('create',['as' => "create", 'uses' => "StockTransferOutController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "StockTransferOutController@edit"]);
				$this->post('edit/{id?}',['uses' => "StockTransferOutController@update"]);
				$this->get('stock_transfer_out',['as' => "stock_transfer_out", 'uses' => "StockTransferOutController@stock_transfer_out"]);
				$this->get('stock_transfer_in',['as' => "stock_transfer_in", 'uses' => "StockTransferOutController@stock_transfer_in"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "StockTransferOutController@destroy"]);
				
			});

			//Sales Returns

			$this->group(['prefix' => "sales_return", 'as' => "sales_return."], function () {
				$this->get('/',['as' => "index", 'uses' => "SalesReturnController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SalesReturnController@create"]);
				$this->post('create',['as' => "create", 'uses' => "SalesReturnController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SalesReturnController@edit"]);
				$this->post('edit/{id?}',['uses' => "SalesReturnController@update"]);

				$this->get('show/{transaction_code?}',['as' => "show", 'uses' => "SalesReturnController@show"]);
				$this->post('show/{transaction_code?}',['uses' => "SalesReturnController@approved"]);
				$this->get('stock_transfer_out',['as' => "stock_transfer_out", 'uses' => "SalesReturnController@stock_transfer_out"]);
				$this->get('stock_transfer_in',['as' => "stock_transfer_in", 'uses' => "SalesReturnController@stock_transfer_in"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SalesReturnController@destroy"]);
	
				
			});

			//Purchase Returns

			$this->group(['prefix' => "purchase_return", 'as' => "purchase_return."], function () {
				$this->get('/',['as' => "index", 'uses' => "PurchaseReturnController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PurchaseReturnController@create"]);
				$this->post('create',['as' => "create", 'uses' => "PurchaseReturnController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PurchaseReturnController@edit"]);
				$this->post('edit/{id?}',['uses' => "PurchaseReturnController@update"]);

				$this->get('show/{transaction_code?}',['as' => "show", 'uses' => "PurchaseReturnController@show"]);
				$this->post('show/{transaction_code?}',['uses' => "PurchaseReturnController@approved"]);
				$this->get('stock_transfer_out',['as' => "stock_transfer_out", 'uses' => "PurchaseReturnController@stock_transfer_out"]);
				$this->get('stock_transfer_in',['as' => "stock_transfer_in", 'uses' => "PurchaseReturnController@stock_transfer_in"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PurchaseReturnController@destroy"]);
				//Get supplier code
				$this->get('transaction_code/{transaction_code?}', ['as' => "transaction_code", 'uses' => "PurchaseReturnController@transaction_code"]);

				//Get Brand Code
				$this->get('brand_code/{transaction_code?}', ['as' => "transaction_code", 'uses' => "PurchaseReturnController@brand_code"]);

				//Get Product Code
				$this->get('product_code/{transaction_code?}', ['as' => "transaction_code", 'uses' => "PurchaseReturnController@product_code"]);
				
			});

			//Measurements

			$this->group(['prefix' => "measurement", 'as' => "measurement."], function () {
				$this->get('/',['as' => "index", 'uses' => "MeasurementController@index"]);
				$this->get('create',['as' => "create", 'uses' => "MeasurementController@create"]);
				$this->post('create',['uses' => "MeasurementController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "MeasurementController@edit"]);
				$this->post('edit/{id?}',['uses' => "MeasurementController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MeasurementController@destroy"]);
			});
			//Purchase Received Header

			$this->group(['prefix' => "purchase_received_hdr", 'as' => "purchase_received_hdr."], function () {
				$this->get('/',['as' => "index", 'uses' => "PurchaseReceivedHeaderController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PurchaseReceivedHeaderController@create"]);
				$this->post('create',['uses' => "PurchaseReceivedHeaderController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PurchaseReceivedHeaderController@edit"]);
				$this->post('edit/{id?}',['uses' => "PurchaseReceivedHeaderController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PurchaseReceivedHeaderController@destroy"]);
				$this->get('show/{transaction_code?}',['as' => "show", 'uses' => "PurchaseReceivedHeaderController@show"]);
				$this->post('show/{transaction_code?}',['uses' => "PurchaseReceivedHeaderController@received"]);
			});
			//Transaction Header

			$this->group(['prefix' => "transaction_hdr", 'as' => "transaction_hdr."], function () {
				$this->get('/',['as' => "index", 'uses' => "TransactionHeaderController@index"]);
				$this->get('create',['as' => "create", 'uses' => "TransactionHeaderController@create"]);
				$this->post('create',['uses' => "TransactionHeaderController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "TransactionHeaderController@edit"]);
				$this->post('edit/{id?}',['uses' => "TransactionHeaderController@update"]);
				
				$this->get('show/{transaction_code?}',['as' => "show", 'uses' => "TransactionHeaderController@show"]);
				$this->post('show/{transaction_code?}',['uses' => "TransactionHeaderController@approved"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TransactionHeaderController@destroy"]);
				
				$this->get('cost_price/{product_code?}', ['as' => "cost_price", 'uses' => "TransactionHeaderController@cost_price"]);
			});

			//Purchase Entry Detail

			$this->group(['prefix' => "purchase_entry_dtl", 'as' => "purchase_entry_dtl."], function () {
				$this->get('/',['as' => "index", 'uses' => "PurchaseEntryDetailController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PurchaseEntryDetailController@create"]);
				$this->post('create',['uses' => "PurchaseEntryDetailController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PurchaseEntryDetailController@edit"]);
				$this->post('edit/{id?}',['uses' => "PurchaseEntryDetailController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PurchaseEntryDetailController@destroy"]);
			});



			$this->group(['prefix' => "employees", 'as' => "employee.",'middleware' => "backoffice.super_user_only"], function () {
				$this->get('/',['as' => "index", 'uses' => "EmployeeController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EmployeeController@create"]);
				$this->post('create',['uses' => "EmployeeController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EmployeeController@edit"]);
				$this->post('edit/{id?}',['uses' => "EmployeeController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmployeeController@destroy"]);

				$this->any('credentials',['as' => "users_credentials", 'uses' => "EmployeeController@users_credentials"]);

				$this->get('export',['as' => "export", 'uses' => "EmployeeController@export"]);
			});

			$this->group(['prefix' => "clients", 'as' => "client."], function () {
				$this->get('/',['as' => "index", 'uses' => "ClientController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ClientController@create"]);
				$this->post('create',['uses' => "ClientController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ClientController@edit"]);
				$this->post('edit/{id?}',['uses' => "ClientController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ClientController@destroy"]);

				$this->any('view/{id?}',['as' => "view", 'uses' => "ClientController@view"]);

				$this->any('view/{id?}/{type}',['as' => "update_account", 'uses' => "ClientController@update_account"]);

				$this->get('edit-remarks/{id?}',['as' => "edit_remarks", 'uses' => "ClientController@edit_remarks"]);
				$this->post('edit-remarks/{id?}',['uses' => "ClientController@update_remarks"]);

				$this->get('export',['as' => "export", 'uses' => "ClientController@export_process"]);
				$this->post('import',['as' => "import", 'uses' => "ClientController@import"]);
				$this->post('update-excel',['as' => "update_excel", 'uses' => "ClientController@update_excel"]);
			});


			$this->group(['prefix' => "branches", 'as' => "branches."], function () {
				$this->get('/',['as' => "index", 'uses' => "BranchesController@index"]);
				$this->get('create',['as' => "create", 'uses' => "BranchesController@create"]);
				$this->post('create',['uses' => "BranchesController@store"]);
				$this->get('edit/{id?}',['as' => 'edit','uses' => "BranchesController@edit"]);
				$this->post('edit/{id?}',['uses' => "BranchesController@update"]);
				$this->get('show/{branch_code?}',['as' => 'show','uses' => "BranchesController@show"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BranchesController@destroy"]);
				$this->get('excel',['as' => "excel", 'uses' => "BranchesController@excel"]);


				$this->get('ajax_detail/{client_id?}',['as' => "ajax_detail", 'uses' => "BranchesController@ajax_detail"]);
			});

			$this->group(['prefix' => "sales-agents", 'as' => "sales_agents."], function () {
				$this->get('/',['as' => "index", 'uses' => "SalesAgentsController@index"]);
			});

			$this->group(['prefix' => "client-list", 'as' => "client_list."], function () {
				$this->get('/',['as' => "index", 'uses' => "ClientListController@index"]);
			});

			$this->group(['prefix' => "sales-incentive", 'as' => "sales_incentive."], function () {
				$this->get('/',['as' => "index", 'uses' => "SalesAgentsController@index"]);
			});

			$this->group(['prefix' => "inventory", 'as' => "inventory."], function () {
				$this->get('/',['as' => "index", 'uses' => "InventoryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "InventoryController@create"]);
				$this->post('create',['uses' => "InventoryController@store"]);
				$this->get('show/{product_code?}',['as' => "show",'uses' => "InventoryController@show"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "InventoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "InventoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "InventoryController@destroy"]);
				$this->get('stock-info/{id?}',['as' => "stock_info", 'uses' => "InventoryController@stock_info"]);
				$this->get('excel',['as' => "excel", 'uses' => "InventoryController@excel"]);
			});

			$this->group(['prefix' => "stock_in", 'as' => "stock_in."], function () {
				$this->get('/',['as' => "index", 'uses' => "StockInController@index"]);
				$this->get('create',['as' => "create", 'uses' => "StockInController@create"]);
				$this->post('create',['uses' => "StockInController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "StockInController@edit"]);
				$this->post('edit/{id?}',['uses' => "StockInController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "StockInController@destroy"]);
				$this->any('excel',['as' => "excel", 'uses' => "StockInController@excel"]);
			});

			$this->group(['prefix' => "products", 'as' => "products."], function () {
				$this->get('/',['as' => "index", 'uses' => "ProductController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ProductController@create"]);
				$this->post('create',['uses' => "ProductController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ProductController@edit"]);
				$this->post('edit/{id?}',['uses' => "ProductController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ProductController@destroy"]);


				$this->get('promote',['as' => "promote", 'uses' => "ProductController@promote"]);
				$this->get('create-promote',['as' => "create_promote", 'uses' => "ProductController@create_promote"]);
				$this->post('create-promote',['uses' => "ProductController@store_promote"]);
				$this->get('edit-promote/{id?}',['as' => "edit_promote", 'uses' => "ProductController@edit_promote"]);
				$this->post('edit-promote/{id?}',['uses' => "ProductController@update_promote"]);
				$this->any('delete-promote/{id?}',['as' => "destroy_promote", 'uses' => "ProductController@destroy_promote"]);

				$this->get('export',['as' => "export", 'uses' => "ProductController@export"]);
				$this->post('import',['as' => "import", 'uses' => "ProductController@import"]);
				$this->get('excel',['as' => "excel", 'uses' => "ProductController@excel"]);

			});

			$this->group(['prefix' => "sales", 'as' => "sales."], function () {
				$this->get('/',['as' => "index", 'uses' => "SalesController@index"]);
				$this->get('collectibles',['as' => "collectibles", 'uses' => "SalesController@collectibles"]);
				$this->post('create',['as' => "create", 'uses' => "SalesController@create"]);
				$this->get('add-item/{transaction_code?}',['as' => "add", 'uses' => "SalesController@add"]);
				$this->post('add-item/{transaction_code?}',['as' => "add", 'uses' => "SalesController@store"]);
				$this->post('client/{id?}',['as' => "client", 'uses' => "SalesController@client"]);
				
				$this->get('save/{id?}',['as' => "save",'uses' => "SalesController@save"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SalesController@edit"]);
				$this->post('edit/{id?}',['uses' => "SalesController@update"]);
				$this->get('invoice/{id?}',['as' => "invoice", 'uses' => "SalesController@invoice"]);
				$this->any('remove/{id?}',['as' => "remove", 'uses' => "SalesController@remove"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SalesController@destroy"]);
				$this->any('cancel/{id?}',['as' => "cancel", 'uses' => "SalesController@cancel"]);
				$this->post('mod/{id}',['as' => "mod", 'uses' => "SalesController@mod"]);
				$this->post('ups/{id}',['as' => "ups", 'uses' => "SalesController@ups"]);
				$this->post('add-discount/{id}',['as' => "add_discount", 'uses' => "SalesController@add_discount"]);
				$this->any('remove-discount/{id}',['as' => "remove_discount", 'uses' => "SalesController@remove_discount"]);

				$this->get('delivery-receipt/{id}',['as' => "dr", 'uses' => "SalesController@delivery_receipt"]);
				$this->post('delivery-receipt',['as' => "create_dr", 'uses' => "SalesController@create_delivery_receipt"]);
				$this->post('delivery-receipt/{id}',['as' => "update_dr", 'uses' => "SalesController@update_delivery_receipt"]);
				$this->get('delivery-receipt-form/{id}',['as' => "view_dr", 'uses' => "SalesController@delivery_receipt_form"]);
				$this->get('history/{id}',['as' => "history", 'uses' => "SalesController@history"]);

				$this->get('excel',['as' => "excel", 'uses' => "SalesController@excel"]);
			});

			$this->group(['prefix' => "contracts", 'as' => "contracts."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContractController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ContractController@create"]);
				$this->post('create',['uses' => "ContractController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ContractController@edit"]);
				$this->post('edit/{id?}',['uses' => "ContractController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ContractController@destroy"]);

				$this->get('sent',['as' => "sent", 'uses' => "ContractController@sent"]);
				$this->get('creating',['as' => "create_contract", 'uses' => "ContractController@creating"]);
				$this->post('creating',[ 'uses' => "ContractController@send"]);
				$this->get('editing/{id?}',['as' => "edit_contract", 'uses' => "ContractController@edit_contract"]);
				$this->post('editing/{id?}',['uses' => "ContractController@update_contract"]);
				$this->any('deleting/{id?}',['as' => "destroy_contract", 'uses' => "ContractController@destroy_contract"]);
				$this->get('excel',['as' => "excel", 'uses' => "ContractController@excel"]);
				$this->get('excel-sent-contracts',['as' => "excel_sent_contracts", 'uses' => "ContractController@excel_sent_contracts"]);
			});

			$this->group(['prefix' => "cluster", 'as' => "cluster."], function () {
				$this->get('/',['as' => "index", 'uses' => "ClusterController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ClusterController@create"]);
				$this->post('create',['uses' => "ClusterController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ClusterController@edit"]);
				$this->post('edit/{id?}',['uses' => "ClusterController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ClusterController@destroy"]);

				$this->get('export',['as' => "export", 'uses' => "ClusterController@export"]);
			});

			//clients
			$this->group(['prefix' => "clients", 'as' => "clients."], function () {
				$this->get('/',['as' => "index", 'uses' => "ClientsController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ClientsController@create"]);
				$this->post('create',['uses' => "ClientsController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ClientsController@edit"]);
				$this->post('edit/{id?}',['uses' => "ClientsController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ClientsController@destroy"]);

				$this->get('export',['as' => "export", 'uses' => "ClientsController@export"]);
			});

			$this->group(['prefix' => "order-list", 'as' => "orders."], function () {
				$this->get('/',['as' => "index", 'uses' => "OrdersController@index"]);
				$this->get('item-list/{slug}',['as' => "view", 'uses' => "OrdersController@view"]);
				$this->get('ship/{id}',['as' => "ship",'uses' => "OrdersController@ship"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "OrdersController@edit"]);
				$this->post('edit/{id?}',['uses' => "OrdersController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "OrdersController@destroy"]);
				$this->any('po/{id?}',['as' => "po", 'uses' => "OrdersController@po"]);
				$this->post('add-discount/{id}',['as' => "add_discount", 'uses' => "OrdersController@add_discount"]);
				$this->any('remove-discount/{id}',['as' => "remove_discount", 'uses' => "OrdersController@remove_discount"]);
				
				$this->get('delivery-receipt/{id}',['as' => "dr", 'uses' => "OrdersController@delivery_receipt"]);
				$this->post('delivery-receipt',['as' => "create_dr", 'uses' => "OrdersController@create_delivery_receipt"]);
				$this->post('delivery-receipt/{id}',['as' => "update_dr", 'uses' => "OrdersController@update_delivery_receipt"]);
				$this->get('delivery-receipt-form/{id}',['as' => "view_dr", 'uses' => "OrdersController@delivery_receipt_form"]);

				$this->post('create-invoice',['as' => "create_invoice", 'uses' => "OrdersController@create_invoice"]);
				$this->get('edit-invoice/{id}',['as' => "edit_invoice", 'uses' => "OrdersController@edit_invoice"]);
				$this->post('edit-invoice/{id}',['uses' => "OrdersController@update_invoice"]);
			});

			$this->group(['prefix' => "reporting", 'as' => "reporting."], function () {
				$this->any('sales',['as' => "sales", 'uses' => "ReportingController@sales"]);
				$this->any('expenses',['as' => "expenses", 'uses' => "ReportingController@expenses"]);
				$this->any('export-expenses',['as' => "export_expenses", 'uses' => "ReportingController@export_expenses"]);
			});

			$this->group(['prefix' => "allowance-expenses", 'as' => "expenses."], function () {
				$this->get('/',['as' => "index", 'uses' => "ExpensesController@index"]);
				$this->post('create-form',['as' => "create_form", 'uses' => "ExpensesController@create_bak"]);
				$this->post('create',['as' => "create", 'uses' => "ExpensesController@create"]);
				$this->get('add/{id}',['as' => "add",'uses' => "ExpensesController@add"]);
				$this->post('add/{id}',['uses' => "ExpensesController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ExpensesController@edit"]);
				$this->post('edit/{id?}',['uses' => "ExpensesController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ExpensesController@destroy"]);
				$this->any('remove/{id?}',['as' => "remove", 'uses' => "ExpensesController@remove"]);
				$this->any('submit/{id?}',['as' => "submit", 'uses' => "ExpensesController@submit"]);
				$this->any('reponse/{id?}/{status}',['as' => "response", 'uses' => "ExpensesController@response"]);
				$this->any('update-change/{id?}',['as' => "update_change", 'uses' => "ExpensesController@update_change"]);
				$this->get('excel-expenses',['as' => "excel_expenses", 'uses' => "ExpensesController@excel_expenses"]);
				$this->get('excel-requests',['as' => "excel_request", 'uses' => "ExpensesController@excel_request"]);
			});

			$this->group(['prefix' => "request-forms", 'as' => "requests."], function () {
				$this->get('/',['as' => "index", 'uses' => "RequestFormsController@index"]);
				$this->get('edit/{id}',['as' => "edit", 'uses' => "RequestFormsController@edit"]);
				$this->any('respond/{id}/{status}',['as' => "respond", 'uses' => "RequestFormsController@status"]);
				$this->get('excel',['as' => "excel", 'uses' => "RequestFormsController@excel"]);
			});

			$this->group(['prefix' => "quota", 'as' => "quota."], function () {
				$this->get('/',['as' => "index", 'uses' => "QuotaController@index"]);
				$this->post('/',[ 'uses' => "QuotaController@store"]);
			});

			$this->group(['prefix' => "product-returns", 'as' => "product_return."], function () {
				$this->get('/',['as' => "index", 'uses' => "ProductReturnController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ProductReturnController@create"]);
				$this->post('create',['uses' => "ProductReturnController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ProductReturnController@edit"]);
				$this->post('edit/{id?}',['uses' => "ProductReturnController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ProductReturnController@destroy"]);
				$this->get('excel',['as' => "excel", 'uses' => "ProductReturnController@excel"]);
			});

			$this->group(['prefix' => "access-control", 'as' => "access_control."], function () {
				$this->get('/',['as' => "index", 'uses' => "AccessControlController@index"]);
				$this->post('/',['as' => "update", 'uses' => "AccessControlController@update"]);
				$this->post('set-default/',['as' => "default", 'uses' => "AccessControlController@update_default"]);
				$this->get('excel',['as' => "excel", 'uses' => "AccessControlController@excel"]);
			});

			$this->group(['prefix' => "deliveries", 'as' => "deliveries."], function () {
				$this->get('/',['as' => "index", 'uses' => "DeliveryController@index"]);		
				$this->get('{type}/{id}',['as' => "received", 'uses' => "DeliveryController@received"]);
				$this->get('excel',['as' => "excel", 'uses' => "DeliveryController@excel"]);
			});

			$this->group(['prefix' => "stock-out", 'as' => "stock_out."], function () {
				$this->get('/',['as' => "index", 'uses' => "StockOutController@index"]);
				$this->get('for-delivery/{id}',['as' => "for_delivery", 'uses' => "StockOutController@for_delivery"]);
				$this->get('view/{transaction_code}',['as' => "view", 'uses' => "StockOutController@view"]);
				$this->get('excel',['as' => "excel", 'uses' => "StockOutController@excel"]);
			});

		});
	});
});
