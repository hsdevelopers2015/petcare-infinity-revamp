<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Frontend", 
	'as' => "frontend.", 
	// 'middleware'=> "web",
	'domain' => env('FRONTEND_URL',"frontend.vetcare.localhost.com"), 

	], function(){

	$this->group(['middleware' => ["web","frontend.guest"]], function(){
		$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login',['uses' => "AuthController@authenticate"]);
	});

	$this->group(['middleware' => ["web-no-crsf","frontend.auth"]],function(){
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
	});

	/**
	*
	* Routes for authenticated users
	*/
	$this->group(['middleware' => ["web","frontend.auth"]], function(){
		
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);
		

		$this->group(['middleware' => "frontend.lock"], function(){
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);


			$this->group(['prefix' => "profile", 'as' => "profile."], function () {
				$this->get('/',['as' => "settings", 'uses' => "ProfileController@setting"]);
				$this->post('/',['uses' => "ProfileController@update_setting"]);
			});

			

			$this->group(['prefix' => "branches", 'as' => "branches."], function () {
				$this->get('/',['as' => "index", 'uses' => "BranchesController@index"]);
				$this->get('create',['as' => "create", 'uses' => "BranchesController@create"]);
				$this->post('create',['uses' => "BranchesController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "BranchesController@edit"]);
				$this->post('edit/{id?}',['uses' => "BranchesController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BranchesController@destroy"]);
			});

			$this->group(['prefix' => "order-list", 'as' => "orders."], function () {
				$this->get('/',['as' => "index", 'uses' => "OrdersController@index"]);
				$this->get('add-item/{transaction_code}',['as' => "add", 'uses' => "OrdersController@add"]);
				$this->post('add-item/{transaction_code}',['uses' => "OrdersController@add_item"]);
				$this->post('create',['as' => "create", 'uses' => "OrdersController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "OrdersController@edit"]);
				$this->post('edit/{id?}',['uses' => "OrdersController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "OrdersController@destroy"]);
				$this->any('mod/{id?}',['as' => "mod", 'uses' => "OrdersController@mod"]);
				$this->any('ups/{id?}',['as' => "ups", 'uses' => "OrdersController@ups"]);

				$this->any('submit/{id?}',['as' => "submit", 'uses' => "OrdersController@submit"]);
				$this->any('remove/{id?}',['as' => "remove", 'uses' => "OrdersController@ups"]);
				$this->any('remove-item/{id?}/{t_id?}',['as' => "remove_item", 'uses' => "OrdersController@remove_item"]);
				$this->any('received/{id?}',['as' => "received", 'uses' => "OrdersController@received"]);

			});

			$this->group(['prefix' => "business", 'as' => "business."], function () {
				$this->get('/',['as' => "info", 'uses' => "BusinessController@index"]);
				$this->post('/',[ 'uses' => "BusinessController@update"]);
			});

			$this->group(['prefix' => "invoices", 'as' => "invoices."], function () {
				$this->get('/',['as' => "index", 'uses' => "InvoiceController@index"]);
				$this->get('{slug}',['as' => "view", 'uses' => "InvoiceController@view"]);
				$this->get('received/{slug}',['as' => "received", 'uses' => "InvoiceController@received"]);
			});

			$this->group(['prefix' => "contracts", 'as' => "contracts."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContractsController@index"]);
			});

		});
	});
});
