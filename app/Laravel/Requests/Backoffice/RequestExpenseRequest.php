<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class RequestExpenseRequest extends RequestManager{

	public function rules(){

		// $id = $this->segment(3)?:0;

		$rules = [
			'description' => "required",
			// 'type' => "required",
			// 'total_amount' => "required",
		];

		if(!Input::get('total_amount')){
			unset($rules['total_amount']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}