<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class PromoteProductRequest extends RequestManager{

	public function rules(){

		$rules = [
			'file' => "required|image",
			'product_id' => "required",
			'description' => "required",
		];


		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}