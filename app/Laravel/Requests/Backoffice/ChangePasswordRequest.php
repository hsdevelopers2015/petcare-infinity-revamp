<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class ChangePasswordRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'current_password' => "required|check_password:".Auth::user()->id,
			'password' => "required|confirmed",
			'password_confirmation' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'check_password' => "Current password does not match",
		];
	}
}