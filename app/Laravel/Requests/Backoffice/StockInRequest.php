<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class StockInRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'product_id' => "required",
			'qty' => "required",
			'acquisition_cost' => "required",
			'transaction_code' => "required",
			'supplier_name' => "required",
			'expiration_date' => "required",
		];

		if($id){
			unset($rules['password']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}