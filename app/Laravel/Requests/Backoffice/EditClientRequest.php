<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EditClientRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'fname' => "required",
			'lname' => "required",
			'username' => "required|unique:user,username,".$id,
			'email' => "unique:user,email,".$id,
			'contact' => "required",
			'address' => "required",
			'cluster_id' => "required",
			'segment' => "required",

			
			'business_name' => "required",
			'business_type' => "required",
			'file' => "image",
			// 'business_location' => "required",
		];

		if($id){
			unset($rules['password']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
			'email.unique' => "Email is already in use.",
			'password.between' => "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed' => "Password does not match.",
		];
	}
}