<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class ClusterRequest extends RequestManager{

	public function rules(){

		// $id = $this->segment(3)?:0;

		$rules = [
			'island' => "required",
			'city' => "required",
			'area' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}