<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class OrderInvoiceRequest extends RequestManager{

	public function rules(){

		$rules = [
			'approved_by' => "required",
		];


		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}