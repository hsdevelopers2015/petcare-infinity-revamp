<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class DeliveryReceiptRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			// 'dr_number' => "required",
			// 'prepared_by' => "required",
			// 'approved_by' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'check_quantity' => "Insufficient stock. Check inventory in Masterfile Inventory.",
		];
	}
}