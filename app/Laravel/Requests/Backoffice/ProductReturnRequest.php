<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class ProductReturnRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'stock_id' => "required",
			'qty' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'check_password' => "Current password does n0t match",
		];
	}
}