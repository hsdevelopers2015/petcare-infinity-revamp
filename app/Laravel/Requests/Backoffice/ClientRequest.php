<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ClientRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = ['name','birthday','address','email','contact_no','client_address','business_name','business_address','cluster_id','agent_id','status'
	
		];


		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
			'email.unique' => "Email is already in use.",
			'password.between' => "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed' => "Password does not match.",
		];
	}
}