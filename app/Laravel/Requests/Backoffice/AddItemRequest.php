<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class AddItemRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'product_id' => "required",
			'cost_unit' => "required",
			'qty' => "required|check_quantity:".Input::get('product_id'),
			// 'discount' => "required",
			'discount_type' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'check_quantity' => "Insufficient stock. Check inventory in Masterfile Inventory.",
		];
	}
}