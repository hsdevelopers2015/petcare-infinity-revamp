<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class PurchaseHeaderEntryRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'purchase_code' => "required",
			'supplier_code' => "required",
			'description' => "required",
			'product_code' => "required",
			'qty' => "required",
			'cost_price' => "required",
			'selling_price' => "required",
			'expiration_date' => "required",
			
			
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
			'email.unique' => "Email is already in use.",
			'password.between' => "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed' => "Password does not match.",
		];
	}
}