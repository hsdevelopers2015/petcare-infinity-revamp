<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class QuotaConfigurationRequest extends RequestManager{

	public function rules(){


		$rules = [
			'sales_quota' => "required",
			'mother_quota' => "required",
			'special_quota' => "required",
			'group_quota' => "required",
			'luzon' => "required",
			'visayas' => "required",
			'mindanao' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}