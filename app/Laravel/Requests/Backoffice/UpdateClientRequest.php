<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UpdateClientRequest extends RequestManager{

	public function rules(){

		$rules = [
			'client_id' => "required",
		];


		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}