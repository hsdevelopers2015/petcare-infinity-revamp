<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class EmployeeRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'fname' => "required",
			'lname' => "required",
			'type' => "required",
			'username' => "required",
			'email' => "unique:user,email,".$id,
			'password' => "required|confirmed|between:6,25",
			'password_confirmation' => "required",
			'contact' => "required",
			'address' => "required",
		];

		if($id){
			unset($rules['password']);
		}

		if(!in_array(Input::get('type'), ['sales_head','sales_agent'])){
			unset($rules['cluster_id']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'email.unique' => "Email is already in use.",
			'password.between' => "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed' => "Password does not match.",
		];
	}
}