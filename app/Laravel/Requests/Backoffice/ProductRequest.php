<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProductRequest extends RequestManager{

	public function rules(){

		$rules = [
			'category_code' => "required",
			'product_name' => "required",
			'product_code' => "required",
			'product_description' => "required",
			
		];


		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}