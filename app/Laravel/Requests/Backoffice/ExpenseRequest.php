<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class ExpenseRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			// 'description' => "required",
			// 'file' => "image",
			'amount' => "required",
			// // 'vat' => "required",
			// 'net_amount' => "required",
			// 'type' => "required",
			// 'tin_number' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}