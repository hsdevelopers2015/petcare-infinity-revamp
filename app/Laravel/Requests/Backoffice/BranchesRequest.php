<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class BranchesRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'branch_code' => "required",
			'branch_name' => "required",
			'branch_location' => "required",
			'branch_contact' => "required",
			'branch_email' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}