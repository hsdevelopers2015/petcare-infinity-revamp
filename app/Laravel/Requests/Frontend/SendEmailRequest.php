<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class SendEmailRequest extends RequestManager{

	public function rules(){

		return [
			'name'	=> "required",
			// 'contact_number '	=> "required",
			'email'	=> "required|email",
			'subject' => "required|between:6,100",
			'message' => "required|min:10",
			// 'g-recaptcha-response' => 'required|captcha',
		];
			

		
	}

	public function messages(){
		return [

			'required'		=> "Field is required.",
			'message.min'	=> "Message is too short",
			'subject.between'	=> "Subject must be at least 6 characters and does not exceed to 100 characters.",
			'email'			=> "Email Address is invalid",
			// 'captcha'		=> "Invalid Captcha.",
			// 'g-recaptcha-response.required'	=> "Invalid Captcha",
		];
	}

		public function response(array $errors)
		{
			if ($this->ajax() || $this->wantsJson())
			{
				return new JsonResponse($errors, 422);
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Opps!</strong> Some fields are not accepted.');

			return $this->redirector->to($this->getRedirectUrl()."#contact")
	                                        ->withInput($this->except($this->dontFlash))
	                                        ->withErrors($errors, $this->errorBag);
		}
}