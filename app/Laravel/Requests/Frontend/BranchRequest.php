<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class BranchRequest extends RequestManager{

	public function rules(){

		$rules = [
			'branch_name' => "required",
			'branch_contact' => "required",
			'branch_location' => "required",
			'file' => "required|image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}