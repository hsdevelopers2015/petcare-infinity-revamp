<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class AddItemRequest extends RequestManager{

	public function rules(){

		$rules = [
			'product_id' => "required",
			'qty' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}