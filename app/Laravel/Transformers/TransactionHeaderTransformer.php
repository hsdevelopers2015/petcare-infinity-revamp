<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Brand;
use App\Laravel\Models\Product;
use App\Laravel\Models\Supplier;
use League\Fractal\TransformerAbstract;

class TransactionHeaderTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
        'brands', 'products', 'suppliers'
    ];

    public function transform($transaction)
    {
    	return [
    		'transaction_code' => $transaction->transaction_code,
            'account_type' => $transaction->account_type,
            'transaction_type' => $transaction->transaction_type,
            'supplier_code' => $transaction->supplier_code,
            'invoice_number' => $transaction->invoice_number,
            'description' => $transaction->description,
            'cost' => $transaction->cost,
            'selling' => $transaction->selling,
            'status' => $transaction->status
    	];
    }

    public function includeBrands($transaction)
    {
        $codes = $transaction->details->pluck('brand_code');
        $brands = Brand::whereIn('brand_code', $codes)->get();

        return $this->collection($brands, new BrandTransformer);
    }

    public function includeSuppliers($transaction)
    {
        $codes = $transaction->details->pluck('supplier_code');
        $suppliers = Supplier::whereIn('supplier_code', $codes)->get();

        return $this->collection($suppliers, new SupplierTransformer);
    }

    public function includeProducts($transaction)
    {
        $codes = $transaction->details->pluck('product_code');
        $products = Product::whereIn('product_code', $codes)->get();

        return $this->collection($products, new ProductTransformer);
    }

}