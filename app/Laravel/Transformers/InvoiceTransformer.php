<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Invoice;


use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class InvoiceTransformer extends TransformerAbstract{

	protected $availableIncludes = [
        'date','info'
    ];

	public function transform(Invoice $invoice){
	     return [
	     	'id' => $invoice->id,
	     	'service' => $invoice->service,
	     	'discount' => $invoice->discount,
	     	'client_id' => $invoice->client_id,
	     	'invoice_number' => $invoice->invoice_number,
	     	'details' => $invoice->details,
	     	'amount' => $invoice->amount,	
	     	'status' => $invoice->status,
	     ];
	}

	public function includeDate(Invoice $invoice){
        $collection = Collection::make([
    			'added_in'	=> [
    				'date_db' => $invoice->date_db($invoice->created_at),
    				'month_year' => $invoice->month_year($invoice->created_at),
    				'time_passed' => $invoice->time_passed($invoice->created_at),
    				'timestamp' => $invoice->created_at
    			],
        	]);

        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(Invoice $invoice){
		$collection = Collection::make([
			'service' => $invoice->service,
	     	'discount' => $invoice->discount,
	     	'client_id' => $invoice->client_id,
	     	'invoice_number' => $invoice->invoice_number,
	     	'details' => $invoice->details,
	     	'amount' => $invoice->amount,	
	     	'status' => $invoice->status,
		]);

		return $this->item($collection, new MasterTransformer);
	}

}