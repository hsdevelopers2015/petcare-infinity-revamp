<?php

namespace App\Laravel\Transformers;

use League\Fractal\TransformerAbstract;

class SupplierTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
        //
    ];

    public function transform($supplier)
    {
    	return [
    		'supplier_code' => $supplier->supplier_code,
            'supplier_name' => $supplier->supplier_name
    	];
    }

}