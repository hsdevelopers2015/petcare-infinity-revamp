<?php

namespace App\Laravel\Transformers;

use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
        //
    ];

    public function transform($product)
    {
    	return [
    		'product_code' => $product->product_code,
            'product_name' => $product->product_name,
            'cost' => $product->cost,
            'selling' => $product->selling,
    	];
    }

}