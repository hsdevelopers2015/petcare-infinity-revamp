<?php

namespace App\Laravel\Transformers;

use League\Fractal\TransformerAbstract;

class TransactionDetailTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
        'brand'
    ];

    public function transform($transaction_dtl)
    {
    	return [
    		'transaction_code' => $transaction_dtl->transaction_code,
    		// 'from' => $transaction_dtl->from,
    		// 'to' => $transaction_dtl->to,
    		// 'account_type' => $transaction_dtl->account_type,
    		// 'supplier_code' => $transaction_dtl->supplier_code,
    		'brand_code' => $transaction_dtl->brand_code,
    		'product_code' => $transaction_dtl->product_code,
    		// 'product_name' => $transaction_dtl->product_name,
    		// 'transaction_type' => $transaction_dtl->transaction_type,
    		// 'stock_trasfer_in' => $transaction_dtl->stock_trasfer_in,
    		// 'stock_trasfer_out' => $transaction_dtl->stock_trasfer_out,
    		// 'unit' => $transaction_dtl->unit,
    		// 'quantity' => $transaction_dtl->quantity,
    		'cost' => $transaction_dtl->cost,
    		'selling' => $transaction_dtl->selling,
    		// 'expiration_date' => $transaction_dtl->expiration_date,
    		// 'status' => $transaction_dtl->status
    	];
    }

    public function includeBrand($transaction_dtl)
    {
    	return $this->item($transaction_dtl->brand, new BrandTransformer);
    }

}