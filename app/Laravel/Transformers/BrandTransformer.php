<?php

namespace App\Laravel\Transformers;

use League\Fractal\TransformerAbstract;

class BrandTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
        //
    ];

    public function transform($brand)
    {
    	return [
    		'brand_code' => $brand->brand_code,
            'brand_name' => $brand->brand_name
    	];
    }

}