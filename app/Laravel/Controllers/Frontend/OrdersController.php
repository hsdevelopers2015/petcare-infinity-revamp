<?php namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\Product;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\OrderRequest;
use App\Laravel\Requests\Frontend\AddItemRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, DB;

class OrdersController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Orders";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "orders";
		$this->data['types'] = [''=>'Choose type','sales_agent'=>'Sales Agent','admin'=>"Admin",'super_user'=>"Super User"];
		$this->data['products'] = ['' => "Type the product name"] + Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();

    	// DB::table('inventory as u')
    	// ->join('products as p', 'p.id', '=', 'u.product_id')
    	// ->selectRaw('CONCAT(p.product_name) as concatname, u.product_id')

    	$this->data['modes'] = ['cash_on_delivery' => "Cash On Delivery", 'dated_checks'=>"Dated Checks",'post_dated_cheque'=>"Post Dated Cheque"];

    	$this->data['terms'] = ['1_month' => "1 Month",'2_months' => "2 Months",'3_months' => "3 Months",'6_months' => "6 Months",];

		$this->data['statuses'] = ['pending' => "Pending",'partially_paid' => "Partially Paid",'fully_paid' => "Fully Paid"];

		$this->data['product_lists'] = Product::checkInventory()->get();
		
	}

	public function index () {
		$this->data['orders'] = Order::where('client_id',$this->data['auth']->id)->orderBy('transaction_code','DESC')->get();
		return view('frontend.'.$this->data['route_file'].'.index',$this->data);
	}

	public function add ($transaction_code = NULL) {
		$order = Order::where('transaction_code',$transaction_code)->where('client_id',$this->data['auth']->id)->first();
		if($order){

			$this->data['transaction_code'] = $transaction_code;
			$this->data['order'] = $order;

			$this->data['items'] = OrderItem::where('transaction_id',$this->data['order']->id)->orderBy('updated_at','DESC')->get();

			return view('frontend.'.$this->data['route_file'].'.create',$this->data);

		}else{
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function add_item(AddItemRequest $request){
		try {
			$add_item = new OrderItem;
			$add_item->fill($request->all());

			$transaction = Order::find($request->get('transaction_id'));

			$total_qty = $transaction->total_qty;

			$transaction->total_qty = $total_qty + $request->qty;

			// dd($add_item);

			if($add_item->save() AND $transaction->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Item successfully added.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');
			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	public function store(){
		try {
			$order = new Order;

			$check = Order::where('status','draft')->where('client_id',$this->data['auth']->id)->get();

			if($check->count() > 0){
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Oops you still have an order that is not submitted yet.');
				return redirect()->back();
			}

			$order_count = str_pad(Order::where('client_id',$this->data['auth']->id)->count()+1,'4','0',STR_PAD_LEFT);

			$this->data['transaction_code'] = "OC".$this->data['auth']->id."-".Helper::date_format(Carbon::now(),'ymd').'-'.$order_count;

			$order->client_id = $this->data['auth']->id;
			$order->transaction_code = $this->data['transaction_code'];

			if($order->save()) {

				Helper::user_timeline($this->data['auth']->id,$order->id,'orders','created an order.','create');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your order successfully created.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.add',[$this->data['transaction_code']]);
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');
			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update (OrderRequest $request, $id = NULL) {
		try {
			$order = Order::find($this->data['auth']->business_info->id);

			if (!$order) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.info');
			}

			$order->fill($request->all());

			if($request->has('password')){
				$order->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$order->directory}/{$order->filename}")){
						File::delete("{$order->directory}/{$order->filename}");
					}
					if (File::exists("{$order->directory}/resized/{$order->filename}")){
						File::delete("{$order->directory}/resized/{$order->filename}");
					}
					if (File::exists("{$order->directory}/thumbnails/{$order->filename}")){
						File::delete("{$order->directory}/thumbnails/{$order->filename}");
					}
				}
				
				$order->directory = $upload["directory"];
				$order->filename = $upload["filename"];
			}

			if($order->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your business has been updated.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.info');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL){
		try {
			$order = Order::find($id);

			$order->status = 'cancelled';

			if($order->save()) {

				Helper::user_timeline($this->data['auth']->id,$order->id,'orders','cancelled an order.','delete');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your order was cancelled.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');
			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function submit($id = NULL){
		try {
			$order = Order::find($id);

			$order->status = 'order_confirmation';

			if($order->total_qty == 0){
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Please add some item first before you submit your order.');
				return redirect()->back();
			}

			if($order->save()) {
				Helper::user_timeline($this->data['auth']->id,$order->id,'orders','submitted an order.','submit');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your order successfully sent, wait for the admin verification.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		

	}

	public function remove_item($id=NULL,$t_id=NULL){
		try {
			$item = OrderItem::find($id);
			$transaction = Order::find($t_id);

			$transaction->total_qty = $transaction->total_qty - $item->qty ;

			if($item->delete() AND $transaction->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','An order item has been removed.');
				return redirect()->back();
			}else{
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Error.');
				return redirect()->back();
			}
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function received($id=NULL){
		try {
			$order = Order::find($id);

			$order->status = "delivered";

			if($order->save()){

				Helper::user_timeline($this->data['auth']->id,$order->id,'orders','successfully received the order.','receive');

				Session::flash('notification-status','success');
				Session::flash('notification-msg','Successfully received your order, Thank you for your business.');
				return redirect()->back();
			}
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}