<?php namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\BusinessInfo;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\BusinessRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image;

class BusinessController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Business Information";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "business";
		$this->data['types'] = [''=>'Choose type','sales_agent'=>'Sales Agent','admin'=>"Admin",'super_user'=>"Super User"];
	}

	public function index () {
		$this->data['business'] = BusinessInfo::find($this->data['auth']->business_info->id);
		return view('frontend.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function create () {
		return view('frontend.'.$this->data['route_file'].'.create',$this->data);
	}

	public function update (BusinessRequest $request, $id = NULL) {
		try {
			$business = BusinessInfo::find($this->data['auth']->business_info->id);

			if (!$business) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.info');
			}

			$business->fill($request->all());

			if($request->has('password')){
				$business->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$business->directory}/{$business->filename}")){
						File::delete("{$business->directory}/{$business->filename}");
					}
					if (File::exists("{$business->directory}/resized/{$business->filename}")){
						File::delete("{$business->directory}/resized/{$business->filename}");
					}
					if (File::exists("{$business->directory}/thumbnails/{$business->filename}")){
						File::delete("{$business->directory}/thumbnails/{$business->filename}");
					}
				}
				
				$business->directory = $upload["directory"];
				$business->filename = $upload["filename"];
			}

			if($business->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your business has been updated.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.info');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/business"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

}