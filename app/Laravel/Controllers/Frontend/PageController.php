<?php 

namespace App\Laravel\Controllers\Frontend;


/*
*
* Requests used for validating inputs
*/

use App\Laravel\Models\Career;


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;
use Mail,Event;
use App\Laravel\Requests\Frontend\SendEmailRequest;
use App\Laravel\Events\SendEmail;

class PageController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		return view('frontend.index',$this->data);
	}

	public function send_email(SendEmailRequest $request){
		$notification_data = new SendEmail($request->all());

		Event::fire('send-email', $notification_data);

		Session::flash('notification-status','success');
		Session::flash('notification-msg','<strong><i class="fa fa-check"></i> Success</strong> Email Sent.');
		return redirect()->to('/#contact');
	}
}