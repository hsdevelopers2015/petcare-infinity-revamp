<?php 
namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\UserSettingRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Helper, Carbon, Session, Str,File,Image;

class ProfileController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Profile Setting";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";

		$this->data['form_positions'] = ["" => "Choose Position","center" => "Center","left" => "Left" , "right" => "Right"];
	}

	public function setting () {
		return view('frontend.auth.setting',$this->data);
	}

	public function update_setting (UserSettingRequest $request) {
		try {
			$user = User::find($this->data['auth']->id);

			$user->fill($request->all());

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$user->directory = $upload["directory"];
				$user->filename = $upload["filename"];
			}

			if($user->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"User profile has been updated.");
				return redirect()->route('frontend.profile.settings');
			}

			Session::flash('notification-status','success');
			Session::flash('notification-msg',"User profile has been updated.");
			return redirect()->route('frontend.profile.settings');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::find($id);

			if (!$user) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('frontend.users.index');
			}

			$user->email .= "-deleted-" . Helper::date_format(Carbon::now(),"YmdHis");
			$user->username .= "-deleted-" . Helper::date_format(Carbon::now(),"YmdHis");

			if($user->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A user has been deleted.");
				return redirect()->route('frontend.users.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request,$field = "file", $directory = "uploads/profile"){
		$file = $request->file($field);
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->fit(1000,1000)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

}