<?php 

namespace App\Laravel\Controllers\Frontend;


/*
*
* Requests used for validating inputs
*/

use App\Laravel\Models\SentContract;


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;
use Mail,Event;
use App\Laravel\Requests\Frontend\SendEmailRequest;
use App\Laravel\Events\SendEmail;

class ContractsController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['page_title'] = "Contracts";
	}

	public function index () {
		$this->data['contracts'] = SentContract::where('client_id',$this->data['auth']->id)->get();
		return view('frontend.contracts.index',$this->data);
	}

}