<?php namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Sales;
use App\Laravel\Models\SalesItem;
use App\Laravel\Models\SalesDiscount;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\BusinessRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, PDF;

class InvoiceController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Invoice List";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "invoices";
		$this->data['types'] = [''=>'Choose type','sales_agent'=>'Sales Agent','admin'=>"Admin",'super_user'=>"Super User"];
	}

	public function index () {
		$this->data['invoices'] = Sales::where('client_id',$this->data['auth']->id)->whereIn('status',['posted','posted_cancelled','received'])->get();
		return view('frontend.'.$this->data['route_file'].'.index',$this->data);
	}

	public function view($id){
		$this->data['sales'] = Sales::where('id',$id)->first();
		$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
		$this->data['discounts'] = SalesDiscount::where('sales_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
		
		if (!$this->data['sales']) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('frontend.'.$this->data['route_file'].'.index');
		}

		$pdf = PDF::loadView('dompdf.invoice', $this->data);
		return $pdf->stream('dompdf.invoice');
	}

	public function received($slug = NULL){
		$sales = Sales::where('id',$slug)->first();
		$sales->status = 'received';

		if($sales->save()){
			Session::flash('notification-status',"success");
			Session::flash('notification-msg',"You successfully received the product.");
			return redirect()->route('frontend.'.$this->data['route_file'].'.index');
		}
	}

}