<?php namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Branch;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\BranchRequest;
use App\Laravel\Requests\Frontend\EditBranchRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image;

class BranchesController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Branches";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "branches";
		$this->data['types'] = [''=>'Choose type','sales_agent'=>'Sales Agent','admin'=>"Admin",'super_user'=>"Super User"];
	}

	public function index () {
		$this->data['branches'] = Branch::orderBy('created_at',"DESC")->where('user_id',$this->data['auth']->id)->where('business_id',$this->data['auth']->business_info->id)->get();
		return view('frontend.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('frontend.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (BranchRequest $request) {
		try {

			$new_branch = new Branch;
			$new_branch->fill($request->all());
			$new_branch->user_id = $this->data['auth']->id;
			$new_branch->business_id = $this->data['auth']->business_info->id;

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_branch->directory = $upload["directory"];
				$new_branch->filename = $upload["filename"];
			}

			if($new_branch->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New branch has been added.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$branch = Branch::find($id);

		if (!$branch) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('frontend.'.$this->data['route_file'].'.index');
		}

		$this->data['branch'] = $branch;
		return view('frontend.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditBranchRequest $request, $id = NULL) {
		try {
			$branch = Branch::find($id);

			if (!$branch) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			$branch->fill($request->all());

			if($request->has('password')){
				$branch->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$branch->directory}/{$branch->filename}")){
						File::delete("{$branch->directory}/{$branch->filename}");
					}
					if (File::exists("{$branch->directory}/resized/{$branch->filename}")){
						File::delete("{$branch->directory}/resized/{$branch->filename}");
					}
					if (File::exists("{$branch->directory}/thumbnails/{$branch->filename}")){
						File::delete("{$branch->directory}/thumbnails/{$branch->filename}");
					}
				}
				
				$branch->directory = $upload["directory"];
				$branch->filename = $upload["filename"];
			}

			if($branch->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An branch has been updated.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$branch = Branch::find($id);

			$branch->email = $id."@domain.com";

			if (!$branch) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$branch->directory}/{$branch->filename}")){
				File::delete("{$branch->directory}/{$branch->filename}");
			}
			if (File::exists("{$branch->directory}/resized/{$branch->filename}")){
				File::delete("{$branch->directory}/resized/{$branch->filename}");
			}
			if (File::exists("{$branch->directory}/thumbnails/{$branch->filename}")){
				File::delete("{$branch->directory}/thumbnails/{$branch->filename}");
			}

			if($branch->save() AND $branch->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An branch has been deleted.");
				return redirect()->route('frontend.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/branches"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

}