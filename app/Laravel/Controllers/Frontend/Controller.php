<?php 

namespace App\Laravel\Controllers\Frontend;


use App\Laravel\Models\PromoteProduct;
use App\Laravel\Models\SentContract;

use App\Http\Controllers\Controller as MainController;
use Auth, Session,Carbon, Helper,Route;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_frontend_route();
		self::set_current_route();
		self::set_user_info();
		self::set_default_page_title();
		self::set_promoted_products();
		$this->data['contracts_count'] = SentContract::where('client_id',$this->data['auth']->id)->count();
	}


	public function get_data(){
		return $this->data;
	}

	public function set_current_route(){
		 $this->data['current_route'] = Route::currentRouteName();
	}

	public function set_frontend_route(){
		$this->data['routes'] = array(
			"dashboard" => ['frontend.dashboard'],
			"business" => ['frontend.business.info'],
			"invoices" => ['frontend.invoices.index'],
			"branches" => ['frontend.branches.index','frontend.branches.create','frontend.branches.edit','frontend.branches.destroy'],
			"orders" => ['frontend.orders.index','frontend.orders.create','frontend.orders.edit','frontend.orders.destroy'],
			"contracts" => ['frontend.contracts.index'],
		);
	}

	public function set_default_page_title(){
		$this->data['page_title']= "Dashboard";
		$this->data['page_Description']= "Dashboard";
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}

	public function get_user_info(){
		return $this->data['auth'];
	}

	public function set_promoted_products(){
		$this->data['promoted_products'] = PromoteProduct::all();
	}

}