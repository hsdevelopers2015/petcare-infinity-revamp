<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\OrdersRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\DeliveryReceiptRequest;
use App\Laravel\Requests\Backoffice\OrderInvoiceRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF;

class OrdersController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Orders";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "orders";

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();/*Inventory::where('quantity','>',0)
																	->pluck('id','id')->toArray();*/
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
	}

	public function index () {
		$this->data['orders'] = Order::where('status','!=','draft')->where('status','!=','cancelled')->checkuser()->orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function view ($slug = NULL) {
		$this->data['order'] = Order::find($slug);
		$this->data['discounts'] = OrderDiscount::where('order_id',$slug)->get();

		if($this->data['order']){
			$this->data['items'] = OrderItem::where('transaction_id',$this->data['order']->id)->get();
			$this->data['transaction_code'] = $this->data['order']->transaction_code;
			return view('backoffice.'.$this->data['route_file'].'.view',$this->data);
		}else{
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Record not found went wrong.');

			return redirect()->back();
		}
	}

	public function ship($id){
		try {

			$order = Order::find($id);

			$order->status = 'shipped';

			if($order->save()){

				Helper::user_timeline($this->data['auth']->id,$order->id,'orders','shipped '.$order->client_info($order->client_id)->fname.' '.$order->client_info($order->client_id)->lname.' order.','ship');

				Session::flash('notification-status','success');
				Session::flash('notification-msg','The customer order was successfully shipped.');

				return redirect()->back();
			}
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Error.');

			return redirect()->back();
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function po($id = NULL){
		$this->data['sales'] = Order::where('id',$id)->first();
		$this->data['items'] = OrderItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
		$this->data['discounts'] = OrderDiscount::where('order_id',$id)->get();

		if (!$this->data['sales']) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$pdf = PDF::loadView('dompdf.order_invoice', $this->data);
		return $pdf->stream('dompdf.order_invoice');
	}

	public function add_discount($id = NULL){
		try {
			$new_discount = new OrderDiscount;

			$new_discount->fill(Input::all());

			// dd(Input::all());

			if($new_discount->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','Discount successfully added.');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function remove_discount($id = NULL){
		try {
			$sales_discount = OrderDiscount::find($id);

			if (!$sales_discount) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($sales_discount->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A discount has been removed.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function delivery_receipt($id = NULL){
		$this->data['page_title'] = "Delivery Receipt";
		$dr = DeliveryReceipt::where('reference_id',$id)->where('reference','orders')->first();
		$sales = Order::where('id',$id)->first();

		// dd($sales);

		if(!$dr){
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->back();
		}

		$this->data['delivery_receipt'] = $dr;
		$this->data['sales'] = $sales;
		return view('backoffice.'.$this->data['route_file'].'.delivery_receipt',$this->data);
	}

	public function create_delivery_receipt(DeliveryReceiptRequest $request){
		try {
			$new_dr = new DeliveryReceipt;

			$new_dr->fill($request->all());

			if($new_dr->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A delivery receipt successfully created.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.dr',$new_dr->reference_id);
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update_delivery_receipt(DeliveryReceiptRequest $request, $id = NULL){
		$dr = DeliveryReceipt::where('reference_id',$id)->where('reference','orders')->first();
		$dr->fill($request->all());

		if($dr->save()){
			Session::flash('notification-status',"success");
			Session::flash('notification-msg',"Delivery Receipt successfully updated.");
			return redirect()->back();
		}
	}

	public function delivery_receipt_form($id = NULL){
		$delivery_receipt = DeliveryReceipt::find($id);
		$reference = $delivery_receipt->reference;
		$reference_id = $delivery_receipt->reference_id;

		$this->data['delivery_receipt'] = $delivery_receipt;

		if (!$delivery_receipt) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		if($reference == 'sales'){
			$this->data['sales'] = Sales::find($reference_id);
			$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
			$pdf = PDF::loadView('dompdf.sales_delivery_receipt', $this->data);
			return $pdf->stream('dompdf.sales_delivery_receipt');
		}else{
			$this->data['sales'] = Order::find($reference_id);
			$this->data['items'] = OrderItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
			$pdf = PDF::loadView('dompdf.order_delivery_receipt', $this->data);
			return $pdf->stream('dompdf.order_delivery_receipt');
		}

	}

	public function create_invoice(){
		try {
			$id = Input::get('id');
			$order = Order::find($id);

			$order->prepared_by = Input::get('prepared_by');
			$order->invoice_is_created = 'yes';

			if($order->save()){
				Session::flash('notification-status',"success");
				Session::flash('notification-msg',"Invoice is successfully created.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.edit_invoice',$order->id);
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Record not found.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit_invoice($id = NULL){
		try {
			
			$order = Order::find($id);

			if($order){
				$this->data['order'] = $order;
				return view('backoffice.'.$this->data['route_file'].'.edit_invoice',$this->data);
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Record not found.');
			return redirect()->back();
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update_invoice(OrderInvoiceRequest $request,$id){
		try {
			$order = Order::find($id);
			$order->fill($request->all());

			if($order->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','The Invoice detail was successfully updated.');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}