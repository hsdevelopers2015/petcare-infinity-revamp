<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\AccessControl;
use App\Laravel\Models\Cluster;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ClientRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\EditRemarksClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class AccessControlController extends Controller{

	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Access Control";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "access_control";
	}

	public function index () {
		$user = User::where('type','<>','super_user')->where('type','<>','client')->get();

		$this->data['user'] = $user;
		return view('backoffice.access_control.index',$this->data);

	}

	public function update(){
		$access_control = AccessControl::all();
		$modules = Input::get('module_name');
		$user_id = ' '.Input::get('user_id').',';

		if($modules){
			foreach($modules as $index => $value){

				$access_control = AccessControl::find($index);
				$user_ids = $access_control->user_ids;

				$access_control->user_ids = $user_ids.$user_id;

				if($access_control->access_control($user_id,$access_control->module_name)==0){
					$access_control->save();
				}
			}
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"User access control successfully updated.");
			return redirect()->back();
		}else{
			return redirect()->back();
		}
	}

	public function update_default(){
		$modules = AccessControl::all();
		$user_id = ' '.Input::get('user_id').',';

		foreach ($modules as $index => $value) {
			$module = AccessControl::find($value->id);

			$user_ids = $module->user_ids;
			$module->user_ids = str_replace($user_id,'',$user_ids);
			$module->save();
		}
		Session::flash('notification-status','success');
		Session::flash('notification-msg',"User access control successfully set to default.");
		return redirect()->back();
	}

	public function excel(){
		try {
			$this->data['access_control'] = User::where('type','<>','super_user')->where('type','<>','client')->get();

			$ext = "xls";

			$filename = "Access Control : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Access Control', function($sheet) {
					$sheet->loadView('excel.access_control', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}
}