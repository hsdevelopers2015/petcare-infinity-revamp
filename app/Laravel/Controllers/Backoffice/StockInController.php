<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\StockIn;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\Inventory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\StockInRequest;
use App\Laravel\Requests\Backoffice\EditStockInRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class StockInController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Stock In";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "stock_in";

		$this->data['products'] = [''=>"Choose a product"] + Product::all()->pluck('product_name','id')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['statuses'] = ['submitted'=>'Submitted','approved'=>'Approved','posted'=>'Posted'];

		$this->data['stock_in'] = StockIn::all();

	}

	public function index () {
		$this->data['stock_in'] = StockIn::orderBy('created_at',"DESC")->checkproduct()->get();
		$total_amount = [];

		foreach($this->data['stock_in'] as $index => $info){
			array_push($total_amount,$info->qty * $info->acquisition_cost);
		}

		$this->data['total_amount'] = $total_amount ;

		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (StockInRequest $request) {
		try {
			$new_stock_in = new StockIn;
			$new_stock_in->fill($request->all());
			$new_stock_in->expiration_date = Helper::date_format($request->expiration_date,'Y-m-d');

			if($new_stock_in->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A stock has been added to the stock in.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$product = StockIn::find($id);

		if (!$product) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['product'] = $product;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditStockInRequest $request, $id = NULL) {
		try {
			$product = StockIn::find($id);
			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$product->fill($request->all());
			$product->expiration_date = Helper::date_format($request->expiration_date,'Y-m-d');

			if($request->status == "posted"){
				$inventory = Inventory::where('product_id',$product->product_id)->first();
				if($inventory){
					$inventory->quantity = $product->qty + $inventory->quantity;
				}else{
					$inventory = new Inventory;

					$inventory->product_id = $product->product_id;
					$inventory->quantity = $product->qty;
				}

				$inventory->save();
			}

			if($product->save() ){

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = StockIn::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($product->save() AND $product->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/client/business_establishment"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function excel(){
		try {
			$this->data['stock_in'] = StockIn::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Stock In : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Stock In', function($sheet) {

					$sheet->loadView('excel.stock_in', $this->data);

				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}