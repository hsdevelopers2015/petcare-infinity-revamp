<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Cluster;
use App\Laravel\Models\BusinessInfo;
use App\Laravel\Models\Clients;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ImportRequest;
use App\Laravel\Requests\Backoffice\ClientRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\EditRemarksClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ClientsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Clients";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "clients";

		$this->data['clusters'] = ['' => '-Select cluster-'] + Cluster::select(DB::raw("CONCAT(island,' - ','Area No.',area) AS cluster"),'id')->pluck('cluster','id')->toArray();
		$this->data['agents'] = ['' => '-Select agents-']+User::select(DB::raw("CONCAT(fname,' ',lname) AS fullname"),'id')->where('type','sales_agent')->pluck('fullname','id')->toArray();
		$this->data['status'] = ['active' => 'Active','not_active' => 'Not Active'];

	}

	

	public function index () {
		$this->data['clients'] = Clients::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (ClientRequest $request) {
		try {
			$new_client = new Clients;
			$new_client->fill($request->all());
			$new_client->birthday = Helper::date_format($request->birthdate,'Y-m-d');

			if($new_client->save()) {
				$new_client->fill($request->all());
				

				if($new_client->save()){
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New client has been added.");
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$client = Clients::find($id);
		// $client->birthdate = Helper::date_format($request->birthdate,'m-d-Y');

		if (!$client) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['client'] = $client;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (ClientRequest $request, $id = NULL) {
		try {
			$client = Clients::find($id);
			if (!$client) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$client->fill($request->all());
			$client->birthday = Helper::date_format($request->birthday,'Y-m-d');



			if($client->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A client has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit_remarks ($id = NULL) {
		$client = User::find($id);

		if (!$client) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['client'] = $client;
		return view('backoffice.'.$this->data['route_file'].'.edit_remarks',$this->data);
	}

	public function update_remarks (EditRemarksClientRequest $request, $id = NULL) {
		try {
			$client = User::find($id);
			$business_info = BusinessInfo::where('user_id',$id)->first();

			if (!$client) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$client->fill($request->all());

			if($request->has('password')){
				$employee->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$business_info->directory}/{$business_info->filename}")){
						File::delete("{$business_info->directory}/{$business_info->filename}");
					}
					if (File::exists("{$business_info->directory}/resized/{$business_info->filename}")){
						File::delete("{$business_info->directory}/resized/{$business_info->filename}");
					}
					if (File::exists("{$business_info->directory}/thumbnails/{$business_info->filename}")){
						File::delete("{$business_info->directory}/thumbnails/{$business_info->filename}");
					}
				}
				
				$business_info->directory = $upload["directory"];
				$business_info->filename = $upload["filename"];
			}

			$business_info->business_name = $request->business_name;
			$business_info->business_type = $request->business_type;
			$business_info->business_location = $request->business_location;

			if($client->save() AND $business_info->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A client has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$client = User::find($id);

			$client->email = $id."@domain.com";

			if (!$client) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$client->directory}/{$client->filename}")){
				File::delete("{$client->directory}/{$client->filename}");
			}
			if (File::exists("{$client->directory}/resized/{$client->filename}")){
				File::delete("{$client->directory}/resized/{$client->filename}");
			}
			if (File::exists("{$client->directory}/thumbnails/{$client->filename}")){
				File::delete("{$client->directory}/thumbnails/{$client->filename}");
			}

			if($client->save() AND $client->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A client has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/client/business_establishment"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}


	public function view ($id = NULL) {
		$client = User::find($id);

		if (!$client) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		return redirect()->route('backoffice.profile.user_profile',[$client->username]);
	}

	public function update_account($id = NULL , $type = NULL){
		try {
			$update = User::find($id);

			if($type == "no"){
				$update->bad_account = "yes";
			}else{
				$update->bad_account = "no";
			}

			if($update->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','You successfully updated the client account type.');
				return redirect()->back();
			}
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function export_process() {
		try {
			$this->__init_xls();

			$this->data['clients'] = User::where('type','client')->get();
			$this->__init_xls();

			$ext = "xls";

			$filename = "Client List : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Client List', function($sheet) {

					$sheet->loadView('excel.clients', $this->data);

				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

	public function import(ImportRequest $request){
		try {
			$this->data['error_checker'] = 0;
			$this->data['cluster_id'] = $request->cluster_id;

			$ext = $request->file('file')->getClientOriginalExtension();
			$filename = Helper::create_filename($ext);
			$request->file('file')->move("temp_upload", $filename); 


			$import = Excel::load("temp_upload/".$filename, function($reader) {
				$result = $reader->all();
				$sheet_count = count($result);

				foreach ($result as $index => $sheet) {
					// dd($sheet);
					foreach($sheet as $i => $row){
						// $input_data = $row->toArray();
						
						$new_user = new User;
						// $new_user->fill($input_data);
						// dd($row->email? : rand(1000,100000).'@vetcare.com');

						$new_user->fname = $row->contact_person? : "No Contact Person";
						$new_user->contact = $row->contact_no;
						$new_user->email = $row->email? : Str::random($length = 8).'@vetcare.com';
						$new_user->address = $row->home_address? : '---';
						$new_user->segment = $row->segmentation;
						$new_user->username = $row->contact_person.Str::random($length = 8);
						$new_user->password = bcrypt($row->contact_person);
						$new_user->type = "client";
						$new_user->remarks = $row->remarks;
						$new_user->cluster_id = $this->data['cluster_id'];

						if($row->veterinary_clinicpet_supply != null){

							if($row->birthdate != null){
								foreach ($row->birthdate as $key => $value) {
									$new_user->birthdate = Helper::date_format($value,'Y-m-d');
								}
							}

							if($new_user->save()){
								$new_business = new BusinessInfo;
								$new_business->business_name = $row->veterinary_clinicpet_supply;
									// $new_business->business_type = $row->business_type;
								$new_business->business_location = $row->address;
								$new_business->prc_number = $row->prc_number;
								$new_business->purchaser_number = $row->account_number;
								$new_business->purchaser = $row->account_name;
								$new_business->bank_name = $row->bank_name;
								$new_business->user_id = $new_user->id;

								$new_business->save();
							}
						}else{
							$this->data['error_checker'] = ++$this->data['error_checker'];
						}
					}
				}
			});

			if($this->data['error_checker'] > 0){
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Failed to upload.');
				return redirect()->back();
			}else{
				Session::flash('notification-status','success');
				Session::flash('notification-msg','The user  list was successfully uploaded.');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Please contact your system administator.');
			return redirect()->back();
		}
	}
	public function update_excel(ImportRequest $request){
		try {
			$users = User::where('cluster_id',$request->cluster_id)->where('type','client')->get();
			foreach($users as $index => $user){
				$user->delete();
			}
			$this->data['error_checker'] = 0;
			$this->data['cluster_id'] = $request->cluster_id;

			$ext = $request->file('file')->getClientOriginalExtension();
			$filename = Helper::create_filename($ext);
			$request->file('file')->move("temp_upload", $filename); 


			$import = Excel::load("temp_upload/".$filename, function($reader) {
				$result = $reader->all();
				$sheet_count = count($result);

				foreach ($result as $index => $sheet) {
					// dd($sheet);
					foreach($sheet as $i => $row){
						// $input_data = $row->toArray();
						
						$new_user = new User;
						// $new_user->fill($input_data);
						// dd($row->email? : rand(1000,100000).'@vetcare.com');

						$new_user->fname = $row->contact_person? : "No Contact Person";
						$new_user->contact = $row->contact_no;
						$new_user->email = $row->email? : Str::random($length = 8).'@vetcare.com';
						$new_user->address = $row->home_address? : '---';
						$new_user->segment = $row->segmentation;
						$new_user->username = $row->contact_person.Str::random($length = 8);
						$new_user->password = bcrypt($row->contact_person);
						$new_user->type = "client";
						$new_user->remarks = $row->remarks;
						$new_user->cluster_id = $this->data['cluster_id'];

						if($row->veterinary_clinicpet_supply != null){

							if($row->birthdate != null){
								foreach ($row->birthdate as $key => $value) {
									$new_user->birthdate = Helper::date_format($value,'Y-m-d');
								}
							}

							if($new_user->save()){
								$new_business = new BusinessInfo;
								$new_business->business_name = $row->veterinary_clinicpet_supply;
									// $new_business->business_type = $row->business_type;
								$new_business->business_location = $row->address;
								$new_business->prc_number = $row->prc_number;
								$new_business->purchaser_number = $row->account_number;
								$new_business->purchaser = $row->account_name;
								$new_business->bank_name = $row->bank_name;
								$new_business->user_id = $new_user->id;

								$new_business->save();
							}
						}else{
							$this->data['error_checker'] = ++$this->data['error_checker'];
						}
					}
				}
			});

			if($this->data['error_checker'] > 0){
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Failed to upload.');
				return redirect()->back();
			}else{
				Session::flash('notification-status','success');
				Session::flash('notification-msg','The user  list was successfully uploaded.');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Please contact your system administator.');
			return redirect()->back();
		}
	}

}