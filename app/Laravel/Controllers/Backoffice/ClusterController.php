<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Cluster;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\Inventory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ClusterRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ClusterController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Clustering";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "cluster";

		$this->data['products'] = [''=>"Choose a product"] + Product::all()->pluck('product_name','id')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['islands'] = [''=>'--Select--','luzon'=>'Luzon','visayas'=>'Visayas','mindanao'=>'Mindanao'];
		$this->data['areas'] = [''=>'--Select--','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'];

		$this->data['clusters'] = Cluster::all();

	}

	public function index () {
		$this->data['clusters'] = Cluster::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (ClusterRequest $request) {
		try {
			$new_area = new Cluster;
			$new_area->fill($request->all());

			if($new_area->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An area has been added");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$cluster = Cluster::find($id);

		if (!$cluster) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.edit');
		}

		$this->data['cluster'] = $cluster;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (ClusterRequest $request, $id = NULL) {
		try {
			$cluster = Cluster::find($id);
			if (!$cluster) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$cluster->fill($request->all());

			if($cluster->save() ){

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An area has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$cluster = Cluster::find($id);

			if (!$cluster) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($cluster->save() AND $cluster->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	private function __init_xls(){

		$header = [
			'Island',
			'City',
			'Area',
			'Area Quota',
       	];

		$this->data['header'] = $header;
	}

	public function export() {
		try {
			$this->__init_xls();
			$this->data['clusters'] = Cluster::all();
			$ext = "xls";
			$filename = Helper::create_filename($ext, true);

			Excel::create($filename, function($excel) {

				$excel->setTitle("License Exported Data")
				->setCreator('HS Backend Team')
				->setCompany('Highly Succeed Inc.');

				$excel->sheet('License', function($sheet) {
					$sheet->freezeFirstRow();
					$sheet->setOrientation('landscape');
					$sheet->row(1,$this->data['header']);

					$counter = 2;

					foreach ($this->data['clusters'] as $index => $info) {
						$sheet->row($counter++, [
							Str::title($info->island), 
							$info->city, 
							$info->area, 
							number_format($info->are_quota,2),
						]);
					}
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}