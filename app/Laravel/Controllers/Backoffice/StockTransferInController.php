<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Product;
use App\Laravel\Models\User;
use App\Laravel\Models\PromoteProduct;
use App\Laravel\Models\Category;
use App\Laravel\Models\Measurement;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\Brand;
use App\Laravel\Models\Branch;



/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ImportRequest;
use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\PromoteProductRequest;
use App\Laravel\Requests\Backoffice\EditPromoteProductRequest;
use App\Laravel\Requests\Backoffice\StockTransferOutRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class StockTransferInController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Products";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "stock_transfer_in";
		$this->data['supplier_code'] = [''=>'-Supplier Code-']+Supplier::pluck('supplier_name','supplier_code')->toArray();
		$this->data['brand_code'] = [''=>'-Brand Code-']+Brand::pluck('brand_name','brand_code')->toArray();
		$this->data['product_code'] = [''=>'-Product Code-']+Product::pluck('product_name','product_code')->toArray();
		$this->data['branch_code'] = [''=>'-Branch Code-']+Branch::pluck('branch_name','branch_code')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['product_list'] = ['' => "Choose a product"] + Product::pluck('product_name','id')->toArray();
		$this->data['quota_types'] = ['mother_quota' => "Mother Quota", 'group_quota' => "Group Product Quota"];
		$this->data['focus_types'] = ['amount' => "Amount", 'qty' => "Quantity"];
		$this->data['categories'] = ['' => "-Select Category-"]+Category::pluck('category_name','category_code')->toArray();
		$this->data['measurement'] = [''=>'-Select Unit-']+Measurement::pluck('description','code')->toArray();
	}

	public function index(){
		$this->data['transaction_hdr'] = TransactionHeader::orderBy('created_at',"DESC")->where('account_type','stock_transfer')->orwhere('status','pending')
		->where('status','received')
		->get() ? : new TransactionHeader;
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function show($transaction_code =  NULL){
		$transaction_hdr = TransactionHeader::where('transaction_code',$transaction_code)->first();
		$this->data['totalQty'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('quantity');
		$this->data['totalCost'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('cost');
		$this->data['totalSelling'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('selling');
		if (!$transaction_hdr) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['transaction_hdr'] = $transaction_hdr;
		return view('backoffice.'.$this->data['route_file'].'.show',$this->data);
	}

	public function received($transaction_code = NULL){
		try {					

			$transaction_hdr = TransactionHeader::where('transaction_code',$transaction_code)->first();
			$transaction_hdr->transaction_type = "outwards";
			$transaction_hdr->status = "received";

			$transaction_dtl = TransactionDetail::where('transaction_code',$transaction_code)->get();
			foreach($transaction_dtl as $index => $value){
			  $value = TransactionDetail::find($value->id);
			  $value->transaction_type = "outwards";
			  $value->stock_trasfer_in = "yes";
			  $value->status = "received";
			  $value->save();
			}
			
			
			if($transaction_hdr->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create(){
		$type = "STO";
		$current_sequence = TransactionHeader::orderBy('transaction_code','DESC')->where('transaction_code','like',$type.'%')->first();
		$initial_counter = 0000000;
		if(!$current_sequence){		
			$current_sequence = TransactionHeader::orderBy('transaction_code',"DESC")->where('transaction_code','like',$type.'%')->first();			
			$this->data['sequence'] = $type.str_pad(($initial_counter)+1, 7, "0", STR_PAD_LEFT);
		}else{
			$var = trim($current_sequence->transaction_code,$type);			
			$this->data['sequence'] = $type.str_pad(($var)+1, 7, "0", STR_PAD_LEFT);
		}

		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
		return view('backoffice.'.$this->data['route_file'].'.stock_transfer_in',$this->data);
	}

	public function store (StockTransferOutRequest $request) {
		try {

			$supplier_code = Input::get('supplier_code');
			$transaction_code = Input::get('transaction_code');
			$description = Input::get('description');
			$to = Input::get('to');
			// $invoice_number = Input::get('invoice_number');				

			$transaction_hdr = new TransactionHeader;
			$transaction_hdr->supplier_code = $supplier_code;
			$transaction_hdr->account_type = "stock_transfer";
			$transaction_hdr->transaction_type = "stock_transfer_out";
			$transaction_hdr->transaction_code = $transaction_code;
			$transaction_hdr->description = $description;
			
			$brand_code = $request->get('brand_code') ?: array();			
			$product_code = $request->get('product_code') ?: array();
			$qty = $request->get('qty') ?: array();
			$cost = $request->get('cost') ?: array();
			$selling = $request->get('selling') ?: array();
			$expiration_date = $request->get('expiration_date') ?: array();
	
			
			if(count($product_code) == count($qty) && 
			    count($qty) == count($product_code)
			) {
			    for ($i=0; $i < count($product_code); $i++) { 
			    	
			    	
			        $transaction_dtl = new TransactionDetail;                      
			        $transaction_dtl->fill([
			            
			            'transaction_code' => $transaction_code,
			            'from' => $supplier_code,
			            'to' => $to,
			            'account_type' => "stock_transfer",
			            'supplier_code' => $supplier_code,
			            'brand_code' => $brand_code[$i],			            
			            'product_code' => $product_code[$i],
			            'product_name' => "product name",
			            'transaction_type' => "stock_transfer_out",
			            'unit' => "unit",
			            'quantity' => $qty[$i],
			            'cost' => $cost[$i],
			            'selling' => $selling[$i],
			            'expiration_date' => $expiration_date[$i]                 
			        ]);
			        $transaction_dtl->save();
			    }
			}

			if($transaction_hdr->save()) {
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New category has been added.");
					dd('saving na bes');
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}

	public function stock_transfer_in () {
		dd('in');
		$this->data['stock_transfer_in'] = TransactionHeader::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.stock_transfer_in',$this->data);
	}
	public function stock_transfer_out () {
		$this->data['stock_transfer_out'] = TransactionHeader::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.stock_transfer_out',$this->data);
	}

	
}