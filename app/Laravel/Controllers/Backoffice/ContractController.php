<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Contract;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\Inventory;
use App\Laravel\Models\SentContract;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ContractRequest;
use App\Laravel\Requests\Backoffice\CreateContractRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\EditSentContractRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ContractController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Contracts";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "contracts";

		$this->data['products'] = [''=>"Choose a product"] + Product::all()->pluck('product_name','id')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['statuses'] = ['submitted'=>'Submitted','approved'=>'Approved','posted'=>'Posted'];

		$this->data['contract_count'] = Contract::all()->count()+1;

		if($this->data['auth']->type == 'sales_head' OR $this->data['auth']->type == 'sales_agent'){

			$clients = $this->data['auth']->get_clients($this->data['auth']->cluster_id);
			if($this->data['auth']->type == 'sales_agent'){
				$this->data['clients'] = [''=>"Choose a client"] + User::where('cluster_id',$this->data['auth']->id)->select(
										DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->pluck('name','id')->toArray();
			}else{
				$this->data['clients'] = [''=>"Choose a client"] + User::whereIn('id',$clients)->select(
										DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->pluck('name','id')->toArray();
			}
		
		}else{
			$this->data['clients'] = [''=>"Choose a client"] + User::orderBy('created_at',"DESC")->where('type','client')->select(
									DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->pluck('name','id')->toArray();
		}


	}

	public function index () {
		$this->data['contracts'] = Contract::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function sent () {

		$this->data['page_title'] = "Sent Contracts";
		if($this->data['auth']->type == "sales_agent"){
			$this->data['sent_contracts'] = SentContract::orderBy('created_at',"DESC")->checkclient()->where('sa_id',$this->data['auth']->id)->get();
		}else{
			$this->data['sent_contracts'] = SentContract::orderBy('created_at',"DESC")->checkclient()->get();
		}
		return view('backoffice.'.$this->data['route_file'].'.sent',$this->data);
	}

	public function creating () {
		$this->data['contracts'] = [''=> "Select a contract"] + Contract::orderBy('created_at',"DESC")->select(
									DB::raw("CONCAT(contract_code,' - ',detail) AS contract"),'id')->pluck('contract','id')->toArray();
		return view('backoffice.'.$this->data['route_file'].'.create_contract',$this->data);
	}

	public function send (CreateContractRequest $request) {
		try {
			$new_contract = new SentContract;
			$new_contract->fill($request->all());
			$new_contract->sa_id = $this->data['auth']->id;

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_contract->directory = $upload["directory"];
				$new_contract->filename = $upload["filename"];
			}

			if($new_contract->save()) {

				Helper::user_timeline($this->data['auth']->id,$new_contract->id,'sent_contracts','was sent a contract to '.$new_contract->client_info($new_contract->client_id)->fname.' '.$new_contract->client_info($new_contract->client_id)->lname.'.','sent');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been sent.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function store (ContractRequest $request) {
		try {
			$new_contract = new Contract;
			$new_contract->fill($request->all());
			// $new_contract->sa_id = $this->data['auth']->id;

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_contract->directory = $upload["directory"];
				$new_contract->filename = $upload["filename"];
			}

			if($new_contract->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$contract = Contract::find($id);

		if (!$contract) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['contract'] = $contract;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function edit_contract ($id = NULL) {
		$contract = SentContract::find($id);

		$this->data['contracts'] = [''=> "Select a contract"] + Contract::orderBy('created_at',"DESC")->select(
									DB::raw("CONCAT(contract_code,' - ',detail) AS contract"),'id')->pluck('contract','id')->toArray();

		$clients = $this->data['auth']->get_clients($this->data['auth']->cluster_id);

		$this->data['clients'] = [''=>"Choose a client"] + User::select(
										DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->whereIn('id',$clients)->pluck('name','id')->toArray();

		if (!$contract) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
		}

		$this->data['contract'] = $contract;
		return view('backoffice.'.$this->data['route_file'].'.edit_contract',$this->data);
	}

	public function update_contract (EditSentContractRequest $request, $id = NULL) {
		try {
			$contract = SentContract::find($id);
			if (!$contract) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
			}

			$contract->fill($request->all());

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$contract->directory = $upload["directory"];
				$contract->filename = $upload["filename"];
			}


			if($contract->save() ){

				Helper::user_timeline($this->data['auth']->id,$contract->id,'sent_contracts','was updated a contract for '.$contract->client_info($contract->client_id)->fname.' '.$contract->client_info($contract->client_id)->lname.'.','update');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update (EditContractRequest $request, $id = NULL) {
		try {
			$contract = Contract::find($id);
			if (!$contract) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$contract->fill($request->all());

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$contract->directory = $upload["directory"];
				$contract->filename = $upload["filename"];
			}


			if($contract->save() ){

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$contract = Contract::find($id);

			if (!$contract) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($contract->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy_contract ($id = NULL) {
		try {
			$contract = SentContract::find($id);

			if (!$contract) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
			}

			Helper::user_timeline($this->data['auth']->id,$contract->id,'sent_contracts','was deleted the contract for '.$contract->client_info($contract->client_id)->fname.' '.$contract->client_info($contract->client_id)->lname.'.','delete');

			if($contract->delete()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A contract has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.sent');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/contracts"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		// Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		// Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function excel(){
		try {
			$this->data['contracts'] = Contract::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Contract List : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Contract List', function($sheet) {
					$sheet->loadView('excel.contract_list', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

	public function excel_sent_contracts(){
		try {
			if($this->data['auth']->type == "sales_agent"){
				$this->data['sent_contracts'] = SentContract::orderBy('created_at',"DESC")->checkclient()->where('sa_id',$this->data['auth']->id)->get();
			}else{
				$this->data['sent_contracts'] = SentContract::orderBy('created_at',"DESC")->checkclient()->get();
			}

			$ext = "xls";

			$filename = "Sent Contract List : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Sent Contract List', function($sheet) {
					$sheet->loadView('excel.sent_contract_list', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}