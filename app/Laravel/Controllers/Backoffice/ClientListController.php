<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Cluster;
// use App\User;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class ClientListController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Owner";
	}

	public function index () {
		if(in_array($this->data['auth']->type, ['sales_agent'])){
			$this->data['clients'] = User::where('type','client')->where('cluster_id',$this->data['auth']->cluster_id)->get();
		}else{
			$this->data['clients'] = User::where('type','client')->get();
		}
		return view('backoffice.client.list',$this->data);
	}

}