<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\RequestExpense;
use App\User;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Excel;

class RequestFormsController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['route_file'] = "requests";
		$this->data['page_title'] = "Requests";
	}

	public function index () {
		$this->data['requests'] = RequestExpense::all();
		return view('backoffice.requests.index',$this->data);
	}

	public function edit ($id=NULL) {
		$this->data['requests'] = RequestExpense::where('id',$id)->first();
		return view('backoffice.requests.edit',$this->data);
	}

	public function status($id = NULL, $status = NULL){
		try {
			$request = RequestExpense::find($id);
			$request->status = $status;
			if($request->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request successfully updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function excel(){
		try {
			$this->data['requests'] = RequestExpense::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Request Expense : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Request Expense', function($sheet) {
					$sheet->loadView('excel.requests', $this->data);
				});

			})->export($ext);

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}
}