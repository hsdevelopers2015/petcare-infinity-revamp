<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Sales;
use App\Laravel\Models\SalesItem;
use App\Laravel\Models\SalesDiscount;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ProductRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class StockOutController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$this->data['route_file'] = "stock_in";
		$this->data['page_title'] = "Stock Out";
	}

	public function index () {
		$this->data['sales'] = Sales::orderBy('created_at',"DESC")->where('status','posted')->where('payment_status','pending')->get();
		return view('backoffice.stock_out.index',$this->data);
	}

	public function for_delivery($id = NULL)
	{
		$sales = Sales::find($id);
		$sales->status = "for_delivery";
		if($sales->save()){
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"The items was successfully confirmed.");

			Helper::transaction_log($this->data['auth']->id,$sales->id,'sales','cancelled','destroy','Cancel the delivery for '.$sales->client_info($sales->client_id)->fname.' '.$sales->client_info($sales->client_id)->lname);

			return redirect()->route('backoffice.stock_out.index');
		}
	}

	public function view($transaction_code = NULL)
	{
		$this->data['transaction_code'] = $transaction_code;
		$this->data['sales'] = Sales::where('transaction_code',$transaction_code)->first();
		$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->get();
		$this->data['discounts'] = SalesDiscount::where('sales_id',$this->data['sales']->id)->get();
		return view('backoffice.stock_out.view',$this->data);
	}

	public function excel(){
		try {
			$this->data['stock_out'] = Sales::orderBy('created_at',"DESC")->where('status','posted')->where('payment_status','pending')->get();

			$ext = "xls";

			$filename = "Stock Out : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Stock Out', function($sheet) {
					$sheet->loadView('excel.stock_out', $this->data);
				});

			})->export($ext);

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}
}