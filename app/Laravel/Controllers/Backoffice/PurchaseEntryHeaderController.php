<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\PurchaseEntryHeader;
use App\Laravel\Models\PurchaseReceivedHeader;
use App\Laravel\Models\PurchaseReceivedDetail;
use App\Laravel\Models\PurchaseEntryDetail;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CategoryRequest;
use App\Laravel\Requests\Backoffice\SupplierRequest;
use App\Laravel\Requests\Backoffice\PurchaseHeaderEntryRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF;

class PurchaseEntryHeaderController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Purchase Entry Form";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "purchase_entry_hdr";
		$this->data['supplier_code'] = [''=>'-Supplier Code-']+Supplier::pluck('supplier_name','supplier_code')->toArray();
		$this->data['product_code'] = [''=>'-Product Code-']+Product::pluck('product_name','product_code')->toArray();
		$this->data['purchase_code'] = [''=>'-Purchased Code-']+PurchaseEntryHeader::pluck('purchase_code','purchase_code')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();/*Inventory::where('quantity','>',0)
																	->pluck('id','id')->toArray();*/
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
	}
	public function create(){
		 $this->data['purchasehdr_code'] = "PO".Helper::date_format(Carbon::now(),"Y").Str::upper(Str::random(4));
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function index () {
		$this->data['purchase_entry_hdr'] = PurchaseEntryHeader::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (PurchaseHeaderEntryRequest $request) {
		try {
			$supplier_code = Input::get('supplier_code');
			$purchase_code = Input::get('purchase_code');
			$description = Input::get('description');			
					

			$purchase_entry_hdr = new PurchaseEntryHeader;
			// $purchase_entry_hdr->fill($request->all());
			$purchase_entry_hdr->supplier_code = $supplier_code;
			$purchase_entry_hdr->purchase_code = $purchase_code;
			$purchase_entry_hdr->description = $description;
			$purchase_entry_hdr->save(); 

			$product_code = $request->get('product_code') ?: array();
			$qty = $request->get('qty') ?: array();
			$cost_price = $request->get('cost_price') ?: array();
			$selling_price = $request->get('selling_price') ?: array();
			$expiration_date = $request->get('expiration_date') ?: array();
			
			if(count($product_code) == count($qty) && 
			    count($qty) == count($product_code)
			) {
			    for ($i=0; $i < count($product_code); $i++) { 
			    	
			        $purchase_entry_dtl = new PurchaseEntryDetail;                      
			        $purchase_entry_dtl->fill([
			            
			            'purchasehdr_id' => $purchase_entry_hdr->id,
			            'purchasehdr_code' => $purchase_entry_hdr->purchase_code,
			            'supplier_code' => $supplier_code,
			            'product_code' => $product_code[$i],
			            'product_name' => "sdsd",
			            'qty' => $qty[$i],
			            'cost_price' => $cost_price[$i],
			            'selling_price' => $selling_price[$i],
			            'expiration_date' => $expiration_date[$i]                 
			        ]);
			        $purchase_entry_dtl->save();
			    }
			}

			if($purchase_entry_hdr->save()) {
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New category has been added.");
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}	

	public function edit ($id = NULL) {
		$purchase_entry_hdr = PurchaseEntryHeader::find($id);

		if (!$purchase_entry_hdr) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['purchase_entry_hdr'] = $purchase_entry_hdr;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (PurchaseHeaderEntryRequest $request, $id = NULL) {
				try {
			$supplier_code = Input::get('supplier_code');
			$purchase_code = Input::get('purchase_code');
			$description = Input::get('description');

			$purchase_entry_hdr = PurchaseEntryHeader::find($id);
			$purchase_entry_hdr->supplier_code = $supplier_code;
			$purchase_entry_hdr->purchase_code = $purchase_code;
			$purchase_entry_hdr->description = $description;
			// $purchase_entry_hdr->save(); 
			// dd($purchase_entry_hdr->purchase_code);

			if (!$purchase_entry_hdr) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			 

			 $product_code = $request->get('product_code') ?: array();
			 $qty = $request->get('qty') ?: array();
			 $cost_price = $request->get('cost_price') ?: array();
			 $selling_price = $request->get('selling_price') ?: array();
			 $expiration_date = $request->get('expiration_date') ?: array();
			 $purchase_entry_dtl = PurchaseEntryDetail::where('purchasehdr_code',$purchase_entry_hdr->purchase_code)->get();

              foreach($purchase_entry_dtl as $index => $value){
                $value = PurchaseEntryDetail::find($value->id);
                $value->delete();
              }


              if(count($product_code) == count($qty) && 
			    count($qty) == count($product_code)
			) {
			    for ($i=0; $i < count($product_code); $i++) { 
			    	
			        $purchase_entry_dtl = new PurchaseEntryDetail;                      
			        $purchase_entry_dtl->fill([
			            
			            'purchasehdr_id' => $purchase_entry_hdr->id,
			            'purchasehdr_code' => $purchase_entry_hdr->purchase_code,
			            'supplier_code' => $supplier_code,
			            'product_code' => $product_code[$i],
			            'product_name' => "sdsd",
			            'qty' => $qty[$i],
			            'cost_price' => $cost_price[$i],
			            'selling_price' => $selling_price[$i],
			            'expiration_date' => $expiration_date[$i]                 
			        ]);
			        $purchase_entry_dtl->save();
			    }
			}

			
			
			if($purchase_entry_hdr->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function destroy ($id = NULL) {
		try {
			$supplier = Supplier::find($id);

			if (!$supplier) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($supplier->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}