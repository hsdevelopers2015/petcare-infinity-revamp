<?php 

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\Order;
use App\Laravel\Models\Sales;
use App\Laravel\Models\Inventory;
use App\Laravel\Models\QuotaConfiguration;
use App\Laravel\Models\AccessControl;
use App\Laravel\Models\Cluster;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller as MainController;
use Auth, Session,Carbon, Helper,Route;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_backoffice_routes();
		self::set_user_info();
		self::set_date_today();
		self::set_current_route();
		self::set_default_page_title();
		self::set_total_customers();
		self::set_most_ordered_products();
		self::set_on_going_orders();
		self::set_total_sales_agents();
		self::set_on_going_sales();
		self::set_most_demanding_business();
		self::set_quota_config();
		self::set_total_inventory();
		self::set_check_replenish_product();
		self::set_mother_quota();
		self::set_group_quota();
		self::set_quotas();
		self::set_special_product_focus();
		self::set_date_range();
		self::set_access_control();
	}

	public function set_backoffice_routes(){
		$this->data['routes'] = array(
			"dashboard" => ['backoffice.dashboard'],
			"category" => ['backoffice.category.index','backoffice.category.create'],
			"supplier" => ['backoffice.supplier.index','backoffice.supplier.create'],
			"branches" => ['backoffice.branches.index','backoffice.branches.create'],
			"sales_agents" => ['backoffice.sales_agents.index'],
			"sales_incentive" => ['backoffice.sales_incentive.index'],
			"client_list" => ['backoffice.client_list.index'],
			"cluster" => ['backoffice.cluster.index','backoffice.cluster.create'],
			"users" => ['backoffice.users.index','backoffice.users.create','backoffice.users.edit','backoffice.users.destroy'],
			"clients" => ['backoffice.clients.index','backoffice.clients.create','backoffice.clients.edit','backoffice.clients.destroy'],
			// "employee" => ['backoffice.employee.index','backoffice.employee.create','backoffice.employee.edit','backoffice.employee.destroy'],
			// "client" => ['backoffice.client.index','backoffice.client.create','backoffice.client.edit','backoffice.client.destroy'],

			"master_file" =>['backoffice.inventory.index','backoffice.inventory.create','backoffice.inventory.edit','backoffice.inventory.destroy','backoffice.employee.index','backoffice.employee.create','backoffice.employee.edit','backoffice.employee.destroy','backoffice.client.index','backoffice.client.create','backoffice.client.edit','backoffice.client.destroy','backoffice.products.index','backoffice.products.create','backoffice.products.edit','backoffice.products.destroy','backoffice.clusters.index','backoffice.clusters.create','backoffice.clusters.edit','backoffice.clusters.destroy','backoffice.measurement.create','backoffice.measurement.index'],
			"purchase_entry_hdr" =>['backoffice.purchase_entry_hdr.index','backoffice.purchase_entry_hdr.create'],
			"purchase_received_hdr" =>['backoffice.purchase_received_hdr.index','backoffice.purchase_received_hdr.create'],
			"transaction_hdr" =>['backoffice.transaction_hdr.index','backoffice.transaction_hdr.create'],
			"sales" => ['backoffice.sales.index','backoffice.sales.create','backoffice.sales.edit','backoffice.sales.destroy'],
			"stock_in" => ['backoffice.stock_in.index','backoffice.stock_in.create','backoffice.stock_in.edit','backoffice.stock_in.destroy'],
			"stock_out" => ['backoffice.stock_out.index','backoffice.stock_out.for_delivery'],
			"contracts" => ['backoffice.contracts.index','backoffice.contracts.create','backoffice.contracts.edit','backoffice.contracts.destroy'],
			"orders" => ['backoffice.orders.index','backoffice.orders.create','backoffice.orders.edit','backoffice.orders.destroy'],
			"expenses" => ['backoffice.expenses.index','backoffice.expenses.create','backoffice.expenses.edit','backoffice.expenses.destroy'],

		);
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}

	public function set_current_route(){
		 $this->data['current_route'] = Route::currentRouteName();
	}

	public function get_user_info(){
		return $this->data['auth'];
	}

	public function set_default_page_title(){
		$this->data['page_title']= "Dashboard";
		$this->data['page_Description']= "Dashboard";
	}
	
	public function get_data(){
		return $this->data;
	}

	public function set_date_today(){
		$this->data['date_today'] = Helper::date_db(Carbon::now());
	}

	public function set_total_customers(){
		$this->data['total_customers'] = User::where('type','client')->get();
		$this->data['sales_agent_total_customers'] = User::where('type','client')->where('cluster_id',Auth::user()->cluster_id)->get();
	}

	public function set_total_sales_agents(){
		$this->data['total_sales_agents'] = User::whereIn('type',['sales_agent','sales_head'])->get();
	}

	public function set_most_ordered_products(){
		$this->data['most_ordered_products'] = Product::checkOrder()->get();
	}

	public function set_on_going_orders(){
		$this->data['on_going_orders'] = Order::whereIn('status',['shipped','order_confirmation'])->get();
	}

	public function set_on_going_sales(){
		if(in_array(Auth::user()->type, ['admin','super_user'])){
			$this->data['pending_sales'] = Sales::where('payment_status','pending')->get();
			$this->data['sales'] = Sales::whereIn('payment_status',['partially_paid','fully_paid'])->get();
		}else{
			$this->data['pending_sales'] = Sales::where('payment_status','pending')->where('sa_id',Auth::user()->id)->get();
			$this->data['sales'] = Sales::whereIn('payment_status',['partially_paid','fully_paid'])->where('sa_id',Auth::user()->id)->get();
		}

		$order = Order::where('status','shipped')->count();
		$sales = Sales::where('status','for_delivery')->count();

		$this->data['for_deliveries'] = $order + $sales;
	}

	public function set_most_demanding_business(){
		$this->data['most_demanding_business'] = User::where('type','client')->checkOrder()->get();
	}

	public function set_quota_config(){
		$quota = QuotaConfiguration::orderBy('created_at','DESC')->first();

		if(!$quota){
			$this->data['sales_quota_amount'] = 0;
		}else{
			$this->data['sales_quota_amount'] = $quota->sales_quota;
		}
	}

	public function set_total_inventory(){
		$physical_count = Inventory::all();

		$count = [];

		foreach($physical_count as $index => $info){
			array_push($count,$info->quantity);
		}

		$count = array_sum($count);

		$this->data['total_inventory_count'] = $count;
	}

	public function set_check_replenish_product(){

		// $this->data['check_replenish'] = Inventory::where('quantity','<=',[$replenish])->count();
		$physical_count = Inventory::checkproduct()->get();

		$replenish = 0;
		foreach($physical_count as $index => $info){
			if($info->product_info($info->product_id)->replenish){

				if($info->quantity <= $info->product_info($info->product_id)->replenish){
					$replenish = 1;
				}
			}
			else{
				$replenish = 0;
			}
		}

		$this->data['check_replenish'] = $replenish;
	}

	public function set_mother_quota(){
		$this->data['mother_quota'] = Product::all(); 
	}

	public function set_quotas(){
		$quota = QuotaConfiguration::orderBy('created_at','DESC')->first();
		if($quota){
			$this->data['mother_quota_amount'] = $quota->mother_quota;
			$this->data['group_quota_amount'] = $quota->group_quota;
		}else{
			$this->data['mother_quota_amount'] = 1;
			$this->data['group_quota_amount'] = 1;
		}

		if(in_array(Auth::user()->type, ['sales_agent'])){
			$sales_agent_quota = Cluster::where('id',Auth::user()->cluster_id)->first()? : new Cluster;

			if($sales_agent_quota->area_quota > 0){
				$this->data['sales_agent_quota_amount'] = $sales_agent_quota->area_quota;
				$this->data['area'] = 'Area '.$sales_agent_quota->area.' - '.$sales_agent_quota->island.' | ';
			}else{
				$this->data['sales_agent_quota_amount'] = 1;
				$this->data['area'] = 'Area '.$sales_agent_quota->area.' - '.$sales_agent_quota->island.' | ';
			}
		}

	}

	public function set_special_product_focus(){
		$this->data['special_product_focus'] = Product::where('is_focus','yes')->get(); 

		// $quota = QuotaConfiguration::orderBy('created_at','DESC')->first();
		// if($quota){
		// 	$this->data['product_focus_amount'] = $quota->special_quota;
		// }else{
		// 	$this->data['product_focus_amount'] = 1;
		// }
		
	}

	public function set_group_quota(){
		$this->data['group_quota'] = Product::where('quota_type','group_quota')->get(); 
	}

	public function set_date_range(){
		$date_range = QuotaConfiguration::orderBy('created_at','DESC')->first();

		if(!$date_range){
			$this->data['date_range'] = date('F');
		}else{
			if($date_range->date_range == 'month'){
				$this->data['date_range'] = date('F');
			}else{
				$this->data['date_range'] = 'year '.date('Y');
			}
		}
	}

	public function set_access_control(){
		$this->data['module_request_form'] = AccessControl::where('module_name','request_form')->first();
		$this->data['module_physical_count'] = AccessControl::where('module_name','physical_count')->first();
		$this->data['module_quota'] = AccessControl::where('module_name','quota')->first();
		$this->data['module_clients'] = AccessControl::where('module_name','clients')->first();
		$this->data['module_product_returns'] = AccessControl::where('module_name','product_returns')->first();
		$this->data['module_clustering'] = AccessControl::where('module_name','clustering')->first();
		$this->data['module_reporting'] = AccessControl::where('module_name','reporting')->first();
		$this->data['module_products'] = AccessControl::where('module_name','products')->first();

		$this->data['modules'] = AccessControl::all();

		// $Date = "2010-09-17";
		// dd(date('Y-m-d', strtotime($Date. ' + 1 days')));
		// echo date('Y-m-d', strtotime($Date. ' + 2 days'));
	}
}