<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Inventory;
use App\Laravel\Models\ProductReturn;
use App\Laravel\Models\StockIn;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\InventoryRequest;
use App\Laravel\Requests\Backoffice\ProductReturnRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ProductReturnController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Product Returns";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "product_return";

		$this->data['stocks'] = [''=>"Choose a stock in transaction"] + StockIn::pluck('transaction_code','id')->toArray();

		$this->data['stock_in'] = StockIn::all();
	}

	public function index () {
		$this->data['product_returns'] = ProductReturn::orderBy('created_at',"DESC")->checkstock()->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}
	
	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}
	
	public function store (ProductReturnRequest $request) {
		try {
			$new_ProductReturn = new ProductReturn;
			$new_ProductReturn->fill($request->all());
			$stock_id = $request->stock_id;
			$qty = $request->qty;

			$stock = StockIn::find($stock_id);
			$stock->qty = $stock->qty - $qty;

			$inventory = Inventory::where('product_id',$stock->product_id)->first();
			$inventory->quantity = $inventory->quantity - $qty;

			if($new_ProductReturn->save() AND $stock->save() AND $inventory->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Product Return has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function excel(){
		try {
			$this->data['product_returns'] = ProductReturn::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Product Returns : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Product Returns', function($sheet) {
					$sheet->loadView('excel.product_returns', $this->data);
				});

			})->export($ext);

			// Session::flash('notification-status','success');
			// Session::flash('notification-msg','Export successful. See result below.');
			// return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}