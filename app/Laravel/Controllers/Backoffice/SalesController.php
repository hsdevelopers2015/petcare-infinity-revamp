<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Order;
use App\Laravel\Models\Sales;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\Inventory;
use App\Laravel\Models\SalesItem;
use App\Laravel\Models\SalesDiscount;
use App\Laravel\Models\TransactionLog;
use App\Laravel\Models\DeliveryReceipt;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SalesRequest;
use App\Laravel\Requests\Backoffice\AddItemRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\SalesConfigRequest;
use App\Laravel\Requests\Backoffice\UpdateClientRequest;
use App\Laravel\Requests\Backoffice\DeliveryReceiptRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF, Excel;

class SalesController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Sales";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "sales";
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();

		$this->data['modes'] = ['cash_on_delivery' => "Cash On Delivery", 'dated_checks'=>"Dated Checks",'post_dated_cheque'=>"Post Dated Cheque"];

		$this->data['terms'] = ['30_days' => "30 Days",'60_days' => "60 Days",'90_days' => "90 Days",'180_days' => "180 days",];

		$this->data['statuses'] = ['pending' => "Collectible",'partially_paid' => "Partially Paid",'fully_paid' => "Fully Paid"];

		$this->data['product_lists'] = Product::checkInventory()->get();

		if(in_array($this->data['auth']->type, ['sales_head','sales_agent'])){
			$clients = $this->data['auth']->get_clients($this->data['auth']->cluster_id);

			// dd($clients);

			if($this->data['auth']->type == 'sales_agent'){
				$this->data['clients'] = [''=>"Choose a client"] + User::select(DB::raw("id"),DB::raw("CONCAT_WS(' ',fname,lname,'-',(SELECT business_name FROM business_info WHERE user_id = `user`.`id` LIMIT 1)) AS client_name"))->whereIn('id',$clients)->where('cluster_id',$this->data['auth']->cluster_id)->pluck("client_name","id")->toArray();
			}else{
				$this->data['clients'] = [''=>"Choose a client"] + User::select(DB::raw("id"),DB::raw("CONCAT_WS(' ',fname,lname,'-',(SELECT business_name FROM business_info WHERE user_id = `user`.`id` LIMIT 1)) AS client_name"))->where('type','client')->pluck("client_name","id")->toArray();
			}

		}else{
			$this->data['clients'] = [''=>"Choose a client"] + User::select(DB::raw("id"),DB::raw("CONCAT_WS(' ',fname,lname,'-',(SELECT business_name FROM business_info WHERE user_id = `user`.`id` LIMIT 1)) AS client_name"))->where('type','client')->pluck("client_name","id")->toArray();
		}
		// dd($this->data['auth']->type);
		// dd($this->data['clients']);
		// $this->data['clients'] = [''=>"Choose a client"] + User::select(DB::raw("id"),DB::raw("CONCAT_WS(' ',fname,lname,'-',(SELECT business_name FROM business_info WHERE user_id = `user`.`id` LIMIT 1)) AS client_name"))->where('type','client')->pluck("client_name","id")->toArray();
	}

	public function index () {
		if(in_array($this->data['auth']->type, ['sales_head','admin','super_user','spectator','finance','auditor'])){
			$this->data['sales'] = Sales::orderBy('created_at',"DESC")->whereIn('payment_status',['partially_paid','fully_paid'])->checkuser()->get();
		}else{
			$this->data['sales'] = Sales::orderBy('created_at',"DESC")->whereIn('payment_status',['partially_paid','fully_paid'])->checkuser()->where('sa_id',$this->data['auth']->id)->get();
		}
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function collectibles () {
		$this->data['page_title'] = "Collectibles";
		if($this->data['auth']->type == 'sales_agent'){
			$this->data['sales'] = Sales::orderBy('created_at',"DESC")->where('payment_status','pending')->checkuser()->where('sa_id',$this->data['auth']->id)->get();
		}else{
			$this->data['sales'] = Sales::orderBy('created_at',"DESC")->where('payment_status','pending')->checkuser()->get();
		}
		return view('backoffice.'.$this->data['route_file'].'.collectibles',$this->data);
	}

	public function client(UpdateClientRequest $request,$id=NULL){
		$sales = Sales::find($id);

		$sales->client_id = $request->client_id;
		$sales->due_date = $request->due_date;

		if($sales->save()){
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"Successfully updated.");
			return redirect()->back();
		}

		Session::flash('notification-status',"failed");
		Session::flash('notification-msg',"Error.");
		return redirect()->back();
	}

	public function create () {
		$sales_count = str_pad(Sales::all()->count()+1,'4','0',STR_PAD_LEFT);
		$this->data['transaction_code'] = "S-".Helper::date_format(Carbon::now(),'ymd').'-'.$sales_count;
		$sales = new Sales;
		$sales->transaction_code = $this->data['transaction_code'];
		$sales->client_id = Input::get('client_id');
		$sales->sa_id = $this->data['auth']->id;
		$sales->due_day = Input::get('due_day');
		// $sales->due_date = Input::get('due_date');

		if($sales->save()){

			Helper::user_timeline($this->data['auth']->id,$sales->id,'sales','was created an invoice for '.$sales->client_info($sales->client_id)->fname.' '.$sales->client_info($sales->client_id)->lname.'.','create');

			Helper::transaction_log($this->data['auth']->id,$sales->id,'sales','draft','create','Created a sales transaction.');

			return redirect()->route('backoffice.'.$this->data['route_file'].'.add',[$this->data['transaction_code']]);
		}
	}

	public function invoice($id = NULL){
		$this->data['sales'] = Sales::where('id',$id)->first();
		$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
		$this->data['discounts'] = SalesDiscount::where('sales_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
		
		if (!$this->data['sales']) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$pdf = PDF::loadView('dompdf.invoice', $this->data);
		$pdf->setPaper('A5', 'portrait');
		return $pdf->stream('dompdf.invoice');
	}

	public function add($transaction_code = NULL ){
		$this->data['transaction_code'] = $transaction_code;
		$this->data['sales'] = Sales::where('transaction_code',$transaction_code)->first();
		$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->get();
		$this->data['discounts'] = SalesDiscount::where('sales_id',$this->data['sales']->id)->get();
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function save ($id = NULL) {
		$sales = Sales::find($id);
		$sales->status = "posted";

		$sales_item = SalesItem::where('transaction_id',$id)->get();

		if($sales_item->count() == 0){
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',"Please add an item first.");
			return redirect()->back();
		}

		$sales->sales_items($sales->id);

		if($sales->save()){

			Helper::user_timeline($this->data['auth']->id,$sales->id,'sales','was posted the invoice for '.$sales->client_info($sales->client_id)->fname.' '.$sales->client_info($sales->client_id)->lname.'.','post');

			Helper::transaction_log($this->data['auth']->id,$sales->id,'sales','posted','update','posted the invoice for '.$sales->client_info($sales->client_id)->fname.' '.$sales->client_info($sales->client_id)->lname);

			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}
	}

	public function store (AddItemRequest $request) {
		try {
			$new_item = new SalesItem;
			$new_item->fill($request->all());

			$total = $request->cost_unit * $request->qty;

			if($request->discount!=""){
				if($request->discount_type == "amount"){
					$final_cost = $total - $request->discount;
				}else{
					$final_cost = $total-($total*($request->discount/100));
				}
			}else{
				$final_cost = $total;
			}
			$new_item->final_cost = $final_cost;

			$sales = Sales::find($request->transaction_id);

			$sales->total_qty = $request->qty + $sales->total_qty;
			$sales->total_amount = $final_cost + $sales->total_amount;

			// $inventory = Inventory::find($request->product_id);

			// $inventory->quantity = $inventory->quantity - $request->qty;

			if($new_item->save() AND $sales->save() /*AND $inventory->save()*/) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New item has been added to sales.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function remove ($id = NULL) {
		try {
			$product = SalesItem::find($id);

			$sales = Sales::find($product->transaction_id);

			$sales->total_qty = $sales->total_qty - $product->qty;
			$sales->total_amount = $sales->total_amount - $product->final_cost;

			// $inventory = Inventory::find($product->product_id);

			// $inventory->quantity = $inventory->quantity + $product->qty;

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($product->delete() AND $sales->save() /*AND $inventory->save()*/) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An item has been removed.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = Sales::find($id);
			$product->status = "cancelled";

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($product->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A sales has been cancelled.");

				Helper::transaction_log($this->data['auth']->id,$product->id,'sales','cancelled','destroy','Cancel the invoice for '.$product->client_info($product->client_id)->fname.' '.$product->client_info($product->client_id)->lname);

				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function cancel ($id = NULL) {
		try {
			$sales = Sales::find($id);
			$sales->status = "posted_cancelled";		

			$items = SalesItem::where('transaction_id',$sales->id)->get();

			foreach ($items as $index => $info) {
				$product = Inventory::where('product_id',$info->product_id)->first();
				$total = $product->quantity + $info->qty + $info->free;
				$product->quantity = $total;

				$product->save();
			}

			if (!$sales) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($sales->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A sales has been deleted.");

				Helper::transaction_log($this->data['auth']->id,$sales->id,'sales','cancelled','destroy','Cancel the delivery for '.$sales->client_info($sales->client_id)->fname.' '.$sales->client_info($sales->client_id)->lname);

				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function mod ($id = NULL) {
		try {
			$update_mod = Sales::find($id);

			$update_mod->mode_of_payment = Input::get('mode_of_payment');
			// $update_mod->payment_term = Input::get('payment_term');

			if (!$update_mod) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($update_mod->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A sales has been updated.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function ups ($id = NULL) {
		try {
			$update_ups = Sales::find($id);

			$update_ups->payment_status = Input::get('payment_status');
			$total_amount = $update_ups->total_amount;
			$update_ups->balance = $total_amount - Input::get('balance');

			if (!$update_ups) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($update_ups->save()) {

				Helper::user_timeline($this->data['auth']->id,$update_ups->id,'sales','was updated the payment status of invoice '.$update_ups->transaction_code.' to '.str_replace('_', ' ', $update_ups->payment_status).' for '.$update_ups->client_info($update_ups->client_id)->fname.' '.$update_ups->client_info($update_ups->client_id)->lname.'.','update');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A sales has been updated.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function add_discount($id = NULL){
		try {
			$new_discount = new SalesDiscount;

			$new_discount->fill(Input::all());

			if($new_discount->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','Discount successfully added.');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function remove_discount($id = NULL){
		try {
			$sales_discount = SalesDiscount::find($id);

			if (!$sales_discount) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($sales_discount->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A discount has been removed.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function delivery_receipt($id = NULL){
		$this->data['page_title'] = "Delivery Receipt";
		$dr = DeliveryReceipt::where('reference_id',$id)->where('reference','sales')->first();
		$sales = Sales::where('id',$id)->first();

		// dd($sales);

		if(!$dr){
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->back();
		}

		$this->data['delivery_receipt'] = $dr;
		$this->data['sales'] = $sales;
		return view('backoffice.'.$this->data['route_file'].'.delivery_receipt',$this->data);
	}

	public function create_delivery_receipt(DeliveryReceiptRequest $request){
		try {
			$new_dr = new DeliveryReceipt;

			$new_dr->fill($request->all());

			if($new_dr->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A delivery receipt successfully created.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.dr',$new_dr->reference_id);
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update_delivery_receipt(DeliveryReceiptRequest $request, $id = NULL){
		$dr = DeliveryReceipt::find($id);

		$dr->fill($request->all());
		// dd($request->approved_by);
		// $dr->approved_by = $request->approved_by;

		if($dr->save()){
			Session::flash('notification-status',"success");
			Session::flash('notification-msg',"Delivery Receipt successfully updated.");
			return redirect()->back();
		}
	}

	public function delivery_receipt_form($id = NULL){
		$delivery_receipt = DeliveryReceipt::find($id);
		$reference = $delivery_receipt->reference;
		$reference_id = $delivery_receipt->reference_id;

		$this->data['delivery_receipt'] = $delivery_receipt;

		// dd('im here');

		if (!$delivery_receipt) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		if($reference == 'sales'){
			$this->data['sales'] = Sales::find($reference_id);
			$this->data['items'] = SalesItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
			$pdf = PDF::loadView('dompdf.sales_delivery_receipt', $this->data);
			$pdf->setPaper('A5', 'portrait');
			return $pdf->stream('dompdf.sales_delivery_receipt');
		}else{
			$this->data['sales'] = Order::find($reference_id);
			$this->data['items'] = OrderItem::where('transaction_id',$this->data['sales']->id)->orderBy('updated_at','DESC')->get();
			$pdf = PDF::loadView('dompdf.order_delivery_receipt', $this->data);
			return $pdf->stream('dompdf.order_delivery_receipt');
		}

	}

	public function edit($id = NULL){
		$sales = Sales::find($id);

		if($sales){
			$this->data['sales'] = $sales;
			return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
		}
	}

	public function update(SalesConfigRequest $request, $id = NULL){
		$sales = Sales::find($id);

		$sales->fill($request->all());

		if($sales->save()){
			Session::flash('notification-status',"success");
			Session::flash('notification-msg',"Sales Configuration was successfully updated.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}


		Session::flash('notification-status',"failed");
		Session::flash('notification-msg',"Record not found.");
		return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
	}

	public function history($id){
		$this->data['sales'] = Sales::find($id);
		$this->data['transactions'] = TransactionLog::where('reference_id',$id)->orderBy('created_at','DESC')->get();

		return view('backoffice.sales.history',$this->data);
	}

	public function excel(){
		try {
			$type = Input::get('type');
			if ($type == "sales") {
				if(in_array($this->data['auth']->type, ['sales_head','admin','super_user','spectator','finance','auditor'])){
					$this->data['sales'] = Sales::orderBy('created_at',"DESC")->whereIn('payment_status',['partially_paid','fully_paid'])->checkuser()->get();
				}else{
					$this->data['sales'] = Sales::orderBy('created_at',"DESC")->whereIn('payment_status',['partially_paid','fully_paid'])->checkuser()->where('sa_id',$this->data['auth']->id)->get();
				}
			}else{
				if(in_array($this->data['auth']->type, ['sales_head','admin','super_user','spectator','finance','auditor'])){
					$this->data['sales'] = Sales::orderBy('created_at',"DESC")->where('payment_status','pending')->checkuser()->get();
				}else{
					$this->data['sales'] = Sales::orderBy('created_at',"DESC")->where('payment_status','pending')->checkuser()->where('sa_id',$this->data['auth']->id)->get();
				}
			}

			$ext = "xls";

			$filename = "Sales : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Sales', function($sheet) {
					$sheet->loadView('excel.sales', $this->data);
				});

			})->export($ext);

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}


	}

}