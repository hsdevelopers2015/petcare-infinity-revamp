<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Models\Product;
use App\Laravel\Models\Brand;
use App\Laravel\Models\TransactionDetail;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Transformers\ProductTransformer;
use App\Laravel\Transformers\BrandTransformer;
use App\Laravel\Transformers\TransactionHeaderTransformer;
use App\Laravel\Transformers\TransformerManager;

class AjaxController extends Controller
{
	public function __construct()
	{
		$this->transformer = new TransformerManager;
	}

	public function transaction()
	{
		$transaction_code = request('transaction_code');

		$transaction = TransactionHeader::with('details.brand', 'details.supplier', 'details.prod')
							->where('transaction_code', $transaction_code)
							->first();

			
		
		// if(!$transaction) {
		// 	return response()->json('No records'); 
		// }				

		if(!$transaction) {
			return response()->json([
				'error' => "Transaction not found."
			], 404);
		}

		$payload = [
			'data' => $this->transformer->transform($transaction, new TransactionHeaderTransformer, 'item')
		];


		return response()->json($payload, 200);
	}

	public function products()
	{
		
		$transaction_code = request('transaction_code');
		$brand_code = request('brand_code');

		$details = TransactionDetail::where('transaction_code', $transaction_code)
							->where('brand_code', $brand_code)
							->get();

		$codes = $details->pluck('product_code');
		$products = Product::whereIn('product_code', $codes)->get();

		$payload = [
			'data' => $this->transformer->transform($products, new ProductTransformer, 'collection')
		];


		return response()->json($payload, 200);
	}

	public function brands()
	{
		$transaction_code = request('transaction_code');
		$supplier_code = request('supplier_code');

		$details = TransactionDetail::where('transaction_code', $transaction_code)
							->where('supplier_code', $supplier_code)
							->get();

		$codes = $details->pluck('brand_code');
		$brands = Brand::whereIn('brand_code', $codes)->get();

		$payload = [
			'data' => $this->transformer->transform($brands, new BrandTransformer, 'collection')
		];

		return response()->json($payload, 200);
	}
}