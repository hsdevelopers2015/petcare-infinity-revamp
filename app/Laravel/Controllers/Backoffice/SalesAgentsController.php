<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Cluster;
// use App\User;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Input;

class SalesAgentsController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Sales Agents";
	}

	public function index () {
		$type = Input::get('type');
		if($type){
			$this->data['type'] = $type;
			$this->data['clusters'] = Cluster::orderBy('area')->get();
			$this->data['sales_agents'] = User::whereIn('type',['sales_agent'])->orderBy('area')->get();
		}else{
			$this->data['type'] = 'all';
			$this->data['clusters'] = Cluster::orderBy('area')->get();
			$this->data['sales_agents'] = User::whereIn('type',['sales_agent'])->orderBy('area')->get();
		}
		return view('backoffice.sales_agents.index',$this->data);
	}

}