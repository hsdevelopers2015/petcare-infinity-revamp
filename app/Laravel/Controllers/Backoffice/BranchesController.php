<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\Branch;
use App\Laravel\Models\Clients;


use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\PurchaseEntryHeader;
use App\Laravel\Models\PurchaseEntryDetail;
use App\Laravel\Models\PurchaseReceivedHeader;
use App\Laravel\Models\PurchaseReceivedDetail;
use App\Laravel\Models\Measurement;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;

/*
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\BranchesRequest;

/*
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Helper, Carbon, Session, Str, DB, File, Image, Excel;

class BranchesController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Branches";
		$this->data['route_file'] = "branches";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";

		$this->data['clients'] = [''=>"Choose a client"] + User::select(DB::raw("id"),DB::raw("CONCAT_WS(' ',fname,lname,'-',(SELECT business_name FROM business_info WHERE user_id = `user`.`id` LIMIT 1)) AS client_name"))->where('type','client')->pluck("client_name","id")->toArray();
		$this->data['clients'] =[''=>'-Please select clients-']+Clients::orderBy('created_at','DESC')->pluck('name','id')->toArray();
	}

	public function index () {
		$this->data['branches'] = Branch::orderBy('updated_at','DESC')->get();
		return view('backoffice.branches.index',$this->data);
	}

	public function show($branch_code = NULL){
		$this->data['branch_information'] = "Branch Information";
		$this->data['branch'] = Branch::where('branch_code',$branch_code)->first() ?: new Branch;
		// $this->data['supplier'] = "Supplier Information";
		// $this->data['stock_tranfer_item_history'] = "Stock Transafer Item History";
		$this->data['stock_received'] = "Stock Received";
		$this->data['sales_received'] = "Sales History";
		$this->data['sales_return'] = "Sales Return";
		$this->data['stock_tranfer'] = "Stock Transfer Item History";
		// $this->data['stock_transder_out_history'] = "Stock Transfer Out Item History";
		// $this->data['purchase_return_item_history'] = "Purchase Return Item History";
		// $this->data['products'] = Product::where('product_code',$product_code)->first();
		// $this->data['suppliers'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		// ->groupBy('supplier_code')
		// ->get();
		// $this->data['purchased_item_detail'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','purchase')->get();
		$this->data['stock_item_received'] = TransactionDetail::where('to',$branch_code)
		->orwhere('account_type','stock_transfer')
		->orwhere('stock_trasfer_in','yes')
		->where('account_type','purchase')
		->get();
		
		

		$this->data['stock_item_received_count'] = TransactionDetail::where('to',$branch_code)
		->orwhere('account_type','stock_transfer')
		->orwhere('stock_trasfer_in','yes')
		->where('account_type','purchase')
		->sum('quantity');


		$this->data['stock_item_received_sum'] = TransactionDetail::where('to',$branch_code)
		->orwhere('account_type','stock_transfer')
		->orwhere('stock_trasfer_in','yes')
		->where('account_type','purchase')
		->sum('selling');


		$this->data['stock_item_transfer'] = TransactionDetail::where('to',$branch_code)
		->where('account_type','stock_transfer')
		->where('stock_trasfer_in','yes')
		->get();


		$this->data['stock_tranfer_item_history'] = TransactionDetail::where('to',$branch_code)
		->where('account_type','stock_transfer')
		->get();

		// $this->data['purchase_return_history'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','purchase_return')->get();
		// $this->data['total_qty_po_return'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		// ->where('account_type','purchase_return')
		// ->where('status','approved')
		// ->sum('quantity');
		// $this->data['total_amount_po_return'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		// ->where('account_type','purchase_return')
		// ->where('status','approved')
		// ->sum('selling');



		$this->data['total_qty_stock_transfer'] = TransactionDetail::where('to',$branch_code)->where('account_type','stock_transfer')->sum('quantity');
		$this->data['total_sell_stock_transfer'] = TransactionDetail::where('to',$branch_code)->where('account_type','stock_transfer')->sum('selling');

		// $this->data['totalQty'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		// ->where('account_type','purchase')
		// ->sum('quantity');
		// $this->data['totalSelling'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->sum('selling');

		return view('backoffice.'.$this->data['route_file'].'.show',$this->data);

	}


	public function ajax_detail($client_id =  NULL){
		if ($client_id = Clients::select('business_name','business_address','email','contact_no')->where("id",$client_id)->first()) {
		    return response()->json($client_id);
		}else{
		   return response()->json(""); 
		}
	}




	public function create(){
		
		$type = "BR";
		$current_sequence = Branch::orderBy('branch_code','DESC')->where('branch_code','like',$type.'%')->first();
		$initial_counter = 0000000;
		if(!$current_sequence){		
			$current_sequence = Branch::orderBy('branch_code',"DESC")->where('branch_code','like',$type.'%')->first();			
			$this->data['sequence'] = $type.str_pad(($initial_counter)+1, 7, "0", STR_PAD_LEFT);
		}else{
			$var = trim($current_sequence->branch_code,$type);			
			$this->data['sequence'] = $type.str_pad(($var)+1, 7, "0", STR_PAD_LEFT);
		}
		return view('backoffice.branches.create',$this->data);
	}

	public function store(BranchesRequest $request){
		try {
			$new_branch = new Branch;

			$new_branch->fill($request->all());		

			if($new_branch->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New branch has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}


		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit($id =  NULL){
		$branch = Branch::find($id);
		if (!$branch) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['branch'] = $branch;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update(BranchesRequest $request,$id =  NULL){
		try {
			$branch = Branch::find($id);
			if (!$branch) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$branch->fill($request->all());
						
			if($branch->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A branch has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}