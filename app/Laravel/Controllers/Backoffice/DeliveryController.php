<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Sales;
use App\Laravel\Models\Order;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ClientRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\EditRemarksClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class DeliveryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Deliveries";
		$this->data['route_file'] = "deliveries";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
	}

	public function index () {
		$order = Order::where('status','shipped')->get();
		$sales = Sales::where('status','for_delivery')->where('sa_id',$this->data['auth']->id)->get();

		$this->data['orders'] = $order;
		$this->data['sales'] = $sales;

		return view('backoffice.deliveries.index',$this->data);

	}

	public function received($type = NULL, $id = NULL){
		if($type == 'sales'){
			$sales = Sales::find($id);
			$sales->status = 'received';

			if($sales->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"The client was already received their product.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
		}else{
			$order = Order::find($id);
			$order->status = 'received';

			if($order->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"The client was already received their product.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
		}
	}

	public function excel(){
		try {
			$this->data['deliveries'] = Sales::where('status','for_delivery')->orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Deliveries : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Deliveries', function($sheet) {
					$sheet->loadView('excel.deliveries', $this->data);
				});

			})->export($ext);

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}