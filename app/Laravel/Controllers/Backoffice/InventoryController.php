<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\PurchaseEntryHeader;
use App\Laravel\Models\PurchaseEntryDetail;
use App\Laravel\Models\PurchaseReceivedHeader;
use App\Laravel\Models\PurchaseReceivedDetail;
use App\Laravel\Models\Measurement;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CategoryRequest;
use App\Laravel\Requests\Backoffice\SupplierRequest;
use App\Laravel\Requests\Backoffice\PurchaseHeaderEntryRequest;
use App\Laravel\Requests\Backoffice\PurchaseHeaderReceivedRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class InventoryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Inventory";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "inventory";
		$this->data['supplier_code'] = [''=>'-Supplier Code-']+Supplier::pluck('supplier_name','supplier_code')->toArray();
		$this->data['product_code'] = [''=>'-Product Code-']+Product::pluck('product_name','product_code')->toArray();
		$this->data['purchase_code'] = [''=>'-Purchased Code-']+PurchaseEntryHeader::pluck('purchase_code','purchase_code')->toArray();
		$this->data['measurement'] = [''=>'-Measurement Code-']+Measurement::pluck('description','code')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();/*Inventory::where('quantity','>',0)
																	->pluck('id','id')->toArray();*/
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];

	}

	public function index () {
		$this->data['products'] = Product::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function show($product_code = NULL){
		$this->data['product_information'] = "Product Stock Information";
		$this->data['supplier'] = "Supplier Information";
		$this->data['purchased_item_history'] = "Purchased Item History";
		$this->data['stock_transfer_in_out_history'] = "Stock Transfer In/Out Item History";
		$this->data['stock_transder_out_history'] = "Stock Transfer Out Item History";
		$this->data['purchase_return_item_history'] = "Purchase Return Item History";
		$this->data['products'] = Product::where('product_code',$product_code)->first();
		$this->data['suppliers'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		->groupBy('supplier_code')
		->get();
		$this->data['purchased_item_detail'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','purchase')->get();
		$this->data['transaction_item_detail'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','stock_transfer')->get();

		$this->data['purchase_return_history'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','purchase_return')->get();
		$this->data['total_qty_po_return'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		->where('account_type','purchase_return')
		->where('status','approved')
		->sum('quantity');
		$this->data['total_amount_po_return'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		->where('account_type','purchase_return')
		->where('status','approved')
		->sum('selling');



		$this->data['total_qty_stock_transfer'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','stock_transfer')->sum('quantity');
		$this->data['total_sell_stock_transfer'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->where('account_type','stock_transfer')->sum('selling');

		$this->data['totalQty'] = TransactionDetail::where('product_code',$this->data['products']->product_code)
		->where('account_type','purchase')
		->sum('quantity');
		$this->data['totalSelling'] = TransactionDetail::where('product_code',$this->data['products']->product_code)->sum('selling');
		

		
		return view('backoffice.'.$this->data['route_file'].'.show',$this->data);
	}

	public function create () {
		dd('inventory create');
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function stock_info ($id = NULL) {
		$this->data['page_title'] = "Product Stock Information";
		$this->data['stock_in'] = StockIn::orderBy('created_at',"DESC")->where('product_id',$id)->where('status','posted')->get();
		return view('backoffice.'.$this->data['route_file'].'.stock_info',$this->data);
	}

	public function store (InventoryRequest $request) {
		try {
			$new_product = new Inventory;
			$check_product = Inventory::where('product_name',$request->product_name)->first();
			$new_product->fill($request->all());

			if($check_product){
				$new_product = Inventory::where('product_name',$request->product_name)->first();
				$new_product->quantity = $check_product->quantity + $request->quantity;
			}

			if($new_product->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New product has been added to the inventory.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		dd('edit bes');
		$product = Inventory::find($id);

		if (!$product) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['product'] = $product;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (InventoryRequest $request, $id = NULL) {
		try {
			$product = Inventory::find($id);
			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$product->fill($request->all());

			if($product->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = Inventory::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($product->save() AND $product->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/client/business_establishment"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function excel(){
		try {
			$this->data['physical_count'] = Inventory::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Inventory Physical Count : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Inventory Physical Count', function($sheet) {

					$sheet->loadView('excel.physical_count', $this->data);

				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}