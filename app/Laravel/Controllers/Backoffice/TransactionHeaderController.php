<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\PurchaseEntryHeader;
use App\Laravel\Models\PurchaseEntryDetail;
use App\Laravel\Models\PurchaseReceivedHeader;
use App\Laravel\Models\PurchaseReceivedDetail;
use App\Laravel\Models\Measurement;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;
use App\Laravel\Models\Brand;
use App\Laravel\Models\Branch;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CategoryRequest;
use App\Laravel\Requests\Backoffice\SupplierRequest;
use App\Laravel\Requests\Backoffice\PurchaseHeaderEntryRequest;
use App\Laravel\Requests\Backoffice\PurchaseHeaderReceivedRequest;
use App\Laravel\Requests\Backoffice\TransactionHeaderRequest;


/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF;

class TransactionHeaderController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Purchase Order Form";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "transaction_hdr";
		$this->data['supplier_code'] = [''=>'-Supplier-','warehouse' => 'Warehouse']+Supplier::pluck('supplier_name','supplier_code')->toArray();
		$this->data['product_code'] = [''=>'-Product Code-']+Product::pluck('product_name','product_code')->toArray();
		$this->data['brand_code'] = [''=>'-Brand Code-']+Brand::pluck('brand_name','brand_code')->toArray();
		$this->data['purchase_code'] = [''=>'-Purchased Code-']+PurchaseEntryHeader::pluck('purchase_code','purchase_code')->toArray();
		$this->data['measurement'] = [''=>'-Measurement Code-']+Measurement::pluck('description','code')->toArray();

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    		->pluck('product_name', 'id')->toArray();
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
		$this->data['branches'] = [''=>'-Select branch-','warehouse'=> 'Warehouse']+Branch::pluck('branch_name','branch_code')->toArray();
	}
	public function create(){
		$type = "PO";
		$current_sequence = TransactionHeader::orderBy('transaction_code','DESC')->where('transaction_code','like',$type.'%')->first();
		$initial_counter = 0000000;
		if(!$current_sequence){		
			$current_sequence = TransactionHeader::orderBy('transaction_code',"DESC")->where('transaction_code','like',$type.'%')->first();			
			$this->data['sequence'] = $type.str_pad(($initial_counter)+1, 7, "0", STR_PAD_LEFT);
		}else{
			$var = trim($current_sequence->transaction_code,$type);			
			$this->data['sequence'] = $type.str_pad(($var)+1, 7, "0", STR_PAD_LEFT);
		}
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function index () {

		$this->data['transaction_hdr'] = TransactionHeader::orderBy('created_at',"DESC")->orwhere('status','pending')
		->orwhere('status','approved')
		->get() ? : new TransactionHeader;
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (TransactionHeaderRequest $request) {
		try {

			$supplier_code = Input::get('supplier_code');
			$transaction_code = Input::get('transaction_code');
			$description = Input::get('description');
			$to = Input::get('to');
			// $invoice_number = Input::get('invoice_number');				

			$transaction_hdr = new TransactionHeader;
			$transaction_hdr->supplier_code = $supplier_code;
			$transaction_hdr->from = $supplier_code;
			$transaction_hdr->to = $to;
			$transaction_hdr->account_type = "purchased";
			$transaction_hdr->transaction_type = "inwards";
			$transaction_hdr->transaction_code = $transaction_code;
			$transaction_hdr->description = $description;
			
			$brand_code = $request->get('brand_code') ?: array();			
			$product_code = $request->get('product_code') ?: array();
			$qty = $request->get('qty') ?: array();
			$measurement_code = $request->get('measurement') ?: array();
			$cost = $request->get('cost') ?: array();
			$selling = $request->get('selling') ?: array();
			$expiration_date = $request->get('expiration_date') ?: array();
	
			
			if(count($product_code) == count($qty) && 
			    count($qty) == count($product_code)
			) {
			    for ($i=0; $i < count($product_code); $i++) { 
			    	
			    	
			        $transaction_dtl = new TransactionDetail;                      
			        $transaction_dtl->fill([
			            
			            'transaction_code' => $transaction_code,
			            'from' => $supplier_code,
			            'to' => $to,
			            'account_type' => "purchase",
			            'supplier_code' => $supplier_code,
			            'brand_code' => $brand_code[$i],			            
			            'product_code' => $product_code[$i],
			            'product_name' => "product name",
			            'transaction_type' => "inwards",
			            'unit' => "unit",
			            'quantity' => $qty[$i],
			            'cost' => $cost[$i],
			            'selling' => $selling[$i],
			            'expiration_date' => $expiration_date[$i]                 
			        ]);
			        $transaction_dtl->save();
			    }
			}

			if($transaction_hdr->save()) {
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New category has been added.");
					dd('saving na bes');
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}	
	public function show($transaction_code =  NULL){
		$transaction_hdr = TransactionHeader::where('transaction_code',$transaction_code)->first();
		$this->data['totalQty'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('quantity');
		$this->data['totalCost'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('cost');
		$this->data['totalSelling'] = TransactionDetail::where('transaction_code',$transaction_code)->sum('selling');
		if (!$transaction_hdr) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['transaction_hdr'] = $transaction_hdr;
		return view('backoffice.'.$this->data['route_file'].'.show',$this->data);
	}

	public function approved($transaction_code = NULL){
		try {						

			$transaction_hdr = TransactionHeader::where('transaction_code',$transaction_code)->first();
			$transaction_hdr->status = "approved";

			$transaction_dtl = TransactionDetail::where('transaction_code',$transaction_code)->get();
			foreach($transaction_dtl as $index => $value){
			  $value = TransactionDetail::find($value->id);
			  $value->status = "approved";
			  $value->save();
			}
			
			
			if($transaction_hdr->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {

		$transaction_hdr = TransactionHeader::find($id);
		if (!$transaction_hdr) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['transaction_hdr'] = $transaction_hdr;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (TransactionHeaderRequest $request, $id = NULL) {
		try {
			$supplier_code = Input::get('supplier_code');
			$description = Input::get('description');
			$transaction_code = Input::get('transaction_code');

			$transaction_hdr = TransactionHeader::find($id);
			$transaction_hdr->supplier_code = $supplier_code;
			$transaction_hdr->account_type = "purchased";
			$transaction_hdr->transaction_type = "inwards";
			$transaction_hdr->transaction_code = $transaction_code;
			$transaction_hdr->description = $description;



			 $brand_code = $request->get('brand_code') ?: array();			
			 $product_code = $request->get('product_code') ?: array();
			 $quantity = $request->get('quantity') ?: array();
			 $cost = $request->get('cost') ?: array();
			 $selling = $request->get('selling') ?: array();
			 $expiration_date = $request->get('expiration_date') ?: array();

			 $transaction_dtl = TransactionDetail::where('transaction_code',$transaction_code)->get();
			 foreach($transaction_dtl as $index => $value){
			   $value = TransactionDetail::find($value->id);
			   $value->delete();
			 }			

			 	if(count($product_code)) {				
				    for ($i=0; $i < count($product_code); $i++) { 				    	
				        $transaction_dtl = new TransactionDetail;                      
				        $transaction_dtl->fill([
				            
				            'transaction_code' => $transaction_code,
				            'from' => "supplier",
				            'to' => "warehouse",
				            'account_type' => "purchase",
				            'supplier_code' => $supplier_code,
				            'brand_code' => $brand_code[$i],			            
				            'product_code' => $product_code[$i],
				            'product_name' => "product name",
				            'transaction_type' => "inwards",
				            'unit' => "unit",
				            'quantity' => $quantity[$i],
				            'cost' => $cost[$i],
				            'selling' => $selling[$i],
				            'expiration_date' => $expiration_date[$i]                 
				        ]);
				        $transaction_dtl->save();
				    }
				}			
			
			if($transaction_hdr->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function destroy ($id = NULL) {
		try {
			$supplier = Supplier::find($id);

			if (!$supplier) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($supplier->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function cost_price($product_code =  NULL){
		if ($cost_price = Product::select('cost','selling')->where("product_code",$product_code)->first()) {
		    return response()->json($cost_price);
		}else{
		   return response()->json('0.00'); 
		}
	}
}