<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\Sales;
use App\Laravel\Models\Expense;
use App\Laravel\Models\ExpensesItem;
use App\Laravel\Models\User;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Input, Excel;

class ReportingController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['page_title'] = "Sales Report";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "sales";

		$this->data['sa_ids'] = ['' => "Choose a sales agent"] + User::select(
            													DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->
																whereIn('type',['sales_head','sales_agent'])->pluck('name','id')
																->toArray();

		$this->data['statuses'] = ['received' => 'Received','posted' => 'Posted', 'cancelled' => 'Cancelled', 'draft' => 'Draft', 'posted_cancelled' => 'Cancelled Post', 'for_delivery' => 'For Delivery'];
		$this->data['payment_statuses'] = ['fully_paid' => 'Fully Paid','partially_paid' => 'Partially Paid', 'pending' => 'Collectibles'];
	}

	private function __init_xls(){

		$header = [
			'DATE',
			'PURPOSE',
			'INV.NO.',
			'VAT/TIN NUMBER',
			'AMOUNT NET OF VAT',
			'VAT INPUT TAX',
			'TOTAL AMOUNT',
       	];

   //     	$tips = [
   //     		'',
   //     		'Unique Code',
			// 'Category Reference ID',
			// '',
			// // 'Slug',
			// '',
			// // 'Description',
			// 'Any text separated by comma',
			// // 'In Stock',
			// '[ 1 / 0 ]',
			// '[ 1 / 0 ]',
   //     	];

		$this->data['header'] = $header;
		// $this->data['tips'] = $tips;
	}

	public function sales () {
		$from = Input::get('from');
		$to = Input::get('to');
		$status = Input::get('status','received');
		$payment_status = Input::get('payment_status');

		if($status){
			$this->data['status'] = $status; 			
		}else{
			$this->data['status'] = 'received'; 
		}

		if($payment_status){
			$this->data['payment_status'] = $payment_status; 			
		}else{
			$this->data['payment_status'] = "fully_paid"; 
		}

		$this->data['from'] = $from;
		$this->data['to'] = $to;

		// dd($this->data['from']);

		if($from AND $to){
			$this->data['sales'] = Sales::whereBetween('updated_at',array(Helper::date_format($from,'Y-m-d'),Helper::date_format($to,'Y-m-d')))
									// ->where('updated_at','>=',$from)
									->whereIn('payment_status',['fully_paid','partially_paid'])
									->where('status',$this->data['status'])
									->get();
		}else{
			$this->data['sales'] = Sales::whereIn('payment_status',['fully_paid','partially_paid'])->where('status',$this->data['status'])->get();
		}

		$total_gross = [];
		foreach($this->data['sales'] as $index => $info){
			array_push($total_gross,$info->total_amount);
		}

		$this->data['total_gross'] = array_sum($total_gross);
		return view('backoffice.reporting.sales',$this->data);
	}

	public function expenses(){

		$this->data['page_title'] = "Expenses Report";
		// $from = Input::get('from');
		// $to = Input::get('to');
		$sa_id = Input::get('sa_id');

		// dd(Input::all());

		$this->data['from'] = /*Helper::date_format($from,'y-m-d')*/"";
		$this->data['to'] = /*Helper::date_format($to,'y-m-d')*/"";
		$this->data['sa_id'] = $sa_id;

		// if($from OR $to OR $sa_id){
		// 	if($sa_id){
		// 		$this->data['expenses'] = Expense::where('updated_at','<=',$to)
		// 		->where('updated_at','>=',$from)
		// 		->where('sa_id',$sa_id)
		// 		->where('status','approved')
		// 		->get();
		// 	}else{
		// 		$this->data['expenses'] = Expense::where('updated_at','<=',$to)
		// 		->where('updated_at','>=',$from)
		// 		->where('status','approved')
		// 		->get();
		// 	}
		// }else{
		// 	$this->data['expenses'] = Expense::where('status','approved')->get();
		// }


		if(/*$from OR $to OR*/ $sa_id){
			if($sa_id){
				$this->data['expenses'] = Expense::/*where('updated_at','<=',$to)
				->where('updated_at','>=',$from)*/
				where('sa_id',$sa_id)
				->where('status','approved')
				->get();
			}/*else{
				$this->data['expenses'] = Expense::where('updated_at','<=',$to)
				->where('updated_at','>=',$from)
				->where('status','approved')
				->get();
			}*/
		}else{
			$this->data['expenses'] = Expense::where('status','approved')->get();
		}

		$total_expenses = [];
		foreach($this->data['expenses'] as $index => $info){
			array_push($total_expenses,$info->total_amount);
		}

		$this->data['total_expenses'] = array_sum($total_expenses);

		return view('backoffice.reporting.expenses',$this->data);
	}

	public function export_expenses() {
		try {
			$this->__init_xls();

			$expenses = Expense::where('status','approved')->get();

			$ids = [];

			foreach ($expenses as $key => $value) {
				array_push($ids, $value->id);
			}

			$this->data['expenses'] = ExpensesItem::orderBy('created_at')->whereIn('expense_id',$ids)->get();

			// dd($this->data['expenses']);

			$ext = "xls";
			$filename = Helper::create_filename($ext, true);
			// $path = "uploads/excel/license" . Carbon::now()->format("YmdHis");

			Excel::create($filename, function($excel) {

				$excel->setTitle("Expenses Exported Data")
				->setCreator('HS Backend Team')
				->setCompany('Highly Succeed Inc.');

				$excel->sheet('Expenses', function($sheet) {
					$sheet->freezeFirstRow();
					$sheet->setOrientation('landscape');
					$sheet->row(1,$this->data['header']);
					// $sheet->row(2,$this->data['tips']);


					$counter = 2;

					// foreach($this->data['results'] as $result) {
					// 	$sheet->row($counter++, [
					// 		$result->id, $result->code, $result->category_id, $result->title, $result->price, $result->tags, $result->is_featured, $result->best_seller
					// 	]);
					// }

					foreach ($this->data['expenses'] as $index => $info) {
						$sheet->row($counter++, [
							Helper::date_format($info->created_at,'F d, Y'),
							$info->description,
							$info->invoice_no,
							$info->tin_number,
							$info->net_amount,
							$info->vat,
							$info->amount,
						]);
					}
				});

			})->export($ext);

			// $request->flash();
			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}
}