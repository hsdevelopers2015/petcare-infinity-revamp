<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Cluster;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\Expense;
use App\Laravel\Models\RequestExpense;
use App\Laravel\Models\ExpensesItem;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ExpenseRequest;
use App\Laravel\Requests\Backoffice\RequestExpenseRequest;
use App\Laravel\Requests\Backoffice\EditExpenseRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ExpensesController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Expenses";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "expenses";

		$this->data['clusters'] = Expense::all();

		$this->data['types'] = ['liquidation'=>"Liquidation",'reimbursement'=>"Reimbursement"];

		$count = RequestExpense::all()->count()+1;

		$this->data['request_code'] = "REQUEST-".Helper::date_format(Carbon::now(),'ymd')."-".$this->data['auth']->id.str_pad($count,3,"0",STR_PAD_LEFT);


		$this->data['request_list'] = [''=>"Choose a request form"] + RequestExpense::orderBy('created_at',"DESC")/*->requestExpense()*/->where('sa_id',$this->data['auth']->id)->where('status','approved')->pluck('request_code','id')->toArray();

		$this->data['expense_types'] = ['' => "Choose a type", 'odometer' => "Odometer", 'fuel' => "Fuel", 'toll_fees' => "Toll Fees", 'parking_fees' => "Parking Fee", 'car_maintenance' => "Car Maintenance", 'public_relations' => "Public Relations", 'others' => "Others"];
		$this->data['expense_sub_types'] = ['' => "Choose a type", 'liters' => "Liters", 'amount' => "Amount"];

	}

	public function index () {
		if(in_array($this->data['auth']->type, ['sales_agent','sales_head','admin','super_user'])){
			$this->data['expenses'] = Expense::orderBy('created_at',"DESC")->where('sa_id',$this->data['auth']->id)->get();
			$this->data['requests'] = RequestExpense::orderBy('created_at',"DESC")->where('sa_id',$this->data['auth']->id)->get();
		}else{
			$this->data['expenses'] = Expense::orderBy('created_at',"DESC")->whereIn('status',['submitted','denied','approved'])->get();
			$this->data['requests'] = RequestExpense::orderBy('created_at',"DESC")->get();
		}
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create(RequestExpenseRequest $request){
		try {
			$new_request = new RequestExpense;
			$new_request->fill($request->all());

			$new_request->sa_id = $this->data['auth']->id;

			if($new_request->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','A request has been created.');
				return redirect()->back();
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create_bak () {
		try {
			$new_expense = new Expense;
			$new_expense->request_id = Input::get('request_id');

			if( Input::get('type') == "liquidation"){

				$new_expense->type = Input::get('type');
				$new_expense->sa_id = $this->data['auth']->id;
				$new_expense->requested_amount = Input::get('requested_amount');

			}else{

				$new_expense->type = Input::get('type');
				$new_expense->sa_id = $this->data['auth']->id;

			}

			if($new_expense->save()){
				return redirect()->route('backoffice.'.$this->data['route_file'].'.add',[$new_expense->id]);
			}

		}catch(Exception $e){
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function add($id){
		$expense = Expense::find($id);
		
		$this->data['expense'] = $expense;
		$this->data['items'] = ExpensesItem::where('expense_id',$id)->get();

		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (ExpenseRequest $request,$id = NULL) {
		try {
			$new_expense = new ExpensesItem;
			$new_expense->fill($request->all());
			$new_expense->expense_id = $id;

			if($request->type == 'others'){
				$new_expense->type = $request->remarks;
			}

			$expense = Expense::find($id); 

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_expense->directory = $upload["directory"];
				$new_expense->filename = $upload["filename"];
			}


			$expense->total_amount = $expense->total_amount + $new_expense->amount;

			if($new_expense->save() AND $expense->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An expense has been added");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$expenses = Expense::find($id);

			$expenses->status = "cancelled";

			if (!$expenses) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($expenses->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request successfully cancelled.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function remove($id = NULL){
		try {
			$expenses = ExpensesItem::find($id);

			$total_amount = Expense::find($expenses->expense_id);

			$total_amount->total_amount = $total_amount->total_amount - $expenses->amount;

			if (!$expenses) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($total_amount->save() AND $expenses->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An item has been removed.");
				return redirect()->back();
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function submit($id = NULL){
		try {
			$expense = Expense::find($id);

			$expense->status = "submitted";

			$items = ExpensesItem::where('expense_id',$id)->get();

			if($items->count() == 0){
				Session::flash('notification-status','failed');
				Session::flash('notification-msg',"Please add an item first.");
				return redirect()->back();
			}

			if($expense->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Expenses successfully submitted.");
				return redirect()->route('backoffice.expenses.index');
			}
		} catch(Exception $e){
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function response($id,$status){
		try {
			$expense = Expense::find($id);

			$expense->status = $status;

			// dd($status);

			if($expense->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An item has been removed.");
				return redirect()->route('backoffice.expenses.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg',"An error occured.");
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update_change($id = NULL){
		try {
			$update_change = Expense::find($id);

			$change = Input::get('change');
			$reimbursement_amount = Input::get('reimbursement_amount');

			$update_change->change = $change;
			$update_change->reimbursement_amount = $reimbursement_amount;

			if($update_change->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Change and the amount to reimburse was successfully updated.");
				return redirect()->back();
			}
			
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	private function __upload(Request $request, $directory = "uploads/expenses"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function excel_expenses(){
		try {
			if(in_array($this->data['auth']->type, ['sales_agent','sales_head','admin','super_user'])){
				$this->data['expenses'] = Expense::orderBy('created_at',"DESC")->where('sa_id',$this->data['auth']->id)->get();
			}else{
				$this->data['expenses'] = Expense::orderBy('created_at',"DESC")->whereIn('status',['submitted','denied','approved'])->get();
			}

			$ext = "xls";

			$filename = "Expenses : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Expenses', function($sheet) {
					$sheet->loadView('excel.expenses', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

	public function excel_request(){
		try {
			if(in_array($this->data['auth']->type, ['sales_agent','sales_head','admin','super_user'])){
				$this->data['requests'] = RequestExpense::orderBy('created_at',"DESC")->where('sa_id',$this->data['auth']->id)->get();
			}else{
				$this->data['requests'] = RequestExpense::orderBy('created_at',"DESC")->get();
			}

			$ext = "xls";

			$filename = "My Requests : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('My Requests', function($sheet) {
					$sheet->loadView('excel.my_requests', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}