<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Cluster;
use App\Laravel\Models\UsersCredential;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\EmployeeRequest;
use App\Laravel\Requests\Backoffice\EditEmployeeRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, DB, Excel;

class EmployeeController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Employees";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "employee";
		$this->data['types'] = [''=>'Choose type','spectator'=>'Spectator','finance'=>'Finance','sales_head'=>'Sales Head','sales_agent'=>'Sales Agent','encoder'=>'Inventory Officer','ops_head'=>'Operations Manager','auditor'=>'Auditor','admin'=>"Admin",'super_user'=>"Super User"];

		// $this->data['islands'] = [''=>'--Select--','luzon'=>'Luzon','visayas'=>'Visayas','mindanao'=>'Mindanao'];
		$this->data['areas'] = [''=>'--Select--','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'];

		$this->data['islands'] = [''=>'--Select--'] + Cluster::select(DB::raw("CONCAT('Area ',area,' - ',UCASE(SUBSTRING(island ,1 ,1)),LOWER(SUBSTRING(island, 2))) AS cluster"),'id')
													->orderBy('area')->pluck('cluster','id')->toArray();

	}

	public function index () {
		$this->data['employees'] = User::orderBy('created_at',"DESC")->whereNotIn('type',['super_user','client'])->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (EmployeeRequest $request) {
		try {
			$new_employee = new User;
			$new_employee->fill($request->all());
			$new_employee->email = Str::lower($request->get('email'));
			$new_employee->password = bcrypt($request->get('password'));

			

			if($new_employee->save()) {
				$credentials = new UsersCredential;
				$credentials->user_id = $new_employee->id;
				$credentials->password = $request->get('password');
				if($credentials->save()){
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New employee has been added.");
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$employee = User::find($id);

		if (!$employee) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['employee'] = $employee;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditEmployeeRequest $request, $id = NULL) {
		try {
			$employee = User::find($id);
			if($request->has('cluster_id')){
				$employee->area = Helper::cluster($request->cluster_id)->area;
			}

			if (!$employee) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$employee->fill($request->all());

			if($request->has('password')){
				$employee->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$employee->directory}/{$employee->filename}")){
						File::delete("{$employee->directory}/{$employee->filename}");
					}
					if (File::exists("{$employee->directory}/resized/{$employee->filename}")){
						File::delete("{$employee->directory}/resized/{$employee->filename}");
					}
					if (File::exists("{$employee->directory}/thumbnails/{$employee->filename}")){
						File::delete("{$employee->directory}/thumbnails/{$employee->filename}");
					}
				}
				
				$employee->directory = $upload["directory"];
				$employee->filename = $upload["filename"];
			}

			if($employee->save()) {
				if($request->has('password')){
					$credential = UsersCredential::where('user_id',$employee->id)->first();
					$credential->password = $request->get('password');
					if($credential->save()){
						goto callback;
					}
				}

				callback:

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An employee has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$employee = User::find($id);

			$employee->email = $id."@domain.com";

			if (!$employee) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$employee->directory}/{$employee->filename}")){
				File::delete("{$employee->directory}/{$employee->filename}");
			}
			if (File::exists("{$employee->directory}/resized/{$employee->filename}")){
				File::delete("{$employee->directory}/resized/{$employee->filename}");
			}
			if (File::exists("{$employee->directory}/thumbnails/{$employee->filename}")){
				File::delete("{$employee->directory}/thumbnails/{$employee->filename}");
			}

			if($employee->save() AND $employee->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An employee has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/employee"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function users_credentials(){
		$this->data['page_title'] = "Employee Credentials";
		$credentials = UsersCredential::all();

		$this->data['credentials'] = $credentials;

		return view('backoffice.'.$this->data['route_file'].'.credentials',$this->data);
	}

	private function __init_xls(){

		$header = [
			'Name',
			'Email',
			'Contact #',
			'Address',
			'User Type',
       	];

		$this->data['header'] = $header;
	}

	public function export() {
		try {
			$this->__init_xls();
			$this->data['users'] = User::where('type','!=','client')->get();
			$ext = "xls";
			$filename = Helper::create_filename($ext, true);

			Excel::create($filename, function($excel) {

				$excel->setTitle("License Exported Data")
				->setCreator('HS Backend Team')
				->setCompany('Highly Succeed Inc.');

				$excel->sheet('License', function($sheet) {
					$sheet->freezeFirstRow();
					$sheet->setOrientation('landscape');
					$sheet->row(1,$this->data['header']);

					$counter = 2;

					foreach ($this->data['users'] as $index => $info) {
						$sheet->row($counter++, [
							$info->fname.' '.$info->lname, 
							$info->email, 
							$info->contact.', '.$info->contact_2.', '.$info->contact_3, 
							$info->address.', '.$info->address_2, 
							str_replace('_', ' ', Str::title($info->type)), 
						]);
					}
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

}