<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\QuotaConfiguration;
use App\Laravel\Models\Cluster;

/*
*
* Requests used for validating inputs
*/

use App\Laravel\Requests\Backoffice\QuotaConfigurationRequest;
/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class QuotaController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['page_title'] = "Quota Configuration";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";

		$this->data['date_ranges'] = ['month' => "Monthly",'year' => "Yearly"];
	}

	public function index () {
		$this->data['configuration'] = QuotaConfiguration::orderBy('created_at','DESC')->first();
		$this->data['clusters'] = Cluster::orderBy('area')->get();
		$this->data['luz_count'] = Cluster::where('island','luzon')->count();
		$this->data['vis_count'] = Cluster::where('island','visayas')->count();
		$this->data['min_count'] = Cluster::where('island','mindanao')->count();
		return view('backoffice.quota.configuration',$this->data);
	}

	public function store(QuotaConfigurationRequest $request){
		try {
			$quota = new QuotaConfiguration;
			$quota->fill($request->all());
			$quota->month = date('F');
			
			$clusters = Cluster::orderBy('area')->get();
			foreach($clusters as $index => $info){
				$update_cluster = Cluster::find($info->id);
				$update_cluster->area_quota = $request->area_quota[$info->id];
				$update_cluster->save();
			}

			if($quota->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg','A quota configuration was successfully updated');
				return redirect()->back();
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}