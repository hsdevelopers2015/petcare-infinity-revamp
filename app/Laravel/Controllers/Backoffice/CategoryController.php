<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CategoryRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF;

class CategoryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Categories";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "category";

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();/*Inventory::where('quantity','>',0)
																	->pluck('id','id')->toArray();*/
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
	}
	public function create(){
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function index () {
		$this->data['categories'] = Category::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (CategoryRequest $request) {
		try {
			$new_category = new Category;
			$new_category->fill($request->all());

			if($new_category->save()) {
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New category has been added.");
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}

	public function edit ($id = NULL) {
		$new_category = Category::find($id);

		if (!$new_category) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['category'] = $new_category;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (CategoryRequest $request, $id = NULL) {
		try {
			$category = Category::find($id);
			if (!$category) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$category->fill($request->all());
						
			if($category->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A category has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$category = Category::find($id);

			if (!$category) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($category->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A category has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}