<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Sales;
use App\Laravel\Models\SentContract;
use App\Laravel\Models\UserTimeline;
use App\Laravel\Models\UsersCredential;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\UserSettingRequest;
use App\Laravel\Requests\Backoffice\ChangePasswordRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Helper, Carbon, Session, Str,File,Image;

class ProfileController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Profile Setting";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";

		$this->data['form_positions'] = ["" => "Choose Position","center" => "Center","left" => "Left" , "right" => "Right"];
	}

	public function setting () {
		return view('backoffice.auth.setting',$this->data);
	}

	public function view ($username = NULL) {
		$this->data['profile'] = User::where('username',$username)->first();
		if($this->data['profile']){

			if($this->data['profile']->type == "client"){

				$this->data['invoice'] = Sales::where('client_id',$this->data['profile']->id)->where('status','posted')->get();
				$this->data['contracts'] = SentContract::where('client_id',$this->data['profile']->id)->get();
				$this->data['user_timeline'] = UserTimeline::where('user_id',$this->data['profile']->id)->orderBy('created_at','DESC')->get();

				return view('backoffice.profile.client',$this->data);
			}else{

				$this->data['user_timeline'] = UserTimeline::where('user_id',$this->data['profile']->id)->orderBy('created_at','DESC')->get();
				return view('backoffice.profile.index',$this->data);
			}
		}else{
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Profile not found.');
			return redirect()->back();
		}
		
	}

	public function update_setting (UserSettingRequest $request) {
		try {
			$user = User::find($this->data['auth']->id);

			$user->fill($request->all());

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$user->directory = $upload["directory"];
				$user->filename = $upload["filename"];
			}

			if($user->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"User profile has been updated.");
				return redirect()->route('backoffice.profile.settings');
			}

			Session::flash('notification-status','success');
			Session::flash('notification-msg',"User profile has been updated.");
			return redirect()->route('backoffice.profile.settings');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::find($id);

			if (!$user) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.users.index');
			}

			$user->email .= "-deleted-" . Helper::date_format(Carbon::now(),"YmdHis");
			$user->username .= "-deleted-" . Helper::date_format(Carbon::now(),"YmdHis");

			if($user->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A user has been deleted.");
				return redirect()->route('backoffice.users.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request,$field = "file", $directory = "uploads/profile"){
		$file = $request->file($field);
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->fit(1000,1000)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}

	public function password_setting(){
		$this->data['page_title'] = "Edit Password";
		$user = User::find($this->data['auth']->id);

		$this->data['user'] = $user;
		return view('backoffice.auth.password',$this->data);
	}

	public function update_password(ChangePasswordRequest $request){
		$user = User::find($this->data['auth']->id);

		$user->password = bcrypt($request->password);
		if($user->save()){

			$credential = UsersCredential::where('user_id',$user->id)->first();

			if($credential){
				$credential->password = $request->password;
				if($credential->save()){
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"Your password was successfully updated.");
					return redirect()->back();
				}
			}else{
				$credential = new UsersCredential;
				$credential->user_id = $user->id;
				$credential->password = $request->password;
				
				if($credential->save()){
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"Your password was successfully updated.");
					return redirect()->back();
				}
			}
		}
	}

}