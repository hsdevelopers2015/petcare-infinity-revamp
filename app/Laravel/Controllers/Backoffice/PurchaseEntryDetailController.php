<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Order;
use App\Laravel\Models\OrderItem;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\OrderDiscount;
use App\Laravel\Models\DeliveryReceipt;
use App\Laravel\Models\Category;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\PurchaseEntryHeader;
use App\Laravel\Models\PurchaseEntryDetail;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CategoryRequest;
use App\Laravel\Requests\Backoffice\SupplierRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, PDF;

class PurchaseEntryDetailController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Suppliers";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "supplier";

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['products'] = ['' => "Type the product name"] +  Product::checkInventory()
    																->pluck('product_name', 'id')->toArray();/*Inventory::where('quantity','>',0)
																	->pluck('id','id')->toArray();*/
		$this->data['product_lists'] = Product::all();
		$this->data['discount_types'] = ['amount' => "by Amount" , 'percentage' => "by Percentage"];
	}
	public function create(){
		dd('purchase create entry');
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function index () {
		dd('purchase entry index');
		$this->data['suppliers'] = Supplier::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (SupplierRequest $request) {
		try {
			$new_supplier = new Supplier;
			$new_supplier->fill($request->all());

			if($new_supplier->save()) {
					Session::flash('notification-status','success');
					Session::flash('notification-msg',"New category has been added.");
					return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
				}
			}catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}

	public function edit ($id = NULL) {
		$supplier = Supplier::find($id);

		if (!$supplier) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['supplier'] = $supplier;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (SupplierRequest $request, $id = NULL) {
		try {
			$new_supplier = Supplier::find($id);
			$new_supplier->fill($request->all());
			if (!$new_supplier) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			
			
			if($new_supplier->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function destroy ($id = NULL) {
		try {
			$supplier = Supplier::find($id);

			if (!$supplier) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($supplier->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}