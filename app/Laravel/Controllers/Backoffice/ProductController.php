<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Product;
use App\Laravel\Models\User;
use App\Laravel\Models\PromoteProduct;
use App\Laravel\Models\Category;
use App\Laravel\Models\Measurement;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ImportRequest;
use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Requests\Backoffice\EditClientRequest;
use App\Laravel\Requests\Backoffice\PromoteProductRequest;
use App\Laravel\Requests\Backoffice\EditPromoteProductRequest;
/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, GMaps, DB, Excel;

class ProductController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Products";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "products";

		$this->data['sales_agents'] = [''=>"Choose a sales agent for this client"] + User::select(
            DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->where('type','sales_agent')->pluck('name','id')->toArray();

		$this->data['product_list'] = ['' => "Choose a product"] + Product::pluck('product_name','id')->toArray();
		$this->data['quota_types'] = ['mother_quota' => "Mother Quota", 'group_quota' => "Group Product Quota"];
		$this->data['focus_types'] = ['amount' => "Amount", 'qty' => "Quantity"];
		$this->data['categories'] = ['' => "-Select Category-"]+Category::pluck('category_name','category_code')->toArray();
		$this->data['measurement'] = [''=>'-Select Unit-']+Measurement::pluck('description','code')->toArray();
	}

	public function index () {
		$this->data['products'] = Product::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (ProductRequest $request) {
		
		try {
			$new_product = new Product;
			$new_product->fill($request->all());
			$new_product->category_code = $request->category_code;
		
			
			if($new_product->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New product has been added to the inventory.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$product = Product::find($id);

		if (!$product) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['product'] = $product;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (ProductRequest $request, $id = NULL) {
		try {
			$product = Product::find($id);
			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$product->fill($request->all());

			if(!$request->has('is_focus')){
				$product->is_focus = 'no';
			}
			
			if($product->save()) {

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = Product::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if($product->save() AND $product->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	/**
	*
	*@param App\Http\Requests\RequestRequest $request
	*@param string $request
	*
	*@return array
	*/
	private function __upload(Request $request, $directory = "uploads/products"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",90);
		Image::make("{$path_directory}/{$filename}")->resize(250,250)->save("{$thumb_directory}/{$filename}",90);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}


	public function promote(){
		$this->data['products'] = PromoteProduct::all();
		$this->data['page_title'] = "Products With Promotion";

		return view('backoffice.'.$this->data['route_file'].'.promote',$this->data);
	}

	public function create_promote () {
		return view('backoffice.'.$this->data['route_file'].'.create_promote',$this->data);
	}

	public function store_promote (PromoteProductRequest $request) {
		try {
			$new_product = new PromoteProduct;

			$new_product->fill($request->all());

			$new_product->sa_id = $this->data['auth']->id;

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_product->directory = $upload["directory"];
				$new_product->filename = $upload["filename"];
			}

			if($new_product->save()) {

				Helper::user_timeline($this->data['auth']->id,$new_product->id,'promote_products','was promoted a product.','promote');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New product has been promoted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit_promote ($id = NULL) {
		$product = PromoteProduct::find($id);

		if (!$product) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
		}

		$this->data['product'] = $product;
		return view('backoffice.'.$this->data['route_file'].'.edit_promote',$this->data);
	}

	public function update_promote (EditPromoteProductRequest $request, $id = NULL) {
		try {
			$product = PromoteProduct::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
			}

			$product->fill($request->all());

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_product->directory = $upload["directory"];
				$new_product->filename = $upload["filename"];
			}

			if($product->save()) {
				Helper::user_timeline($this->data['auth']->id,$product->id,'promote_products','was updated a promoted product.','update');

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A promoted product has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy_promote ($id = NULL) {
		try {
			$product = PromoteProduct::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
			}

			Helper::user_timeline($this->data['auth']->id,$product->id,'promote_products','was deleted a promoted product.','delete');

			if($product->save() AND $product->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A product has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.promote');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	private function __init_xls(){

		$header = [
			'Prodcut CODE',
			'Product Name',
			'Category',
			'Cost',
			'Unit',
       	];

		$this->data['header'] = $header;
	}

	public function export() {
		try {
			$this->data['products'] = Product::all();
			$this->__init_xls();

			$ext = "xls";

			$filename = "Product List : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Product List', function($sheet) {

					$sheet->loadView('excel.product', $this->data);

				});

			})->export($ext);

			// $request->flash();
			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}

	public function import(ImportRequest $request){
		try {
			$ext = $request->file('file')->getClientOriginalExtension();
			$filename = Helper::create_filename($ext);
			$request->file('file')->move("temp_upload", $filename); 

			$import = Excel::load("temp_upload/".$filename, function($reader) {
				$result = $reader->all();
				$sheet_count = count($result);

				foreach ($result as $index => $sheet) {
					foreach($sheet as $i => $row){
						$input_data = $row->toArray();

						$new_product = new Product;
						$new_product->product_code = Str::upper(Str::limit($input_data['product_name'],$limit = '2',$end = '')).'-'.Str::upper(Str::limit($input_data['category']? : Str::random($length = 3),$limit = '4',$end = '')).'-'.rand(1000,9999);
						$new_product->product_name = $input_data['product_name'];
						$new_product->category = $input_data['category']? : Str::random($length = 3);
						$new_product->cost = $input_data['price'];
						$new_product->unit = $input_data['unit'];
						$new_product->deal_condition = $input_data['condition'];
						$new_product->deal_incentive = $input_data['incentive'];
						$new_product->replenish = $input_data['replenish'];

						$new_product->save();
					}
				}
			});

			Session::flash('notification-status','success');
			Session::flash('notification-msg','The product was successfully imported.');
			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Please contact your system administator.');
			return redirect()->back();
		}
	}

	public function excel(){
		try {
			$this->data['products'] = PromoteProduct::orderBy('created_at','DESC')->get();

			$ext = "xls";

			$filename = "Promoted Products : ".Helper::date_format(Carbon::now(),'Y-m-d').".".$ext;

			Excel::create($filename, function($excel) {

				$excel->sheet('Promoted Products', function($sheet) {
					$sheet->loadView('excel.promoted_products', $this->data);
				});

			})->export($ext);

			Session::flash('notification-status','success');
			Session::flash('notification-msg','Export successful. See result below.');
			return redirect()->back();

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','<strong>Error</strong> Please contact your system administator.');
			return redirect()->back();
		}
	}
}