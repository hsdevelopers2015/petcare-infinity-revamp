<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Contact Form Data</title>
</head>
<body>
	<h2>Inquiry From {{"{$name} ({$email})"}}</h2>
	<p>Contact Number : {{$contact_number}}</p>
	<p>Subject : {{$subject}}</p>
	<p>{{$msg}}</p>
</body>
</html>