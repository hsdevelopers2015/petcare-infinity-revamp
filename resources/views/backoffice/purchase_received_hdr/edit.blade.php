@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>					
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form id="form-target" class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="purchase_code">Purchase Code</label>
										
											<div class="col-md-9">
											{!!Form::select("purchase_code", $purchase_code, old('purchase_code',$purchase_received_hdr->purchasehdr_code), ['id' => "purchase_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('purchase_code')}}"]) !!}
										</div>
										
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="supplier_code">Supplier</label>
										<div class="col-md-9">
											{!!Form::select("supplier_code", $supplier_code, old('supplier_code',$purchase_received_hdr->supplier_code), ['id' => "supplier_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('supplier_code')}}"]) !!}
										</div>
									</div>
								</div>	
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="invoice_number">Invoice Number</label>
										<div class="col-md-9">
											<input type="text" name="invoice_number" id="invoice_number" class="form-control {{$errors->first('invoice_number') ? 'border-danger' : NULL}}" placeholder="Invoice Number" value="{{old('invoice_number',$purchase_received_hdr->invoice_number)}}" data-toggle="tooltip" title="{{$errors->first('invoice_number') ? $errors->first('invoice_number') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="description">Description</label>
										<div class="col-md-9">
											<input type="text" name="description" id="description" class="form-control {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="Description" value="{{old('description',$purchase_received_hdr->description)}}" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">
										</div>
									</div>
								</div>						
							</div>
						</div>
						<hr>
						<div class="form-body">
							<div class="row mt-3">
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="supplier_code">Product</label>
									<div class="col-md-9">
										{!!Form::select("product_code", $product_code, old('product_code'), ['id' => "product_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('product_code')}}"]) !!}
									</div>
								</div>
							</div>	
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="measurement">Unit</label>
									<div class="col-md-9">
										{!!Form::select("measurement", $measurement, old('measurement'), ['id' => "measurement", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('measurement')}}"]) !!}
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Quantity</label>
									<div class="col-md-9">
										<input type="text" name="qty" id="qty" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Cost Price</label>
									<div class="col-md-9">
										<input type="text" name="cost_price" id="cost_price" class="form-control {{$errors->first('cost_price') ? 'border-danger' : NULL}}" placeholder="Cost price" value="{{old('cost_price')}}" data-toggle="tooltip" title="{{$errors->first('cost_price') ? $errors->first('cost_price') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Selling Price</label>
									<div class="col-md-9">
										<input type="text" name="selling_price" id="selling_price" class="form-control {{$errors->first('selling_price') ? 'border-danger' : NULL}}" placeholder="Selling price" value="{{old('selling_price')}}" data-toggle="tooltip" title="{{$errors->first('selling_price') ? $errors->first('selling_price') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Expiration Date</label>
									<div class="col-md-9">
										<input type="date" name="expiration_date" id="expiration_date" class="form-control daterange-basic {{$errors->first('expiration_date') ? 'border-danger' : NULL}}" placeholder="Expiration date" value="{{old('expiration_date')}}" data-toggle="tooltip" title="{{$errors->first('expiration_date') ? $errors->first('expiration_date') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control"></label>
									<div class="col-md-9">
										<input type="button" class="add-row" value="Add Row">
									</div>
								</div>
							</div>
							<div class="container">
								<table id="product-table">
								       <thead>
								           <tr>
								               <th>Select</th>
								               <th>Product Name</th>
								               <th>Quantity</th>
								               <th>Unit</th>
								               <th>Cost Price</th>
								               <th>Selling Price</th>
								               <th>Expiration Date</th>
								           </tr>
								       </thead>
								       <tbody>
								       	@foreach($purchase_received_hdr->purchase_received_dtl as $index => $value)
								           <tr>
								           	<td><input type='checkbox' name='record'></td><td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='{{$value->product_code}}'>{{$value->product_code}}</td>
								           	<td><input type='hidden'  class='form-control input-sm' name='qty[]' value='{{$value->qty}}'> {{$value->qty}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='measurement[]' value=' {{$value->measurement_code}}'>  {{$value->measurement_code}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='cost_price[]' value='{{$value->cost_price}}'> {{$value->cost_price}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='selling_price[]' value='{{$value->selling_price}}'> {{$value->selling_price}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='{{$value->expiration_date}}'> {{$value->expiration_date}} </td>
								           </tr>
								           @endforeach
								       </tbody>
								   </table>
								   <button type="button" class="delete-row">Delete Row</button>								
							</div>
						</div>
					</div>	
					<div class="left">
					<hr>
					<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
						<i class="ft-x"></i> Cancel
					</a>
					<button type="submit" class="btn btn-primary overlay-unblock">
						<i class="fa fa-check-square-o"></i> Save
					</button>
					</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('page-styles')
<style type="text/css">
    form{
        margin: 20px 0;
    }
    form input, button{
        padding: 5px;
    }
    table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
    table, th, td{
        border: 1px solid #cdcdcd;
    }
    table th, table td{
        padding: 10px;
        text-align: left;
    }
</style>
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

	// $.fn.serializeProducts = function() {
	// 	var products = [];
	// 	$(this).find("tr").each(function() {
	// 		products.push({
	// 			product_code: $(this).find("td:nth(1)").text(),
	// 			qty: $(this).find("td:nth(2)").text(),
	// 			cost_price: $(this).find("td:nth(3)").text(),
	// 			selling_price: $(this).find("td:nth(4)").text(),
	// 			expiration_date: $(this).find("td:nth(5)").text()
	// 		});
	// 	});

	// 	return products;
	// }

    $(function() {

    	$(".add-row").click(function(){
            var product_code = $("#product_code").val();
            var qty = $("#qty").val();
            var measurement = $("#measurement").val();
            var cost_price = $("#cost_price").val();
            var selling_price = $("#selling_price").val();
            var expiration_date = $("#expiration_date").val();
            var markup = "<tr><td><input type='checkbox' name='record'></td><td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='" + product_code + "'>"+product_code +"</td><td><input type='hidden'  class='form-control input-sm' name='qty[]' value='" + qty + "'>" + qty + "</td><td><input type='hidden'  class='form-control input-sm' name='measurement[]' value='" + measurement + "'>" + measurement + "</td><td><input type='hidden'  class='form-control input-sm' name='cost_price[]' value='" + cost_price + "'>" + cost_price + "</td><td><input type='hidden'  class='form-control input-sm' name='selling_price[]' value='" + selling_price + "'>" + selling_price + "</td><td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='" + expiration_date + "'>" + expiration_date + "</td></tr>";
            $("table tbody").append(markup);

            
        });     	   
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });


        

        // $("#form-target").submit(function(e) {
        // 	e.preventDefault();
        // 	var products = $("#product-table").serializeProducts();
	       //  console.log(products);
        // }); 	

    });
</script>
@stop
