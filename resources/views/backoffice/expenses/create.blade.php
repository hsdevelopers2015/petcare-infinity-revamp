@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>

		@if(in_array($auth->type, ['admin','super_user']) AND $expense->status == "submitted")
		
		<a href="#" data-url="{{route('backoffice.expenses.response',[$expense->id,'approved'])}}" class="btn btn-success pull-right action-approve" data-toggle="modal" data-target="#confirm-approve"><i class="ft-check"></i>&nbsp;&nbsp;Approve&nbsp;</a>

		<a href="#" data-url="{{route('backoffice.expenses.response',[$expense->id,'denied'])}}" class="btn btn-danger pull-right action-deny" data-toggle="modal" style="margin-right: 10px;" data-target="#confirm-deny"><i class="ft-x"></i>&nbsp;&nbsp;Deny&nbsp;</a>

		@endif
		@if(in_array($auth->type, ['sales_agent','sales_head','admin','super_user']) AND $expense->status == "draft" )
		<a href="#" data-url="{{route('backoffice.expenses.submit',$expense->id)}}" class="btn btn-primary pull-right action-create" data-toggle="modal" data-target="#confirm-create"><i class="ft-check"></i>&nbsp;&nbsp;Submit&nbsp;</a>
		@endif
	</div>
</div>

@if(in_array($auth->type, ['sales_agent','sales_head','admin','super_user']) AND in_array($expense->status, ['draft']))
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="fa fa-money"></i>Item of Your Expenses</h4>

							{{-- <div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2">Purpose</label>
										<div class="col-md-9">
											<textarea type="text" name="description" rows="3" id="userinput2" class="form-control prod_cat {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="Purpose e.g. For the Fuel, Transportation and etc." value="" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">{{old('description')}}</textarea>
										</div>
									</div>
								</div>
							</div> --}}

							<div class="row date mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2511">Date</label>
										<div class="col-md-9">
											<input type="date" name="expense_date" id="userinput2511" class="form-control {{$errors->first('expense_date') ? 'border-danger' : NULL}}" placeholder="YYYY-MM-DD" value="{{old('expense_date',Helper::date_format(Carbon::now(),'Y-m-d'))}}" data-toggle="tooltip" title="{{$errors->first('expense_date') ? $errors->first('expense_date') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row plate_number">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2511">Plate Number</label>
										<div class="col-md-9">
											<input type="text" name="plate_number" id="userinput2511" class="form-control {{$errors->first('plate_number') ? 'border-danger' : NULL}}" placeholder="Plate Number" value="{{old('plate_number')}}" data-toggle="tooltip" title="{{$errors->first('plate_number') ? $errors->first('plate_number') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="type">Type</label>
										<div class="col-md-9">
											<?php
											if($errors->first('type')){
												$status = 'border-danger';
												$msg = $errors->first('type');
											}else{
												$status = "";
												$msg  = "";
											}
											?>
											{{-- <input type="number" name="amount" id="userinput3" class="form-control prod_cat {{$errors->first('amount') ? 'border-danger' : NULL}}" placeholder="Amount" value="{{old('amount')}}" data-toggle="tooltip" title="{{$errors->first('amount') ? $errors->first('amount') : NULL}}"> --}}
											{!!Form::select("type", $expense_types, old('type'), ['id' => "type", 'class' => "form-control ".$status]) !!}
										</div>
									</div>
								</div>
							</div>

							<div class="row remarks">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput251">Remarks</label>
										<div class="col-md-9">
											<input type="text" name="remarks" id="userinput251" class="form-control {{$errors->first('remarks') ? 'border-danger' : NULL}}" placeholder="Remarks" value="{{old('remarks')}}" data-toggle="tooltip" title="{{$errors->first('remarks') ? $errors->first('remarks') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row sub-type">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="sub_type">Sub Type</label>
										<div class="col-md-9">
											<?php
											if($errors->first('sub_type')){
												$status = 'border-danger';
												$msg = $errors->first('sub_type');
											}else{
												$status = "";
												$msg  = "";
											}
											?>
											{!!Form::select("sub_type", $expense_sub_types, old('sub_type'), ['id' => "sub_type", 'class' => "form-control ".$status]) !!}
										</div>
									</div>
								</div>
							</div>

							<div class="row liter">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="liter">Liter</label>
										<div class="col-md-9">
											<input type="text" name="liters" value="{{old('liters')}}" class="form-control">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput25">Invoice Number</label>
										<div class="col-md-9">
											<input type="text" name="invoice_no" id="userinput25" class="form-control prod_cat {{$errors->first('invoice_no') ? 'border-danger' : NULL}}" placeholder="Invoice Number" value="{{old('invoice_no')}}" data-toggle="tooltip" title="{{$errors->first('invoice_no') ? $errors->first('invoice_no') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput26">Tin Number</label>
										<div class="col-md-9">
											<input type="text" name="tin_number" id="userinput26" class="form-control prod_cat {{$errors->first('tin_number') ? 'border-danger' : NULL}}" placeholder="Tin Number" value="{{old('tin_number')}}" data-toggle="tooltip" title="{{$errors->first('tin_number') ? $errors->first('tin_number') : NULL}}" step=".01">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput31">NET<br>Amount</label>
										<div class="col-md-9">
											<input type="number" name="net_amount" id="userinput31" class="form-control prod_cat {{$errors->first('net_amount') ? 'border-danger' : NULL}}" placeholder="Amount NET of VAT" value="{{old('net_amount')}}" data-toggle="tooltip" title="{{$errors->first('net_amount') ? $errors->first('net_amount') : NULL}}" step=".01">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput32">VAT</label>
										<div class="col-md-9">
											<input type="number" name="vat" id="userinput32" class="form-control prod_cat {{$errors->first('vat') ? 'border-danger' : NULL}}" placeholder="VAT Input Tax" value="{{old('vat')}}" data-toggle="tooltip" title="{{$errors->first('vat') ? $errors->first('vat') : NULL}}" step=".01">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Total Amount</label>
										<div class="col-md-9">
											<input type="number" name="amount" id="userinput3" class="form-control prod_cat {{$errors->first('amount') ? 'border-danger' : NULL}}" placeholder="Total Amount" value="{{old('amount')}}" data-toggle="tooltip" title="{{$errors->first('amount') ? $errors->first('amount') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							{{-- <div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Your Receipt<br><span class="text-mute">Scanned Image</span></label>
										<div class="col-md-9">
											<input type="file" id="userinput5"  name="file" class="form-control {{$errors->first('file') ? 'border-danger' : NULL}}" title="{{$errors->first('file') ? $errors->first('file') : NULL}}" data-toggle="tooltip" >
										</div>
									</div>
								</div>
							</div> --}}
							
						</div>

						<div class="left">
						<hr>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Add
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@if($expense->type == 'liquidation')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">Change or the Amount Return Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<form class="form form-horizontal" method="POST" action="{{route('backoffice.expenses.update_change',$expense->id)}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="fa fa-money"></i>Change or Amount Return</h4>

							<div class="row m-t-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput311">Change</label>
										<div class="col-md-9">
											<input type="number" name="change" id="userinput311" class="form-control prod_cat {{$errors->first('change') ? 'border-danger' : NULL}}" placeholder="Change" value="{{old('change',$expense->change)}}" data-toggle="tooltip" title="{{$errors->first('change') ? $errors->first('change') : NULL}}" min="0">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput311">Amount to <br>reimburse</label>
										<div class="col-md-9">
											<input type="number" name="reimbursement_amount" id="userinput311" class="form-control prod_cat {{$errors->first('reimbursement_amount') ? 'border-danger' : NULL}}" placeholder="Amount to reimburse" value="{{old('reimbursement_amount',$expense->reimbursement_amount)}}" data-toggle="tooltip" title="{{$errors->first('reimbursement_amount') ? $errors->first('reimbursement_amount') : NULL}}" min="0">
											<small>Note: The <code>amount you spent</code> if the given budget was not enough.</small>
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Update
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endif

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<section id="file-export">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Item List</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
									<table class="table table-striped table-bordered dataTable file-export">
										<thead>
											<tr>
												<th style="width: 3%;"></th>
												{{-- <th class="text-center">Thumbnail</th> --}}
												{{-- <th class="text-center">Purpose</th> --}}
												<th class="text-center">Type</th>
												<th class="text-center">Tin Number</th>
												<th class="text-center">NET Amount</th>
												<th class="text-center">VAT</th>
												<th class="text-center">Total Amount</th>
												<th class="text-center">Expense Date</th>
												@if($expense->status == "draft")
												<th class="text-center" style="width: 3%;"></th>
												@endif
											</tr>
										</thead>
										<tbody>
											@foreach($items as $index => $info)
											<tr>
												<td>{{$index+1}}</td>
												{{-- <td class="text-center">
													<a data-toggle="modal" data-target="#view-{{$info->id}}">
													<img src="{{asset($info->directory.'/resized/'.$info->filename)}}" class="height-50 img-responsive img-thumbnail">
													</a>
												</td> --}}
												{{-- <td class="text-center">{{$info->description}}</td> --}}
												<td class="text-center">{{Str::upper($info->type)}} {{$info->type == 'fuel'? ' - '.Str::title($info->sub_type) : NULL}}</td>
												<td class="text-center">{{$info->tin_number}}</td>
												<td class="text-center">{{number_format($info->net_amount,2)}}</td>
												<td class="text-center">{{number_format($info->vat,2)}}</td>
												<td class="text-center">{{number_format($info->amount,2)}}</td>
												<td class="text-center">{{Helper::date_format($info->expense_date,'F d, Y')}}</td>
												@if($expense->status == "draft")
												<td class="text-center">
													<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
														<div role="group" class="btn-group">
															<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
																<i class="ft-cog icon-left"></i> Action
															</button>
															<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
																<a href="{{route('backoffice.'.$route_file.'.remove',[$info->id])}}" class="dropdown-item">Remove Item</a>
															</div>
														</div>
													</div>
												</td>
												@endif
											</tr>
											@endforeach
										</tbody>
									</table>
									<a href="{{route('backoffice.expenses.index')}}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to the expenses list</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0 text-danger">Total Expenses: {{$expense->total_amount($expense->id)}}</h3>
	</div>
</div>
@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<div class="modal-body">
				
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Note :</strong> Upon submitting it will not be editable any further.
	            </div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Submit</a>
			</div>
		</div>
	</div>
</div>

<div id="confirm-deny" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<div class="modal-body">
				
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Message :</strong> Are you sure you want to deny this request?.
	            </div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-deny">Yes</a>
			</div>
		</div>
	</div>
</div>

<div id="confirm-approve" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<div class="modal-body">
				
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Message :</strong> Are you sure you want to approve this request?.
	            </div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-approve">Yes</a>
			</div>
		</div>
	</div>
</div>

@foreach($items as $index => $info)
<div id="view-{{$info->id}}" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Item Scanned Receipt</h5>
			</div>
			<div class="modal-body">
				
	            {{-- <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Note :</strong> Upon submitting it will not be editable any further.
	            </div> --}}
	            <div class="row">
	            	<div class="col-md-12">
	            		<img src="{{asset($info->directory.'/resized/'.$info->filename)}}" class="img-responsive img-thumbnail" style="width: 100%">
	            	</div>
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@include('backoffice._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')

@include('backoffice._includes.scripts')

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        $( ".select_date" ).datepicker({
                dateFormat: "mm/dd/yyyy",
        });
    })
</script>
<script type="text/javascript">
	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});

	$(".action-deny").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-deny").attr({"href" : btn.data('url')});
	});

	$(".action-approve").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-approve").attr({"href" : btn.data('url')});
	});

</script>

<script>
$(".prod_cat").click(function(){
	var x = $("#userinput1").val();
	var y = $("#userinput2").val();

	if(x != "" && y != ""){
		var z = y.toUpperCase().substring(0,2) +'-'+ x.toUpperCase().substring(0,4)+'-'+ Math.floor((Math.random() * 10000) + 1);
		$("#userinput4").attr('value',z);
		// $("#userinput4").prop('disabled', true);
	}
});
</script>
<script>
$(".prod_cat").ready(function(){
	var x = $("#userinput1").val();
	var y = $("#userinput2").val();

	if(x != "" && y != ""){
		var z = y.toUpperCase().substring(0,2) +'-'+ x.toUpperCase().substring(0,4)+'-'+ Math.floor((Math.random() * 10000) + 1);
		$("#userinput4").attr('value',z);
		// $("#userinput4").prop('disabled', true);
	}
});

$(".sub-type").hide(500);
$(".liter").hide(500);

$("#sub_type").change(function(){
	var val =  $(this).val();
	if(val == 'liters'){
		$(".liter").show(500);
	}else{
		$(".liter").hide(500);
	}
});

$(".remarks").hide(500);

$("#type").click(function(){
	var val = $(this).val();

	if(val == 'fuel'){
		$(".sub-type").show(500);
	}else{
		$(".sub-type").hide(500);
	}
});

$("#type").click(function(){
	var val = $(this).val();

	if(val == 'others'){
		$(".remarks").show(500);
	}else{
		$(".remarks").hide(500);
	}
});
</script>
@stop
