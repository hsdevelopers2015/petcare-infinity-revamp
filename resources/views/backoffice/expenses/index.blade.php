@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
		@if(in_array($auth->type, ['sales_agent','sales_head','admin','super_user']))
		
		<a href="{{route('backoffice.expenses.create')}}" class="btn btn-primary pull-right action-create-form" data-toggle="modal" data-target="#confirm-create-form"><i class="ft-plus"></i>&nbsp;&nbsp;Expenses Form</a>

		<a href="#" data-url="{{route('backoffice.expenses.create_form')}}" class="btn btn-primary pull-right action-create mr-2" data-toggle="modal" data-target="#confirm-create"><i class="ft-plus"></i>&nbsp;&nbsp;Create Request Form&nbsp;</a>
		@endif
	</div>
</div>
<section id="file-export">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$page_title}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
						<a href="{{route('backoffice.expenses.excel_expenses')}}" class="btn btn-primary pull-left" style="margin-right: 15px;">Excel</a>
						<table class="table table-striped table-bordered file-export ">
							<thead>
								<tr>
									{{-- <th style="width: 3%;"></th> --}}
									<th>Type</th>

									@if(in_array($auth->type, ['admin','super_user','finance']))
									<th>Employee</th>
									@endif
									
									<th>Status</th>
									<th>Request Details</th>
									<th>Last Modified</th>
									{{-- @if(in_array($auth->type, ['sales_head','sales_agent'])) --}}
									<th></th>
									{{-- @endif --}}
								</tr>
							</thead>
							<tbody>
								@foreach($expenses as $index => $info)
								<tr style="color: {{in_array($info->status,['cancelled','denied'])? "#c2c2c2": NULL}}" >
									{{-- <td>{{$index+1}}</td> --}}
									<td>{!!Helper::request_status_badge($info->type)!!}</td>

									@if(in_array($auth->type, ['admin','super_user','finance']))
									<td>{{$info->sa_info($info->sa_id)->fname}} {{$info->sa_info($info->sa_id)->lname}}</td>
									@endif
									<td>{!!Helper::expense_status_badge($info->status)!!}</td>
									<td>
									@if($info->type == "liquidation")
									Requested : <span class="text-primary">{{round($info->requested_amount,2)}}</span> <br>
									@endif
									@if($info->item_count($info->id)->count() > 0)
									Qty : <span class="text-success">{{$info->item_count($info->id)->count()}}</span><br>
									Total : <span class="text-success">{{round($info->total_amount,2)}}</span><br>
									@if($info->type == 'liquidation')
									Change : <span class="text-success">{{round($info->change,2)}}</span><br>
									Reimbursable : <span class="text-warning">{{round($info->reimbursement_amount,2)}}</span>
									@endif
									@endif
									</td>
									<td>{{$info->last_modified()}}</td>
									{{-- @if(in_array($auth->type, ['sales_head','sales_agent'])) --}}
									<td>
										@if(in_array($auth->type, ['sales_head','sales_agent']))
											@if($info->status=="approved")
											<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
												<div role="group" class="btn-group">
													<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
														<i class="ft-cog icon-left"></i> Action
													</button>
													<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
														<a href="{{route('backoffice.'.$route_file.'.add',[$info->id])}}" class="dropdown-item">View Items</a>
													</div>
												</div>
											</div>
											@else
											<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
												<div role="group" class="btn-group">
													<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
														<i class="ft-cog icon-left"></i> Action
													</button>
													<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
														<a href="{{route('backoffice.'.$route_file.'.add',[$info->id])}}" class="dropdown-item">
														View Items</a>
														@if($info->status == "draft")
														<a href="#" class="action-delete dropdown-item" data-url="{{route('backoffice.'.$route_file.'.destroy',[$info->id])}}" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eraser3"></i> Cancel</a> 
														@endif
													</div>
												</div>
											</div>
											@endif
										@else
										<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
											<div role="group" class="btn-group">
												<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
													<i class="ft-cog icon-left"></i> Action
												</button>
												<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
													<a href="{{route('backoffice.'.$route_file.'.add',[$info->id])}}" class="dropdown-item">
													View Items</a>
													@if($info->status == "submitted")
													<a href="{{route('backoffice.'.$route_file.'.response',[$info->id,'approved'])}}" class="dropdown-item">
													Approve</a>
													<a href="{{route('backoffice.'.$route_file.'.response',[$info->id,'denied'])}}" class="dropdown-item">
													Deny</a>
													@endif
												</div>
											</div>
										</div>
										@endif
									</td>
									{{-- @endif --}}
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@if(in_array($auth->type, ['sales_agent','sales_head','admin','super_user']))
<section id="file-export">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Request</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">Here are the list of all <code>requests</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
						<a href="{{route('backoffice.expenses.excel_request')}}" class="btn btn-primary pull-left" style="margin-right: 15px;">Excel</a>
						<table class="table table-striped table-bordered file-export table-responsive">
							<thead>
								<tr>
									<th style="width: 3%;"></th>
									<th>Request Code</th>
									{{-- <th>Type</th> --}}
									<th>Description</th>
									<th>Status</th>
									<th>Last Modified</th>
								</tr>
							</thead>
							<tbody>
								@foreach($requests as $index => $info)
								<tr>
									<td>{{$index+1}}</td>
									<td>{{$info->request_code}}</td>
									{{-- <td>{!!Helper::request_status_badge($info->type)!!}</td> --}}
									<td>{{$info->description}}</td>
									<td>{!!Helper::expense_status_badge($info->status)!!}</td>
									<td>{{$info->last_modified()}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endif
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Cancelling Request...</h6>
				<p>You are about to cancel a request, this action can no longer be undone, are you sure you want to proceed?</p>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Cancel</a>
			</div>
		</div>
	</div>
</div>

<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<form action="{{route('backoffice.'.$route_file.'.create')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="request_code" value="{{$request_code}}">
			<div class="modal-body">
				
	            {{-- <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Hello !</strong> For your <a href="#" class="alert-link">information! </a>, save and encode new expenses ?.
	            </div> --}}

	            <div class="row mt-2">
	            	<div class="col-md-12">
	            		<label>Request Code</label>
	            		<input type="text" name="request_code" value="{{$request_code}}" class="form-control " required="" disabled="">
	            	</div>
	            </div>

	            <div class="row mt-2">
	            	<div class="col-md-12">
	            		<label>Request Description</label>
	            		<textarea class="form-control" placeholder="Description e.g. Event Name, Business Meeting @Place, and etc." rows="5" name="description" required="">{{old('description')}}</textarea> 
	            	</div>
	            </div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Create</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div id="confirm-create-form" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<form action="{{route('backoffice.'.$route_file.'.create_form')}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Hello !</strong> For your <a href="#" class="alert-link">information! </a>, save and encode new expenses ?.
	            </div>

	            <div class="row mt-2">
	            	<div class="col-md-12">
	            		<label>Choose a request form</label>
	            		{!!Form::select("request_id", $request_list, old('request_id'), ['id' => "select2-array ",'required' => "", 'class' => "form-control select2 request_id col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
	            	</div>
	            </div>

	            <div class="row mt-2">
	            	<div class="col-md-12">
	            		<label>Expenses Form For</label>
	            		{!!Form::select("type", $types, old('type'), ['id' => "select2-array ",'required' => "", 'class' => "form-control select2 type col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
	            	</div>
	            </div>

	            <div class="row mt-2 request">
	            	<div class="col-md-12">
	            		<label>Requested Amount</label>
	            		<input type="number" name="requested_amount" class="form-control  request-field" required="">
	            	</div>
	            </div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Create</button>
			</div>
			</form>
		</div>
	</div>
</div>
@stop

@section('page-styles')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/feather/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/pace.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/toastr.css')}}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/app.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/colors.min.css')}}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/plugins/extensions/toastr.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/simple-line-icons/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-climacon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-gradient.min.css')}}">
<!-- END Page Level CSS-->

<style type="text/css">
	.dropdown-item:focus, .dropdown-item:hover {
    color: #00b5b8; 
    text-decoration: none!important;
    background-color: #ffffff!important; 
    width: inherit!important;
}
</style>

@stop

@section('page-scripts')
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script type="text/javascript">
	$(function(){
		$(".action-delete").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-delete").attr({"href" : btn.data('url')});
		});

		$(".action-create").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-create").attr({"href" : btn.data('url')});
		});

		$(".action-create-form").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-create-form").attr({"href" : btn.data('url')});
		});

		$.extend( $.fn.dataTable.defaults, {
	        autoWidth: false,
	        columnDefs: [{ 
	            orderable: false,
	            width: '100px',
	            targets: [ 5 ]
	        }],
	        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	        language: {
	            search: '<span>Filter:</span> _INPUT_',
	            lengthMenu: '<span>Show:</span> _MENU_',
	            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	        },
	        drawCallback: function () {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
	        },
	        preDrawCallback: function() {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
	        }
	    })

	    $('.datatable-basic').DataTable();

	    $('.datatable-pagination').DataTable({
	        pagingType: "simple",
	        language: {
	            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
	        }
	    });

	    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');

	    $('.dataTables_length select').select2({
	        minimumResultsForSearch: Infinity,
	        width: 'auto'
	    });
	});
</script>

<script type="text/javascript">
	var val = $(".type").val();

	console.log(val);

	if(val=="liquidation"){
		$(".request").show(500);
		$(".request-field").prop('disabled', false);
	}else{
		$(".request").hide(500);
		$(".request-field").prop('disabled', true);
	}

	$(".type").change(function(){

		var val = $(".type").val();

		if(val=="liquidation"){
			$(".request").show(500);
			$(".request-field").prop('disabled', false);
		}else{
			$(".request").hide(500);
			$(".request-field").prop('disabled', true);
		}
	});

</script>
<style>
	.buttons-copy{
		display: none;
	}
	.buttons-csv{
		display: none;
	}
</style>
@stop
