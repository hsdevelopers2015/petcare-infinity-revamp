<!DOCTYPE html>
<html class="loading" data-textdirection="ltr" lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" name="viewport">
    <meta content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities." name="description">
    <meta content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app" name="keywords">
    <meta content="{{env('APP_TITLE','Localhost')}}" name="Highly Succeed Inc.">
    <title>Login :: {{env('APP_TITLE','Localhost')}} Admin</title>
    <link href="{{asset('backoffice/app-assets/images/ico/apple-icon-120.png')}}" rel="apple-touch-icon">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet"><!-- BEGIN VENDOR CSS-->
    <link href="{{asset('backoffice/app-assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/fonts/feather/style.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/vendors/css/extensions/pace.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/vendors/css/forms/icheck/icheck.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/vendors/css/forms/icheck/custom.css')}}" rel="stylesheet" type="text/css"><!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link href="{{asset('backoffice/app-assets/css/bootstrap-extended.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/css/app.min.css')}}?v=2.2" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/css/colors.min.css')}}" rel="stylesheet" type="text/css"><!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backoffice/app-assets/css/pages/login-register.min.css')}}" rel="stylesheet" type="text/css"><!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link href="{{asset('backoffice/assets/style.css')}}" rel="stylesheet" type="text/css"><!-- END Custom CSS-->
</head>
<body class="horizontal-layout horizontal-menu 1-column bg-full-screen-image blank-page blank-page" data-col="1-column" data-menu="horizontal-menu" data-open="click">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content container center-layout mt-2">
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>

    <script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript">
    </script> <!-- BEGIN VENDOR JS-->
     <!-- BEGIN PAGE VENDOR JS-->
     
    <script src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}" type="text/javascript">
    </script> 
    <script src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}" type="text/javascript">
    </script> 
    <script src="{{asset('backoffice/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript">
    </script> 
    <script src="{{asset('backoffice/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript">
    </script> <!-- END PAGE VENDOR JS-->
     <!-- BEGIN STACK JS-->
     
    <script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript">
    </script> 
    <script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript">
    </script> 
     <!-- BEGIN PAGE LEVEL JS-->
     
    <script src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}" type="text/javascript">
    </script> 
    <script src="{{asset('backoffice/app-assets/js/scripts/forms/form-login-register.min.js')}}" type="text/javascript">
    </script> <!-- END PAGE LEVEL JS-->
</body>
</html>