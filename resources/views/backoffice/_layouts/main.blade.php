<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<head>
    @include('backoffice._components.metas')
    <!-- BEGIN VENDOR CSS-->
    @yield('page-styles')
    <style type="text/css">
        .dropdown-item:focus, .dropdown-item:hover {
            color: #00b5b8; 
            text-decoration: none!important;
            background-color: #ffffff!important; 
            width: inherit!important;
        }
    </style>
</head>
<body data-open="click" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns ">

    <!-- navbar-fixed-top-->
    @include('backoffice._components.header')



    <!-- Horizontal navigation-->
    <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
        <!-- Horizontal menu content-->
        <div data-menu="menu-container" class="navbar-container main-menu-content container center-layout">
            <!-- include includes/mixins-->
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
              @include('backoffice._components.nav')
            </ul>
        </div>
      <!-- /horizontal menu content-->
    </div>
    <!-- Horizontal navigation-->

    <div class="app-content container center-layout mt-2">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>

@include('backoffice._components.footer')

@yield('page-modals')

@include('backoffice._components.notification-status')
@yield('page-scripts')
<style>
    .buttons-excel{
        display: none!important;
    }
</style>
<script type="text/javascript">
    $(function(){

        // $("body").delegate(".nav-item","mouseover",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#f1f1f1" })
        // }).delegate(".nav-item","mouseout",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#fff" })
        // })

        // $("body").delegate(".dropdown-user","mouseover",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#47c8ca" })
        // }).delegate(".dropdown-user","mouseout",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#47c8ca" })
        // })

        // $("body").delegate(".dropdown-user","mouseover",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#47c8ca" })
        // }).delegate(".dropdown-user","mouseout",function(){
        //     var _selector = $(this);

        //     _selector.css({ 'background-color' : "#47c8ca" })
        // })

    });
</script>
</body>

</html>