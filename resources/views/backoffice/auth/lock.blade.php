@extends('backoffice._layouts.auth')
@section('content')
<div class="content-header row"></div>
<div class="content-body">
    <section class="flexbox-container">
        <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-3 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header no-border">
                    <div class="card-title text-xs-center">{{-- <img alt="branding logo" src="{{asset('backoffice/images/logo/stack-logo-dark.png')}}"> --}}
                    {{-- <h4>{{env('APP_TITLE','Localhost')}} Admin Panel</h4> --}}
                    @if($auth->filename)
                    <img src="{{asset($auth->directory.'/'.$auth->filename)}}" alt="" class="media-object avatar avatar-lg rounded-circle">
                    @else
                    <img src="{{asset('backoffice/images/face0.jpg')}}" alt="" class="media-object avatar avatar-lg rounded-circle">
                    @endif
                    </div>
                </div>
                <div class="card-body collapse in">
                    <p class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2 my-1"><span>Unlock your account</span></p>
                    <div class="card-block">
                        <form action="" class="form-horizontal" method="post">
                        <input type="hidden" name="_token" value={{csrf_token()}}>
                            @include('backoffice._components.notification')

                            <fieldset class="form-group position-relative has-icon-left">
                                <input class="form-control" id="user-password" name="password" placeholder="Enter Password" type="password">
                                <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group row">
                                <div class="col-md-6 col-xs-12 text-xs-center text-sm-left">
                                    <fieldset>
                                        <input class="chk-remember" id="remember-me" name="remember_me" type="checkbox"> <label for="remember-me">Remember Me</label>
                                    </fieldset>
                                </div>
                            </fieldset>
                            <button class="btn btn-outline-primary btn-block" type="submit"><i class="ft-unlock"></i> Login</button>
                            <a href="{{route('backoffice.logout')}}" class="btn btn-outline-danger btn-lg btn-block"><i class="ft-power"></i> Logout</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop