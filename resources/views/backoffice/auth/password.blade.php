@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">

							<h4 class="form-section"><i class="ft-lock"></i> Update Password </h4>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Current Password</label>
										<div class="col-md-7">
											<input class="form-control {{$errors->first('current_password') ? 'border-danger' : NULL}}" name="current_password" type="password" placeholder="Current Password" id="userinput5" data-toggle="tooltip" title="{{$errors->first('current_password') ? $errors->first('current_password') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">New Password</label>
										<div class="col-md-7">
											<input class="form-control {{$errors->first('password') ? 'border-danger' : NULL}}" name="password" type="password" placeholder="New Password" id="userinput5" data-toggle="tooltip" title="{{$errors->first('password') ? $errors->first('password') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Confirm Password</label>
										<div class="col-md-7">
											<input class="form-control {{$errors->first('password_confirmation') ? 'border-danger' : NULL}}" name="password_confirmation" type="password" placeholder="Confirm Password" id="userinput5" data-toggle="tooltip" title="{{$errors->first('password_confirmation') ? $errors->first('password_confirmation') : NULL}}">
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.dashboard')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
@stop
