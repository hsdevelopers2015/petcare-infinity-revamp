@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!} You may set the amount of quota need for the month.</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-cog"></i> Quota Configuration</h4>

							<div class="row mt-3">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput1">Sales Agent<br>Quota Amount</label>
										<div class="col-md-8">
											<input type="number" name="sales_quota" id="userinput1" class="form-control {{$errors->first('sales_quota') ? 'border-danger' : NULL}}" placeholder="Sales Quota Amount" value="{{old('sales_quota',$configuration? $configuration->sales_quota:NULL)}}" data-toggle="tooltip" title="{{$errors->first('sales_quota') ? $errors->first('sales_quota') : NULL}}" min="1">
											<small>Note: This is the amount needed of sales agent to consume the <code>sales incentive</code>.</small>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput13">Group Product<br>Quota Amount</label>
										<div class="col-md-8">
											<input type="number" name="group_quota" id="userinput13" class="form-control {{$errors->first('group_quota') ? 'border-danger' : NULL}}" placeholder="Group Product Quota Amount" value="{{old('group_quota',$configuration? $configuration->group_quota:NULL)}}" data-toggle="tooltip" title="{{$errors->first('group_quota') ? $errors->first('group_quota') : NULL}}" min="1">
											<small>Note: This is the <code>group product quota</code>.</small>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput12">Product Focus<br>Quota Amount</label>
										<div class="col-md-8">
											<input type="number" name="special_quota" id="userinput12" class="form-control {{$errors->first('special_quota') ? 'border-danger' : NULL}}" placeholder="Special Quota Amount" value="{{old('special_quota',$configuration? $configuration->special_quota:NULL)}}" data-toggle="tooltip" title="{{$errors->first('special_quota') ? $errors->first('special_quota') : NULL}}" min="1">
											<small>Note: This is the current <code>product focus</code> quota.</small>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<?php
										if($errors->first('date_range')){
											$status = 'border-danger';
											$msg = $errors->first('date_range');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-4 label-control" for="userinput4">Date Range</label>
										<div class="col-md-8" title="{{$errors->first('date_range')}}" data-toggle="tooltip">
											{!!Form::select("date_range", $date_ranges, old('date_range',$configuration? $configuration->date_range:NULL), ['id' => "date_range", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
											<small>Note: This is the <code>date range</code> and the <code>reset duration</code>.</small>
										</div>
									</div>
								</div>
							</div>

							<h4 class="form-section"><i class="ft-cog"></i> Mother Quota Distribution</h4>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-2 label-control" for="userinput111">Mother Quota<br>Amount</label>
										<div class="col-md-10">
											<input type="number" name="mother_quota" id="userinput111" class="form-control {{$errors->first('mother_quota') ? 'border-danger' : NULL}}" placeholder="Mother Quota Amount" value="{{old('mother_quota',$configuration? $configuration->mother_quota:NULL)}}" data-toggle="tooltip" title="{{$errors->first('mother_quota') ? $errors->first('mother_quota') : NULL}}" min="1" onchange="updateDistribution()">
											{{-- <small>Note: This is the current <code>mother quota</code> for this month.</small> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-2 label-control" for="userinput112">Luzon</label>
										<div class="col-md-10">
											<input type="number" name="luzon" id="userinput112" class="form-control {{$errors->first('luzon') ? 'border-danger' : NULL}}" placeholder="Luzon Quota Amount" value="{{old('luzon',$configuration? $configuration->luzon:NULL)}}" data-toggle="tooltip" title="{{$errors->first('luzon') ? $errors->first('luzon') : NULL}}" min="1" onchange="updateMotherQuota()" onclick="updateLuzonDistribution()">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-2 label-control" for="userinput113">Visayas</label>
										<div class="col-md-10">
											<input type="number" name="visayas" id="userinput113" class="form-control {{$errors->first('visayas') ? 'border-danger' : NULL}}" placeholder="Visayas Quota Amount" value="{{old('visayas',$configuration? $configuration->visayas:NULL)}}" data-toggle="tooltip" title="{{$errors->first('visayas') ? $errors->first('visayas') : NULL}}" min="1" onchange="updateMotherQuota()" onclick="updateVisayasDistribution()">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-2 label-control" for="userinput114">Mindanao</label>
										<div class="col-md-10">
											<input type="number" name="mindanao" id="userinput114" class="form-control {{$errors->first('mindanao') ? 'border-danger' : NULL}}" placeholder="Mindanao Quota Amount" value="{{old('mindanao',$configuration? $configuration->mindanao:NULL)}}" data-toggle="tooltip" title="{{$errors->first('mindanao') ? $errors->first('mindanao') : NULL}}" min="1" onchange="updateMotherQuota()" onclick="updateMindanaoDistribution()">
										</div>
									</div>
								</div>
							</div>

							<h4 class="form-section"><i class="ft-cog"></i> Area Distribution</h4>

							<h5 class="ml-3 mt-2"><strong>Luzon</strong></h5>
							<div class="row">
								@foreach($clusters as $index => $info)
								@if($info->island == 'luzon')
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="cluster{{$info->id}}">Area {{$info->area}} - {{Str::title($info->island)}}</label>
										<div class="col-md-8">
											<input type="number" name="area_quota[{{$info->id}}]" id="cluster{{$info->id}}" class="form-control {{$errors->first('area_quota') ? 'border-danger' : NULL}}" placeholder="area_quota Quota Amount" value="{{old('area_quota',$info->area_quota? $info->area_quota:0)}}" data-toggle="tooltip" title="{{$errors->first('area_quota') ? $errors->first('area_quota') : NULL}}" min="1" onchange="updateLuzonAreaDistribution()" required="required">
										</div>
									</div>
								</div>
								@endif
								@endforeach
							</div>
							<hr>
							<h5 class="ml-3 mt-2"><strong>Visayas</strong></h5>
							<div class="row">
								@foreach($clusters as $index => $info)
								@if($info->island == 'visayas')
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="cluster{{$info->id}}">Area {{$info->area}} - {{Str::title($info->island)}}</label>
										<div class="col-md-8">
											<input type="number" name="area_quota[{{$info->id}}]" id="cluster{{$info->id}}" class="form-control {{$errors->first('area_quota') ? 'border-danger' : NULL}}" placeholder="area_quota Quota Amount" value="{{old('area_quota',$info->area_quota? $info->area_quota:0)}}" data-toggle="tooltip" title="{{$errors->first('area_quota') ? $errors->first('area_quota') : NULL}}" min="1" onchange="updateVisayasAreaDistribution()" required="required">
										</div>
									</div>
								</div>
								@endif
								@endforeach
							</div>
							<hr>
							<h5 class="ml-3 mt-2 mb-3"><strong>Mindanao</strong></h5>
							<div class="row">
								@foreach($clusters as $index => $info)
								@if($info->island == 'mindanao')
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="cluster{{$info->id}}">Area {{$info->area}} - {{Str::title($info->island)}}</label>
										<div class="col-md-8">
											<input type="number" name="area_quota[{{$info->id}}]" id="cluster{{$info->id}}" class="form-control {{$errors->first('area_quota') ? 'border-danger' : NULL}}" placeholder="area_quota Quota Amount" value="{{old('area_quota',$info->area_quota? $info->area_quota:0)}}" data-toggle="tooltip" title="{{$errors->first('area_quota') ? $errors->first('area_quota') : NULL}}" min="1" onchange="updateMindanaoAreaDistribution()" required="required">
										</div>
									</div>
								</div>
								@endif
								@endforeach
							</div>

						</div>

						<div class="left">
						<hr>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script>
	window.updateMotherQuota = function() {
		var luz = $("#userinput112").val();
		var vis = $("#userinput113").val();
		var min = $("#userinput114").val();

		$("#userinput111").val(parseInt(luz)+parseInt(vis)+parseInt(min));
    };

    window.updateDistribution = function(){
    	var mother = $("#userinput111").val();

    	$("#userinput112").val(mother/3);
		$("#userinput113").val(mother/3);
		$("#userinput114").val(mother/3);


    };

    window.updateLuzonDistribution = function(){
    	var luzon = $("#userinput112").val();

    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "luzon"){

    			?>

    	$("#cluster{!!$info->id!!}").val(luzon/{!!$luz_count!!});

    			<?php
    		}
    	}
    	?>
    };



    window.updateLuzonAreaDistribution = function(){
    	var luzon = $("#userinput112").val();

    	var area = [];
    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "luzon"){
    			?>
    	var cluster{!!$info->id!!} = $("#cluster{!!$info->id!!}").val();
    	area.push(cluster{!!$info->id!!});
    			<?php
    		}
    	}
    	?>
    	var sum = 0;
    	for(var i = 0; i < area.length; i++){
    		sum += parseInt(area[i]);
    	}
    	console.log(sum);
    	$("#userinput112").val(sum);

    	var vis = $("#userinput113").val();
		var min = $("#userinput114").val();

    	$("#userinput111").val(parseInt(sum)+parseInt(vis)+parseInt(min));
    };


    window.updateVisayasDistribution = function(){
    	var visayas = $("#userinput113").val();

    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "visayas"){
    			?>
    	$("#cluster{!!$info->id!!}").val(visayas/{!!$vis_count!!});
    			<?php
    		}
    	}
    	?>
    };



    window.updateVisayasAreaDistribution = function(){
    	var visayas = $("#userinput113").val();

    	var area = [];
    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "visayas"){
    			?>
    	var cluster{!!$info->id!!} = $("#cluster{!!$info->id!!}").val();
    	area.push(cluster{!!$info->id!!});
    			<?php
    		}
    	}
    	?>
    	var sum = 0;
    	for(var i = 0; i < area.length; i++){
    		sum += parseInt(area[i]);
    	}
    	console.log(sum);
    	$("#userinput113").val(sum);

    	var luz = $("#userinput112").val();
		var min = $("#userinput114").val();
		
    	$("#userinput111").val(parseInt(luz)+parseInt(sum)+parseInt(min));
    };


    window.updateMindanaoDistribution = function(){
    	var mindanao = $("#userinput114").val();

    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "mindanao"){
    			?>
    	$("#cluster{!!$info->id!!}").val(mindanao/{!!$min_count!!});
    			<?php
    		}
    	}
    	?>
    };



    window.updateMindanaoAreaDistribution = function(){
    	var mindanao = $("#userinput114").val();

    	var area = [];
    	<?php
    	foreach($clusters as $index => $info){
    		if($info->island == "mindanao"){
    			?>
    	var cluster{!!$info->id!!} = $("#cluster{!!$info->id!!}").val();
    	area.push(cluster{!!$info->id!!});
    			<?php
    		}
    	}
    	?>
    	var sum = 0;
    	for(var i = 0; i < area.length; i++){
    		sum += parseInt(area[i]);
    	}
    	console.log(sum);
    	$("#userinput114").val(sum);

    	var luz = $("#userinput112").val();
		var vis = $("#userinput113").val();
		
    	$("#userinput111").val(parseInt(luz)+parseInt(vis)+parseInt(sum));
    };
</script>
@stop
