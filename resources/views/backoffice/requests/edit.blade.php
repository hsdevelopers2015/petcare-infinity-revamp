@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Edit {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		<a href="#" class="btn btn-primary pull-right action-create" data-url="{{route('backoffice.'.$route_file.'.respond',[$requests->id,"approved"])}}" data-toggle="modal" data-target="#confirm-create"><i class="ft-check"></i>&nbsp;&nbsp;Approve&nbsp;</a>
		</div>
		<a href="#" class="btn btn-danger pull-right action-create mr-2" data-url="{{route('backoffice.'.$route_file.'.respond',[$requests->id,"denied"])}}" data-toggle="modal" data-target="#confirm-create"><i class="ft-x"></i>&nbsp;&nbsp;Deny&nbsp;</a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="fa fa-file-text-o"></i>Request Form</h4>
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Quantity</label>
										<div class="col-md-9">
											<input type="text" name="request_code" id="userinput3" class="form-control prod_cat {{$errors->first('request_code') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('request_code',$requests->request_code)}}" data-toggle="tooltip" title="{{$errors->first('request_code') ? $errors->first('request_code') : NULL}}" disabled="">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Description</label>
										<div class="col-md-9">
											<textarea type="text" name="description" rows="5" id="userinput5" class="form-control prod_cat {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="" min="1" value="" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}" disabled="">{{old('description',$requests->description)}}</textarea>
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-info mr-1">
								<i class="fa fa-arrow-left"></i> Back
							</a>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            		<span aria-hidden="true">×</span>
	            	</button> --}}
	            	<strong>Note :</strong> Upon submitting it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Submit</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script>
	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});
</script>
@stop
