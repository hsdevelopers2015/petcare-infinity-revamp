@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} | Transaction Code: {{$transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>

		@if(!in_array($order->status, ['shipped','delivered']) AND $order->status == 'received' AND Auth::user()->type != 'spectator' AND Auth::user()->type != 'finance')
		<a href="#" class="btn btn-primary pull-right action-create" data-url="{{route('backoffice.'.$route_file.'.ship',[$order->id])}}" data-toggle="modal" data-target="#confirm-create"><i class="ft-check"></i>&nbsp;&nbsp;Ship this order&nbsp;</a>
		@endif
	</div>
</div>
<div class="row">
	@if(!in_array($order->status, ['shipped','delivered']) AND $order->status == 'received' AND Auth::user()->type != 'spectator' AND Auth::user()->type != 'finance')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body collapse in">
				<div class="card-block">
						<div class="form-body mt-2">
							<div class="row">
								<div class="col-md-10">
									<h4 class="form-section">
										<i class="ft-box"></i> Search for the item to check its physical count
									</h4>
								</div>
								<div class="col-md-2">
									<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#discount">
										<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Add Discount
									</a>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<?php
										if($errors->first('product_id')){
											$status = 'border-danger';
											$msg = $errors->first('product_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<div class="col-md-12" data-toggle="tooltip" title="{{$msg}}">
											{!!Form::select("product_id", $products, old('product_id'), ['id' => "select2-array ", 'class' => "form-control select2 product".$status]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<div class="col-md-12">
											<input type="number" name="qty" id="userinput3" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" min="1" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							
						</div>

				</div>
			</div>
		</div>
	</div>
	@endif
	<div class="col-md-12">
		<div class="card">
			<section id="file-export">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Item List</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Product Code</th>
												<th>Product Name</th>
												<th>Quantity</th>
												<th style="width: 20%;">Last Modified</th>
											</tr>
										</thead>
										<tbody>
										@foreach($items as $index => $info)
											@if($info->product_info($info->product_id))
											<tr>
												<td>{{$info->product_info($info->product_id)->product_code? : "--"}}</td>
												<td>{{$info->product_info($info->product_id)->product_name? : "--"}}</td>
												<td>{{$info->qty}}</td>
												<td>{{$info->last_modified()}}</td>
											</tr>
											@endif
										@endforeach
										</tbody>
									</table>
									<a href="{{route('backoffice.orders.index')}}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to the order list</a>

									@if($discounts->count() > 0 AND $order->status == 'order_confirmation')
									<a href="#" class="btn btn-outline-primary ml-1" data-toggle="modal" data-target="#discount_list">View Discounting</a>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Note :</strong> Upon shipping the quantity of the product on the order item list will subtract the product physical count.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Ship this order</a>
			</div>
		</div>
	</div>
</div>

<div id="discount" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add Discount</h5>
			</div>
			<form action="{{route('backoffice.orders.add_discount',[$order->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<input type="hidden" name="order_id" value="{{$order->id}}">
				<div class="form-body">
					<div class="row">

						<div class="col-md-4">
							<input type="text" name="discount_name" id="userinput4" min="0" class="form-control {{$errors->first('discount_name') ? 'border-danger' : NULL}}" placeholder="Discount Name" value="{{old('discount_name')}}" data-toggle="tooltip" title="{{$errors->first('discount_name') ? $errors->first('discount_name') : NULL}}" required="">
						</div>
						<div class="col-md-4">
							<input type="number" name="discount_amount" id="userinput4" min="0" class="form-control {{$errors->first('discount_amount') ? 'border-danger' : NULL}}" placeholder="Discount Amount" value="{{old('discount_amount')}}" data-toggle="tooltip" title="{{$errors->first('discount_amount') ? $errors->first('discount_amount') : NULL}}" required="">
						</div>
						<div class="col-md-4">
							{!!Form::select("discount_type", $discount_types, old('discount_type'), ['id' => "discount_type", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Add</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div id="discount_list" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Discount List</h5>
			</div>
			
			<div class="modal-body">
				<table class="table table-striped table-bordered table-responsive dataTable file-export">
					<thead>
						<tr>
							<th style="width: 3%;"></th>
							<th class="text-center">Discount Name</th>
							<th class="text-center">Amount</th>
							@if($order->status == "draft")
							<th style="width: 10%;"></th>
							@endif
						</tr>
					</thead>
					<tbody>
						@foreach($discounts as $index => $info)
						<tr>
							<td>{{$index+1}}</td>
							<td>{{$info->discount_name}}</td>
							@if($info->discount_type == 'percentage')
							<td class="text-center">PHP {{number_format($info->order_items($order->id)*($info->discount_amount/100),2)}} ({{$info->discount_amount}}%) </td>
							@else
							<td class="text-center">PHP {{number_format($info->discount_amount,2)}}</td>
							@endif
							@if($order->status == "draft")
							<td>
							<a href="{{route('backoffice.'.$route_file.'.remove_discount',[$info->id])}}" class="danger"><i class="fa fa-trash"></i></a>
							</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});

	$( ".product" ).change(function() {
		var val = $(this).val();
		<?php 
		foreach($product_lists as $index => $info){
		?>
			if(val == "<?php echo $info->id ;?>"){
				var input_3 = $("#userinput3").val("<?php echo $info->physical_count($info->id) ? $info->physical_count($info->id)->quantity : NULL;?>");
				console.log('<?php echo $info->physical_count($info->id) ? $info->physical_count($info->id)->quantity : NULL;?>');
			}
		<?php
		}
		?>
	});
</script>

<script src="{{asset('backoffice/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(".payment_term").ready(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});
	$(".mod").change(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});
</script>
@stop
