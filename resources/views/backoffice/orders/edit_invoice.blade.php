@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} of {{$order->transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Edit Invoice {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">Edit Invoice Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<form class="form form-horizontal" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section">
								<i class="ft-box"></i>Edit Invoice Form
								<a href="#" target="_blank"></a>
							</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Approved by</label>
										<input type="text" name="approved_by" value="{{old('approved_by',$order->approved_by)}}" class="form-control {{$errors->first('approved_by') ? 'border-danger' : NULL}}" data-toggle="tooltip" title="{{$errors->first('approved_by') ? $errors->first('approved_by') : NULL}}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Delivery Receipt Number</label>
										<input type="text" name="dr_number" value="{{old('dr_number',$order->dr_number)}}" class="form-control {{$errors->first('dr_number') ? 'border-danger' : NULL}}" data-toggle="tooltip" title="{{$errors->first('dr_number') ? $errors->first('dr_number') : NULL}}">
									</div>
								</div>
								{{-- <div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput1">Vatable Sales<br><small class="text-warning">(optional)</small></label>
										<div class="col-md-8">
											<input type="text" name="vat_sales" id="userinput1" class="form-control {{$errors->first('vat_sales') ? 'border-danger' : NULL}}" placeholder="Vatable Sales" value="{{old('vat_sales')}}" data-toggle="tooltip" title="{{$errors->first('vat_sales') ? $errors->first('vat_sales') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput1">Vat-Exempt Sales<br><small class="text-warning">(optional)</small></label>
										<div class="col-md-8">
											<input type="text" name="vat_ex" id="userinput1" class="form-control {{$errors->first('vat_ex') ? 'border-danger' : NULL}}" placeholder="Vat-Exempt Sales" value="{{old('vat_ex')}}" data-toggle="tooltip" title="{{$errors->first('vat_ex') ? $errors->first('vat_ex') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput1">Zero Rated Sales<br><small class="text-warning">(optional)</small></label>
										<div class="col-md-8">
											<input type="text" name="zero" id="userinput1" class="form-control {{$errors->first('zero') ? 'border-danger' : NULL}}" placeholder="Zero Rated Sales" value="{{old('zero')}}" data-toggle="tooltip" title="{{$errors->first('zero') ? $errors->first('zero') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-4 label-control" for="userinput1">Vat Amount<br><small class="text-warning">(optional)</small></label>
										<div class="col-md-8">
											<input type="text" name="vat_amount" id="userinput1" class="form-control {{$errors->first('vat_amount') ? 'border-danger' : NULL}}" placeholder="Vat Amount" value="{{old('vat_amount')}}" data-toggle="tooltip" title="{{$errors->first('vat_amount') ? $errors->first('vat_amount') : NULL}}">
										</div>
									</div>
								</div>
							</div> --}}
								<div class="col-md-1 mr-1">
									<button type="submit" class="btn btn-primary overlay-unblock">
										<i class="fa fa-check-square-o"></i> Update
									</button>
								</div>
								<div class="col-md-1">
									<a class="btn btn-primary" href="{{route('backoffice.orders.po',[$order->id])}}" target="_blank">
										<i class="fa fa-eye"></i> View
									</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 mb-3">
		<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to Order List</a>
	</div>
</div>

@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Save and Print Sale</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});

	$( ".product" ).change(function() {
		var val = $(this).val();
		<?php 
		foreach($product_lists as $index => $info){
		?>
			if(val == "<?php echo $info->id ;?>"){
				$("#userinput6").val("<?php echo $info->cost;?>");
				$("#userinput3").attr('max',"<?php echo $info->physical_count($info->id)? $info->physical_count($info->id)->quantity : NULL;?>");
			}
		<?php
		}
		?>
	});
</script>

<script src="{{asset('backoffice/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

@stop