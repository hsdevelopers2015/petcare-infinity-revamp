@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} of {{$sales->transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">Delivery Receipt Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<form class="form form-horizontal" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="reference_id" value="{{$sales->id}}">
					<input type="hidden" name="reference" value="orders">
						<div class="form-body">
							<h4 class="form-section">
								<i class="ft-box"></i>Delivery Receipt Form
								<a href="#" target="_blank"></a>
							</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Approved by</label>
										<input type="text" name="approved_by" value="{{old('approved_by',$delivery_receipt->approved_by)}}" class="form-control {{$errors->first('approved_by') ? 'border-danger' : NULL}}" data-toggle="tooltip" title="{{$errors->first('approved_by') ? $errors->first('approved_by') : NULL}}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Sales Invoice Number</label>
										<input type="text" name="si_number" value="{{old('si_number',$delivery_receipt->si_number)}}" class="form-control {{$errors->first('si_number') ? 'border-danger' : NULL}}" data-toggle="tooltip" title="{{$errors->first('si_number') ? $errors->first('si_number') : NULL}}">
									</div>
								</div>
								<div class="col-md-1 mr-1">
									<button type="submit" class="btn btn-primary overlay-unblock">
										<i class="fa fa-check-square-o"></i> Update
									</button>
								</div>
								<div class="col-md-1">
									<a class="btn btn-primary" href="{{route('backoffice.'.$route_file.'.view_dr',$delivery_receipt->id.'-'.Str::slug($delivery_receipt->dr_number))}}" target="_blank">
										<i class="fa fa-eye"></i> View
									</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 mb-3">
		<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to Sales List</a>
	</div>
</div>

@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Save and Print Sale</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')


<script src="{{asset('backoffice/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

@stop
