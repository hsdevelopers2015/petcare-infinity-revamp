@extends('backoffice._layouts.main')

@section('content')
<section id="">
	<div class="row">

	    <div class="col-xs-4">
	        <a href="#">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                                <h3 class="success">0</h3>
	                                <span class="text-success">Suppliers</span>
	                            </div>
	                            <div class="media-right media-middle">
	                                <i class="icon-wallet success font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>



	    <div class="col-xs-4">
	        <a href="http://vetcare.localhost.com/sales">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                            <h3 class="info">0</h3>
	                                <span class="text-info">Branches</span>
	                            </div>
	                            <div class="media-right media-middle">
	                            <i class="fa fa-money info font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>

	    <div class="col-xs-4">
	        <a href="#">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                            <h3 class="warning">0</h3>
	                                <span class="text-warning">Purchase Order</span>
	                            </div>
	                            <div class="media-right media-middle">
	                            <i class="icon-basket-loaded warning font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>

	    <div class="col-xs-4">
	        <a href="#">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                            <h3 class="danger">0</h3>
	                                <span class="text-danger">In Transit</span>
	                            </div>
	                            <div class="media-right media-middle">
	                            <i class="fa fa-truck danger font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>
	    <div class="col-xs-4">
	        <a href="#">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                            <h3 class="danger">0</h3>
	                                <span class="text-danger">In Transit</span>
	                            </div>
	                            <div class="media-right media-middle">
	                            <i class="fa fa-truck danger font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>
	    <div class="col-xs-4">
	        <a href="#">
	            <div class="card">
	                <div class="card-body">
	                    <div class="card-block" style="background-color: rgb(255, 255, 255);">
	                        <div class="media">
	                            <div class="media-body text-xs-left">
	                            <h3 class="danger">0</h3>
	                                <span class="text-danger">In Transit</span>
	                            </div>
	                            <div class="media-right media-middle">
	                            <i class="fa fa-truck danger font-large-2 float-xs-right"></i>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </a>
	    </div>
	</div>
	<div class="row">
		<div class="col-md-6">
	        <div class="card" style="height: 500px;">
	            <div class="card-header no-border">
	                <h4 class="card-title">Recent Transactions</h4>
	                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                <div class="heading-elements">
	                    <ul class="list-inline mb-0">
	                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
	                    </ul>
	                </div>
	            </div>
	            <div class="card-content">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>Goals</th>
                                <th>Goal Value</th>
                                <th>Conversion rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Account</td>
                                <td>$0.18</td>
                                <td class="text-center font-small-2">85% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 95%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Subscribe</td>
                                <td>$0.12</td>
                                <td class="text-center font-small-2">75% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Mobile</td>
                                <td>$220</td>
                                <td class="text-center font-small-2">65% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-warning" role="progressbar" style="width: 55%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Laptop</td>
                                <td>$880</td>
                                <td class="text-center font-small-2">35% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-warning" role="progressbar" style="width: 45%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>LED TV</td>
                                <td>$1002</td>
                                <td class="text-center font-small-2">25% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-warning" role="progressbar" style="width: 30%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>AC</td>
                                <td>$1200</td>
                                <td class="text-center font-small-2">15% 
                                <div class="progress progress-sm mt-1 mb-0">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 15%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
	            </div>
	        </div>
		</div>
	    		<div class="col-md-6">
	    	        <div class="card" style="height: 500px;">
	    	            <div class="card-header no-border">
	    	                <h4 class="card-title">Recent Transactions</h4>
	    	                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	    	                <div class="heading-elements">
	    	                    <ul class="list-inline mb-0">
	    	                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
	    	                    </ul>
	    	                </div>
	    	            </div>
	    	            <div class="card-content">
	                        <table class="table mb-0">
	                            <thead>
	                                <tr>
	                                    <th>Goals</th>
	                                    <th>Goal Value</th>
	                                    <th>Conversion rate</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <td>Account</td>
	                                    <td>$0.18</td>
	                                    <td class="text-center font-small-2">85% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-success" role="progressbar" style="width: 95%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>Subscribe</td>
	                                    <td>$0.12</td>
	                                    <td class="text-center font-small-2">75% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>Mobile</td>
	                                    <td>$220</td>
	                                    <td class="text-center font-small-2">65% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-warning" role="progressbar" style="width: 55%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>Laptop</td>
	                                    <td>$880</td>
	                                    <td class="text-center font-small-2">35% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-warning" role="progressbar" style="width: 45%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>LED TV</td>
	                                    <td>$1002</td>
	                                    <td class="text-center font-small-2">25% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-warning" role="progressbar" style="width: 30%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>AC</td>
	                                    <td>$1200</td>
	                                    <td class="text-center font-small-2">15% 
	                                    <div class="progress progress-sm mt-1 mb-0">
	                                      <div class="progress-bar bg-success" role="progressbar" style="width: 15%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                    </div>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	    	            </div>
	    	        </div>
	    		</div>
		
	</div>
	

</section>





@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" />
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script src="{{asset('backoffice/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/pages/dashboard-analytics.min.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>




@stop