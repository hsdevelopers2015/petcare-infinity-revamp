@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} | {{$transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>

		<button class="btn btn-primary btn-md pull-right action-confirm" type="button" data-url="{{route('backoffice.stock_out.for_delivery',['id' => $sales->id])}}" data-toggle="modal" data-target="#approve"><i class="fa fa-check"></i>&nbsp;&nbsp; Confirm</button>
		
		<button class="btn btn-danger btn-md pull-right action-cancel" type="button" data-url="{{route('backoffice.sales.cancel',['id' => $sales->id])}}" data-toggle="modal" data-target="#cancel" style="margin-right: 10px;"><i class="fa fa-times"></i>&nbsp;&nbsp; Cancel</button>

	</div>
</div>

<div class="row">
	
	<div class="col-md-12">
		<div class="card">
			<section id="file-export">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Item List</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
									<table class="table table-striped table-bordered dataTable file-export">
										<thead>
											<tr>
												<th class="text-center">Product Info</th>
												<th class="text-center">Qty.</th>
												<th class="text-center">Free</th>
												<th class="text-center">Price/Unit</th>
												<th class="text-center">Total Price</th>
												<th class="text-center">Discount</th>
												<th class="text-center">Final Price</th>
												@if($sales->status == "draft")
												<th class="text-center"></th>
												@endif
											</tr>
										</thead>
										<tbody>
											<?php
												$total_cost = [];
											?>
											@foreach($items as $index => $info)
											@if($info->product_info($info->product_id))
											<tr>
												<td class="text-center">
													{{$info->product_info($info->product_id)->product_code}}<br>
													{{$info->product_info($info->product_id)->product_name}}
												</td>
												<td class="text-center">{{$info->qty}}</td>
												<td class="text-center">{{$info->free}}</td>
												<td class="text-center">{{$info->cost_unit}}</td>
												<td class="text-center">{{number_format($info->qty*$info->cost_unit,2)}}</td>
												<?php 
												$total = $info->cost_unit * $info->qty;
												array_push($total_cost,$info->final_cost);
												?>
												<td class="text-center">{{$info->discount_type=="amount"? $info->discount :$total*($info->discount/100) }}</td>
												<td class="text-center">{{number_format($info->final_cost,2)}}</td>
												@if($sales->status == "draft")
												<td class="text-center">
													<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
														<div role="group" class="btn-group">
															<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
																<i class="ft-cog icon-left"></i> Action
															</button>
															<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
																<a href="{{route('backoffice.'.$route_file.'.remove',[$info->id])}}" class="dropdown-item">Remove Item</a>
															</div>
														</div>
													</div>
												</td>
												@endif
											</tr>
											@endif
											@endforeach
											<tr>
												<td colspan="6" style="text-align: right;">Total</td>
												<td colspan="2" style="text-align: left; color: green"><strong>{{number_format(array_sum($total_cost),2)}}</strong></td>
											</tr>
										</tbody>
									</table>
									<a href="{{ route('backoffice.stock_out.index') }}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to the stock out list</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@stop

@section('page-modals')
<div id="approve" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            		<span aria-hidden="true">×</span>
	            	</button> --}}
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm">Confirm</a>
			</div>
		</div>
	</div>
</div>

<div id="cancel" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            		<span aria-hidden="true">×</span>
	            	</button> --}}
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-cancel">Confirm</a>
			</div>
		</div>
	</div>
</div>

@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">

	$(".action-cancel").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-cancel").attr({"href" : btn.data('url')});
	});

	$(".action-confirm").on("click",function(){
		var btn = $(this);
		$("#btn-confirm").attr({"href" : btn.data('url')});
	});



</script>

<script src="{{asset('backoffice/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
@stop
