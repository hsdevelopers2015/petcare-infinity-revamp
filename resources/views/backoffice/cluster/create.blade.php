@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>

					
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-user"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<?php
										if($errors->first('area')){
											$status = 'border-danger';
											$msg = $errors->first('area');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="brand_name">Cluster</label>
										<div class="col-md-9">
											{!!Form::select("island", $islands, old('island'), ['id' => "island", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<?php
										if($errors->first('area')){
											$status = 'border-danger';
											$msg = $errors->first('area');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="brand_name">Area</label>
										<div class="col-md-9">
											{!!Form::select("area", $areas, old('area'), ['id' => "area", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="brand_name">City</label>
										<div class="col-md-9">
											<input type="text" name="city" id="userinput2" class="form-control prod_cat {{$errors->first('city') ? 'border-danger' : NULL}}" placeholder="City" value="{{old('city')}}" data-toggle="tooltip" title="{{$errors->first('city') ? $errors->first('city') : NULL}}">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script type="text/javascript">


</script>
@stop
