@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
	</div>
</div>
<section id="user-profile-cards-with-stats" class="row">
    <div class="col-xs-12">
        <p>These are the lists of Clients of {{env('APP_TITLE','Localhost')}}.</p>
        <hr>
    </div>
    @foreach($clients as $index => $data)
    <div class="col-xl-4 col-md-6 col-xs-12">
    	<div class="card profile-card-with-stats {{$data->type == 'sales_head'? 'border-info':NULL}}">
    		<div class="text-xs-center">
    			<div class="card-block">
    				<a href="{{route('backoffice.profile.user_profile',[$data->username])}}">
    					@if($data->filename)
    					<img src="{{asset($data->directory.'/thumbnails/'.$data->filename)}}" class="rounded-circle  height-150" alt="Card image">
    					@else
    					<img src="{{asset('backoffice/face0.jpg')}}" class="rounded-circle  height-150" alt="Card image">
    					@endif
    				</a>
    			</div>
    			<div class="card-block">
                    <h3 class="card-title">{{$data->business_info->business_name}}</h3>
                    <p>{{$data->fname.' '.$data->lname}}</p>
    				<ul class="list-inline list-inline-pipe">
    					<li>{{$data->email}}</li>
    					<li>{{$data->contact}}</li>
    				</ul>
    				<h6 class="card-subtitle text-muted">{{Str::title(str_replace('_',' ',$data->type))}}</h6>
    			</div>
    			<div class="btn-group mb-3" role="group" aria-label="Profile example">
    				<button onclick="window.location.href='<?php echo route('backoffice.profile.user_profile',[$data->username]);?>'" type="button" title="Purchased Products" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
    					<span class="ladda-label">
    						<i class="icon-bag"></i>
    						<span>{{$data->total_purchased($data->id)->count()}}</span>
    					</span>
    					<span class="ladda-spinner"></span>
    				</button>
    				<button onclick="window.location.href='<?php echo route('backoffice.profile.user_profile',[$data->username]);?>'" type="button" title="On Going Orders" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
    					<span class="ladda-label">
    						<i class="icon-basket-loaded"></i>
    						<span>{{$data->total_orders($data->id)->count()}}</span>
    					</span>
    					<span class="ladda-spinner"></span>
    				</button>
    				<button onclick="window.location.href='<?php echo route('backoffice.profile.user_profile',[$data->username]);?>'" type="button" title="Invoice" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
    					<span class="ladda-label">
    						<i class="fa fa-file-text-o"></i>
    						<span>{{$data->invoice_count($data->id)->count()}}</span>
    					</span>
    					<span class="ladda-spinner"></span>
    				</button>
    			</div>
		    </div>
		</div>
	</div>
	@endforeach
</section>
@stop

@section('page-modals')

@stop

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.card').matchHeight();
    });
</script>
@stop
