@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
	</div>
</div>
<section id="user-profile-cards-with-stats" class="row">
    <div class="col-xs-12">
        <p>These are the lists of sales agents of {{env('APP_TITLE','Localhost')}}.</p>
        <hr>
    </div>
@if($type == 'Luzon' OR $type == 'all')
    <h3 class="ml-2">TEAM LUZON</h3>
    <div class="row mt-2">
    @foreach($clusters as $index => $info)
		@if($info->island == 'luzon')
			@foreach($info->sa_lists($info->id) as $index => $data)
			<div class="col-xl-4 col-md-6 col-xs-12">
				<div class="card profile-card-with-stats {{$data->type == 'sales_head'? 'border-info':NULL}}">
					<div class="text-xs-center">
						<div class="card-block">
							<h4 class="mb-1" style="text-align: left;">{{-- {$info->city} - --}}{{"Area {$info->area}"}}</h4>
							<hr class="mb-2">
							<a href="{{route('backoffice.profile.user_profile',[$data->username])}}">
								@if($data->filename)
								<img src="{{asset($data->directory.'/thumbnails/'.$data->filename)}}" class="rounded-circle  height-150" alt="Card image">
								@else
								<img src="{{asset('backoffice/face0.jpg')}}" class="rounded-circle  height-150" alt="Card image">
								@endif
							</a>
						</div>
						<div class="card-block">
							<h4 class="card-title">{{$data->fname.' '.$data->lname}}</h4>
							<ul class="list-inline list-inline-pipe">
								<li>{{$data->email}}</li>
								<li>{{$data->contact}}</li>
							</ul>
							<h6 class="card-subtitle text-muted">{{Str::title(str_replace('_',' ',$data->type))}}</h6>
						</div>
						@if($data->type=="sales_agent")
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-business-{{$data->id}}" type="button" title="Clients" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-user-o"></i>
									<span>{{$data->client_count($data->cluster_id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div>
						{{-- @else
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-team-{{$data->id}}" type="button" title="Team Members" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-users"></i>
									<span>{{$data->team_member($data->id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div> --}}
						@endif
		                {{-- <div class="card-block">
		                    <p> Lorem ipsum dolor sit amet, autem imperdiet et nam. Nullam labores id quo, sed ei.</p>
		                    <button type="button" class="btn btn-outline-danger btn-md mr-1"><i class="fa fa-plus"></i> Follow</button>
		                    <button type="button" class="btn btn-outline-primary btn-md"><i class="ft-user"></i> Profile</button>
		                </div> --}}
		            </div>
		        </div>
		    </div>
		     @endforeach
		@endif
    @endforeach
    </div>
    <hr>
@endif

@if($type == 'Visayas' OR $type == 'all')
	<h3 class="ml-2">TEAM VISAYAS</h3>
	<div class="row mt-2">
	 @foreach($clusters as $index => $info)
		@if($info->island == 'visayas')
			@foreach($info->sa_lists($info->id) as $index => $data)
			<div class="col-xl-4 col-md-6 col-xs-12">
				<div class="card profile-card-with-stats {{$data->type == 'sales_head'? 'border-info':NULL}}">
					<div class="text-xs-center">
						<div class="card-block">
							<h4 class="mb-1" style="text-align: left;">{{"Area {$info->area}"}}</h4>
							<hr class="mb-2">
							<a href="{{route('backoffice.profile.user_profile',[$data->username])}}">
								@if($data->filename)
								<img src="{{asset($data->directory.'/thumbnails/'.$data->filename)}}" class="rounded-circle  height-150" alt="Card image">
								@else
								<img src="{{asset('backoffice/face0.jpg')}}" class="rounded-circle  height-150" alt="Card image">
								@endif
							</a>
						</div>
						<div class="card-block">
							<h4 class="card-title">{{$data->fname.' '.$data->lname}}</h4>
							<ul class="list-inline list-inline-pipe">
								<li>{{$data->email}}</li>
								<li>{{$data->contact}}</li>
							</ul>
							<h6 class="card-subtitle text-muted">{{Str::title(str_replace('_',' ',$data->type))}}</h6>
						</div>
						@if($data->type=="sales_agent")
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-business-{{$data->id}}" type="button" title="Clients" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-user-o"></i>
									<span>{{$data->client_count($data->cluster_id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div>
						{{-- @else
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-team-{{$data->id}}" type="button" title="Team Members" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-users"></i>
									<span>{{$data->team_member($data->id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div> --}}
						@endif
		            </div>
		        </div>
		    </div>
		     @endforeach
		@endif
    @endforeach
    </div>
    <hr>
@endif

@if($type == 'Mindanao' OR $type == 'all')
    <h3 class="ml-2">TEAM MINDANAO</h3>
	<div class="row mt-2">
	 @foreach($clusters as $index => $info)
		@if($info->island == 'mindanao')
			@foreach($info->sa_lists($info->id) as $index => $data)
			<div class="col-xl-4 col-md-6 col-xs-12">
				<div class="card profile-card-with-stats {{$data->type == 'sales_head'? 'border-info':NULL}}">
					<div class="text-xs-center">
						<div class="card-block">
							<h4 class="mb-1" style="text-align: left;">{{"Area {$info->area}"}}</h4>
							<hr class="mb-2">
							<a href="{{route('backoffice.profile.user_profile',[$data->username])}}">
								@if($data->filename)
								<img src="{{asset($data->directory.'/thumbnails/'.$data->filename)}}" class="rounded-circle  height-150" alt="Card image">
								@else
								<img src="{{asset('backoffice/face0.jpg')}}" class="rounded-circle  height-150" alt="Card image">
								@endif
							</a>
						</div>
						<div class="card-block">
							<h4 class="card-title">{{$data->fname.' '.$data->lname}}</h4>
							<ul class="list-inline list-inline-pipe">
								<li>{{$data->email}}</li>
								<li>{{$data->contact}}</li>
							</ul>
							<h6 class="card-subtitle text-muted">{{Str::title(str_replace('_',' ',$data->type))}}</h6>
						</div>
						@if($data->type=="sales_agent")
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-business-{{$data->id}}" type="button" title="Clients" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-user-o"></i>
									<span>{{$data->client_count($data->cluster_id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div>
						{{-- @else
						<div class="btn-group mb-3" role="group" aria-label="Profile example">
							<button href="#" data-toggle="modal" data-target="#view-team-{{$data->id}}" type="button" title="Team Members" data-toggle="tooltip" class="btn btn-float box-shadow-0 btn-outline-info">
								<span class="ladda-label">
									<i class="fa fa-users"></i>
									<span>{{$data->team_member($data->id)->count()}}</span>
								</span>
								<span class="ladda-spinner"></span>
							</button>
						</div> --}}
						@endif
		            </div>
		        </div>
		    </div>
			@endforeach
		@endif
	@endforeach
	</div>
@endif


</section>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>

@stop

@foreach($sales_agents as $index => $info)
<div id="view-business-{{$info->id}}" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$info->fname}}'s Clients </h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Information!</span> These are the information about the {{$info->fname}}'s clients.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>
				
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="card" style="height: 469px;">
	            			<div class="card-header">
	            				<h4 class="card-title">Clients</h4>
	            				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	            				<div class="heading-elements">
	            					<ul class="list-inline mb-0">
	            						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
	            					</ul>
	            				</div>
	            			</div>
	            			<div class="card-body">
	            				<div id="friends-activity" class="list-group height-400 position-relative ps-container ps-theme-default ps-active-y" data-ps-id="189e53c2-1904-37af-2b63-3f3864f462cc">
	            					<table class="table table-striped table-bordered dataTable file-export">
	            						<thead>
	            							<tr>
	            								<th>Name</th>
	            								<th>Contact Info</th>
	            								<th>Address</th>
	            							</tr>
	            						</thead>
	            						<tbody>
	            							@if($info->clients_count($info->cluster_id)->count() > 0)
	            							@foreach($info->clients_count($info->cluster_id) as $index => $data)
	            							<tr>
	            								<td><a href="{{route('backoffice.profile.user_profile',[$data->username])}}">{{$data->fname.' '.$data->lname}}</a></td>
	            								<td>{{$data->contact}}<br><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
	            								<td>{{Str::limit($data->address,$limit=15)}}</td>
	            							</tr>
	            							@endforeach
	            							@else
											<tr>
												<td colspan="3" style="text-align: center;">No Clients Yet</td>
											</tr>
	            							@endif
	            						</tbody>
	            					</table>
	            					{{-- <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
	            						<div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
	            					</div>
	            					<div class="ps-scrollbar-y-rail" style="top: 0px; height: 400px; right: 3px;">
	            						<div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 296px;"></div>
	            					</div> --}}
	            				</div>
	            			</div>
	            		</div>
	            	</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach

@foreach($sales_agents as $index => $info)
<div id="view-team-{{$info->id}}" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$info->fname}}'s Team Members </h5>
			</div>

			<div class="modal-body">
				
				{{-- <div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Information!</span> These are the information about the {{$info->fname}}'s clients.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div> --}}
				
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="card" style="height: 469px;">
	            			<div class="card-header">
	            				<h4 class="card-title">Team {{$info->cluster($info->cluster_id)?$info->cluster($info->cluster_id)->island:'---'}} ( {{$info->cluster($info->cluster_id)?$info->cluster($info->cluster_id)->city:'---'}} - Area no. {{$info->cluster($info->cluster_id)?$info->cluster($info->cluster_id)->area:'---'}} )</h4>
	            				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	            				<div class="heading-elements">
	            					<ul class="list-inline mb-0">
	            						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
	            					</ul>
	            				</div>
	            			</div>
	            			<div class="card-body">
	            				<div id="friends-activity" class="list-group height-400 position-relative ps-container ps-theme-default ps-active-y" data-ps-id="189e53c2-1904-37af-2b63-3f3864f462cc">
	            					<table class="table table-striped table-bordered dataTable file-export">
	            						<thead>
	            							<tr>
	            								<th>Name</th>
	            								<th>Contact Info</th>
	            								<th>Position</th>
	            								<th>Address</th>
	            							</tr>
	            						</thead>
	            						<tbody>
											@if($info->team_member($info->id)->count() >0 )
	            							@foreach($info->team_member($info->id) as $index => $data)
	            							<tr>
	            								<td>{{$data->fname.' '.$data->lname}}</td>
	            								<td>{{$data->contact}}<br><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
	            								<td>{{str_replace('_', ' ', Str::upper($data->type))}}</td>
	            								<td>{{Str::limit($data->address,$limit=15)}}</td>
	            							</tr>
	            							@endforeach
	            							@else
											<tr>
												<td colspan="4" style="text-align: center;">No Members Yet</td>
											</tr>
	            							@endif
	            						</tbody>
	            					</table>
	            					
	            				</div>
	            			</div>
	            		</div>
	            	</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.card').matchHeight();
    });
</script>
@stop
