@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Edit {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-box"></i>About The Product</h4>
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput1">Product Name</label>
										<div class="col-md-9">
											<input type="text" name="product_name" id="userinput1" class="form-control prod_cat {{$errors->first('product_name') ? 'border-danger' : NULL}}" placeholder="Product Name" value="{{old('product_name',$product->product_name)}}" data-toggle="tooltip" title="{{$errors->first('product_name') ? $errors->first('product_name') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2">Category</label>
										<div class="col-md-9">
											<input type="text" name="category" id="userinput2" class="form-control prod_cat {{$errors->first('category') ? 'border-danger' : NULL}}" placeholder="Category" value="{{old('category',$product->category)}}" data-toggle="tooltip" title="{{$errors->first('category') ? $errors->first('category') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Quantity</label>
										<div class="col-md-9">
											<input type="number" name="quantity" id="userinput3" class="form-control prod_cat {{$errors->first('quantity') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('quantity',$product->quantity)}}" data-toggle="tooltip" title="{{$errors->first('quantity') ? $errors->first('quantity') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput4">Product Code</label>
										<div class="col-md-9">
											<input type="text" name="product_code" id="userinput4" class="form-control {{$errors->first('product_code') ? 'border-danger' : NULL}}" placeholder="" value="{{old('product_code')}}" data-toggle="tooltip" title="{{$errors->first('product_code') ? $errors->first('product_code') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	 function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 14.599512,
       lng: 120.984222
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>
<script>
$(".prod_cat").ready(function(){
	var x = $("#userinput1").val();
	var y = $("#userinput2").val();

	if(x != "" && y != ""){
		var z = y.toUpperCase().substring(0,2) +'-'+ x.toUpperCase().substring(0,4)+'-'+ Math.floor((Math.random() * 10000) + 1);
		$("#userinput4").attr('value',z);
		// $("#userinput4").prop('disabled', true);
	}
});
</script>
@stop
