@if(Session::has('notification-status'))

@if(Session::get('notification-status') == "info")
<div class="alert bg-info alert-icon-left alert-dismissible fade in mb-2" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<strong>{{Session::get('notification-title',"Information")}} !</strong> {!! Session::get('notification-msg') !!}.
</div>
@endif

@if(Session::get('notification-status') == "success")
<div class="alert bg-success alert-icon-left alert-dismissible fade in mb-2" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<strong>{{Session::get('notification-title',"Success")}}!</strong> {!! Session::get('notification-msg') !!}.
</div>
@endif

@if(Session::get('notification-status') == "failed")
<div class="alert bg-danger alert-icon-left alert-dismissible fade in mb-2" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<strong>{{Session::get('notification-title',"Failed")}}!</strong> {!! Session::get('notification-msg') !!}.
</div>
@endif

@if(Session::get('notification-status') == "warning")
<div class="alert bg-warning alert-icon-left alert-dismissible fade in mb-2" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<strong>{{Session::get('notification-title',"Warning")}}!</strong> {!! Session::get('notification-msg') !!}.
</div>
@endif



@endif