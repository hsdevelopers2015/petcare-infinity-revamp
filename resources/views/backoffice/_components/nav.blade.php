<li class="nav-item {{Helper::is_active($routes['dashboard'])? 'active':NULL}}">
    <a class="nav-link" href="{{route('backoffice.dashboard')}}"><i class="ft-home"></i><span>Dashboard</span></a>
</li>

<li data-menu="dropdown" class="dropdown nav-item {{Helper::is_active($routes['master_file'])? 'active':NULL}}">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link" aria-expanded="true"><i class="ft-folder"></i><span>Master File</span></a>
    <ul class="dropdown-menu">
      
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Branch</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.branches.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_branches->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.branches.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Supplier</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.supplier.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_supplier->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.supplier.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Units</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.measurement.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_category->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.measurement.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Categories</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.category.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_category->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.category.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Brands</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.brand.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_brand->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.brand.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Products</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.products.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_products->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.products.create')}}">Create</a>
            </li>
            @endif

            @if(in_array($auth->type, ['super_user','admin','sales_head','sales_agent','auditor']))
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.products.promote')}}">Promotions</a>
            </li>
            @endif
          </ul>
      </li>

    
      
      @if(in_array($auth->type, ['super_user','admin','sales_head','sales_agent','auditor']) OR in_array($auth->id, [$module_clients->user_ids]))
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Cluster</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.cluster.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_cluster->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.cluster.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
          <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Clients</a>
          <ul class="dropdown-menu">
              <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.clients.index')}}">Record Data</a>
            </li>
             @if(in_array($auth->type, ['super_user','admin']) OR in_array($auth->id, [$module_clients->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.clients.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
      @endif

      @if(in_array($auth->type, ['super_user']))
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">User</a>
        <ul class="dropdown-menu">
          <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.employee.index')}}">Record Data</a>
            </li>
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.employee.create')}}">Create</a>
            </li>
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.employee.users_credentials')}}">Credentials</a>
            </li>
          </ul>
      </li>
      @endif
      

        

    
      @if(in_array($auth->type, ['admin','super_user']))
      <li>
        <a href="{{route('backoffice.access_control.index')}}" class="dropdown-item">Access Control</a>
      </li>
      @endif
  </ul>
</li>
@if(in_array($auth->type, ['super_user','admin','encoder','auditor']))
<li data-menu="dropdown" class="dropdown nav-item {{Helper::is_active($routes['master_file'])? 'active':NULL}}">

    <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link" aria-expanded="true"><i class="ft-box"></i><span>Transactions</span></a>
    <ul class="dropdown-menu">
      {{--
         <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Purchase Order</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.purchase_entry_hdr.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_supplier->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.purchase_entry_hdr.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li>
        --}}
         <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Purchase Order</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.transaction_hdr.index')}}">Record Data</a>
            </li>

            @if(in_array($auth->type, ['super_user','admin','auditor','encoder']) OR in_array($auth->id, [$module_supplier->user_ids]))
            <li data-menu="">
                <a class="dropdown-item" href="{{route('backoffice.transaction_hdr.create')}}">Create</a>
            </li>
            @endif
          </ul>
      </li> 
       
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Purchase Received</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.purchase_received_hdr.index')}}">Record Data</a>
            </li>
          </ul>
      </li> 
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Stock Transfer Out</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.stock_transfer_out.index')}}">Record Data</a>
            </li>
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.stock_transfer_out.create')}}">Create</a>
            </li>
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Stock Transfer In</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.stock_transfer_in.index')}}">Record Data</a>
            </li>
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Purchase Return</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.purchase_return.index')}}">Record Data</a>
            </li>
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.purchase_return.create')}}">Create</a>
            </li>
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Sales</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.sales_return.index')}}">Record Data</a>
            </li>
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.sales_return.create')}}">Create</a>
            </li>
          </ul>
      </li>
      <li data-menu="dropdown-submenu" class="dropdown dropdown-submenu">
        <a href="#" data-toggle="dropdown" class="dropdown-item dropdown-toggle">Sales Return</a>
        <ul class="dropdown-menu">
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.sales_return.index')}}">Record Data</a>
            </li>
            <li data-menu="">
            <a class="dropdown-item" href="{{route('backoffice.sales_return.create')}}">Create</a>
            </li>
          </ul>
      </li>
        
  </ul>
</li>

@endif

@if(in_array($auth->type, ['super_user','admin','encoder','auditor']))
<li data-menu="dropdown" class="dropdown nav-item {{Helper::is_active($routes['master_file'])? 'active':NULL}}">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link" aria-expanded="true"><i class="ft-box"></i><span>Product Management</span></a>
    <ul class="dropdown-menu">
      <li>
        <li>
        <a href="{{route('backoffice.inventory.index')}}" class="dropdown-item"><i class="icon-calculator"></i><span>Inventory</span></a>
      </li>
      </li>             
  </ul>
</li>

@endif



