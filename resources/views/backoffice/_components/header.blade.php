<nav class="header-navbar navbar navbar-with-menu navbar-static-top navbar-border navbar-brand-center navbar-dark bg-gradient-x-primary">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a>
                </li>
                <a href="{{route('backoffice.dashboard')}}" >
                <img src="{{asset('backoffice/2.png')}}?v=1.1" height="50" style="margin-top: 5px;">
                </a>
                <li class="nav-item">
                    {{-- <a class="navbar-brand" href="{{route('backoffice.dashboard')}}"> --}}
                    {{-- <h2 class="brand-text">{{env('APP_TITLE','Localhost')}}</h2></a> --}}
                </li>
                <li class="nav-item hidden-md-up float-xs-right">
                    <a class="nav-link open-navbar-container" data-target="#navbar-mobile" data-toggle="collapse"><i class="fa fa-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container container center-layout">
            <div class="collapse navbar-toggleable-sm" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li class="nav-item hidden-sm-down">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a>
                    </li>
                    <li class="nav-item hidden-sm-down">
                        <a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a>
                    </li>
                    {{-- <li class="nav-item nav-search">
                        <a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
                        <div class="search-input">
                            <input class="input" placeholder="Explore Stack..." type="text">
                        </div>
                    </li> --}}
                </ul>
                <ul class="nav navbar-nav float-xs-right">
                    <!-- <li class="dropdown dropdown-language nav-item">
                        <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" id="dropdown-flag"><i class="flag-icon flag-icon-gb"></i><span class="selected-language"></span></a>
                        <div aria-labelledby="dropdown-flag" class="dropdown-menu">
                            <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-gb"></i> English</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> German</a>
                        </div>
                    </li> -->
                    {{-- <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" data-toggle="dropdown" href="#"><i class="ficon ft-bell"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0">5 New</span></h6>
                            </li>
                            <li class="list-group scrollable-container">
                                <a class="list-group-item" href="javascript:void(0)">
                                <div class="media">
                                    <div class="media-left valign-middle">
                                        <i class="ft-plus-square icon-bg-circle bg-cyan"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">You have new order!</h6>
                                        <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small>
                                    </div>
                                </div></a> <a class="list-group-item" href="javascript:void(0)">
                                <div class="media">
                                    <div class="media-left valign-middle">
                                        <i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading red darken-1">99% Server load</h6>
                                        <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Five hour ago</time></small>
                                    </div>
                                </div></a> <a class="list-group-item" href="javascript:void(0)">
                                <div class="media">
                                    <div class="media-left valign-middle">
                                        <i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                                        <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time></small>
                                    </div>
                                </div></a> <a class="list-group-item" href="javascript:void(0)">
                                <div class="media">
                                    <div class="media-left valign-middle">
                                        <i class="ft-check-circle icon-bg-circle bg-cyan"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Complete the task</h6><small><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time></small>
                                    </div>
                                </div></a><a class="list-group-item" href="javascript:void(0)">
                                <div class="media">
                                    <div class="media-left valign-middle">
                                        <i class="ft-file icon-bg-circle bg-teal"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Generate monthly report</h6><small><time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time></small>
                                    </div>
                                </div></a>
                            </li>
                            <li class="dropdown-menu-footer">
                                <a class="dropdown-item text-muted text-xs-center" href="javascript:void(0)">Read all notifications</a>
                            </li>
                        </ul>
                    </li> --}}
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" data-toggle="dropdown" href="#">
                        <span class="avatar avatar-online">
                        @if($auth->filename)
                          <img alt="avatar" src="{{asset($auth->directory.'/thumbnails/'.$auth->filename)}}"><i></i>
                        @else
                          <img alt="avatar" src="{{asset('backoffice/app-assets/images/face0.jpg')}}"><i></i>
                        @endif
                        </span>
                        <span class="user-name">{{$auth->fname.' '.$auth->lname}}</span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{route('backoffice.profile.settings')}}"><i class="ft-user"></i>Edit Profile</a>
                             <a class="dropdown-item" href="{{route('backoffice.profile.password')}}"><i class="ft-lock"></i>Edit Password</a>
                            <!-- <a class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a>
                            <a class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a>
                            <a class="dropdown-item" href="#"><i class="ft-comment-square"></i> Chats</a> -->
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="{{route('backoffice.logout')}}"><i class="ft-power"></i>Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>