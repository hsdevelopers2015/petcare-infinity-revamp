@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="supplier_code">Supplier Code</label>
										<div class="col-md-9">
											<input type="text" name="supplier_code" id="supplier_code" class="form-control {{$errors->first('supplier_code') ? 'border-danger' : NULL}}" placeholder="Supplier code" value="{{old('supplier_code')}}" data-toggle="tooltip" title="{{$errors->first('supplier_code') ? $errors->first('supplier_code') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="supplier_name">Supplier Name</label>
										<div class="col-md-9">
											<input type="text" name="supplier_name" id="supplier_name" class="form-control prod_cat {{$errors->first('supplier_name') ? 'border-danger' : NULL}}" placeholder="Supplier name" value="{{old('supplier_name')}}" data-toggle="tooltip" title="{{$errors->first('supplier_name') ? $errors->first('supplier_name') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="type">Supplier Type</label>
										<div class="col-md-9">
											{!!Form::select("type", $supplier_type, old('type'), ['id' => "type", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('type')}}"]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="supplier_address">Address</label>
										<div class="col-md-9">
											<input type="text" name="supplier_address" id="supplier_address" class="form-control prod_cat {{$errors->first('supplier_address') ? 'border-danger' : NULL}}" placeholder="Supplier address" value="{{old('supplier_address')}}" data-toggle="tooltip" title="{{$errors->first('supplier_address') ? $errors->first('supplier_address') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="supplier_email">Email</label>
										<div class="col-md-9">
											<input type="text" name="supplier_email" id="supplier_email" class="form-control prod_cat {{$errors->first('supplier_email') ? 'border-danger' : NULL}}" placeholder="Supplier email" value="{{old('supplier_email')}}" data-toggle="tooltip" title="{{$errors->first('supplier_email') ? $errors->first('supplier_email') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="supplier_contact_number">Contact No</label>
										<div class="col-md-9">
											<input type="text" name="supplier_contact_number" id="supplier_contact_number" class="form-control prod_cat {{$errors->first('supplier_contact_number') ? 'border-danger' : NULL}}" placeholder="Supllier contact no" value="{{old('supplier_contact_number')}}" data-toggle="tooltip" title="{{$errors->first('supplier_contact_number') ? $errors->first('supplier_contact_number') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="supplier_contact_person">Contact Person</label>
										<div class="col-md-9">
											<input type="text" name="supplier_contact_person" id="supplier_contact_person" class="form-control prod_cat {{$errors->first('supplier_contact_person') ? 'border-danger' : NULL}}" placeholder="Supplier contact person" value="{{old('supplier_contact_person')}}" data-toggle="tooltip" title="{{$errors->first('supplier_contact_person') ? $errors->first('supplier_contact_person') : NULL}}">
										</div>
									</div>
								</div>
							</div>							
						</div>
						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
{{-- {!! $map['js'] !!} --}}
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	 function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 14.599512,
       lng: 120.984222
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>

<script>
$(".prod_cat").click(function(){
	var x = $("#userinput1").val();
	var y = $("#userinput2").val();

	if(x != "" && y != ""){
		var z = y.toUpperCase().substring(0,2) +'-'+ x.toUpperCase().substring(0,4)+'-'+ Math.floor((Math.random() * 10000) + 1);
		$("#userinput4").attr('value',z);
		// $("#userinput4").prop('disabled', true);
	}
});
</script>
<script>
$(".prod_cat").ready(function(){
	var x = $("#userinput1").val();
	var y = $("#userinput2").val();

	if(x != "" && y != ""){
		var z = y.toUpperCase().substring(0,2) +'-'+ x.toUpperCase().substring(0,4)+'-'+ Math.floor((Math.random() * 10000) + 1);
		$("#userinput4").attr('value',z);
		// $("#userinput4").prop('disabled', true);
	}
});

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="row"><div class="col-md-5"><div class="form-group row"><label class="col-md-6 label-control" for="userinput72">Deal Condition</label><div class="col-md-6"><input type="number" name="deal_condition[]" required id="userinput72" class="form-control prod_cat" placeholder="" min="1" value="" data-toggle="tooltip" title=""></div></div></div><div class="col-md-5"><div class="form-group row"><label class="col-md-5 label-control" for="userinput62">Deal Incentive</label><div class="col-md-7"><input type="number" name="deal_incentive[]" required id="userinput62" class="form-control prod_cat" placeholder="" min="1" value="" data-toggle="tooltip" title=""></div></div></div><a class="col-md-2 mt-1 href="#" id="remove_field"><i class="fa fa-trash text-danger"></i></a></div> </div>'); //add input box
        }
    });
    
    $(wrapper).on("click","#remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    	$(".is_focus_yes").hide();
        $('input[type="checkbox"]').click(function(){
            if($(this).is(":checked")){
            	$(".is_focus_yes").show(500);
            }
            else if($(this).is(":not(:checked)")){
            	$(".is_focus_yes").hide(500);
            }
        });
    });
</script>
@stop
