@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="fa fa-file-text-o"></i>About The Contract</h4>

							<!-- <div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<?php
										if($errors->first('client_id')){
											$status = 'border-danger';
											$msg = $errors->first('client_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="userinput4">Client</label>
										<div class="col-md-9" title="{{$errors->first('client_id')}}" data-toggle="tooltip">
											{!!Form::select("client_id", $clients, old('client_id'), ['id' => "client_id", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
							</div> -->
							
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2">Contract Code</label>
										<div class="col-md-9">
											<input type="text" name="contract_code" id="userinput2" rows="5" class="form-control {{$errors->first('contract_code') ? 'border-danger' : NULL}}" placeholder="Contract Transaction Code" value="{{old('contract_code')}}" data-toggle="tooltip" title="{{$errors->first('contract_code') ? $errors->first('contract_code') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput1">Detail</label>
										<div class="col-md-9">
											<textarea type="text" name="detail" id="userinput1" rows="5" class="form-control {{$errors->first('detail') ? 'border-danger' : NULL}}" placeholder="Detail" value="" data-toggle="tooltip" title="{{$errors->first('detail') ? $errors->first('detail') : NULL}}">{{old('detail')}}</textarea> 
										</div>
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="form-group row">
									<div class="col-md-10">
										<label class="col-md-3 label-control" for="userinput3">Contract File</label>
										<div class="col-md-4">
											<input type="file" name="file" id="userinput3" value="{{old('file')}}" data-toggle="tooltip" class="form-control {{$errors->first('file') ? 'border-danger' : NULL}}" title="{{$errors->first('file') ? $errors->first('file') : NULL}}">
											<label class="mt-1">Please <code>upload</code> the contract.</label>
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
{{-- {!! $map['js'] !!} --}}
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	 function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 14.599512,
       lng: 120.984222
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>

<script type="text/javascript">
	$("#userinput2").ready(function(){
		var x = 'CTR-'+<?php echo Helper::date_format(Carbon::now(),'ymd'); ?>;
		var str = "" + <?php echo $contract_count; ?>;
		var pad = "0000";
		var ans = pad.substring(0, pad.length - str.length) + str;

		 $("#userinput2").val(x+"-"+ans);
	});

</script>
@stop
