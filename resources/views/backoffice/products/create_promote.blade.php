@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-box"></i>About The Product Promotion</h4>
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<?php
										if($errors->first('product_id')){
											$status = 'border-danger';
											$msg = $errors->first('product_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="userinput3">Product</label>
										<div class="col-md-9" title="{{$errors->first('product_id')}}" data-toggle="tooltip">
											{!!Form::select("product_id", $product_list, old('product_id'), ['id' => "userinput3", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput4">Product Description</label>
										<div class="col-md-9">
											<textarea type="text" name="description" rows="5" id="userinput4" class="form-control {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">{{old('description')}}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Product Image</label>
										<div class="col-md-9">
											<input type="file" id="userinput5"  name="file" class="form-control {{$errors->first('file') ? 'border-danger' : NULL}}" title="{{$errors->first('file') ? $errors->first('file') : NULL}}" data-toggle="tooltip" >
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
{{-- {!! $map['js'] !!} --}}
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
@stop
