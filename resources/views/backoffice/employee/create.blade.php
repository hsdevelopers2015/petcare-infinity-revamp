@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-user"></i> About Employee</h4>
							<div class="row mt-3">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput1">Fist Name</label>
										<div class="col-md-9">
											<input type="text" name="fname" id="userinput1" class="form-control {{$errors->first('fname') ? 'border-danger' : NULL}}" placeholder="First Name" value="{{old('fname')}}" data-toggle="tooltip" title="{{$errors->first('fname') ? $errors->first('fname') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2">Last Name</label>
										<div class="col-md-9">
											<input type="text" name="lname" id="userinput2" class="form-control {{$errors->first('lname') ? 'border-danger' : NULL}}" placeholder="Last Name" value="{{old('lname')}}" data-toggle="tooltip" title="{{$errors->first('lname') ? $errors->first('lname') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Username</label>
										<div class="col-md-9">
											<input type="text" name="username" id="userinput3" class="form-control {{$errors->first('username') ? 'border-danger' : NULL}}" placeholder="Username" value="{{old('username')}}" data-toggle="tooltip" title="{{$errors->first('username') ? $errors->first('username') : NULL}}">
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group row">
										<?php
										if($errors->first('type')){
											$status = 'border-danger';
											$msg = $errors->first('type');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="userinput4">Employee Type</label>
										<div class="col-md-9" title="{{$errors->first('type')}}" data-toggle="tooltip">
											{!!Form::select("type", $types, old('type'), ['id' => "type", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput23">Password</label>
										<div class="col-md-9">
											<input type="password" name="password" id="userinput23" class="form-control {{$errors->first('password') ? 'border-danger' : NULL}}" placeholder="Password" value="{{old('password')}}" data-toggle="tooltip" title="{{$errors->first('password') ? $errors->first('password') : NULL}}">
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput24">Confirm Password</label>
										<div class="col-md-9">
											<input type="password" name="password_confirmation" id="userinput24" class="form-control {{$errors->first('password_confirmation') ? 'border-danger' : NULL}}" placeholder="Confirm Password" value="{{old('password_confirmation')}}" data-toggle="tooltip" title="{{$errors->first('password_confirmation') ? $errors->first('password_confirmation') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<h4 class="form-section"><i class="ft-mail"></i> Contact Info </h4>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Email</label>
										<div class="col-md-9">
											<input class="form-control {{$errors->first('email') ? 'border-danger' : NULL}}" name="email" type="email" placeholder="Email" id="userinput5" value="{{old('email')}}" data-toggle="tooltip" title="{{$errors->first('email') ? $errors->first('email') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput7">Contact Number</label>
										<div class="col-md-9">
											<input class="form-control col-md-9 {{$errors->first('contact') ? 'border-danger' : NULL}}" name="contact" type="tel" placeholder="Contact Number" id="userinput7" value="{{old('contact')}}" data-toggle="tooltip" title="{{$errors->first('contact') ? $errors->first('contact') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput71">Other Contact<br><small style="color: goldenrod">(optional)</small></label>
										<div class="col-md-9">
											<input class="form-control col-md-9 {{$errors->first('contact_2') ? 'border-danger' : NULL}}" name="contact_2" type="tel" placeholder="Contact Number 2" id="userinput71" value="{{old('contact_2')}}" data-toggle="tooltip" title="{{$errors->first('contact_2') ? $errors->first('contact_2') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-6">


									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput72">Other Contact<br><small style="color: goldenrod">(optional)</small></label>
										<div class="col-md-9">
											<input class="form-control col-md-9 {{$errors->first('contact_3') ? 'border-danger' : NULL}}" name="contact_3" type="tel" placeholder="Contact Number 3" id="userinput72" value="{{old('contact_3')}}" data-toggle="tooltip" title="{{$errors->first('contact_3') ? $errors->first('contact_3') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput8">Permanent Address</label>
										<div class="col-md-9">
											<input type="text" id="userinput8" rows="5" class="form-control col-md-9 {{$errors->first('address') ? 'border-danger' : NULL}}" name="address" placeholder="Permanent Address" data-toggle="tooltip" title="{{$errors->first('address') ? $errors->first('address') : NULL}}" value="{{old('address')}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput81">Secondary Address</label>
										<div class="col-md-9">
											<input type="text" id="userinput81" rows="5" class="form-control col-md-9 {{$errors->first('address_2') ? 'border-danger' : NULL}}" name="address_2" placeholder="Primary Address" data-toggle="tooltip" title="{{$errors->first('address_2') ? $errors->first('address_2') : NULL}}" value="{{old('address_2')}}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script type="text/javascript">
	var type = $("#type").val();
	if(type == 'sales_head' || type == 'sales_agent'){
		$("#additional").show(500);
	}else{
		$("#additional").hide(500);
	}
	$("#type").change(function(){
		var type = $("#type").val();

		console.log(type);
		if(type == 'sales_head' || type == 'sales_agent'){
			$("#additional").show(500);
		}else{
			$("#additional").hide(500);
		}
	});

</script>
@stop
