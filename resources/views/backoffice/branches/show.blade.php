@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$branch_information}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6 mb-2">
	<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
			</div>
			<a href="{{route('backoffice.'.$route_file.'.create')}}" class="btn btn-primary pull-right"><i class="ft-plus"></i>&nbsp;&nbsp;Create New&nbsp;</a>
		</div>
	</div>
</div>
<section id="">
	<div class="row">
		<div class="col-xs-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$branch_information}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-hover m-b-0 table-bordered" width="100%">
                                <tbody>                 
                                    <tr>
                                        <td colspan="4" class="text-center"><b>Branch Details</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><small class="form-text">Branch Code</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->branch_code}}</small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><small class="form-text">Business name</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->branch_name}}</small></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2"><small class="form-text">Branch Owner</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->client_id}}</small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><small class="form-text">Branch Address</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->branch_location}}</small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><small class="form-text">Branch Contact No</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->branch_contact}}</small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><small class="form-text">Branch Email</small></td>
                                        <td colspan="2"><small class="form-text text-muted">{{$branch->branch_email}}</small></td>
                                    </tr>


                                </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
			
		<div class="row">
	        <div class="col-xs-12">
	            <div class="card">
	                <div class="card-header">
	                    <h4 class="card-title">{{$stock_received}}</h4>
	                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                    <div class="heading-elements">
	                        <ul class="list-inline mb-0">
	                        </ul>
	                    </div>
	                </div>
	                <div class="card-body collapse in">
	                    <div class="card-block card-dashboard">
	                        <table class="table table-hover m-b-0 table-bordered" width="100%" id = "stock_received">
	                            <thead>
	                            <tr>
	                                <th><small class="form-text">#</small></th>
	                                <th><small class="form-text">Transaction Code</small></th>
	                                <th><small class="form-text">From</small></th>
	                                <th><small class="form-text">To</small></th>
	                                <th><small class="form-text">Product Name</small></th>
	                                <th><small class="form-text">Unit</small></th>                                
	                                <th><small class="form-text">Qty</small></th>
	                                <th><small class="form-text">Selling</small></th>
	                                <th><small class="form-text">Value</small></th>
	                            </tr>
	                            </thead>
	                            <tbody> 
	                                @forelse($stock_item_received as $index => $value)
	                                <tr>
	                                    <td><small class="form-text">{{++$index}}</small></td>
	                                    <td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
	                                        <small class="form-text">{{Helper::date_format($value->created_at)}}</small>
	                                        
	                                    </td>
	                                    <td><small class="form-text">{{$value->from}}</small></td>
	                                    <td><small class="form-text">{{$value->to}}</small></td>
	                                    <td><small class="form-text">{{$value->product_code}}</small></td>
	                                    <td><small class="form-text">{{$value->unit}}</small></td>
	                                    <td><small class="form-text">{{$value->quantity}}</small></td>
	                                    <td><small class="form-text">{{$value->selling}}</small></td>
	                                    <td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
	                                @empty
	                                <tr>
	                                    <td colspan="11"><small class="form-text">No records yet!</td>  
	                                </tr>
	                                @endforelse                                       
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <td colspan="6"><small class="form-text">Total</small></td>
	                                    <td colspan="2"><small class="form-text">{{$stock_item_received_count ? : "0.00"}}</small></td>
	                                    <td colspan="3"><small class="form-text"><div id = "total_sum"></div></small></td>
	                                </tr>
	                            </tfoot>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="card">
	                <div class="card-header">
	                    <h4 class="card-title">{{$sales_received}}</h4>
	                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                    <div class="heading-elements">
	                        <ul class="list-inline mb-0">
	                        </ul>
	                    </div>
	                </div>
	                <div class="card-body collapse in">
	                    <div class="card-block card-dashboard">
	                        <table class="table table-hover m-b-0 table-bordered" width="100%">
	                            <thead>
	                            <tr>
	                                <th><small class="form-text">#</small></th>
	                                <th><small class="form-text">Transaction Code</small></th>
	                                <th><small class="form-text">From</small></th>
	                                <th><small class="form-text">To</small></th>
	                                <th><small class="form-text">Product Name</small></th>
	                                <th><small class="form-text">Unit</small></th>                                
	                                <th><small class="form-text">Qty</small></th>
	                                <th><small class="form-text">Selling</small></th>
	                                <th><small class="form-text">Value</small></th>
	                            </tr>
	                            </thead>
	                            <tbody> 
	                                @forelse($stock_item_received as $index => $value)
	                                <tr>
	                                    <td><small class="form-text">{{++$index}}</small></td>
	                                    <td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
	                                        <small class="form-text">{{Helper::date_format($value->created_at)}}</small>
	                                        
	                                    </td>
	                                    <td><small class="form-text">{{$value->from}}</small></td>
	                                    <td><small class="form-text">{{$value->to}}</small></td>
	                                    <td><small class="form-text">{{$value->product_code}}</small></td>
	                                    <td><small class="form-text">{{$value->unit}}</small></td>
	                                    <td><small class="form-text">{{$value->quantity}}</small></td>
	                                    <td><small class="form-text">{{$value->selling}}</small></td>
	                                    <td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
	                                @empty
	                                <tr>
	                                    <td colspan="11"><small class="form-text">No records yet!</td>  
	                                </tr>
	                                @endforelse                                       
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <td colspan="6"><small class="form-text">Total</small></td>
	                                    <td colspan="2"><small class="form-text">{{$stock_item_received_count ? : "0.00"}}</small></td>
	                                    <td colspan="3"><small class="form-text">{{Helper::amount($stock_item_received_count * $stock_item_received_sum ? : "0.00")}}</small></td>
	                                </tr>
	                            </tfoot>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="card">
	                <div class="card-header">
	                    <h4 class="card-title">{{$stock_tranfer}}</h4>
	                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                    <div class="heading-elements">
	                        <ul class="list-inline mb-0">
	                        </ul>
	                    </div>
	                </div>
	                <div class="card-body collapse in">
	                    <div class="card-block card-dashboard">
	                        <table class="table table-hover m-b-0 table-bordered" width="100%">
	                            <thead>
	                            <tr>
	                                <th><small class="form-text">#</small></th>
	                                <th><small class="form-text">Transaction Code</small></th>
	                                <th><small class="form-text">From</small></th>
	                                <th><small class="form-text">To</small></th>
	                                <th><small class="form-text">Product Name</small></th>
	                                <th><small class="form-text">Unit</small></th>                                
	                                <th><small class="form-text">Qty</small></th>
	                                <th><small class="form-text">Selling</small></th>
	                                <th><small class="form-text">Value</small></th>
	                            </tr>
	                            </thead>
	                            <tbody> 
	                                @forelse($stock_item_transfer as $index => $value)
	                                <tr>
	                                    <td><small class="form-text">{{++$index}}</small></td>
	                                    <td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
	                                        <small class="form-text">{{Helper::date_format($value->created_at)}}</small>
	                                        
	                                    </td>
	                                    <td><small class="form-text">{{$value->from}}</small></td>
	                                    <td><small class="form-text">{{$value->to}}</small></td>
	                                    <td><small class="form-text">{{$value->product_code}}</small></td>
	                                    <td><small class="form-text">{{$value->unit}}</small></td>
	                                    <td><small class="form-text">{{$value->quantity}}</small></td>
	                                    <td><small class="form-text">{{$value->selling}}</small></td>
	                                    <td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
	                                @empty
	                                <tr>
	                                    <td colspan="11"><small class="form-text">No records yet!</td>  
	                                </tr>
	                                @endforelse                                       
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <td colspan="6"><small class="form-text">Total</small></td>
	                                    <td colspan="2"><small class="form-text">{{$total_qty_stock_transfer ? : "0.00"}}</small></td>
	                                    <td colspan="3"><small class="form-text">{{Helper::amount($total_qty_stock_transfer * $total_sell_stock_transfer ? : "0.00")}}</small></td>
	                                </tr>
	                            </tfoot>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="card">
	                <div class="card-header">
	                    <h4 class="card-title">{{$sales_return}}</h4>
	                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                    <div class="heading-elements">
	                        <ul class="list-inline mb-0">
	                        </ul>
	                    </div>
	                </div>
	                <div class="card-body collapse in">
	                    <div class="card-block card-dashboard">
	                        <table class="table table-hover m-b-0 table-bordered" width="100%">
	                            <thead>
	                            <tr>
	                                <th><small class="form-text">#</small></th>
	                                <th><small class="form-text">Transaction Code</small></th>
	                                <th><small class="form-text">From</small></th>
	                                <th><small class="form-text">To</small></th>
	                                <th><small class="form-text">Product Name</small></th>
	                                <th><small class="form-text">Unit</small></th>                                
	                                <th><small class="form-text">Qty</small></th>
	                                <th><small class="form-text">Selling</small></th>
	                                <th><small class="form-text">Value</small></th>
	                            </tr>
	                            </thead>
	                            <tbody> 
	                                @forelse($stock_item_transfer as $index => $value)
	                                <tr>
	                                    <td><small class="form-text">{{++$index}}</small></td>
	                                    <td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
	                                        <small class="form-text">{{Helper::date_format($value->created_at)}}</small>
	                                        
	                                    </td>
	                                    <td><small class="form-text">{{$value->from}}</small></td>
	                                    <td><small class="form-text">{{$value->to}}</small></td>
	                                    <td><small class="form-text">{{$value->product_code}}</small></td>
	                                    <td><small class="form-text">{{$value->unit}}</small></td>
	                                    <td><small class="form-text">{{$value->quantity}}</small></td>
	                                    <td><small class="form-text">{{$value->selling}}</small></td>
	                                    <td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
	                                @empty
	                                <tr>
	                                    <td colspan="11"><small class="form-text">No records yet!</td>  
	                                </tr>
	                                @endforelse                                       
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <td colspan="6"><small class="form-text">Total</small></td>
	                                    <td colspan="2"><small class="form-text">{{$total_qty_stock_transfer ? : "0.00"}}</small></td>
	                                    <td colspan="3"><small class="form-text">{{Helper::amount($total_qty_stock_transfer * $total_sell_stock_transfer ? : "0.00")}}</small></td>
	                                </tr>
	                            </tfoot>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		{{--
		<div class="col-xs-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$supplier}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-striped">
                            <thead>
                            <tr>
                                <th><small class="form-text">#</small></th>
                                <th><small class="form-text">Supplier Name</small></th>
                                <th><small class="form-text">Status</small></th>
                            </tr>
                            </thead>
                            <tbody>
                            	@forelse($suppliers as $index => $value)
                            <tr>
                                <th scope="row"><small class="form-text">{{++$index}}</small></th>
                                <td>
                                    <small class="form-text text-muted"> <a href="">{{$value->supplier->supplier_name}}</a></small>
                                </td>
                                <td>
                                    <small class="form-text text-muted">{{$value->supplier->status}}</small>
                                </td>                                                               
                             </tr> 
                             <tr>  
                             @empty                              
                                <td>
                                    <small class="form-text text-muted">No records yet</small>
                                </td>                                                               
                              </tr>
                              @endforelse                                       
                            </tbody>            
                        </table>
					</div>
				</div>
			</div>
		</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">{{$purchased_item_history}}</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
							<div class="heading-elements">
								<ul class="list-inline mb-0">
								</ul>
							</div>
						</div>
						<div class="card-body collapse in">
							<div class="card-block card-dashboard">
								<table class="table table-hover m-b-0 table-bordered" width="100%">
		                            <thead>
		                            <tr>
		                                <th><small class="form-text">#</small></th>
		                                <th><small class="form-text">Transaction Code</small></th>
		                                <th><small class="form-text">From</small></th>
		                                <th><small class="form-text">To</small></th>
		                                <th><small class="form-text">Product Name</small></th>
		                                <th><small class="form-text">Unit</small></th>                                
		                                <th><small class="form-text">Qty</small></th>
		                                <th><small class="form-text">Selling</small></th>
		                                <th><small class="form-text">Value</small></th>
		                            </tr>
		                            </thead>
		                            <tbody> 
		                            	@forelse($purchased_item_detail as $index => $value)
		                            	<tr>
		                            		<td><small class="form-text">{{++$index}}</small></td>
		                            		<td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
		                            			<small class="form-text">{{Helper::date_format($value->created_at)}}</small>
		                            		</td>
		                            		<td><small class="form-text">{{$value->from}}</small></td>
		                            		<td><small class="form-text">{{$value->to}}</small></td>
		                            		<td><small class="form-text">{{$value->product_code}}</small></td>
		                            		<td><small class="form-text">{{$value->unit}}</small></td>
		                            		<td><small class="form-text">{{$value->quantity}}</small></td>
		                            		<td><small class="form-text">{{$value->selling}}</small></td>
		                            		<td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
		                            	@empty
		                            	<tr>
		                            		<td><small class="form-text">No records yet!</td>	
		                            	</tr>
		                            	@endforelse                                       
		                            </tbody>
		                            <tfoot>
		                            	<tr>
		                            		<td colspan="6"><small class="form-text">Total</small></td>
		                            		<td colspan="2"><small class="form-text">{{$totalQty ? : "0.00"}}</small></td>
		                            		<td colspan="2"><small class="form-text">{{Helper::amount($totalQty * $totalSelling ? : "0.00")}}</small></td>
		                            	</tr>
		                            </tfoot>
		                        </table>
							</div>
						</div>
					</div>
				</div>
			</div>
	
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$purchased_item_history}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-hover m-b-0 table-bordered" width="100%">
                            <thead>
                            <tr>
                                <th><small class="form-text">#</small></th>
                                <th><small class="form-text">Transaction Code</small></th>
                                <th><small class="form-text">From</small></th>
                                <th><small class="form-text">To</small></th>
                                <th><small class="form-text">Product Name</small></th>
                                <th><small class="form-text">Unit</small></th>                                
                                <th><small class="form-text">Qty</small></th>
                                <th><small class="form-text">Selling</small></th>
                                <th><small class="form-text">Value</small></th>
                            </tr>
                            </thead>
                            <tbody> 
                            	@forelse($purchased_item_detail as $index => $value)
                            	<tr>
                            		<td><small class="form-text">{{++$index}}</small></td>
                            		<td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
                            			<small class="form-text">{{Helper::date_format($value->created_at)}}</small>
                            		</td>
                            		<td><small class="form-text">{{$value->from}}</small></td>
                            		<td><small class="form-text">{{$value->to}}</small></td>
                            		<td><small class="form-text">{{$value->product_code}}</small></td>
                            		<td><small class="form-text">{{$value->unit}}</small></td>
                            		<td><small class="form-text">{{$value->quantity}}</small></td>
                            		<td><small class="form-text">{{$value->selling}}</small></td>
                            		<td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
                            	@empty
                            	<tr>
                            		<td><small class="form-text">No records yet!</td>	
                            	</tr>
                            	@endforelse                                       
                            </tbody>
                            <tfoot>
                            	<tr>
                            		<td colspan="6"><small class="form-text">Total</small></td>
                            		<td colspan="2"><small class="form-text">{{$totalQty ? : "0.00"}}</small></td>
                            		<td colspan="2"><small class="form-text">{{Helper::amount($totalQty * $totalSelling ? : "0.00")}}</small></td>
                            	</tr>
                            </tfoot>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$stock_transfer_in_out_history}}</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <table class="table table-hover m-b-0 table-bordered" width="100%">
                            <thead>
                            <tr>
                                <th><small class="form-text">#</small></th>
                                <th><small class="form-text">Transaction Code</small></th>
                                <th><small class="form-text">From</small></th>
                                <th><small class="form-text">To</small></th>
                                <th><small class="form-text">Product Name</small></th>
                                <th><small class="form-text">Unit</small></th>                                
                                <th><small class="form-text">Qty</small></th>
                                <th><small class="form-text">Selling</small></th>
                                <th><small class="form-text">Value</small></th>
                                <th><small class="form-text">MO</small></th>
                                <th><small class="form-text">MI</small></th>
                            </tr>
                            </thead>
                            <tbody> 
                                @forelse($transaction_item_detail as $index => $value)
                                <tr>
                                    <td><small class="form-text">{{++$index}}</small></td>
                                    <td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
                                        <small class="form-text">{{Helper::date_format($value->created_at)}}</small>
                                        
                                    </td>
                                    <td><small class="form-text">{{$value->from}}</small></td>
                                    <td><small class="form-text">{{$value->to}}</small></td>
                                    <td><small class="form-text">{{$value->product_code}}</small></td>
                                    <td><small class="form-text">{{$value->unit}}</small></td>
                                    <td><small class="form-text">{{$value->quantity}}</small></td>
                                    <td><small class="form-text">{{$value->selling}}</small></td>
                                    <td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
                                    <td><small class="form-text">{{$value->stock_trasfer_out}}</small></td>
                                    <td><small class="form-text">{{$value->stock_trasfer_in ? : "--"}}</small></td>
                                @empty
                                <tr>
                                    <td colspan="11"><small class="form-text">No records yet!</td>  
                                </tr>
                                @endforelse                                       
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6"><small class="form-text">Total</small></td>
                                    <td colspan="2"><small class="form-text">{{$total_qty_stock_transfer ? : "0.00"}}</small></td>
                                    <td colspan="3"><small class="form-text">{{Helper::amount($total_qty_stock_transfer * $total_sell_stock_transfer ? : "0.00")}}</small></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$purchase_return_item_history}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-hover m-b-0 table-bordered" width="100%">
                            <thead>
                            <tr>
                                <th><small class="form-text">#</small></th>
                                <th><small class="form-text">Transaction Code</small></th>
                                <th><small class="form-text">From</small></th>
                                <th><small class="form-text">To</small></th>
                                <th><small class="form-text">Product Name</small></th>
                                <th><small class="form-text">Unit</small></th>                                
                                <th><small class="form-text">Qty</small></th>
                                <th><small class="form-text">Selling</small></th>
                                <th><small class="form-text">Value</small></th>
                            </tr>
                            </thead>
                            <tbody> 
                            	@forelse($purchase_return_history as $index => $value)
                            	<tr>
                            		<td><small class="form-text">{{++$index}}</small></td>
                            		<td><small class="form-text"><strong>{{$value->transaction_code}}</strong></small>
                            			<small class="form-text">{{Helper::date_format($value->created_at)}}</small>
                            			
                            		</td>
                            		<td><small class="form-text">{{$value->from}}</small></td>
                            		<td><small class="form-text">{{$value->to}}</small></td>
                            		<td><small class="form-text">{{$value->product_code}}</small></td>
                            		<td><small class="form-text">{{$value->unit}}</small></td>
                            		<td><small class="form-text">{{$value->quantity}}</small></td>
                            		<td><small class="form-text">{{$value->selling}}</small></td>
                            		<td><small class="form-text">{{Helper::amount($value->quantity * $value->selling)}}</small></td>
                            	@empty
                            	<tr>
                            		<td colspan="11"><small class="form-text">No records yet!</td>	
                            	</tr>
                            	@endforelse                                       
                            </tbody>
                            <tfoot>
                            	<tr>
                            		<td colspan="6"><small class="form-text">Total</small></td>
                            		<td colspan="2"><small class="form-text">{{$total_qty_po_return ? : "0.00"}}</small></td>
                            		<td colspan="3"><small class="form-text">{{Helper::amount($total_qty_po_return * $total_amount_po_return ? : "0.00")}}</small></td>
                            	</tr>
                            </tfoot>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
	--}}
</section>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/feather/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/pace.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/toastr.css')}}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/app.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/colors.min.css')}}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/plugins/extensions/toastr.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/simple-line-icons/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-climacon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-gradient.min.css')}}">
<!-- END Page Level CSS-->

<style type="text/css">
	.dropdown-item:focus, .dropdown-item:hover {
    color: #00b5b8; 
    text-decoration: none!important;
    background-color: #ffffff!important; 
    width: inherit!important;
}
</style>

@stop

@section('page-scripts')
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script type="text/javascript">
	$(function(){
		$(".action-delete").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-delete").attr({"href" : btn.data('url')});
		});	

	    $('.datatable-basic').DataTable();

	    
	});

	$(document).ready(function(){
	     colSum();
	});

	function colSum() {
	    var sum=0;
	    //iterate through each input and add to sum
	    $("td:nth-child(9)","#stock_received").each(function() {     
	       sum += parseInt($(this).text().replace(',',''));                     
	    }); 
	    //change value of total
	    $('#total_sum').html(sum);
	}
</script>

<style>

	.buttons-copy{
		display: none;
	}
	.buttons-csv{
		display: none;
	}
</style>
@stop
