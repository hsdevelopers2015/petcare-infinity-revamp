@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="branch_code">Branch Code</label>
										<div class="col-md-9">
											<input readonly type="text" name="branch_code" id="branch_code" class="form-control {{$errors->first('branch_code') ? 'border-danger' : NULL}}" placeholder="Supplier code" value="{{old('branch_code',$sequence)}}" data-toggle="tooltip" title="{{$errors->first('branch_code') ? $errors->first('branch_code') : NULL}}">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-3 label-control" for="client_id">Clients</label>
										<div class="col-md-9">
											{!!Form::select("client_id", $clients, old('client_id'), ['id' => "client_id",'name' => 'client_id', 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('client_id')}}"]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="branch_name">Branch Name</label>
										<div class="col-md-9">
											<input readonly type="text" name="branch_name" id="branch_name" class="form-control prod_cat {{$errors->first('branch_name') ? 'border-danger' : NULL}}" placeholder="Branch name" value="{{old('branch_name')}}" data-toggle="tooltip" title="{{$errors->first('branch_name') ? $errors->first('branch_name') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="branch_location">Address</label>
										<div class="col-md-9">
											<input readonly type="text" name="branch_location" id="branch_location" class="form-control prod_cat {{$errors->first('branch_location') ? 'border-danger' : NULL}}" placeholder="Branch address" value="{{old('branch_location')}}" data-toggle="tooltip" title="{{$errors->first('branch_location') ? $errors->first('branch_location') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="branch_email">Email</label>
										<div class="col-md-9">
											<input readonly type="text" name="branch_email" id="branch_email" class="form-control prod_cat {{$errors->first('branch_email') ? 'border-danger' : NULL}}" placeholder="Branch email" value="{{old('branch_email')}}" data-toggle="tooltip" title="{{$errors->first('branch_email') ? $errors->first('branch_email') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">	
										<label class="col-md-3 label-control" for="branch_contact">Contact No</label>
										<div class="col-md-9">
											<input readonly type="text" name="branch_contact" id="branch_contact" class="form-control prod_cat {{$errors->first('branch_contact') ? 'border-danger' : NULL}}" placeholder="Branch contact no" value="{{old('branch_contact')}}" data-toggle="tooltip" title="{{$errors->first('branch_contact') ? $errors->first('branch_contact') : NULL}}">
										</div>
									</div>
								</div>
							</div>							
						</div>
						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
{{-- {!! $map['js'] !!} --}}
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">
	$(function() {
		
		$('select[name="client_id"]').on('change', function(){
			var client_id = $("#client_id").val();
			if(!client_id == "") {
				$.ajax({
				  method: "GET",
				  url: "{{ route('backoffice.branches.ajax_detail') }}/"+encodeURI(client_id),
				
				    dataType: "json",
				    success:function(data) {
				     $('#branch_name').val(data.business_name);
				     $('#branch_location').val(data.business_address);
				     $('#branch_email').val(data.email);
				     $('#branch_contact').val(data.contact_no);
				    }
				});

			}else{
				$('#branch_name').val("");
				$('#branch_location').val("");
				$('#branch_email').val("");
				$('#branch_contact').val("");

			}

		});
		});
</script>
@stop
