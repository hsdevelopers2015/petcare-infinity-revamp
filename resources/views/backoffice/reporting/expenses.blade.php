@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">REPORT FILTERS</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
	
					<form class="form form-horizontal" action="" method="POST" enctype="multipart/form-data">
						<div class="form-body">
							<div class="row">
								<div class="col-md-12">
								<h4>Sales Agent</h4>
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
												<label for="userinput3">Sales Agent</label>
												{!!Form::select("sa_id", $sa_ids, old('sa_id'), ['id' => "sa_id", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
												{{-- <input type="date" id="userinput3" name="from" class="form-control" value="{{old('from')}}"> --}}
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<!-- <div class="col-md-12">
								<h4>Date range</h4>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label for="userinput1">From</label>
												<input type="text" id="userinput1" name="from" class="form-control select_date" value="{{Helper::date_format(old('from',Carbon::now()),'m/d/Y')}}">
											</div>
											<div class="col-md-6">
												<label for="userinput2">To</label>
												<input type="text" id="userinput2" name="to" class="form-control select_date" value="{{Helper::date_format(old('to',Carbon::now()),'m/d/Y')}}">
											</div>
										</div>
									</div>
								</div> -->
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary overlay-unblock">
									 	Generate
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<section id="file-export">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$page_title}} 
					@if($from AND $to)
					from {{$from}} to {{$to}}
					@endif
					</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							<li><a data-action="close"><i class="ft-x"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<a href="{{route('backoffice.reporting.export_expenses')}}" class="btn btn-outline-primary pull-right">Export Data</a>
						<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
						<table class="table table-striped table-bordered dataTable file-export">
							<thead>
								<tr>
									<th>Request Code</th>
									<th>Sales Agent</th>
									<th>Type</th>
									<th>Total Amount</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach($expenses as $index => $info)
								<tr style="color: {{$info->status == "cancelled"? '#c2c2c2': NULL}}">
									<td>
										{{$info->request->request_code}}
									</td>
									<td>{{$info->sa_info($info->sa_id)->fname}} {{$info->sa_info($info->sa_id)->lname}}</td>
									<td>{!!Helper::request_status_badge($info->type)!!}</td>
									<td>{{$info->total_amount}}</td>
									<td>{{Helper::date_format($info->updated_at,'M d, Y')}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="row">
	<div class="content-header-right col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0 text-primary ml-2">Total Expenses: {{round($total_expenses,2)}}</h3>
	</div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>


@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@include('backoffice._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
{{-- @include('backoffice._includes.scripts') --}}
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
{{-- <script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/gmaps.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/jquery.knob.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/unslider-min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script> --}}
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/extensions/block-ui.js')}}" type="text/javascript"></script>

<script>
$("#userinput5").click(function(){
	var x = $("#userinput4").val();

	var stock_count = $("#stock-count").val();

	if(x != ""){

		var z =  'SI-PROD'+x+'-'+stock_count;
		$("#userinput5").val(z);
	}
});
</script>

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        $( ".select_date" ).datepicker({
                dateFormat: "mm/dd/yyyy",
        });
    })
</script>
@stop
