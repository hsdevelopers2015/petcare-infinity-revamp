@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6 mb-2">
	<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
			</div>
			<a href="{{route('backoffice.'.$route_file.'.create')}}" class="btn btn-primary pull-right"><i class="ft-plus"></i>&nbsp;&nbsp;Create New&nbsp;</a>
		</div>
	</div>
</div>
<section id="">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$page_title}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-striped table-bordered datatable-basic">
							<thead>
								<tr>
									<th>#</th>
									<th>Code</th>
									<th>Category Name</th>
									<th>Date Added</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($categories as $index => $value)
								<tr>
									<td>{{++$index}}</td>
									<td>{{$value->category_code}}</td>
									<td>{{$value->category_name}}</td>
									<td>{{Helper::date_format($value->created_at)}}</td>
									<td>
										<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
											<div role="group" class="btn-group">
												<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
													<i class="ft-cog icon-left"></i> Action
												</button>
												<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
													<a href="{{route('backoffice.'.$route_file.'.edit',[$value->id])}}" class="dropdown-item">Edit</a>
													<a href="#" class="action-delete dropdown-item" data-url="{{route('backoffice.'.$route_file.'.destroy',[$value->id])}}" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eraser3"></i> Delete</a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/feather/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/pace.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/toastr.css')}}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/app.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/colors.min.css')}}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/plugins/extensions/toastr.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/simple-line-icons/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-climacon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-gradient.min.css')}}">
<!-- END Page Level CSS-->

<style type="text/css">
	.dropdown-item:focus, .dropdown-item:hover {
    color: #00b5b8; 
    text-decoration: none!important;
    background-color: #ffffff!important; 
    width: inherit!important;
}
</style>

@stop

@section('page-scripts')
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script type="text/javascript">
	$(function(){
		$(".action-delete").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-delete").attr({"href" : btn.data('url')});
		});	

	    $('.datatable-basic').DataTable();
	});
</script>

<style>

	.buttons-copy{
		display: none;
	}
	.buttons-csv{
		display: none;
	}
</style>
@stop
