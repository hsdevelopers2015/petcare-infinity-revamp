@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" value="{{str_pad($stock_in->count()+1,4,"0",STR_PAD_LEFT)}}" id="stock-count">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-box"></i>Stock In</h4>
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<?php
										if($errors->first('product_id')){
											$status = 'border-danger';
											$msg = $errors->first('product_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<label class="col-md-3 label-control" for="userinput4">Product</label>
										<div class="col-md-9" title="{{$errors->first('product_id')}}" data-toggle="tooltip">
											{!!Form::select("product_id", $products, old('product_id'), ['id' => "userinput4", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Quantity</label>
										<div class="col-md-9">
											<input type="number" name="qty" id="userinput3" class="form-control prod_cat {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Acquisition Cost</label>
										<div class="col-md-9">
											<input type="text" name="acquisition_cost" id="userinput3" class="form-control {{$errors->first('acquisition_cost') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('acquisition_cost')}}" data-toggle="tooltip" title="{{$errors->first('acquisition_cost') ? $errors->first('acquisition_cost') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput50">Supplier Name</label>
										<div class="col-md-9">
											<input type="text" name="supplier_name" id="userinput50" class="form-control {{$errors->first('supplier_name') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('supplier_name')}}" data-toggle="tooltip" title="{{$errors->first('supplier_name') ? $errors->first('supplier_name') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput51">Expiration Date</label>
										<div class="col-md-9">
											<input type="text" name="expiration_date" id="userinput51" class="form-control select_date {{$errors->first('expiration_date') ? 'border-danger' : NULL}}" placeholder="mm/dd/yyyy" min="1" value="{{Helper::date_format(old('expiration_date',Carbon::now()),'m/d/Y')}}" data-toggle="tooltip" title="{{$errors->first('expiration_date') ? $errors->first('expiration_date') : NULL}}">
											<small>Date format <code>mm/dd/yyyy</code></small>
											{{-- <input class="form-control" type="text" name="billing_date" data-toggle="datepicker" id="date"  id="billing_date" placeholder="" maxlength="50" value="{{Helper::date_format(old('billing_date',Carbon::now()),'m/d/Y')}}" autofocus> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Transaction Code</label>
										<div class="col-md-9">
											<input type="text" name="transaction_code" id="userinput5" class="form-control prod_cat {{$errors->first('transaction_code') ? 'border-danger' : NULL}}" placeholder="" min="1" value="{{old('transaction_code')}}" data-toggle="tooltip" title="{{$errors->first('transaction_code') ? $errors->first('transaction_code') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="left">
						<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@include('backoffice._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
{{-- @include('backoffice._includes.scripts') --}}
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
{{-- <script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/gmaps.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/jquery.knob.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/unslider-min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script> --}}
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/extensions/block-ui.js')}}" type="text/javascript"></script>

<script>
$("#userinput5").click(function(){
	var x = $("#userinput4").val();

	var stock_count = $("#stock-count").val();

	if(x != ""){

		var z =  'SI-PROD'+x+'-'+stock_count;
		$("#userinput5").val(z);
	}
});
</script>

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        $( ".select_date" ).datepicker({
                dateFormat: "mm/dd/yyyy",
        });
    })
</script>
@stop

