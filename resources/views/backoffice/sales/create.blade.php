@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} | {{$transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>

		@if(in_array($sales->status, ['draft']) AND in_array($auth->type, ['super_user','admin','sales_head','auditor']))
		<a href="#" class="btn btn-primary pull-right action-create" data-url="{{route('backoffice.'.$route_file.'.save',[$sales->id])}}" data-toggle="modal" data-target="#confirm-create"><i class="ft-check"></i>&nbsp;&nbsp;Confirm&nbsp;</a>
		@endif

		@if(($sales->status == "posted" OR $sales->status == "received") AND in_array($auth->type, ['super_user','admin','finance','auditor','sales_head']))
		<button class="btn btn-primary btn-md pull-right" type="button" data-toggle="modal" data-target="#ups"><i class="fa fa-money"></i>&nbsp;&nbsp; Update Payment Status</button>
		@endif
	</div>
</div>
<div class="row">
	@if(in_array($sales->status, ['draft','received']) AND Auth::user()->type != 'spectator' AND Auth::user()->type != 'finance')
	<div class="col-md-3">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">CLIENT</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
	
					<form class="form form-horizontal" action="{{route('backoffice.sales.client',[$sales->id])}}" method="POST" enctype="multipart/form-data">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-user"></i> Client</h4>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<?php
										if($errors->first('client_id')){
											$status = 'border-danger';
											$msg = $errors->first('client_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<div class="col-md-12" data-toggle="tooltip" title="{{$msg}}">
											{!!Form::select("client_id", $clients, old('client_id',$sales->client_id), ['id' => "select2-array ", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input type="text" name="due_day" value="{{$sales->due_day}}" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary overlay-unblock">
										<i class="fa fa-check-square-o"></i> Update Client
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">ADD ITEM FORM</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<form class="form form-horizontal" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="transaction_id" value="{{$sales->id}}">
						<div class="form-body">
							<h4 class="form-section">
								<div class="row">
									<div class="col-md-8">
										<i class="ft-box"></i>Add Item to the Sales 
									<small class="pull-left" style="font-size: 12px; margin-top: -25px;margin-bottom: -15px;">Current mode of payment is set to <code>{{str_replace('_', ' ', Str::upper($sales->mode_of_payment)).' '.$sales->payment_term}} </code></small>
									</div>
									<div class="col-md-4">
										<button class="btn btn-outline-primary btn-sm pull-right" type="button" data-toggle="modal" data-target="#mod" >Mode of Payment</button>
									</div>
								</div>
							</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<?php
										if($errors->first('product_id')){
											$status = 'border-danger';
											$msg = $errors->first('product_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<div class="col-md-12" data-toggle="tooltip" title="{{$msg}}">
											{!!Form::select("product_id", $products, old('product_id'), ['id' => "select2-array ", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<div class="col-md-5">
											<input type="number" name="qty" id="userinput3" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" min="1" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
										</div>
										<div class="col-md-3">
											<input type="number" name="free" id="userinput41" class="form-control {{$errors->first('free') ? 'border-danger' : NULL}}" placeholder="Free" min="1" value="{{old('free')}}" data-toggle="tooltip" title="{{$errors->first('free') ? $errors->first('free') : NULL}}">
										</div>
										<div class="col-md-4">
											<input type="number" name="cost_unit" id="userinput6" class="form-control {{$errors->first('cost_unit') ? 'border-danger' : NULL}}" placeholder="Cost/Unit" min="1" value="{{old('cost_unit')}}" data-toggle="tooltip" title="{{$errors->first('cost_unit') ? $errors->first('cost_unit') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row">
										<div class="col-md-6">
											<input type="number" name="discount" id="userinput4" min="0" class="form-control {{$errors->first('discount') ? 'border-danger' : NULL}}" placeholder="Discount Value" value="{{old('discount')}}" data-toggle="tooltip" title="{{$errors->first('discount') ? $errors->first('discount') : NULL}}">
										</div>
										<div class="col-md-6">
										{!!Form::select("discount_type", $discount_types, old('discount_type'), ['id' => "discount_type", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary overlay-unblock">
										<i class="fa fa-check-square-o"></i> Add Item
									</button>
								</div>
								<div class="col-md-2">
									<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#discount">
										<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Add Discount
									</a>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	@endif
</div>

<div class="row">
	
	<div class="col-md-12">
		<div class="card">
			<section id="file-export">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Item List</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
									<table class="table table-striped table-bordered dataTable file-export">
										<thead>
											<tr>
												<th class="text-center">Product Info</th>
												<th class="text-center">Qty.</th>
												<th class="text-center">Free</th>
												<th class="text-center">Price/Unit</th>
												<th class="text-center">Total Price</th>
												<th class="text-center">Discount</th>
												<th class="text-center">Final Price</th>
												@if(in_array($sales->status, ['draft','received']))
												<th class="text-center"></th>
												@endif
											</tr>
										</thead>
										<tbody>
											<?php
												$total_cost = [];
											?>
											@foreach($items as $index => $info)
											@if($info->product_info($info->product_id))
											<tr>
												<td class="text-center">
													{{$info->product_info($info->product_id)->product_code}}<br>
													{{$info->product_info($info->product_id)->product_name}}
												</td>
												<td class="text-center">{{$info->qty}}</td>
												<td class="text-center">{{$info->free}}</td>
												<td class="text-center">{{$info->cost_unit}}</td>
												<td class="text-center">{{number_format($info->qty*$info->cost_unit,2)}}</td>
												<?php 
												$total = $info->cost_unit * $info->qty;
												array_push($total_cost,$info->final_cost);
												?>
												<td class="text-center">{{$info->discount_type=="amount"? $info->discount :$total*($info->discount/100) }}</td>
												<td class="text-center">{{number_format($info->final_cost,2)}}</td>
												@if(in_array($sales->status, ['draft','received']))
												<td class="text-center">
													<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
														<div role="group" class="btn-group">
															<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
																<i class="ft-cog icon-left"></i> Action
															</button>
															<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
																<a href="{{route('backoffice.'.$route_file.'.remove',[$info->id])}}" class="dropdown-item">Remove Item</a>
															</div>
														</div>
													</div>
												</td>
												@endif
											</tr>
											@endif
											@endforeach
											<tr>
												<td colspan="6" style="text-align: right;">Total</td>
												<td colspan="2" style="text-align: left; color: green"><strong>{{number_format(array_sum($total_cost),2)}}</strong></td>
											</tr>
										</tbody>
									</table>
									<a href="{{ url()->previous() }}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to the sales list</a>

									@if($discounts->count() > 0)
									<a href="#" class="btn btn-outline-primary ml-1" data-toggle="modal" data-target="#discount_list">View Discounting</a>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            		<span aria-hidden="true">×</span>
	            	</button> --}}
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="{{route('backoffice.'.$route_file.'.save',[$sales->id])}}" class="btn btn-outline-info block-element" id="btn-confirm-create">Confirm</a>
			</div>
		</div>
	</div>
</div>

<div id="mod" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Mode of Payment</h5>
			</div>
			<form action="{{route('backoffice.sales.mod',[$sales->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Choose a mode of payment</label>
						{!!Form::select("mode_of_payment", $modes, old('mode_of_payment',$sales->mode_of_payment), ['id' => "select2-array", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 mod"]) !!}
					</div>
				</div>
				<!-- <div class="row payment_term ">
					<div class="col-md-12">
						<label>Choose payment term</label>
						{!!Form::select("payment_term", $terms, old('payment_term',$sales->payment_term), ['id' => "select2-array ", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
					</div>
				</div> -->
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Update</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div id="discount" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add Discount</h5>
			</div>
			<form action="{{route('backoffice.sales.add_discount',[$sales->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<input type="hidden" name="sales_id" value="{{$sales->id}}">
				<div class="form-body">
					<div class="row">

						<div class="col-md-4">
							<input type="text" name="discount_name" id="userinput4" min="0" class="form-control {{$errors->first('discount_name') ? 'border-danger' : NULL}}" placeholder="Discount Name" value="{{old('discount_name')}}" data-toggle="tooltip" title="{{$errors->first('discount_name') ? $errors->first('discount_name') : NULL}}" required="">
						</div>
						<div class="col-md-4">
							<input type="number" name="discount_amount" id="userinput4" min="0" class="form-control {{$errors->first('discount_amount') ? 'border-danger' : NULL}}" placeholder="Discount Amount" value="{{old('discount_amount')}}" data-toggle="tooltip" title="{{$errors->first('discount_amount') ? $errors->first('discount_amount') : NULL}}" required="">
						</div>
						<div class="col-md-4">
							{!!Form::select("discount_type", $discount_types, old('discount_type'), ['id' => "discount_type", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Add</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div id="discount_list" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Discount List</h5>
			</div>
			
			<div class="modal-body">
				<table class="table table-striped table-bordered table-responsive dataTable file-export">
					<thead>
						<tr>
							<th style="width: 3%;"></th>
							<th class="text-center">Discount Name</th>
							<th class="text-center">Amount</th>
							@if(in_array($sales->status, ['draft','received']))
							<th style="width: 10%;"></th>
							@endif
						</tr>
					</thead>
					<tbody>
						@foreach($discounts as $index => $info)
						<tr>
							<td>{{$index+1}}</td>
							<td>{{$info->discount_name}}</td>
							@if($info->discount_type == 'percentage')
							<td class="text-center">PHP {{number_format($info->sales->total_amount*($info->discount_amount/100),2)}} ({{$info->discount_amount}}%) </td>
							@else
							<td class="text-center">PHP {{number_format($info->discount_amount,2)}}</td>
							@endif
							@if(in_array($sales->status, ['draft','received']))
							<td>
							<a href="{{route('backoffice.'.$route_file.'.remove_discount',[$info->id])}}" class="danger"><i class="fa fa-trash"></i></a>
							</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="ups" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Update Payment Status</h5>
			</div>
			<form action="{{route('backoffice.sales.ups',[$sales->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Payment Status</label>
						{!!Form::select("payment_status", $statuses, old('payment_status',$sales->payment_status), ['id' => "select2-array", 'class' => "form-control select2 product payment_status  col-xs-12 col-sm-12 col-md-12 col-lg-5 mod"]) !!}
					</div>
				</div>
				<div class="row mt-2" id="balance">
					<div class="col-md-12">
						<label>Amount</label>
						<input type="number" name="balance" value="{{$sales->balance}}" class="form-control" required="">
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Update</button>
			</div>
			</form>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script type="text/javascript">

	$("#balance").hide(500);

	var val = $(".payment_status").val();
	if(val == 'partially_paid'){
		$("#balance").show(500);
	}

	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});

	$(".payment_status").change(function(){
		var val = $(this).val();

		if(val == 'partially_paid'){
			$("#balance").show(500);
		}else{
			$("#balance").hide(500);
		}
	});

	$( ".product" ).change(function() {
		var val = $(this).val();

		console.log(val);
		<?php 
		foreach($product_lists as $index => $info){
		?>
			if(val == "<?php echo $info->id ;?>"){
				$("#userinput6").val("<?php echo $info->cost;?>");
				$("#userinput3").attr('max',"<?php echo $info->physical_count($info->id)->quantity;?>");

				$("#userinput3").change(function(){
					var qty = $("#userinput3").val();

					<?php
					$condition = explode(', ',$info->deal_condition);
					$incentive = explode(', ',$info->deal_incentive);
					foreach($condition as $i => $row){
						?>
						if(qty == <?php echo $row; ?>){
							$("#userinput41").val(<?php echo $incentive[$i];?>);
						}
						<?php
					}
					?>
				});
			}
			<?php
		}
		?>
	});



</script>

<script src="{{asset('backoffice/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(".payment_term").ready(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});

	$(".mod").change(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});
</script>
@stop
