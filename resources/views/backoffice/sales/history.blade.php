@extends('backoffice._layouts.main')

@section('content')
<section class="timeline-center timeline-wrapper" id="timeline">
	<h3 class="page-title text-xs-center" style="margin-bottom: 40px;">"<strong>{{$sales->transaction_code}}</strong>"<br>Transaction History</h3>
	@if($transactions->count() == 0)
	<ul class="timeline">
		<li class="timeline-line"></li>
		<li class="timeline-group">
			<a class="btn btn-primary" href="#">
				<i class="fa fa-hand-pointer-o"></i>
				&nbsp;&nbsp;No History Yet
			</a>
		</li>
	</ul>
	@endif
	<ul class="timeline">
		<li class="timeline-line"></li>
		@foreach($transactions as $index => $info)
		<li class="timeline-item {{$index%2==1? "mt-3": NULL}}">
			<div class="timeline-badge">
				<span class="bg-{{Helper::action_color($info->action)}} bg-lighten-1" data-placement="left" data-toggle="tooltip" title=" {{$info->action}}"><i class="{{Helper::action_icon($info->action)}}"></i></span>
			</div>
			<div class="timeline-card card border-grey border-lighten-2">
				<div class="card-header">
					<h4 class="card-title"><a href="#">{{Str::title($info->action)}}</a></h4>
					<p class="card-subtitle text-muted mb-0 pt-1">
						<span class="font-small-3">{{Helper::date_format($info->created_at,'d M , Y @ h:i a')}}</span>
					</p>
					<a class="heading-elements-toggle">
						<i class="fa fa-ellipsis-v font-medium-3"></i>
					</a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li>
								<a data-action="reload">
									<i class="ft-rotate-cw"></i>
								</a>
							</li>
							<li>
								<a data-action="expand">
									<i class="ft-maximize"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-body collapse in">
						<div class="card-block">
							<h4 class="card-text"> {{Str::title($info->description)}}</h4>
						</div>
					</div>
				</div>
			</div>
		</li>
		@endforeach
		<li style="list-style: none"></li>
	</ul>
</section>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/users.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
@stop

@section('page-scripts')
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBDkKetQwosod2SZ7ZGCpxuJdxY3kxo5Po" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/gmaps.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/photo-swipe/photoswipe.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/pages/timeline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
@stop