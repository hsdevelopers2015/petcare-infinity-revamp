@extends('backoffice._layouts.main')

@section('content')
<div id="user-profile">
	<div class="row">
		<div class="col-xs-12">
			<div class="card profile-with-cover">
				<div class="card-img-top img-fluid bg-cover height-300" style="background: url('') 50%; background-color: lightgray; "></div>
				<div class="media profil-cover-details">
					<div class="media-left pl-2 pt-2">
						<a class="profile-image" href="#">
							@if($profile->filename)
							<img alt="Card image" class="rounded-circle img-border height-100" src="{{asset($profile->directory.'/thumbnails/'.$profile->filename)}}">
							@else
							<img alt="Card image" class="rounded-circle img-border height-100" src="{{asset('backoffice/face0.jpg')}}">
							@endif
						</a>
					</div>
					<div class="media-body media-middle row">
						<div class="col-xs-6">
							<h3 class="card-title">{{$profile->business_info->business_name}}<br>{{$profile->fname}} {{$profile->lname}}</h3>
						</div>
						<div class="col-xs-6 text-xs-right">
							<button class="btn btn-info hidden-xs-down" data-toggle="modal" data-target="#business-info" type="button"><i class="fa fa-briefcase"></i> Business Information</button>
							{{-- <div aria-label="Basic example" class="btn-group hidden-md-down" role="group">
								<button class="btn btn-success" type="button"><i class="fa fa-dashcube"></i> Message</button> <button class="btn btn-success" type="button"><i class="fa fa-cog"></i></button>
							</div> --}}
						</div>
					</div>
				</div>
				<nav class="navbar navbar-light navbar-profile">
					<button aria-controls="exCollapsingNavbar2" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler hidden-sm-up" data-target="#exCollapsingNavbar2" data-toggle="collapse" type="button"></button>
					<div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
						<ul class="nav navbar-nav float-xs-right">
							<li class="nav-item">
								<a class="nav-link"><i class="ft-phone"></i>{{$profile->contact}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="fa fa-envelope"></i>{{$profile->email}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="ft-map-pin"></i>{{$profile->address}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="fa fa-users"></i>Segment No. {{$profile->segment}}</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-3 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="primary">{{$invoice->count()}} </h3>
								<span>Collectibles ({{number_format($profile->unpaid_bills($profile->id),2)}})</span>
							</div>
							<div class="media-right media-middle">
								<i class="fa fa-file-text-o primary font-large-2 float-xs-right"></i>
							</div>
						</div>
						<a href="#" data-toggle="modal" data-target="#invoice" class="btn btn-primary btn-sm mt-1 col-md-12">View</a>
						{{-- <progress class="progress progress-sm progress-primary mt-1 mb-0" value="80" max="100"></progress> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="info">{{$contracts->count()}}</h3>
								<span>Contracts</span>
							</div>
							<div class="media-right media-middle">
								<i class="fa fa-file-text info font-large-2 float-xs-right"></i>
							</div>
							<a href="#" data-toggle="modal" data-target="#contracts" class="btn btn-info btn-sm mt-1 col-md-12">View</a>
							{{-- <progress class="progress progress-sm progress-danger mt-1 mb-0" value="40" max="100"></progress> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="success">{{$profile->total_purchased($profile->id)->count()}}</h3>
								<span>Purchased</span>
							</div>
							<div class="media-right media-middle">
								<i class="icon-layers success font-large-2 float-xs-right"></i>
							</div>
							<a href="#" data-toggle="modal" data-target="#purchased" class="btn btn-success btn-sm mt-1 col-md-12">View</a>
							{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="warning">{{$profile->total_orders($profile->id)->count()}}</h3>
								<span>On Going Orders</span>
							</div>
							<div class="media-right media-middle">
								<i class="icon-basket-loaded warning font-large-2 float-xs-right"></i>
							</div>
							<a href="#" data-toggle="modal" data-target="#on_going" class="btn btn-warning btn-sm mt-1 col-md-12">View</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<section class="timeline-center timeline-wrapper" id="timeline">
        <h3 class="page-title text-xs-center">Timeline</h3>

        @if($user_timeline->count() == 0)
        <ul class="timeline">
            <li class="timeline-line"></li>
            <li class="timeline-group">
                <a class="btn btn-primary" href="#"><i class="fa fa-hand-pointer-o"></i>&nbsp;&nbsp;No Action Taken Yet</a>
            </li>
        </ul>
        @endif
        <ul class="timeline">
            <li class="timeline-line"></li><!-- /.timeline-line -->
            {{-- <li class="timeline-item">
                <div class="timeline-badge">
                    <span class="avatar avatar-online" data-placement="right" data-toggle="tooltip" title="Eu pid nunc urna integer"><img alt="avatar" src="{{asset('backoffice/app-assets/images/portrait/small/avatar-s-5.png')}}"></span>
                </div>
                <div class="timeline-card card card-inverse">
                    <img alt="Card image" class="card-img img-fluid" src="{{asset('backoffice/app-assets/images/portfolio/width-1200/portfolio-2.jpg')}}">
                    <div class="card-img-overlay bg-overlay">
                        <h4 class="card-title">Good Morning</h4>
                        <p class="card-text"><small>26 Aug, 2016 at 2.00 A.M</small></p>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p class="card-text">Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis ut nunc elit! Sociis in et? Rhoncus, vel dignissim in scelerisque. Dolor lacus pulvinar adipiscing adipiscing montes!</p>
                    </div>
                </div>
            </li> --}}
            
            @foreach($user_timeline as $index => $info)
            <li class="timeline-item {{$index%2==1? "mt-3": NULL}}">
                <div class="timeline-badge">
                    <span class="bg-{{Helper::action_color($info->action_type)}} bg-lighten-1" data-placement="left" data-toggle="tooltip" title="{{$info->user_info($info->user_id)->fname}} {{$info->action}}"><i class="{{Helper::action_icon($info->action_type)}}"></i></span>
                </div>
                <div class="timeline-card card border-grey border-lighten-2">
                    <div class="card-header">
                        <h4 class="card-title"><a href="#">{{Str::title($info->action_type)}}</a></h4>
                        <p class="card-subtitle text-muted mb-0 pt-1"><span class="font-small-3">{{Helper::date_format($info->created_at,'d M , Y @ h:i a')}} </span></p><a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                </li>
                                <li>
                                    <a data-action="expand"><i class="ft-maximize"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <h4 class="card-text">{{$info->user_info($info->user_id)->fname}} {{$info->action}}</h4>
                            </div>
                        </div>
                        <!-- <div class="card-footer px-0 py-0">
                            <div class="card-block">
                                {{-- <fieldset class="form-group position-relative has-icon-left mb-0">
                                    <input class="form-control" placeholder="Write comments..." type="text">
                                    <div class="form-control-position">
                                        <i class="fa fa-dashcube"></i>
                                    </div>
                                </fieldset> --}}
                            </div>
                        </div> -->
                    </div>
                </div>
            </li>
            @endforeach

            <li style="list-style: none"></li>
        </ul><!-- 2015 -->


    </section>
	
</div>
@stop

@section('page-modals')
<div id="business-info" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->business_info->business_name}}</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Information!</span> These are the information about the client's business.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>
				
				<div class="row">
					<div class="col-md-12">
						<img src="{{asset($profile->business_info->directory.'/'.$profile->business_info->filename)}}" class="img-responsive img-thumbnail" style="width: 100%;">
					</div>
				</div>

				<hr>

				<h6 class="text-semibold">Business Type :</h6>
				<p>{{$profile->business_info->business_type}}</p>
				<h6 class="text-semibold">Business Address :</h6>
				<p>{{$profile->business_info->business_location}}</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="invoice" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Invoice List</h5>
			</div>

			<div class="modal-body">
				<div class="row">
				    <div class="col-md-4">
						<div class="card border-success">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body text-xs-left">
											<h3 class="success">{{number_format($profile->paid_bills($profile->id),2)}}</h3>
											<span>Paid Amount</span>
										</div>
										<div class="media-right media-middle">
											<i class="fa fa-check success font-large-2 float-xs-right"></i>
										</div>
										{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card border-primary">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body text-xs-left">
											<h3 class="primary">{{number_format($profile->collectible_bills($profile->id),2)}}</h3>
											<span>Collectible Amount</span>
										</div>
										<div class="media-right media-middle">
											<i class="fa fa-file-text-o primary font-large-2 float-xs-right"></i>
										</div>
										{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card border-danger">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body text-xs-left">
											<h3 class="danger">{{number_format($profile->unpaid_bills($profile->id),2)}}</h3>
											<span>Unpaid Amount</span>
										</div>
										<div class="media-right media-middle">
											<i class="fa fa-exclamation danger font-large-2 float-xs-right"></i>
										</div>
										{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">

						<table class="table table-striped table-bordered dataTable file-export">
							<thead>
								<tr>
									<th>Transaction Code</th>
									<th>Agent</th>
									<th>Transaction Details</th>
								</tr>
							</thead>
							<tbody>
								@foreach($invoice as $index => $info)
								<tr>
									<td><a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}">{{$info->transaction_code}}</a></td>
									<td>{{$info->sales_agent_info($info->sa_id)->fname}} {{$info->sales_agent_info($info->sa_id)->lname}}</td>
									<td>
										Qty: {{number_format($info->total_qty)}} ( {{$info->total_amount}} )
										@if($info->mode_of_payment!='cash_on_delivery')
										<br>Payment Term: {{str_replace('_', ' ', $info->payment_term)}}
										@endif
										<br>Payment Status: {!!Helper::payment_status_badge($info->payment_status)!!}
										<br>Mode of Payment: {!!Helper::sales_status_badge($info->mode_of_payment)!!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="contracts" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Contracts</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						@if($contracts->count() > 0)
						<table class="table table-striped table-bordered dataTable file-export">
							<thead>
								<tr>
									<th>Contract Code</th>
									<th>Sales Agent (Sender)</th>
									<th>Contract Details</th>
								</tr>
							</thead>
							<tbody>
								@foreach($contracts as $index => $info)
								<tr>
									<td><a href="{{asset($info->contract_info($info->contract_id)->directory.'/'.$info->contract_info($info->contract_id)->filename)}}" download="{{$info->contract_info($info->contract_id)->contract_code.'|Contract'}}" target="_blank" title="Download Contract" >
										{{$info->contract_info($info->contract_id)->contract_code}}
										</a>
									</td>
									<td>{{$info->sa_info($info->sa_id)->fname}} {{$info->sa_info($info->sa_id)->lname}}</td>
									<td>{{$info->contract_info($info->contract_id)->detail}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h3 class="text-md-center">No Contract Uploaded Yet</h3>
						@endif
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="purchased" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Purchased Products</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						@if($profile->total_purchased($profile->id)->count() > 0)
						<table class="table table-striped table-bordered dataTable file-export">
							<thead>
								<tr>
									<th>Transaction Code</th>
									<th>Status</th>
									<th>Total Quantity</th>
								</tr>
							</thead>
							<tbody>
								@foreach($profile->total_purchased($profile->id) as $index => $info)
								<tr>
									<td>
									<a target="_blank" href="{{route('backoffice.orders.view',[$info->id.'-'.$info->transaction_code])}}">{{$info->transaction_code}}</a>
									</td>
									<td>{!!Helper::order_status_badge($info->status)!!}</td>
									<td>{{$info->total_qty}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h3 class="text-md-center">No Product Purchased Yet</h3>
						@endif
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="on_going" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Ongoing Orders</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						@if($profile->total_orders($profile->id)->count() > 0)
						<table class="table table-striped table-bordered dataTable file-export">
							<thead>
								<tr>
									<th>Transaction Code</th>
									<th>Status</th>
									<th>Total Quantity</th>
								</tr>
							</thead>
							<tbody>
								@foreach($profile->total_orders($profile->id) as $index => $info)
								<tr>
									<td>
									<a target="_blank" href="{{route('backoffice.orders.view',[$info->id.'-'.$info->transaction_code])}}">{{$info->transaction_code}}</a>
									</td>
									<td>{!!Helper::order_status_badge($info->status)!!}</td>
									<td>{{$info->total_qty}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h3 class="text-md-center">No Product Purchased Yet</h3>
						@endif
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/users.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script src="{{asset('backoffice/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/pages/timeline.min.js')}}" type="text/javascript"></script>
@stop