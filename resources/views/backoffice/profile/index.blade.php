@extends('backoffice._layouts.main')

@section('content')
<div id="user-profile">
	<div class="row">
		<div class="col-xs-12">
			<div class="card profile-with-cover">
				<div class="card-img-top img-fluid bg-cover height-300" style="background: url('') 50%; background-color: lightgray; "></div>
				<div class="media profil-cover-details">
					<div class="media-left pl-2 pt-2">
						<a class="profile-image" href="#">
							@if($profile->filename)
							<img alt="Card image" class="rounded-circle img-border height-100" src="{{asset($profile->directory.'/thumbnails/'.$profile->filename)}}">
							@else
							<img alt="Card image" class="rounded-circle img-border height-100" src="{{asset('backoffice/face0.jpg')}}">
							@endif
						</a>
					</div>
					<div class="media-body media-middle row">
						<div class="col-xs-6 pb-3">
							<h2 class="card-title" style="font-size: 30px; margin-bottom: 0px;">TEAM {{Str::title($profile->cluster($profile->cluster_id)->island)}} <br>{{-- {{Str::title($profile->cluster($profile->cluster_id)->city)}} --}} Area {{Str::title($profile->cluster($profile->cluster_id)->area)}}</h2>
							<h3 class="card-title">{{$profile->fname}} {{$profile->lname}}</h3>
						</div>
						{{-- <div class="col-xs-6 text-xs-right">
							<button class="btn btn-primary hidden-xs-down" type="button"><i class="fa fa-plus"></i> Follow</button>
							<div aria-label="Basic example" class="btn-group hidden-md-down" role="group">
								<button class="btn btn-success" type="button"><i class="fa fa-dashcube"></i> Message</button> <button class="btn btn-success" type="button"><i class="fa fa-cog"></i></button>
							</div>
						</div> --}}
					</div>
				</div>
				<nav class="navbar navbar-light navbar-profile">
					<button aria-controls="exCollapsingNavbar2" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler hidden-sm-up" data-target="#exCollapsingNavbar2" data-toggle="collapse" type="button"></button>
					<div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
						<ul class="nav navbar-nav float-xs-right">
							<li class="nav-item">
								<a class="nav-link"><i class="ft-phone"></i>{{$profile->contact}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="fa fa-envelope"></i>{{$profile->email}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="ft-map-pin"></i>{{$profile->address}} |</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"><i class="fa fa-users"></i>TEAM {{Str::title($profile->cluster($profile->cluster_id)->island)}} {{-- {{Str::title($profile->cluster($profile->cluster_id)->city)}} --}} Area {{Str::title($profile->cluster($profile->cluster_id)->area)}}</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-4 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="primary">{{$profile->clients_count($profile->cluster_id)->count()}}</h3>
								<span>Total Cutomers</span>
							</div>
							<div class="media-right media-middle">
								<i class="ft-user primary font-large-2 float-xs-right"></i>
							</div>
						</div>
						<a href="#" data-toggle="modal" data-target="#total_customers" class="btn btn-primary btn-sm mt-1 col-md-12">View</a>
						{{-- <progress class="progress progress-sm progress-primary mt-1 mb-0" value="80" max="100"></progress> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="info">{{$profile->total_ordering_customers($profile->cluster_id)->count()}}</h3>
								<span>Ordering Customers</span>
							</div>
							<div class="media-right media-middle">
								<i class="ft-user-plus info font-large-2 float-xs-right"></i>
							</div>
							<a href="#" data-toggle="modal" data-target="#total_ordering_customers" class="btn btn-info btn-sm mt-1 col-md-12">View</a>
							{{-- <progress class="progress progress-sm progress-danger mt-1 mb-0" value="40" max="100"></progress> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-lg-6 col-xs-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
						<div class="media">
							<div class="media-body text-xs-left">
								<h3 class="success">{{number_format($profile->on_going_sales($profile->cluster_id,$profile->id,'total_amount'),2)}}</h3>
								<span>Ongoing Sales</span>
							</div>
							<div class="media-right media-middle">
								<i class="fa fa-money success font-large-2 float-xs-right"></i>
							</div>
							<a href="#" data-toggle="modal" data-target="#ongoing_sales" class="btn btn-success btn-sm mt-1 col-md-12">View</a>
							{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<section id="chartjs-pie-charts">
				<div class="row">
					<!-- Simple Pie Chart -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Quota</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									<canvas id="simple-pie-chart" height="400"></canvas>
								</div>
							</div>
						</div>
					</div>

				</div>
			</section>
		</div>
		<div class="col-md-8">
			<section class="timeline-center timeline-wrapper" id="timeline">
				<h3 class="page-title text-xs-center">Timeline</h3>
				@if($user_timeline->count() == 0)
				<ul class="timeline">
					<li class="timeline-line"></li>
					<li class="timeline-group">
						<a class="btn btn-primary" href="#"><i class="fa fa-hand-pointer-o"></i>&nbsp;&nbsp;No Action Taken Yet</a>
					</li>
				</ul>
				@endif
				<ul class="timeline">
					<li class="timeline-line"></li>
					@foreach($user_timeline as $index => $info)
					<li class="timeline-item {{$index%2==1? "mt-3": NULL}}">
						<div class="timeline-badge">
							<span class="bg-{{Helper::action_color($info->action_type)}} bg-lighten-1" data-placement="left" data-toggle="tooltip" title="{{$info->user_info($info->user_id)->fname}} {{$info->action}}"><i class="{{Helper::action_icon($info->action_type)}}"></i></span>
						</div>
						<div class="timeline-card card border-grey border-lighten-2">
							<div class="card-header">
								<h4 class="card-title"><a href="#">{{Str::title($info->action_type)}}</a></h4>
								<p class="card-subtitle text-muted mb-0 pt-1"><span class="font-small-3">{{Helper::date_format($info->created_at,'d M , Y @ h:i a')}} </span></p><a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li>
											<a data-action="reload"><i class="ft-rotate-cw"></i></a>
										</li>
										<li>
											<a data-action="expand"><i class="ft-maximize"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-body collapse in">
									<div class="card-block">
										<h4 class="card-text">{{$info->user_info($info->user_id)->fname}} {{$info->action}}</h4>
									</div>
								</div>
							</div>
						</div>
					</li>
					@endforeach

					<li style="list-style: none"></li>
				</ul><!-- 2015 -->


			</section>
		</div>
	</div>
</div>
@stop

@section('page-modals')

@foreach($profile->clients_count($profile->cluster_id) as $index => $info)
<div id="view-business-{{$info->id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$info->business_info->business_name}} </h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Information!</span> These are the information about the client's business.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>
				
				<div class="row">
					<div class="col-md-12">
						<img src="{{asset($info->business_info->directory.'/'.$info->business_info->filename)}}" class="img-responsive img-thumbnail" style="width: 100%;">
					</div>
				</div>

				<hr>

				<h6 class="text-semibold">Business Type :</h6>
				<p>{{$info->business_info->business_type}}</p>
				<h6 class="text-semibold">Business Address :</h6>
				<p>{{$info->business_info->business_location}}</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach

<div id="total_customers" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Customers</h5>
			</div>

			<div class="modal-body">

	            <table class="table table-striped table-bordered dataTable file-export">
	            	<thead>
	            		<tr>
	            			<th>Name</th>
	            			<th>Contact Info</th>
	            			<th>Address</th>
	            			<th>Business Info</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            	@foreach($profile->clients_count($profile->cluster_id) as $index => $data)
	            		<tr>
	            			<td><a href="{{route('backoffice.profile.user_profile',[$data->username])}}">{{$data->fname.' '.$data->lname}}</a></td>
	            			<td>{{$data->contact}}<br><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
	            			<td>{{Str::limit($data->address,$limit=15)}}</td>
	            			<td><a href="#"  data-toggle="modal" data-target="#view-business-{{$data->id}}" data-dismiss="modal"><i class="fa fa-info-circle text-success"></i>&nbsp;{{$data->business_info->business_name}}</a></td>
	            		</tr>
	            		@endforeach
	            	</tbody>
	            </table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="total_ordering_customers" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Total Ordering Customers</h5>
			</div>

			<div class="modal-body">

				<table class="table table-striped table-bordered dataTable file-export">
					<thead>
						<tr>
							<th style="width: 3%;" class="sorting_desc"></th>
							<th>Transaction Code</th>
							<th style="width: 50%">Client</th>
							<th>Status</th>
							<th>Total Quantity</th>
						</tr>
					</thead>
					<tbody>
						@foreach($profile->total_ordering_customers($profile->cluster_id) as $index => $info)
						<tr>
							<td>{{$index+1}}</td>
							<td>
								<a target="_blank" href="{{route('backoffice.orders.view',[$info->id.'-'.$info->transaction_code])}}">{{$info->transaction_code}}</a>
							</td>
							<td>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</td>
							<td>{!!Helper::order_status_badge($info->status)!!}</td>
							<td>{{$info->total_qty}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="ongoing_sales" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s On Going Sales</h5>
			</div>

			<div class="modal-body">

				<div class="row">
					<div class="col-md-6">
						<div class="card border-success">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body text-xs-left">
											<h3 class="success">{{number_format($profile->total_sales($profile->id,'on_going'),2)}}</h3>
											<span>On Going Sales</span>
										</div>
										<div class="media-right media-middle">
											<i class="fa fa-area-chart success font-large-2 float-xs-right"></i>
										</div>
										{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card border-primary">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body text-xs-left">
											<h3 class="primary">{{number_format($profile->total_sales($profile->id,'all'),2)}}</h3>
											<span>Sales Agent Overall Total Sales</span>
										</div>
										<div class="media-right media-middle">
											<i class="fa fa-line-chart primary font-large-2 float-xs-right"></i>
										</div>
										{{-- <progress class="progress progress-sm progress-success mt-1 mb-0" value="60" max="100"></progress> --}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<table class="table table-striped table-bordered dataTable file-export">
					<thead>
						<tr>
							<th style="width: 3%;" class="sorting_desc"></th>
							<th>Transaction Code</th>
							<th style="width: 30%">Client</th>
							<th style="width: 70%">Transaction Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($profile->on_going_sales($profile->cluster_id,$profile->id,'sales') as $index => $info)
						<tr>
							<td>{{$index+1}}</td>
							<td>
								<a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}">{{$info->transaction_code}}</a>
							</td>
							<td>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</td>
							<td>
								<div class="row">
									<div class="col-md-9">
										Qty: {{number_format($info->total_qty)}} ( {{$info->total_amount}} )
										@if($info->mode_of_payment!='cash_on_delivery')
										<br>Payment Term: {{str_replace('_', ' ', $info->payment_term)}}
										@endif
										<br>Payment Status: {!!Helper::payment_status_badge($info->payment_status)!!}
										<br>Mode of Payment: {!!Helper::sales_status_badge($info->mode_of_payment)!!}
									</div>
									<div class="col-md-3">
										<a target="_blank" href="{{route('backoffice.sales.add',[$info->transaction_code])}}" class="btn btn-outline-primary btn-sm">View Items</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="invoice_contract" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$profile->fname}}'s Total Ordering Customers</h5>
			</div>

			<div class="modal-body">

				<table class="table table-striped table-bordered dataTable file-export">
					<thead>
						<tr>
							<th style="width: 3%;" class="sorting_desc"></th>
							<th>Transaction Code</th>
							<th style="width: 50%">Client</th>
							<th>Status</th>
							<th>Total Quantity</th>
						</tr>
					</thead>
					<tbody>
						@foreach($profile->total_ordering_customers($profile->cluster_id) as $index => $info)
						<tr>
							<td>{{$index+1}}</td>
							<td>
								<a target="_blank" href="{{route('backoffice.orders.view',[$info->id.'-'.$info->transaction_code])}}">{{$info->transaction_code}}</a>
							</td>
							<td>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</td>
							<td>{!!Helper::order_status_badge($info->status)!!}</td>
							<td>{{$info->total_qty}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/users.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/pages/timeline.min.css')}}">
@stop

@section('page-scripts')
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBDkKetQwosod2SZ7ZGCpxuJdxY3kxo5Po" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/charts/gmaps.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/photo-swipe/photoswipe.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js')}}" type="text/javascript"></script>
{{-- <script src="{{asset('backoffice/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script> --}}
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
{{-- <script src="{{asset('backoffice/app-assets/js/scripts/charts/echarts/bar-column/stacked-column.js')}}" type="text/javascript"></script> --}}
{{-- <script src="{{asset('backoffice/app-assets/js/scripts/charts/echarts/radar-chord/non-ribbon-chord.min.js')}}" type="text/javascript"></script> --}}
<script src="{{asset('backoffice/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/pages/timeline.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

<script type="text/javascript">
    $(window).on("load", function() {
        var a = $("#simple-pie-chart"),
        t = {
            responsive: !0,
            maintainAspectRatio: !1,
            responsiveAnimationDuration: 500
        },
        i = {
            labels: ["Current Sales ", "Quota",],
            datasets: [{
                label: "My First dataset",
                data: [{!!number_format($profile->total_sales($profile->id,'month'))!!}, {!!$sales_quota_amount!!},],
                backgroundColor: ["#00A5A8", "#FF7D4D", ]
            }]
        },
        e = {
            type: "pie",
            options: t,
            data: i
        };
        new Chart(a, e)
    });
</script>
@stop