@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>					
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form id="form-target" class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="transaction_code">Transaction Code</label>
										<div class="col-md-9">
											<input readonly type="text" name="transaction_code" id="transaction_code" class="form-control {{$errors->first('transaction_code') ? 'border-danger' : NULL}}" placeholder="Category code" value="{{old('transaction_code',$sequence)}}" data-toggle="tooltip" title="{{$errors->first('transaction_code') ? $errors->first('transaction_code') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="supplier_code">Supplier</label>
										<div class="col-md-9">
											{!!Form::select("supplier_code", $supplier_code, old('supplier_code'), ['id' => "supplier_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('supplier_code')}}"]) !!}
										</div>
									</div>
								</div>	
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="to">Branch</label>
										<div class="col-md-9">
											{!!Form::select("to", $branch_code, old('to'), ['id' => "to", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('to')}}"]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="description">Description</label>
										<div class="col-md-9">
											<input type="text" name="description" id="description" class="form-control {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="Description" value="{{old('description')}}" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">
										</div>
									</div>
								</div>						
							</div>
						</div>
						<hr>
						<div class="form-body">
							<div class="row mt-3">
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="supplier_code">Brand</label>
									<div class="col-md-9">
										{!!Form::select("brand_code", $brand_code, old('brand_code'), ['id' => "brand_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('brand_code')}}"]) !!}
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="supplier_code">Product</label>
									<div class="col-md-9">
										{!!Form::select("product_code", $product_code, old('product_code'), ['id' => "product_code",'name' => "product_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('product_code')}}"]) !!}
									</div>
								</div>
							</div>	
							{{--
								<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Code</label>
									<div class="col-md-9">
										<input type="text" name="qty" id="qty" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
									</div>
								</div>
							</div>
								--}}
							
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Quantity</label>
									<div class="col-md-9">
										<input type="text" name="qty" id="qty" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
									</div>
								</div>
							</div>
							
								<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="cost">Cost Price</label>
									<div class="col-md-9">
										<input readonly type="number" name="cost" id="cost" class="form-control {{$errors->first('cost') ? 'border-danger' : NULL}}" placeholder="Cost price" value="{{old('cost')}}" data-toggle="tooltip" title="{{$errors->first('cost') ? $errors->first('cost') : NULL}}">
									</div>
								</div>
							</div>
							
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="selling">Selling Price</label>
									<div class="col-md-9">
										<input readonly type="text" name="selling" id="selling" class="form-control {{$errors->first('selling') ? 'border-danger' : NULL}}" placeholder="Selling price" value="{{old('selling')}}" data-toggle="tooltip" title="{{$errors->first('selling') ? $errors->first('selling') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control"></label>
									<div class="col-md-9">
										<input type="button" class="add-row" name = "add-row" value="Add Row">
									</div>
								</div>
							</div>
							<div class="container">
								<table id="product-table">
								       <thead>
								           <tr>
								               <th>Select</th>
								               <th>Brand Code</th>
								               <th>Product Name</th>
								               <th>Quantity</th>
								               <th>Cost</th>
								               <th>Selling</th>
								               <th>Expiration Date</th>
								           </tr>
								       </thead>
								       <tbody>
								           <tr>
								           </tr>
								       </tbody>
								       <tfoot>
								       	<tr>
								       		<td colspan="3">Total</td>
								       		<td><div id="totalQty">0</div></td>
								       		<td><div id="totalCost" style="display: block;">0.00</div></td>
								       		<td colspan="2"><div id="totalSelling" style="display: block;">0.00</div></td>
								       	</tr>
								       </tfoot>
								   </table>
								   <button type="button" class="delete-row">Delete Row</button>								
							</div>
						</div>
					</div>	
					<div class="left">
					<hr>
					<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
						<i class="ft-x"></i> Cancel
					</a>
					<button type="submit" class="btn btn-primary overlay-unblock">
						<i class="fa fa-check-square-o"></i> Save
					</button>
					</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('page-styles')
<style type="text/css">
    form{
        margin: 20px 0;
    }
    form input, button{
        padding: 5px;
    }
    table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
    table, th, td{
        border: 1px solid #cdcdcd;
    }
    table th, table td{
        padding: 10px;
        text-align: left;
    }
</style>
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">



    $(function() {

    	  $('select[name="product_code"]').on('change', function() {
    	      var product_code = $(this).val();

    	          if(product_code != "") {
    	              $.ajax({
    	                //get the outstanding balance
    	                  url: '/transaction_hdr/cost_price/'+encodeURI(product_code),      
    	                  type: "GET",
    	                  dataType: "json",
    	                  success:function(data) {
    	                  	$('#cost').val(data.cost);
    	                  	$('#selling').val(data.selling);
    	                    
    	                  }
    	              });
    	          }
    	        // $('#cash_advance_balance').val("0.00");
    	});

    	 var a = 1234; 

    	 $('#totalQty').val(a);

    	$(".add-row").click(function(){
            var product_code = $("#product_code").val();
            var qty = $("#qty").val();
            var brand_code = $("#brand_code").val();
            var cost = $("#cost").val();
            var selling = $("#selling").val();
            var expiration_date = "--";
            var markup = "<tr><td><input type='checkbox' name='record'><td><input type='hidden'  class='form-control input-sm' name='brand_code[]' value='" + brand_code + "'>" + brand_code + "</td></td><td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='" + product_code + "'>"+product_code +"</td><td><input type='hidden'  class='form-control input-sm' name='qty[]' value='" + qty + "'>" + qty + "</td><td><input type='hidden'  class='form-control input-sm' name='cost[]' value='" + cost + "'>" + cost + "</td><td><input type='hidden'  class='form-control input-sm' name='selling[]' value='" + selling + "'>" + selling + "</td><td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='" + expiration_date + "'>" + expiration_date + "</td></tr>";
            $("table tbody").append(markup);
            

            
        });     	   
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });


    });
</script>
@stop
