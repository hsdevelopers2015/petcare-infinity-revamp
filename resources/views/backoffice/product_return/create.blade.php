@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="transaction_code" value="{{'PR-'.Helper::date_format(Carbon::now(),'mdy').'-'.Str::upper(Str::quickRandom($length = 4))}}">
						<div class="form-body">
							<div class="row ">
								<div class="col-md-6">
									<h4 class="form-section"><i class="ft-box"></i>Product Returns</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group row">
												<?php
												if($errors->first('stock_id')){
													$status = 'border-danger';
													$msg = $errors->first('stock_id');
												}else{
													$status = "";
													$msg  = "";
												}
												?>
												<label class="col-md-3 label-control" for="stock_id">Stock In</label>
												<div class="col-md-9" title="{{$errors->first('stock_id')}}" data-toggle="tooltip">
													{!!Form::select("stock_id", $stocks, old('stock_id'), ['id' => "stock_id", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
													<label style="font-size: 12px;">Note: Please choose a <code>stock in</code> that you want to return. </label>
												</div>
											</div>
										</div>
									</div>
									<div class="row stock_info">
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-md-3 label-control" for="userinput1">Qty</label>
												<div class="col-md-9" title="{{$errors->first('stock_id')}}" data-toggle="tooltip">
													{{-- <input type="number" class="form-control" name="qty" placeholder="Quantity" id="userinput1"> --}}
													<input type="number" min="0" name="qty" id="userinput2" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
													<label style="font-size: 12px;">Note: Number of products that has <code>defect</code>.</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<h4 class="form-section"><i class="ft-box"></i>Stock In Information</h4>
									<div class="row stock_info">
										<div class="col-md-10">
											<div class="form-group row">
												<label class="col-md-5 label-control">Product Code :</label>
												<div class="col-md-7" data-toggle="tooltip">
													<strong id="product_code"></strong>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-5 label-control">Total Quantity :</label>
												<div class="col-md-7" data-toggle="tooltip">
													<strong id='qty'></strong>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-5 label-control">Expiration Date :</label>
												<div class="col-md-7" data-toggle="tooltip">
													<strong id="expi_date"></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="left">
							<hr>
							<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('backoffice._includes.styles')
{{-- {!! $map['js'] !!} --}}
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')
<script>

	$(".stock_info").hide();
	$("#stock_id").change(function(){

		var stock_id = $(this).val();

		if(stock_id == ""){
			$(".stock_info").hide(500);
		}else{
			$(".stock_info").show(500);
		}

		<?php 
		foreach($stock_in as $index => $info){
		?>
			if(stock_id == "<?php echo $info->id ;?>"){
				$("#qty").text("<?php echo $info->qty;?>");
				$("#userinput1").attr('max',"<?php echo $info->qty;?>");
				$("#product_code").text("<?php echo $info->product ? $info->product->product_code : 'THIS PRODUCT HAS BEEN DELETED';?>");
				$("#expi_date").text("<?php echo Helper::date_format($info->expiration_date,'F d, Y');?>");
			}
		<?php
		}
		?>

	});

	var stock_id = $("#stock_id").val();

		if(stock_id == ""){
			$(".stock_info").hide(500);
		}else{
			$(".stock_info").show(500);
		}

		<?php 
		foreach($stock_in as $index => $info){
		?>
			if(stock_id == "<?php echo $info->id ;?>"){
				$("#qty").text("<?php echo $info->qty;?>");
				$("#userinput1").attr('max',"<?php echo $info->qty;?>");
				$("#product_code").text("<?php echo $info->product ? $info->product->product_code : 'THIS PRODUCT HAS BEEN DELETED';?>");
				$("#expi_date").text("<?php echo Helper::date_format($info->expiration_date,'F d, Y');?>");
			}
		<?php
		}
		?>

</script>
@stop
