@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>					
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form id="form-target" class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="transaction_code">Transaction Code</label>
										<div class="col-md-9">
											<input readonly type="text" name="transaction_code" id="transaction_code" class="form-control {{$errors->first('transaction_code') ? 'border-danger' : NULL}}" placeholder="Category code" value="{{old('transaction_code',$sequence)}}" data-toggle="tooltip" title="{{$errors->first('transaction_code') ? $errors->first('transaction_code') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="purchase_order">Purchase Order</label>
										<div class="col-md-9">
											{!!Form::select("purchase_order", $purchase_order, old('purchase_order'), ['id' => "purchase_order", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('purchase_order')}}"]) !!}
										</div>
									</div>
								</div>	

								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="invoice_number">Invoice No</label>
										<div class="col-md-9">
											<select class = "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5" name="invoice_number" id="invoice_number">
											    <option class="form-control" value="0">-Invoice No-</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="supplier_code">Supplier</label>
										<div class="col-md-9">
											<select class = "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5" name="supplier_code" id="supplier_code">
											    <option class="form-control" value="0">-Select supplier-</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="description">Description</label>
										<div class="col-md-9">
											<input type="text" name="description" id="description" class="form-control {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="Description" value="{{old('description')}}" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">
										</div>
									</div>
								</div>						
							</div>
						</div>
						<hr>
						<div class="form-body">
							<div class="row mt-3">
							
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="brand_code">Brand</label>
									<div class="col-md-9">
										<select class = "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5" name="brand_code" id="brand_code">
										    <option class="form-control" value="">-Select brand-</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="product_code">Product</label>
									<div class="col-md-9">
										<select class = "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5" name="product_code" id="product_code">
										    <option class="form-control" value="">-Select product-</option>
										</select>
									</div>
								</div>
							</div>	
							
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="qty">Quantity</label>
									<div class="col-md-9">
										<input type="text" name="qty" id="qty" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
									</div>
								</div>
							</div>
							
								<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="cost">Cost Price</label>
									<div class="col-md-9">
										<input readonly type="number" name="cost" id="cost" class="form-control {{$errors->first('cost') ? 'border-danger' : NULL}}" placeholder="Cost price" value="{{old('cost')}}" data-toggle="tooltip" title="{{$errors->first('cost') ? $errors->first('cost') : NULL}}">
									</div>
								</div>
							</div>
							
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control" for="selling">Selling Price</label>
									<div class="col-md-9">
										<input readonly type="text" name="selling" id="selling" class="form-control {{$errors->first('selling') ? 'border-danger' : NULL}}" placeholder="Selling price" value="{{old('selling')}}" data-toggle="tooltip" title="{{$errors->first('selling') ? $errors->first('selling') : NULL}}">
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row">
									<label class="col-md-3 label-control"></label>
									<div class="col-md-9">
										<input type="button" class="add-row" name = "add-row" value="Add Row">
									</div>
								</div>
							</div>
							<div class="container">
								<table id="product-table">
								       <thead>
								           <tr>
								               <th>Select</th>
								               <th>Brand Code</th>
								               <th>Product Name</th>
								               <th>Quantity</th>
								               <th>Cost</th>
								               <th>Selling</th>
								               <th>Expiration Date</th>
								           </tr>
								       </thead>
								       <tbody>
								           <tr>
								           </tr>
								       </tbody>
								       <tfoot>
								       	<tr>
								       		<td colspan="3">Total</td>
								       		<td><div id="totalQty">0</div></td>
								       		<td><div id="totalCost" style="display: block;">0.00</div></td>
								       		<td colspan="2"><div id="totalSelling" style="display: block;">0.00</div></td>
								       	</tr>
								       </tfoot>
								   </table>
								   <button type="button" class="delete-row">Delete Row</button>								
							</div>
						</div>
					</div>	
					<div class="left">
					<hr>
					<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
						<i class="ft-x"></i> Cancel
					</a>
					<button type="submit" class="btn btn-primary overlay-unblock">
						<i class="fa fa-check-square-o"></i> Save
					</button>
					</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('page-styles')
<style type="text/css">
    form{
        margin: 20px 0;
    }
    form input, button{
        padding: 5px;
    }
    table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
    table, th, td{
        border: 1px solid #cdcdcd;
    }
    table th, table td{
        padding: 10px;
        text-align: left;
    }
</style>
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">


	function getTransaction(transaction_code) {
		return $.ajax({
			method: "GET",
			url: "{{ route('backoffice.ajax.transaction') }}",
			data: {
				'transaction_code': transaction_code,
				'include': 'brands,products,suppliers',
			},
		});
	}

	function getProducts(transaction_code, brand_code) {
		return $.ajax({
			method: "GET",
			url: "{{ route('backoffice.ajax.products') }}",
			data: {
				'transaction_code': transaction_code,
				'brand_code': brand_code,
			},
		});
	}

	// function getBrands(transaction_code, brand_code) {
	// 	return $.ajax({
	// 		method: "GET",
	// 		url: "{{ route('backoffice.ajax.brands') }}",
	// 		data: {
	// 			'transaction_code': transaction_code,
	// 			'brand_code': brand_code,
	// 		},
	// 	});
	// }

	$.fn.populateBrands = function(brands) {
		var $element = $(this);
		
		$element.find('option').not(":first").remove();
		
		$.each(brands, function(index, brand) {
			$element.append("<option value='" + brand.brand_code + "'>" + brand.brand_name + "</option>")
		});
	}

	$.fn.populateSuppliers = function(suppliers) {
		var $element = $(this);
		
		$element.find('option').not(":first").remove();
		
		$.each(suppliers, function(index, supplier) {
			$element.append("<option value='" + supplier.supplier_code + "'>" + supplier.supplier_name + "</option>")
		});
	}

	$.fn.populateInvoice = function(transaction_code) {
		var $element = $(this);
		
		$element.find('option').not(":first").remove();
		// alert(transaction_code.invoice_number);
		$element.append("<option value='" + transaction_code.invoice_number + "'>" + transaction_code.invoice_number + "</option>")
		
		
	}

	$.fn.populateProducts = function(products) {
		var $element = $(this);
		
		$element.find('option').not(":first").remove();
		
		$.each(products, function(index, product) {
			$element.append("<option data-cost='" + product.cost + "' " +
				"data-selling='" + product.selling + "' " + 
				"value='" + product.product_code + "'>" + 
				product.product_name + 
				"</option>");
		});
	}



    $(function() {



    	$('select[name="product_code"]').on('change', function(){
    		

    		var $option = $("option:selected", this);

    		var cost = $option.attr('data-cost');
    		var selling = $option.attr('data-selling');

    		$("input[name='cost']").val(cost);
    		$("input[name='selling']").val(selling);

    	});

    	$('select[name="supplier_code"]').on('change', function(){    		
    		
    		var supplier_code = $(this).val();
    		var purchase_order = $('select[name="purchase_order"]').val();
    		var ajaxGetBrands = getBrands(purchase_order, supplier_code);
    		ajaxGetBrands.done(function(response) {
    			var brands = response.data;
    			$('select[name="brand_code"]').populateBrands(brands);
    		});

    		ajaxGetBrands.fail(function(jqXHR, errorThrown, status) {
    			// console.log(errorThrown);
    		});

    		ajaxGetBrands.always(function(jqXHR) {

    		});
    		

    	});

    	$('select[name="brand_code"]').on('change', function() {
    		

    		var brand_code = $(this).val();
    		var purchase_order = $('select[name="purchase_order"]').val();

    		var ajaxGetProducts = getProducts(purchase_order, brand_code);

    	    ajaxGetProducts.done(function(response) {
    	    	var products = response.data;
    	    	$('select[name="product_code"]').populateProducts(products);

    	    });

    	    ajaxGetProducts.fail(function(jqXHR, errorThrown, status) {
    	    	// console.log(errorThrown);
    	    });

    	    ajaxGetProducts.always(function(jqXHR) {

    	    });

    	});

    	$('select[name="purchase_order"]').on('change', function() {

    	    var purchase_order = $(this).val();

    	    var ajaxGetTransaction = getTransaction(purchase_order);

    	    ajaxGetTransaction.done(function(response) {
    	    	var transaction = response.data;
    	    	var brands = transaction.brands.data;
    	    	var suppliers = transaction.suppliers.data;
    	    	var products = transaction.products.data;

    	    	if (!transaction == "") {
    	    		$('select[name="supplier_code"]').populateSuppliers(suppliers);
    	    		$('select[name="brand_code"]').populateBrands(brands);
    	    		$('select[name="product_code"]').populateProducts(products);
    	    		$('select[name="invoice_number"]').populateInvoice(transaction);
    	    	}

    	    	
    	    });

    	    ajaxGetTransaction.fail(function(jqXHR, errorThrown, status) {
    	    	// console.log(errorThrown);
    	    });

    	    ajaxGetTransaction.always(function(jqXHR) {

    	    });

    	    // if(purchase_order) {
    	    //     $.ajax({
    	    //         url: '/purchase_return/transaction_code/'+encodeURI(purchase_order),      
    	    //         type: "GET",
    	    //         dataType: "json",
    	    //         success:function(data) {
    	    //         $('select[name="supplier_code"]').empty();
    	    //         $.each(data, function(key, value) {
    	    //             $('select[name="supplier_code"]').append('<option value="'+ key +'">'+ value +'</option>');
    	    //             	if (purchase_order ) {
    	    //             		$.ajax({
    	    //             		    url: '/purchase_return/brand_code/'+encodeURI(purchase_order),      
    	    //             		    type: "GET",
    	    //             		    dataType: "json",
    	    //             		    success:function(data) {
    	    //             		    $('select[name="brand_code"]').empty();
    	    //             		    $.each(data, function(key, value) {
    	    //             		    	$('select[name="brand_code"]').on('change', function() {    	                		    		
    	    //             		    	});
    	    //             		        $('select[name="brand_code"]').append('<option value="'+ key +'">'+ value +'</option>');
    	    //             		        	if (purchase_order) {

    	    //             		        		$.ajax({
    	    //             		        		    url: '/purchase_return/product_code/'+encodeURI(purchase_order),      
    	    //             		        		    type: "GET",
    	    //             		        		    dataType: "json",
    	    //             		        		    success:function(data) {
    	    //             		        		    $('select[name="product_code"]').empty();
    	    //             		        		    $.each(data, function(key, value) {
    	    //             		        		        $('select[name="product_code"]').append('<option value="'+ key +'">'+ value +'</option>');

    	    //             		        		        	if (value) {
    	    //             		        		        		  $('select[name="product_code"]').on('change', function() {

    	    //             		        		        		      var product_code = $(this).val();
    	    //             		        		        		          if(product_code != "") {
    	    //             		        		        		              $.ajax({
    	    //             		        		        		                //get the outstanding balance
    	    //             		        		        		                  url: '/transaction_hdr/cost_price/'+encodeURI(product_code),      
    	    //             		        		        		              		    type: "GET",
    	    //             		        		        		                  dataType: "json",
    	    //             		        		        		                  success:function(data) {
    	    //             		        		        		                  	$('#cost').val(data.cost);
    	    //             		        		        		                  	$('#selling').val(data.selling);
    	                		        		        		                    
    	    //             		        		        		                  }
    	    //             		        		        		              });
    	    //             		        		        		          }
    	    //             		        		        		});
    	    //             		        		        	}
    	                		        		        	
    	    //             		        		        });

    	                		        		    	
    	    //             		        		    }
    	    //             		        		});

    	    //             		        	}
    	    //             		        });

    	                		    	
    	    //             		    }
    	    //             		});
    	    //             	}
    	    //             });

    	            	
    	    //         }
    	    //     });
    	    // }else{
    	    //     $('select[name="supplier_code"]').empty();
    	    //    	}
    	});



    	  

    	 var a = 1234; 

    	 $('#totalQty').val(a);

    	$(".add-row").click(function(){
            var product_code = $("#product_code").val();
            var qty = $("#qty").val();
            var brand_code = $("#brand_code").val();
            var cost = $("#cost").val();
            var selling = $("#selling").val();
            var expiration_date = "--";
            var markup = "<tr><td><input type='checkbox' name='record'><td><input type='hidden'  class='form-control input-sm' name='brand_code[]' value='" + brand_code + "'>" + brand_code + "</td></td><td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='" + product_code + "'>"+product_code +"</td><td><input type='hidden'  class='form-control input-sm' name='qty[]' value='" + qty + "'>" + qty + "</td><td><input type='hidden'  class='form-control input-sm' name='cost[]' value='" + cost + "'>" + cost + "</td><td><input type='hidden'  class='form-control input-sm' name='selling[]' value='" + selling + "'>" + selling + "</td><td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='" + expiration_date + "'>" + expiration_date + "</td></tr>";
            $("table tbody").append(markup);
            

            
        });     	   
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });


    });
</script>
@stop
