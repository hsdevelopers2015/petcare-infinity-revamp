@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0"></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('backoffice.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>					
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls"><i class="ft-box"></i> {{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form id="form-target" class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<div class="row mt-3">
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="transaction_code">Transaction Code</label>
										<div class="col-md-9">
											<input readonly type="text" name="transaction_code" id="transaction_code" class="form-control {{$errors->first('transaction_code') ? 'border-danger' : NULL}}" placeholder="transaction_code" value="{{old('transaction_code',$transaction_hdr->transaction_code)}}" data-toggle="tooltip" title="{{$errors->first('transaction_code') ? $errors->first('transaction_code') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="transaction_code">Invoice No.</label>
										<div class="col-md-9">
											<input readonly type="text" name="transaction_code" id="transaction_code" class="form-control {{$errors->first('transaction_code') ? 'border-danger' : NULL}}" placeholder="transaction_code" value="{{old('transaction_code',$transaction_hdr->invoice_number)}}" data-toggle="tooltip" title="{{$errors->first('transaction_code') ? $errors->first('transaction_code') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="supplier_code">Supplier</label>
										<div class="col-md-9">
											{!!Form::select("supplier_code", $supplier_code, old('supplier_code',$transaction_hdr->supplier_code), ['id' => "supplier_code", 'class' => "form-control  col-xs-12 col-sm-12 col-md-12 col-lg-5 {{$errors->first('supplier_code')}}",'disabled' => true]) !!}
										</div>
									</div>
								</div>	
								<div class="col-md-10">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="description">Description</label>
										<div class="col-md-9">
											<input readonly type="text" name="description" id="description" class="form-control {{$errors->first('description') ? 'border-danger' : NULL}}" placeholder="Description" value="{{old('description',$transaction_hdr->description)}}" data-toggle="tooltip" title="{{$errors->first('description') ? $errors->first('description') : NULL}}">
										</div>
									</div>
								</div>						
							</div>
						</div>
						<hr>
						<div class="form-body">
							<div class="row mt-3">
							
							
							<div class="container">
								<table id="product-table">
								       <thead>
								           <th>#</th>
								           <th>Brand Code</th>
								           <th>Product Name</th>
								           <th>Quantity</th>
								           <th>Cost</th>
								           <th>Selling</th>
								           <th>Expiration Date</th>
								       </thead>
								       <tbody>
								       	@foreach($transaction_hdr->transaction_dtl as $index => $value)
								           <tr>
								           	<td>{{++$index}}</td>
								           	<td><input type='hidden'  class='form-control input-sm' name='brand_code[]' value='{{$value->brand_code}}'>{{$value->brand_code}}</td>
								           	<td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='{{$value->product_code}}'>{{$value->product_code}}</td>
								           	
								           	<td><input type='hidden'  class='form-control input-sm' name='quantity[]' value='{{$value->quantity}}'> {{$value->quantity}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='cost[]' value='{{$value->cost}}'> {{$value->cost}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='selling[]' value='{{$value->selling}}'> {{$value->selling}} </td>
								           	<td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='{{$value->expiration_date}}'> {{$value->expiration_date}} </td>
								           </tr>
								           @endforeach
								       </tbody>
								       <tfoot>
								       	<tr>
								       		<td colspan="3"><strong>Total</strong></td>
								       		<td colspan="">{{$totalQty ? : "0.00"}}</td>
								       		<td colspan="">{{$totalCost ? : "0.00"}}</td>
								       		<td colspan="2">{{$totalSelling ? : "0.00"}}</td>
								       	</tr>
								       </tfoot>
								   </table>							
							</div>
						</div>
					</div>	
					<div class="left">
					<hr>
					<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-warning mr-1">
						<i class="ft-x"></i> Cancel
					</a>
					<button type="submit" class="btn btn-primary overlay-unblock">
						<i class="fa fa-check-square-o"></i> Approve
					</button>
					</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('page-styles')
<style type="text/css">
    form{
        margin: 20px 0;
    }
    form input, button{
        padding: 5px;
    }
    table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
    table, th, td{
        border: 1px solid #cdcdcd;
    }
    table th, table td{
        padding: 10px;
        text-align: left;
    }
</style>
@include('backoffice._includes.styles')
@stop

@section('page-scripts')
@include('backoffice._includes.scripts')

<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">



    $(function() {

    	$('select[name="product_code"]').on('change', function() {
    	      var product_code = $(this).val();

    	          if(product_code != "") {
    	              $.ajax({
    	                //get the outstanding balance
    	                  url: '/transaction_hdr/cost_price/'+encodeURI(product_code),      
    	                  type: "GET",
    	                  dataType: "json",
    	                  success:function(data) {
    	                  	$('#cost').val(data.cost);
    	                  	$('#selling').val(data.selling);
    	                    
    	                  }
    	              });
    	          }
    	});

    	$(".add-row").click(function(){
            var product_code = $("#product_code").val();
            var quantity = $("#quantity").val();
            var brand_code = $("#brand_code").val();
            var cost = $("#cost").val();
            var selling = $("#selling").val();
            var expiration_date = $("#expiration_date").val();
            var markup = "<tr><td><input type='checkbox' name='record'><td><input type='hidden'  class='form-control input-sm' name='brand_code[]' value='" + brand_code + "'>" + brand_code + "</td></td><td><input type='hidden'  class='form-control input-sm' name='product_code[]' value='" + product_code + "'>"+product_code +"</td><td><input type='hidden'  class='form-control input-sm' name='quantity[]' value='" + quantity + "'>" + quantity + "</td><td><input type='hidden'  class='form-control input-sm' name='cost[]' value='" + cost + "'>" + cost + "</td><td><input type='hidden'  class='form-control input-sm' name='selling[]' value='" + selling + "'>" + selling + "</td><td><input type='hidden'  class='form-control input-sm' name='expiration_date[]' value='" + expiration_date + "'>" + expiration_date + "</td></tr>";
            $("table tbody").append(markup);

            
        });     	   
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });


        

        // $("#form-target").submit(function(e) {
        // 	e.preventDefault();
        // 	var products = $("#product-table").serializeProducts();
	       //  console.log(products);
        // }); 	

    });
</script>
@stop
