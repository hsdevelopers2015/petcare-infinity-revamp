@extends('backoffice._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-6 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('backoffice.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
	</div>
</div>
<section id="file-export">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{$page_title}}</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							{{-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> --}}
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							{{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
						<a href="{{route('backoffice.deliveries.excel')}}" class="btn btn-primary pull-left" style="margin-right: 15px;">Excel</a>
						<table class="table table-striped table-bordered file-export">
							<thead>
								<tr>
									<th style="width: 30%">Transaction Code</th>
									<th style="width: 40%">Client Info</th>
									<th style="width: 30%">Status</th>
									<th>Total Qty</th>
									<th>Last Modified</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($sales as $index => $info)
								<tr style="color: {{$info->status == "cancelled"? '#c2c2c2': NULL}}">
									<td>
										@if($info->status=='for_delivery' OR $info->status=="posted_cancelled")
										<a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}">
										@else
										<a href="{{route('backoffice.sales.add',[$info->transaction_code])}}">
										@endif
										{{$info->transaction_code}}
										</a>
									</td>
									<td><strong>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</strong>,<br>
										<!-- Area {{$info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->area:'---'}} --><!-- <br>
										Location: {{Str::title($info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->island:'---')}}<br>
										City: {{$info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->city:'---'}}<br> -->
										<strong>{{$info->client_info($info->client_id)->business_info->business_name}}</strong>
									</td>
									<td>{!!Helper::sales_badge($info->status)!!}
										@if($info->status == "for_delivery")
										<br>
										{{$info->updated_at->diffForHumans()}}
										@endif
									</td>
									<td style="text-align: center">{{number_format($info->total_qty)}}</td>
									<td>{{$info->last_modified()}}</td>
									<td>
										@if($info->status!="cancelled")
										<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-middle">
											<div role="group" class="btn-group">
												<button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right">
													<i class="ft-cog icon-left"></i> Action
												</button>
												<div aria-labelledby="btnGroupDrop1" class="dropdown-menu">
													<a href="{{route('backoffice.sales.add',[$info->transaction_code])}}" class="dropdown-item">View Items</a>
													
													@if(Auth::user()->type != 'spectator' AND Auth::user()->type != 'finance')
													@if($info->status=='draft')
													<a href="#" class="action-delete dropdown-item" data-url="{{route('backoffice.sales.destroy',[$info->id])}}" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eraser3"></i>Cancel</a>
													@else
													<a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}" class="dropdown-item"><i class="icon-eraser3"></i>Show Invoice</a>
													@endif
													@endif

													@if(!in_array(Auth::user()->type, ['spectator','finance']) AND $info->status == "for_delivery")

													@if($info->check_dr($info->id,'sales') == 0)
													<a href="#" class="action-create-dr dropdown-item" data-toggle="modal" data-target="#confirm-create-sales-dr-{{$info->id}}">Delivery Receipt</a>
													@else
													<a href="{{route('backoffice.sales.dr',$info->id)}}" class="dropdown-item" target="_blank">Delivery Receipt</a>
													@endif
													@if($info->payment_status != 'fully_paid')
													<a href="#" class="action-cancel dropdown-item" data-url="{{route('backoffice.sales.cancel',[$info->id])}}" data-toggle="modal" data-target="#confirm-cancel"><i class="icon-eraser3"></i>Cancel</a>
													@endif
													@endif


													@if($info->status != 'received')
													<a href="#" class="action-received dropdown-item" data-url="{{route('backoffice.deliveries.received',['sales',$info->id])}}" data-toggle="modal" data-target="#confirm-received"><i class="icon-eraser3"></i>Received</a>
													@endif
													<a target="_blank" href="{{route('backoffice.sales.history',[$info->id])}}" class="dropdown-item">History</a>
												</div>
											</div>
										</div>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-modals')
<div id="confirm-received" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Info!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Receiving...</h6>
				<p>Are you sure that the client was already received their products?</p>

				{{-- <hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a received command.</p> --}}
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-primary" id="btn-confirm-received">Yes</a>
			</div>
		</div>
	</div>
</div>

@foreach($orders as $index => $info)
<div id="confirm-create-invoice-{{$info->id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create Invoice</h5>
			</div>
			<form method="POST" action="{{route('backoffice.orders.create_invoice')}}">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="prepared_by" value="{{$auth->fname}} {{$auth->lname}}">
				<input type="hidden" name="id" value="{{$info->id}}">
				<div class="modal-body">
					<h6 class="text-semibold">Creating an invoice...</h6>
					<p>Are you sure you want to create an invoice to this order? </p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-outline-primary">Create</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endforeach

<div id="confirm-cancel" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to cancel a sales record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-cancel">Cancel</a>
			</div>
		</div>
	</div>
</div>
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>

@foreach($orders as $index => $info)
<div id="confirm-create-orders-dr-{{$info->id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create Deliver Receipt</h5>
			</div>
			<form method="POST" action="{{route('backoffice.orders.create_dr')}}">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="reference_id" value="{{$info->id}}">
				<input type="hidden" name="reference" value="orders">
				<input type="hidden" name="prepared_by" value="{{$auth->fname}} {{$auth->lname}}">
				<div class="modal-body">
					{{-- <div class="row">
						<div class="col-md-12">
							<label for="">Delivery Receipt Number</label>
							<input type="text" name="dr_number" class="form-control" required="" placeholder="Receipt #">
						</div>
					</div> --}}
					<div class="row">
						<div class="col-md-12">
							<label for="">Approved By</label>
							<input type="text" name="approved_by" class="form-control" required="" placeholder="Approved By">
						</div>
					</div>
					<div class="row  mt-1">
						<div class="col-md-12">
							<label for="">Sales Invoice Number</label>
							<input type="text" name="si_number" class="form-control" placeholder="Sales Invoice Number">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-outline-primary">Create</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endforeach

@foreach($sales as $index => $info)
<div id="confirm-create-sales-dr-{{$info->id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create Deliver Receipt</h5>
			</div>
			<form method="POST" action="{{route('backoffice.sales.create_dr')}}">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="reference_id" value="{{$info->id}}">
				<input type="hidden" name="reference" value="sales">
				<input type="hidden" name="prepared_by" class="form-control" value="{{$auth->fname}} {{$auth->lname}}" placeholder="Prepared By">
				<div class="modal-body">
					{{-- <div class="row">
						<div class="col-md-12">
							<label for="">Delivery Receipt Number</label>
							<input type="text" name="dr_number" class="form-control" required="" placeholder="Receipt #">
						</div>
					</div> --}}
					<div class="row">
						<div class="col-md-12">
							<label for="">Approved By</label>
							<input type="text" name="approved_by" class="form-control" required="" placeholder="Approved By">
						</div>
					</div>
					<div class="row mt-1">
						<div class="col-md-12">
							<label for="">Sales Invoice Number</label>
							<input type="text" name="si_number" class="form-control" placeholder="Sales Invoice Number">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-outline-primary">Create</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endforeach

@foreach($orders as $index => $info)
<div id="view-business-{{$info->id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{$info->business_name}} </h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-info alert-styled-left text-default content-group">
	                <span class="text-semibold">Information!</span> These are the information about the client's business.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>
				
				<div class="row">
					<div class="col-md-12">
						<img src="{{$info->directory.'/'.$info->filename}}" class="img-responsive img-thumbnail">
					</div>
				</div>

				<hr>

				<h6 class="text-semibold">Business Type :</h6>
				<p>{{$info->business_type}}</p>
				<h6 class="text-semibold">Business Address :</h6>
				<p>{{$info->business_location}}</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach
@stop

@section('page-styles')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/feather/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/pace.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/vendors/css/extensions/toastr.css')}}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/bootstrap-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/app.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/colors.min.css')}}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/plugins/extensions/toastr.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/fonts/simple-line-icons/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-climacon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/app-assets/css/core/colors/palette-gradient.min.css')}}">
<!-- END Page Level CSS-->

<style type="text/css">
	.dropdown-item:focus, .dropdown-item:hover {
    color: #00b5b8; 
    text-decoration: none!important;
    background-color: #ffffff!important; 
    width: inherit!important;
}
</style>

@stop

@section('page-scripts')
<!-- BEGIN VENDOR JS-->
<script src="{{asset('backoffice/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('backoffice/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('backoffice/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('backoffice/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script type="text/javascript">
	$(function(){
		$(".action-delete").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-delete").attr({"href" : btn.data('url')});
		});

		$(".action-cancel").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-cancel").attr({"href" : btn.data('url')});
		});

		$(".action-received").on("click",function(){
			var btn = $(this);
			$("#btn-confirm-received").attr({"href" : btn.data('url')});
		});

		$.extend( $.fn.dataTable.defaults, {
	        autoWidth: false,
	        columnDefs: [{ 
	            orderable: false,
	            width: '100px',
	            targets: [ 5 ]
	        }],
	        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	        language: {
	            search: '<span>Filter:</span> _INPUT_',
	            lengthMenu: '<span>Show:</span> _MENU_',
	            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	        },
	        drawCallback: function () {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
	        },
	        preDrawCallback: function() {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
	        }
	    })

	    $('.datatable-basic').DataTable();

	    $('.datatable-pagination').DataTable({
	        pagingType: "simple",
	        language: {
	            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
	        }
	    });

	    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');

	    $('.dataTables_length select').select2({
	        minimumResultsForSearch: Infinity,
	        width: 'auto'
	    });
	});
</script>

<style>
	.buttons-copy{
		display: none;
	}
	.buttons-csv{
		display: none;
	}
</style>
@stop
