<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8"/>
    <title>404 Page not found :: Highly Succeed Inc.</title>
    <meta name="author" content="Highly Succeed Inc."/>
    <meta name="keywords" content="404 page"/>
    <meta name="description" content="404 Page not found"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <!-- Libs CSS -->
    <link type="text/css" media="all" href="/errorpage/style.css" rel="stylesheet"/>
    <!-- Template CSS -->
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,800italic,800,700italic,700,600italic,600,400italic,300' rel='stylesheet' type='text/css'/>

</head>
<body>

    <!-- Load page -->
    <div class="animationload">
        <div class="loader">
        </div>
    </div>
    <!-- End load page -->


    <!-- Content Wrapper -->
    <div id="wrapper">
        <div class="container">

            <!-- brick of wall -->
            <div class="brick"></div>
            <!-- end brick of wall -->

            <!-- Number -->
            <div class="number">
                <div class="four"></div>
                <div class="zero">
                    <div class="nail"></div>
                </div>
                <div class="four"></div>
            </div>
            <!-- end Number -->

            <!-- Info -->
            <div class="info">
                <h2>Something is wrong</h2>
                <p>The page you are looking for was moved, removed, renamed or might never existed.</p>
                <a href="http://highlysucceed.com" class="btn">Go Home</a>
            </div>
            <!-- end Info -->


        </div>
        <!-- end container -->
    </div>
    <!-- end Content Wrapper -->

    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <!-- Worker -->
            <div class="worker"></div>
            <!-- Tools -->
            <div class="tools"></div>
        </div>
        <!-- end container -->
    </footer>
    <!-- end Footer -->

    <!-- Scripts -->
    <script src="/errorpage/js/jquery.min.js" type="text/javascript"></script>
    <script src="/errorpage/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="/errorpage/script.js"></script>

    <script type="text/javascript">$(window).load(function(){"use strict";$(".loader").delay(400).fadeOut();$(".animationload").delay(400).fadeOut("fast");});$("html").niceScroll({cursorcolor:'#fff',cursoropacitymin:'0',cursoropacitymax:'1',cursorwidth:'2px',zindex:999999,horizrailenabled:false,enablekeyboard:false});</script>

</body>
</html>