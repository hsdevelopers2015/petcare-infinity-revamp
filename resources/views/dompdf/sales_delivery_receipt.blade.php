<html>
<head>
	<title>Delivery Receipt {{$delivery_receipt->dr_number}}</title>
	<style type="text/css">


		html
		{
			font-family:Calibri (Body);
			background-color: #000;
		}

		body
		{
			margin: 20px, 20px, 20px, 20px;
			font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;}


		}

		.fonts-size
		{
			font-size: 15px;
			width: 100%;
			text-align: right;
		}

		table
		{
			padding-bottom: 0px;
		}

		.invoice
		{

			font-size: 15px;
			color: maroon;
			padding-left: 300px;
			padding-bottom: 0px;
			padding-top: 0px;   
		}

		.invoice-under
		{
			margin-top: -50px;
			padding-left: 300px;
		}
		.table-amount
		{

			text-align: center;
			font-style: bold;
			font-size: 12px;
		}

		.table-amount td tr
		{ 
			border: 1px solid black;
		}


		.parag-bottom
		{
			text-align: center;
			font-size: 15px;
		}

		.thanks
		{
			text-align: center;
			color: red;
		}

		.last-parag
		{
			text-align: center;
			color: black;
			font-size: 15px;
			font-weight: bold;
		}

		.italize
		{
			font-style: italic;
		}

		.footer-accounts
		{
			padding-right: 60px;
			font-size: 15px;
			padding-top: 30px;
		}

		.computation
		{
			font-weight: bold;
			padding-left: 350px;
		}

		.settle
		{
			padding-left: 60px;
			padding-right: 30px;
			font-size: 15px;
			padding-bottom: 0px;
		}

		.bold
		{
			font-weight: bold;
		}

		.heading
		{
			text-align: center;
			margin: 0px;
			font-size: 15px;
		}
		.header
		{
			width: 100%;
		}

		.wide
		{
			width: 400px;
		}

		.small
		{
			font-size: 15px;
		}
		.padding
		{
			padding: 3px;
		}
		*{
			font-size: 15px!important;
			font-weight: normal!important;
		}
	</style>
</head>
<body style=" padding: -40px -30px -50px -30px;">
	<div class = "main-container" >

		{{-- <table>
			<tr>
				<td style="width: 450px; font-size: 15px;"><b>Date : {{date_format($sales->updated_at,'M d, Y ')}} Time : {{date_format($sales->updated_at,'h:i A ')}}<b></td>
			</tr>
		</table> --}}

		<table style="font-size: 13px; margin-top: 135px;" width="100%" cellspacing="">
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Delivered to: _________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Date: ________________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- TIN: ________________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Terms: _______________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Address: ____________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- S.I. No.: ______________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Business Style: _______________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Salesman: ____________________ --}}
				</td>
			</tr>
		</table>

		<table style="font-size: 12px; margin-top: -90px; font-family: monospace; " width="100%">
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important; margin-left: 30px;">{{$sales->client_info($sales->client_id)->business_info->business_name}}</span>
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important">{{date_format($sales->updated_at,'M d, Y ')}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important; margin-left: 15px;">{{str_replace('_',' ',Str::title($sales->due_day))}} Days</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important; margin-left: 30px;">{{$sales->client_info($sales->client_id)->business_info->business_location}}</span>
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important; margin-left: 20px;"><!-- {{$delivery_receipt->si_number}} --></span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important">{{$sales->client_info($sales->client_id)->business_info->business_type}}</span>
				</td>
				<td style="width: 30%">
					&nbsp;{{-- <span style="">{{$sales->client_info($sales->client_id)? $sales->client_info($sales->client_id)->salesagent_info->fname : '---'}} {{$sales->client_info($sales->client_id)? $sales->client_info($sales->client_id)->salesagent_info->lname : '---'}}</span> --}}
				</td>
			</tr>
		</table>

		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin-top: 0px;" width="100%" class = "table-amount" >
			<tr bgcolor="{{-- lightgrey --}}">
				<td class="padding" style="width: 5%"><b>&nbsp;{{-- QTY --}}<b></td>
				<td class="padding" style="width: 10%"><b>&nbsp;{{-- UNIT --}}<b></td>
				<td class="padding" colspan="5"><b>&nbsp;{{-- DESCRIPTION --}}<b></td>
			</tr>
			<?php 
				$overall_total = [];
			?>
			@foreach($items as $index => $info)
			<tr style="font-family: monospace;">
				<td class="padding" style="font-size: 12px!important; text-align: left;">{{$info->qty}}</td>
				<td class="padding" style="font-size: 12px!important; text-align: left;">{{$info->product_info($info->product_id)? $info->product_info($info->product_id)->unit:'---'}}</td>
				<td class="padding" colspan="5" style="font-size: 12px!important">
					<span style="float: left; font-size: 12px!important">
					@if($info->product_info($info->product_id))
						{{$info->product_info($info->product_id)->product_name}}
					@else
					---
					@endif
					</span>
					<span style="margin-right: 10px; font-size: 12px!important">
						{{number_format($info->cost_unit,2)}}
					</span>
					@if($info->free)
					+ {{$info->free}} Free 
					@endif
				<?php 
				$total = $info->cost_unit * $info->qty;
				array_push($overall_total,$total);
					?>
					<span style="float: right; font-size: 12px!important">
						{{number_format($total,2)}}
					</span>
				</td>
			</tr>
			@endforeach
			@for($x = 1 ; $x <= $z = 11 - $y = $items->count() ; $x++)
			<tr height = "35px;">
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
				<td class="padding" colspan="5">&nbsp;</td>
			</tr>
			@endfor

		</table>
			{{-- <tr>
				<td class="padding" style="text-align: right" colspan="6"><b>Sub Total</b></td>
				<td class="padding" style="text-align: right; ">PHP {{number_format(array_sum($overall_total),2)}}</td>
			</tr> --}}
			<table border="0" cellpadding="10" cellspacing="0" style="width: 50%; margin-top: 25px;" >
				<tr>
					<td><sup style="font-size: 13px">&nbsp;{{-- Prepared by: --}}</sup> <br> 
						<span style="font-size: 15px;  font-family: monospace; margin-left: 100px; font-size: 12px!important">{{$delivery_receipt->approved_by}}</span>
					</td>
				</tr>
				<tr>
					<td><sup style="font-size: 13px">&nbsp;{{-- Approved by: --}}</sup> <br>
						<span style="font-size: 12px!important; font-family: monospace; margin-left: 100px; font-size: 12px!important;"><!-- {{$delivery_receipt->approved_by}} --></span>
					</td>
				</tr>
			</table>

			<span style="float: right; margin-right: 20px; font-family: monospace; font-size: 12px!important"><strong style="font-size: 12px!important">{{number_format(array_sum($overall_total),2)}}</strong></span>	

			<br>

			<center><span><strong style="font-size: 13px;">&nbsp;{{-- RECEIVED ABOVE PRODUCTS IN GOOD ORDER AND CONDITION --}}</strong></span></center>
			
			<table border="0" cellpadding="10" cellspacing="0" style="width: 100%;" >
				<tr>
					<td><sup style="font-size: 13px">&nbsp;{{-- Delivered by: --}}</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">&nbsp;{{-- Date --}}</sup></span><br> 
						<span style="font-size: 12px!important;  font-family: monospace; margin-left: 100px;">&nbsp;{{-- {{$sales->client_info($sales->client_id)->salesagent_info->fname}} {{$sales->client_info($sales->client_id)->salesagent_info->lname}} --}}</span>
					</td>
					<td><sup style="font-size: 13px">&nbsp;{{-- Signature Over Printed Name: --}}</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">&nbsp;{{-- Date --}}</sup></span><br>
						<span style="font-size: 15px; font-family: monospace; margin-left: 100px;">
							&nbsp;
						</span>
					</td>
				</tr>
			</table>
	</body>
</html> 