<table border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin-top: 10px;" width="100%" class = "table-amount" >
	<tr bgcolor="gold" height = "35px;">
		<td class="padding"><b>Code<b></td>
		<td class="padding"><b>Description<b></td>
		<td class="padding"><b>Unit Cost<b></td>
		<td class="padding"><b>Qty<b></td>
		<td class="padding"><b>Total<b></td>
		<td class="padding"><b>Discount<b></td>
		<td class="padding"><b>Final Cost<b></td>
	</tr>
	<?php
		$overall_total = [];
	?>
	@foreach($items as $index => $info)
	<tr>@if($info->product_info($info->product_id))
		<td class="padding">{{$info->product_info($info->product_id)->product_code}}</td>
		<td class="padding" style="text-align: left">&nbsp;{{$info->product_info($info->product_id)->product_name}}</td>
		@else
		<td class="padding">---</td>
		<td class="padding" style="text-align: left">---</td>
		@endif
		<td class="padding" style="text-align: right">{{$info->cost_unit}}&nbsp;</td>
		<td class="padding">{{$info->qty}}</td>
		<td class="padding" style="text-align: right">{{number_format($info->qty*$info->cost_unit,2)}}&nbsp;</td>
		<?php 
		$total = $info->cost_unit * $info->qty;
		array_push($overall_total,$info->final_cost);
		?>
		<td class="padding">{{$info->discount_type=="amount"? $info->discount :$total*($info->discount/100) }}</td>
		<td class="padding" style="text-align: right">{{number_format($info->final_cost,2)}}</td>
	</tr>
	@endforeach
	<tr>
		<td class="padding" style="text-align: right" colspan="6"><b>Sub Total</b></td>
		<td class="padding" style="text-align: right">PHP {{number_format(array_sum($overall_total),2)}}</td>
	</tr>
</table>