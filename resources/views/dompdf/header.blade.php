<table border="0">
	<tr>
		<td style="width: 450px; font-size: 20px;"><b>Sales Invoice<b></td>
	</tr>
	<tr>
		<td style="width: 500px; font-size: 25px;"><b>{{$sales->transaction_code}}<b></td>
		<td style="width: 450px; font-size: 30px; font-weight: bold; margin-left: 15px;">{{env('APP_TITLE','Localhost')}}</td>
	</tr>
</table>
<br>
<table border="0">
	<tr>
		<td style="width: 450px; font-size: 15px;"><b>Date : {{date_format($sales->updated_at,'M d, Y ')}} Time : {{date_format($sales->updated_at,'h:i A ')}}<b></td>
	</tr>
</table>
<br>