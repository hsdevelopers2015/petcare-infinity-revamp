<html>
<head>
	<title>Delivery Receipt {{$delivery_receipt->dr_number}}</title>
	<style type="text/css">


		html
		{
			font-family:Calibri (Body);
			background-color: #000;
		}

		body
		{
			margin: 20px, 20px, 20px, 20px;
			font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;}


		}

		.fonts-size
		{
			font-size: 15px;
			width: 100%;
			text-align: right;
		}

		table
		{
			padding-bottom: 0px;
		}

		.invoice
		{

			font-size: 15px;
			color: maroon;
			padding-left: 300px;
			padding-bottom: 0px;
			padding-top: 0px;   
		}

		.invoice-under
		{
			margin-top: -50px;
			padding-left: 300px;
		}
		.table-amount
		{

			text-align: center;
			font-style: bold;
			font-size: 12px;
		}

		.table-amount td tr
		{ 
			border: 1px solid black;
		}


		.parag-bottom
		{
			text-align: center;
			font-size: 15px;
		}

		.thanks
		{
			text-align: center;
			color: red;
		}

		.last-parag
		{
			text-align: center;
			color: black;
			font-size: 15px;
			font-weight: bold;
		}

		.italize
		{
			font-style: italic;
		}

		.footer-accounts
		{
			padding-right: 60px;
			font-size: 15px;
			padding-top: 30px;
		}

		.computation
		{
			font-weight: bold;
			padding-left: 350px;
		}

		.settle
		{
			padding-left: 60px;
			padding-right: 30px;
			font-size: 15px;
			padding-bottom: 0px;
		}

		.bold
		{
			font-weight: bold;
		}

		.heading
		{
			text-align: center;
			margin: 0px;
			font-size: 15px;
		}
		.header
		{
			width: 100%;
		}

		.wide
		{
			width: 400px;
		}

		.small
		{
			font-size: 15px;
		}
		.padding
		{
			padding: 3px;
		}
	</style>
</head>
<body style=" padding: -40px -30px -50px -30px;">
	<div class = "main-container" >
		<table style="text-align: center; width: 100%;">
			<tr>
				<td style="width: 450px; font-size: 30px;"><b>INFINITY PETCARE, INC.<b></td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">J. Barcial Hulo , Mag - Asawang Sapa, Santa Maria, Bulacan</td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">VAT REG. TIN : 009-122-028-000</td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">Tel.: (02) 861-2740 / Email Add: infinitypetcare@yahoo.com.ph</td>
			</tr>
		</table>
		<br>
		<table style="width: 100%; margin-top: -10px;" >
			<tr>
				<td>
					<p style="font-size: 20px;"><b>DELIVERY RECEIPT</b></p>
				</td>
				<td>
					{{-- <p style="font-size: 20px; float: right; font-family: Times New Roman; color: darkred"><b><span style="margin-left: -40px; margin-right: 40px;">NO. </span>{{$delivery_receipt->dr_number}}</b></p> --}}
				</td>
			</tr>
		</table>

		{{-- <table>
			<tr>
				<td style="width: 450px; font-size: 15px;"><b>Date : {{date_format($sales->updated_at,'M d, Y ')}} Time : {{date_format($sales->updated_at,'h:i A ')}}<b></td>
			</tr>
		</table> --}}

		<table style="font-size: 13px; margin-top: -15px;" width="100%" cellspacing="">
			<tr>
				<td style="width: 70%">
					Delivered to: _________________________________________________________
				</td>
				<td style="width: 30%">
					Date: ________________________
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					 TIN: ________________________________________________________________
				</td>
				<td style="width: 30%">
					Terms: _______________________
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					Address: ____________________________________________________________
				</td>
				<td style="width: 30%">
					S.I. No.: ______________________
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					Business Style: _______________________________________________________
				</td>
				<td style="width: 30%">
					Salesman: ____________________
				</td>
			</tr>
		</table>

		<table style="font-size: 12px; margin-top: -70px; font-family: monospace; " width="100%">
			<tr>
				<td style="width: 70%">
					<span style="margin-left: 100px;">{{$sales->client_info($sales->client_id)->business_info->business_name}}</span>
				</td>
				<td style="width: 30%">
					<span style="margin-left: 90px;">{{date_format($sales->updated_at,'M d, Y ')}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;
				</td>
				<td style="width: 30%">
					<span style="margin-left: 90px;">{{str_replace('_',' ',Str::title($sales->mode_of_payment))}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="margin-left: 100px;">{{$sales->client_info($sales->client_id)->business_info->business_location}}</span>
				</td>
				<td style="width: 30%">
					<span style="margin-left:90px;">{{$delivery_receipt->si_number}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="margin-left: 100px;">{{$sales->client_info($sales->client_id)->business_info->business_type}}</span>
				</td>
				<td style="width: 30%">
					<span style="margin-left: 90px;">{{$sales->client_info($sales->client_id)->salesagent_info->fname}} {{$sales->client_info($sales->client_id)->salesagent_info->lname}}</span>
				</td>
			</tr>
		</table>

		<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin-top: 10px;" width="100%" class = "table-amount" >
			<tr bgcolor="lightgrey">
				<td class="padding"><b>QTY<b></td>
				<td class="padding"><b>UNIT<b></td>
				<td class="padding" colspan="5"><b>DESCRIPTION<b></td>
			</tr>
			<?php 
				$overall_total = [];
			?>
			@foreach($items as $index => $info)
			<tr style="font-family: monospace;">
				<td class="padding">{{$info->qty}}</td>
				<td class="padding">{{$info->product_info($info->product_id)? $info->product_info($info->product_id)->unit:'---'}}&nbsp;</td>
				<td class="padding" colspan="5">
					<span style="float: left">
					@if($info->product_info($info->product_id))
						&nbsp;{{$info->product_info($info->product_id)->product_name}}
					@else
					---
					@endif
					</span>
					<span style="margin-right: 30px;">
						&nbsp;{{number_format($info->product_info($info->product_id)?$info->product_info($info->product_id)->cost:0,2)}}&nbsp;
					</span>
				<?php 
				$total = ($info->product_info($info->product_id)?$info->product_info($info->product_id)->cost:0) * $info->qty;
				array_push($overall_total,$total);
					?>
					<span style="float: right">
						&nbsp;{{number_format($total,2)}}
					</span>
				</td>
			</tr>
			@endforeach
			@for($x = 1 ; $x <= $z = 14 - $y = $items->count() ; $x++)
			<tr height = "35px;">
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
				<td class="padding" colspan="5">&nbsp;</td>
			</tr>
			@endfor

		</table>
			{{-- <tr>
				<td class="padding" style="text-align: right" colspan="6"><b>Sub Total</b></td>
				<td class="padding" style="text-align: right">PHP {{number_format(array_sum($overall_total),2)}}</td>
			</tr> --}}
			<table border="1" cellpadding="10" cellspacing="0" style="width: 50%; margin-top: 15px;" >
				<tr>
					<td><sup style="font-size: 13px">Prepared by:</sup> <br> 
						<span style="font-size: 15px;  font-family: monospace; margin-left: 100px;">{{$delivery_receipt->prepared_by}}</span>
					</td>
				</tr>
				<tr>
					<td><sup style="font-size: 13px">Approved by:</sup> <br>
						<span style="font-size: 15px; font-family: monospace; margin-left: 100px;">{{$delivery_receipt->approved_by}}</span>
					</td>
				</tr>
			</table>

			<span style="float: right; margin-right: 20px; font-family: monospace; font"><strong>{{number_format(array_sum($overall_total),2)}}</strong></span>	

			<br>

			<center><span><strong style="font-size: 13px;">RECEIVED ABOVE PRODUCTS IN GOOD ORDER AND CONDITION</strong></span></center>
			
			<table border="1" cellpadding="10" cellspacing="0" style="width: 100%;" >
				<tr>
					<td><sup style="font-size: 13px">Delivered by:</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">Date</sup></span><br> 
						<span style="font-size: 15px;  font-family: monospace; margin-left: 100px;">{{$sales->client_info($sales->client_id)->salesagent_info->fname}} {{$sales->client_info($sales->client_id)->salesagent_info->lname}}</span>
					</td>
					<td><sup style="font-size: 13px">Signature Over Printed Name:</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">Date</sup></span><br>
						<span style="font-size: 15px; font-family: monospace; margin-left: 100px;">
							&nbsp;
						</span>
					</td>
				</tr>
			</table>
			<table width="100%" style="font-size: 10px; font-weight: bold">
				<tr>
					<td>50 Bklys. (50 x 4) 6501-9000</td>
				</tr>
				<tr>
					<td>BIR Authority to Print No. OCN4AU0001754059</td>
				</tr>
				<tr>
					<td>Date Issued : 3-15-17 Valid until: 3-14-22</td>
				</tr>
				<tr>
					<td>CJC PRINTING & TRADING</td>
					<td>Printer's Accreditation.: 25BMP20130000000013</td>
				</tr>
				<tr>
					<td>1514 By-pass Rd., Sta. Cruz Village, Sta. Cruz, Sta.Maria Bulacan</td>
					<td>Date Issued : 12-23-13</td>
				</tr>
				<tr>
					<td>EDGARDO O. CAPILI, Proprietor</td>
				</tr>
				<tr>
					<td>TIN: 103-362-643-000 Non-Vat</td>
				</tr>
				<tr><td style="text-align: center" colspan="2">THIS DOCUMENT IS NOT VALID FOR CLAIMING INPUT TAXES</td></tr>
				<tr><td style="text-align: center" colspan="2">THIS DELIVERY RECEIPT SHALL BE FOR FIVE (5) YEARS FROM THE DATE OF ATP</td></tr>
			</table>
	</body>
</html> 