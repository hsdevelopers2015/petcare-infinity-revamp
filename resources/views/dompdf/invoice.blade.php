<html>
<head>
<title>Invoice {{$sales->transaction_code}}</title>
<style type="text/css">
html
{
	font-family:Calibri (Body);
	background-color: #000;
}

body
{
	margin: 15px, 20px, 20px, 20px;
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;}


}

.fonts-size
{
	font-size: 7px;
	width: 100%;
	text-align: right;
}

table
{
	padding-bottom: 0px;
}

.invoice
{

	font-size: 7px;
	color: maroon;
	padding-left: 300px;
	padding-bottom: 0px;
	padding-top: 0px;   
}

.invoice-under
{
	margin-top: -50px;
	padding-left: 300px;
}
.table-amount
{

	text-align: center;
	font-style: bold;
	font-size: 12px;
}

.table-amount td tr
{ 
	border: 1px solid black;
}


.parag-bottom
{
	text-align: center;
	font-size: 7px;
}

.thanks
{
	text-align: center;
	color: red;
}

.last-parag
{
	text-align: center;
	color: black;
	font-size: 7px;
	font-weight: bold;
}

.italize
{
	font-style: italic;
}

.footer-accounts
{
	padding-right: 60px;
	font-size: 7px;
	padding-top: 30px;
}

.computation
{
	font-weight: bold;
	padding-left: 350px;
}

.settle
{
	padding-left: 60px;
	padding-right: 30px;
	font-size: 7px;
	padding-bottom: 0px;
}

.bold
{
	font-weight: bold;
}

.heading
{
	text-align: center;
	margin: 0px;
	font-size: 7px;
}
.header
{
	width: 100%;
}

.wide
{
	width: 400px;
}

.small
{
	font-size: 7px;
}
.padding
{
	padding: 2px;
}
*{
	font-size: 15px!important;
}
</style>
</head>
<body style=" padding: -40px -30px -50px -30px;">
	<div class = "main-container" >
		<table style="text-align: center; width: 100%;">
			<tr>
				<td style="width: 450px; font-size: 30px;"><b>&nbsp;{{-- INFINITY PETCARE, INC. --}}<b></td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">&nbsp;{{-- J. Barcial Hulo , Mag - Asawang Sapa, Santa Maria, Bulacan --}}</td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">&nbsp;{{-- VAT REG. TIN : 009-122-028-000 --}}</td>
			</tr>
			<tr>
				<td style="width: 500px; font-size: 13px;">&nbsp;{{-- Tel.: (02) 861-2740 / Email Add: infinitypetcare@yahoo.com.ph --}}</td>
			</tr>
		</table>
		<br>
		<table style="width: 100%; margin-top: -10px;" >
			<tr>
				<td>
					<p style="font-size: 20px;"><b>&nbsp;{{-- SALES INVOICE --}}</b></p>
				</td>
				<td>
					{{-- <p style="font-size: 10px; float: right; font-family: Times New Roman; color: darkred"><b><span style="margin-left: -40px; margin-right: 40px;">NO. </span>{{$delivery_receipt->dr_number}}</b></p> --}}
				</td>
			</tr>
		</table>

		<table style="font-size: 13px; margin-top: -15px;" width="100%" cellspacing="">
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Delivered to: _________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Date: ________________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- TIN: ________________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Terms: _______________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Address: ____________________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- D.R. No.: ______________________ --}}
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;{{-- Business Style: _______________________________________________________ --}}
				</td>
				<td style="width: 30%">
					&nbsp;{{-- Salesman: ____________________ --}}
				</td>
			</tr>
		</table>

		<table style="font-size: 5px; margin-top: -70px; font-family: monospace; " width="100%">
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important; margin-left: 20px!important;">{{$sales->client_info($sales->client_id)->business_info->business_name}}</span>
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important;">{{date_format($sales->updated_at,'M d, Y ')}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					&nbsp;
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important; margin-left: 10px;">{{str_replace('_',' ',Str::title($sales->due_day))}} Days</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important; margin-left: 20px!important;">{{$sales->client_info($sales->client_id)->business_info->business_location}}</span>
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important;">{{$sales->dr_number}}</span>
				</td>
			</tr>
			<tr>
				<td style="width: 70%">
					<span style="font-size: 12px!important;">{{$sales->client_info($sales->client_id)->business_info->business_type}}</span>
				</td>
				<td style="width: 30%">
					<span style="font-size: 12px!important; margin-left: 15px!important">{{$sales->cluster_agent($sales->client_id)}}</span>
				</td>
			</tr>
		</table>

		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin-top: 5px;" width="100%" class = "table-amount" >
			<tr bgcolor="{{-- lightgrey --}}">
				<td class="padding" style="width: 10%;"><b>&nbsp;{{-- QTY --}}<b></td>
				<td class="padding" style="width: 10%;"><b>&nbsp;{{-- UNIT --}}<b></td>
				<td class="padding" style="width: 50%;"><b>&nbsp;{{-- DESCRIPTION --}}<b></td>
				<td class="padding" style="width: 12%"><b>&nbsp;{{-- U. PRICE --}}<b></td>
				<td class="padding" style="width: 20%;"><b>&nbsp;{{-- AMOUNT --}}<b></td>
			</tr>
			<?php 
				$overall_total = [];
			?>
			@foreach($items as $index => $info)
			<tr style="font-family: monospace; ">
				<td class="padding" style="font-size: 12px!important;">
					{{$info->qty}}
					<!-- @if($info->free)
					+ {{$info->free}}
					@endif -->
				</td>
				<td class="padding" style="font-size: 12px!important; padding-right: 5px;">{{$info->product_info($info->product_id)? $info->product_info($info->product_id)->unit:'---'}}</td>
				<td class="padding" style="font-size: 12px!important;">
					<span style="float: left; font-size: 12px!important;">
						{{$info->product_info($info->product_id)? $info->product_info($info->product_id)->product_name:'---'}}
						@if($info->free)
						+ {{$info->free}} Free 
						@endif
						
						@if($info->discount > 0)
						, Discount 
						@if($info->discount_type == 'percentage')
						<?php
						$total = $info->cost_unit * $info->qty;
						$discount = ($total*($info->discount/100))
						?>
						{{number_format($discount,2)}}({{number_format($info->discount)}}%)
						@else
						{{$info->discount}}
						@endif
						@endif
					</span>
				<?php 
					$total = $info->final_cost;
					array_push($overall_total,$total);
				?>
				</td>
				<td class="padding" style="font-size: 12px!important;">
					{{number_format($info->cost_unit,2)}}
				</td>
				<td class="padding" style="text-align: right; font-size: 12px!important;">
					{{number_format($total,2)}}
				</td>
			</tr>
			@endforeach
			@for($x = 1 ; $x <= $z = 11 - $y = $items->count() ; $x++)
			<tr height = "35px;">
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
				<td class="padding">&nbsp;</td>
			</tr>
			@endfor

		</table>

			<table style="width: 100%" cellpadding="0">
				<tr>
					<td style="width: 100%">
						<table border="0" cellpadding="10" cellspacing="0" style="width: 100%; margin-top: 35px;" >
							<tr>
								<td style="height: 40px; "><sup style="font-size: 12px!important;">&nbsp;{{-- Prepared by: --}}</sup> <br> 
									<span style="font-size: 12px!important;;  font-family: monospace; margin-left: 40px;">{{$sales->sales_agent_info($sales->sa_id)->fname}} {{$sales->sales_agent_info($sales->sa_id)->lname}}</span>
								</td>
							</tr>
							<tr>
								<td style="height: 40px;"><sup style="font-size: 12px!important;">&nbsp;{{-- Approved by: --}}</sup> <br>
									<span style="font-size: 7px; font-family: monospace; margin-left: 40px;">{{$sales->approved_by}}</span>
								</td>
							</tr>
						</table>
					</td>
					<td style="width: 100%">
						<table border="0" cellpadding="5" cellspacing="0" style="width: 100%; margin-top: 3px; margin-left: 5px;" >
							<tr>
								<td style="width: 60%; text-align: right;">
									&nbsp;{{-- Vatable Sales --}}
								</td>
								<td style="width: 40%; text-align: right; font-size: 12px!important;">
									<span style="font-size: 12px!important; font-family: monospace; float: right;">
									<?php
									$total_discount = [];
									$discount = 0;
									?>
									@foreach($discounts as $index => $info)
									@if($info->discount_type == 'percentage')
									({{$info->discount_amount}}%) {{$info->sales_items($sales->id)*($info->discount_amount/100)}}<br>
									<?php
									array_push($total_discount,$info->sales_items($sales->id)*($info->discount_amount/100));
									?>
									@else
									{{number_format($info->discount_amount,2)}}<br>
									<?php
									array_push($total_discount,$info->discount_amount); 
									?>
									@endif
									@endforeach
									<?php 
									$discount = array_sum($total_discount);
									?>
									<?php
									$grand_total = array_sum($overall_total) - $discount;
									?>
									</span>
								</td>
							</tr>
							<tr>
								<td style="width: 60%; text-align: right;">
									&nbsp;{{-- Vat-Exempt Sales --}}
								</td>
								<td style="width: 40%; text-align: right;">
								</td>
							</tr>
							<tr>
								<td style="width: 60%; text-align: right;">
									&nbsp;{{-- Zero Rated Sales --}}
								</td>
								<td style="width: 40%;text-align: right;">
								</td>
							</tr>
							<tr>
								<td style="width: 60%; text-align: right; font-size: 12px!important;">
									<!-- &nbsp;{{$sales->vat_amount}} -->
								</td>
								<td style="width: 40%; text-align: right;">
								</td>
							</tr>
							<tr>
								<td style="width: 60%; text-align: right;">
									<strong>&nbsp;{{-- TOTAL AMOUNT DUE --}}</strong>
								</td>
								<td style="width: 40%; text-align: right;">
									<span style="font-family: monospace; margin-top: 20px!important">
										<strong style="font-size: 12px!important; ">{{number_format($grand_total,2)}}<strong>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<center><span><strong style="font-size: 13px;">&nbsp;{{-- RECEIVED ABOVE PRODUCTS IN GOOD ORDER AND CONDITION --}}</strong></span></center>
			
			<table border="0" cellpadding="5" cellspacing="0" style="width: 100%; " >
				<tr>
					<td style="width: 50%">
						<sup style="font-size: 13px">&nbsp;{{-- Delivered by: --}}</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">&nbsp;{{-- Date --}}</sup></span>
						<span style="font-size: 12px!important;;  font-family: monospace; margin-left: 40px;">{{$sales->cluster_agent($sales->client_id)}}</span>
					</td>
					<td style="width: 50%">
						<sup style="font-size: 13px">&nbsp;{{-- Signature Over Printed Name: --}}</sup> 
						<span style="float: right; margin-right: 50px;"><sup style="font-size: 13px">&nbsp;{{-- Date --}}</sup></span><br>
						<span style="font-size: 7px; font-family: monospace; margin-left: 40px;">
							&nbsp;
						</span>
					</td>
				</tr>
			</table>
	</body>
</html>