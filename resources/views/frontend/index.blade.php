@extends('frontend._layouts.main')

@section('content')
<div class="row">
    <a href="{{route('frontend.invoices.index')}}">
        <div class="col-md-4 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-primary bg-darken-2 media-left media-middle">
                           <i class="fa fa-file-text-o font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-gradient-x-primary white media-body">
                            <h5>Invoice</h5>
                            <h5 class="text-bold-400"><!-- <i class="ft-plus"></i>  -->{{$auth->invoice_count($auth->id)->count()}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a href="{{route('frontend.orders.index')}}" >
        <div class="col-md-4 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-danger bg-darken-2 media-left media-middle">
                            <i class="icon-basket-loaded font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-gradient-x-danger white media-body">
                            <h5>New Orders</h5>
                            <h5 class="text-bold-400">{{-- <i class="ft-arrow-up"></i> --}}{{$auth->total_orders($auth->id)->count()}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a href="{{route('frontend.orders.create')}}" data-toggle="modal" data-target="#confirm-create">
        <div class="col-md-4 col-xs-12">

            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-warning bg-darken-2 media-left media-middle">
                            <i class="icon-bag font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-gradient-x-warning white media-body">
                            <h5>Order Now</h5>
                            <h5 class="text-bold-400">&nbsp;</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    {{-- <div class="col-md-4 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="media">
                    <div class="p-2 text-xs-center bg-success bg-darken-2 media-left media-middle">
                        <i class="icon-wallet font-large-2 white"></i>
                    </div>
                    <div class="p-2 bg-gradient-x-success white media-body">
                        <h5>Total Profit</h5>
                        <h5 class="text-bold-400"><i class="ft-arrow-up"></i> 5.6 M</h5>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="grid-hover">
        @if($promoted_products->count() > 0)
        <h4 class="col-xs-12 mt-1">Products</h4>
        @foreach($promoted_products as $index => $info)
        <div class="col-md-4 ">
            <figure class="effect-roxy">
                <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="img15" />
                <figcaption>
                    <h2>{{$info->product_info($info->product_id)->product_name}} <span>{{-- Roxy --}}</span></h2>
                    <p>{{$info->description}}
                    </p>
                    <a href="{{route('frontend.orders.create')}}" data-toggle="modal" title="Order Now" data-target="#confirm-create">View more</a>
                </figcaption>
            </figure>
        </div>
        @endforeach
        @endif
    </div>
</div>
@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>
			<form action="{{route('frontend.orders.create')}}" method="POST">
			<div class="modal-body">
				
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	<strong>Hello !</strong>, Are you sure you want to create an order ?
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element">Create</button>
			</div>
			</form>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('frontend._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/pages/gallery.min.css')}}">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
@stop

@section('page-scripts')

@stop