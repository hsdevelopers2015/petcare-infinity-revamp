<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    @include('frontend._components.metas')
    {{-- @include('frontend._includes.styles') --}}
    @yield('page-styles')
  </head>
  <body data-open="click" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns ">

    <!-- navbar-fixed-top-->
    @include('frontend._components.header')

    <!-- Horizontal navigation-->
    <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
      <!-- Horizontal menu content-->
      <div data-menu="menu-container" class="navbar-container main-menu-content container center-layout">
        <!-- include includes/mixins-->
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
          @include('frontend._components.nav')
        </ul>
      </div>
      <!-- /horizontal menu content-->
    </div>
    <!-- Horizontal navigation-->

    <div class="app-content container center-layout mt-2">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Stats -->
          @yield('content')
        </div>
      </div>
    </div>
    @yield('page-modals')

    @include('frontend._components.footer')
    
    @include('frontend._components.notification-status')

    @include('frontend._includes.scripts')

    @yield('page-scripts')
  </body>


</html>