@extends('frontend._layouts.auth')
@section('content')
<div class="content-header row"></div>
<div class="content-body">
    <section class="flexbox-container">
        <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-3 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header no-border">
                    <div class="card-title text-xs-center">{{-- <img alt="branding logo" src="{{asset('backoffice/images/logo/stack-logo-dark.png')}}"> --}}
                    {{-- <h4>{{env('APP_TITLE','Localhost')}} Admin Panel</h4> --}}
                    <img src="{{asset('frontend/face0.jpg')}}" alt="" class="media-object avatar avatar-lg rounded-circle">
                    </div>
                </div>
                <div class="card-body collapse in">
                    <p class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2 my-1"><span>Welcome to {{env('APP_TITLE','localhost')}} Login Form</span></p>
                    <div class="card-block">
                        <form action="" class="form-horizontal" method="post">
                        <input type="hidden" name="_token" value={{csrf_token()}}>
                            @include('frontend._components.notification')
                            <fieldset class="form-group position-relative has-icon-left">
                                <input class="form-control" id="user-name" name="username" placeholder="Your Username" type="text">
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input class="form-control" id="user-password" name="password" placeholder="Enter Password" type="password">
                                <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group row">
                                <div class="col-md-6 col-xs-12 text-xs-center text-sm-left">
                                    <fieldset>
                                        <input class="chk-remember" id="remember-me" name="remember_me" type="checkbox"> <label for="remember-me">Remember Me</label>
                                    </fieldset>
                                </div>
                            </fieldset>
                            <button class="btn btn-outline-primary btn-block" type="submit"><i class="ft-unlock"></i> Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop