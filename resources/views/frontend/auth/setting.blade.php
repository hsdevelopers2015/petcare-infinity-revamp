@extends('frontend._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('frontend.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">{{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						<li><a data-action="close"><i class="ft-x"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-user"></i> About User</h4>
							<div class="row mb-3 mt-3">
								<div class="col-md-12">
									<label class="col-md-1 label-control"></label>
									<div class="col-md-2">
										@if($auth->filename)
										<img src="{{$auth->directory.'/thumbnails/'.$auth->filename}}" class="rounded-circle  height-150 img-thumbnail" alt="Card image" id="blah">
										@else
										<img src="{{asset('frontend/face0.jpg')}}" class="rounded-circle  height-150" alt="Card image" id="blah">
										@endif
									</div>
									<div class="col-md-4 mt-3">
										@if($auth->filename)
										<h3>Current Thumbnail</h3>
										@else
										<h3>No Current Thumbnail</h3>
										@endif
										<input type="file" name="file" value="{{old('file')}}" data-toggle="tooltip" title="{{$errors->first('file') ? $errors->first('file') : NULL}}" id="imgInp">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput1">Fist Name</label>
										<div class="col-md-9">
											<input type="text" name="fname" id="userinput1" class="form-control {{$errors->first('fname') ? 'border-danger' : NULL}}" placeholder="First Name" value="{{old('fname',$auth->fname)}}" data-toggle="tooltip" title="{{$errors->first('fname') ? $errors->first('fname') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput2">Last Name</label>
										<div class="col-md-9">
											<input type="text" name="lname" id="userinput2" class="form-control {{$errors->first('lname') ? 'border-danger' : NULL}}" placeholder="Last Name" value="{{old('lname',$auth->lname)}}" data-toggle="tooltip" title="{{$errors->first('lname') ? $errors->first('lname') : NULL}}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput3">Username</label>
										<div class="col-md-9">
											<input type="text" name="username" id="userinput3" class="form-control {{$errors->first('username') ? 'border-danger' : NULL}}" placeholder="Username" value="{{old('username',$auth->username)}}" data-toggle="tooltip" title="{{$errors->first('username') ? $errors->first('username') : NULL}}">
										</div>
									</div>
								</div>
								{{-- <div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput4">Nick Name</label>
										<div class="col-md-9">
											<input type="text" id="userinput4" class="form-control {{$errors->first('nickname') ? 'border-danger' : NULL}}" placeholder="Nick Name" name="nickname">
										</div>
									</div>
								</div> --}}
							</div>

							<h4 class="form-section"><i class="ft-mail"></i> Contact Info </h4>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput5">Email</label>
										<div class="col-md-9">
											<input class="form-control {{$errors->first('email') ? 'border-danger' : NULL}}" name="email" type="email" placeholder="Email" id="userinput5" value="{{old('email',$auth->email)}}" data-toggle="tooltip" title="{{$errors->first('email') ? $errors->first('email') : NULL}}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 label-control">Contact Number</label>
										<div class="col-md-9">
											<input class="form-control col-md-9 {{$errors->first('contact') ? 'border-danger' : NULL}}" name="contact" type="tel" placeholder="Contact Number" id="userinput7" value="{{old('contact',$auth->contact)}}" data-toggle="tooltip" title="{{$errors->first('contact') ? $errors->first('contact') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput8">Address</label>
										<div class="col-md-9">
											<textarea id="userinput8" rows="5" class="form-control col-md-9 {{$errors->first('address') ? 'border-danger' : NULL}}" name="address" placeholder="Address" data-toggle="tooltip" title="{{$errors->first('address') ? $errors->first('address') : NULL}}">{{old('address',$auth->address)}}</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="left">
						<hr>
							<a href="{{route('frontend.dashboard')}}" class="btn btn-danger mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('frontend._includes.styles')
@stop

@section('page-scripts')
@include('frontend._includes.scripts')
<script>
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#blah').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#imgInp").change(function(){
		readURL(this);
	});
</script>
@stop
