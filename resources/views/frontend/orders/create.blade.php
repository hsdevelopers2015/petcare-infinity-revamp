@extends('frontend._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}} | Transaction Code: {{$transaction_code}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('frontend.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('frontend.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Create {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-6">
		<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
		</div>
		@if($order->status == 'draft')
		<a href="#" class="btn btn-outline-primary pull-right action-create" data-url="{{route('frontend.'.$route_file.'.submit',[$order->id])}}" data-toggle="modal" data-target="#confirm-create"><i class="ft-check"></i>&nbsp;&nbsp;Submit your order&nbsp;</a>
		@endif

	</div>
</div>
<div class="row">
	@if($order->status == "draft")
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">ADD ITEM FORM</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						<li><a data-action="close"><i class="ft-x"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					{{-- <div class="card-text">
						<p>{!!$page_description!!}</p>
					</div> --}}
					<form class="form form-horizontal" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="transaction_id" value="{{$order->id}}">
						<div class="form-body">
							<h4 class="form-section">
										<i class="ft-box"></i>Add Item to your order
									{{-- <div class="col-md-6">
										<button class="btn btn-outline-primary btn-sm pull-right" type="button" data-toggle="modal" data-target="#mod" >Mode of Payment</button>
										<small class="pull-right" style="font-size: 12px; margin-top: -10px; margin-bottom: -10px;">The current mode of payment is set to <code>{{str_replace('_', ' ', Str::upper($order->mode_of_payment))}}</code></small>
									</div> --}}
							</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<?php
										if($errors->first('product_id')){
											$status = 'border-danger';
											$msg = $errors->first('product_id');
										}else{
											$status = "";
											$msg  = "";
										}
										?>
										<div class="col-md-12" data-toggle="tooltip" title="{{$msg}}">
											{!!Form::select("product_id", $products, old('product_id'), ['id' => "select2-array ", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 ".$status]) !!}
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<div class="col-md-12">
											<input type="number" name="qty" id="userinput3" class="form-control {{$errors->first('qty') ? 'border-danger' : NULL}}" placeholder="Quantity" min="1" value="{{old('qty')}}" data-toggle="tooltip" title="{{$errors->first('qty') ? $errors->first('qty') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary overlay-unblock">
										<i class="fa fa-check-square-o"></i> Add Item
									</button>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
	@endif
	<div class="col-md-12">
		<div class="card">
			<section id="file-export">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Item List</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										<li><a data-action="close"><i class="ft-x"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<p class="card-text">Here are the list of all <code>{{$page_title}}</code> in this application.<strong> Manage each row by clicking the action button on the far right portion of the table.</strong></p>
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Product Code</th>
												<th>Product Name</th>
												<th>Quantity</th>
												<th style="width: 20%;">Last Modified</th>
												@if($order->status == 'draft')
												<th style="width: 3%;"></th>
												@endif
											</tr>
										</thead>
										<tbody>
										@foreach($items as $index => $info)
											<tr>
												@if($info->product_info($info->product_id))
												<td>{{$info->product_info($info->product_id)->product_code}}</td>
												<td>{{$info->product_info($info->product_id)->product_name}}</td>
												@else
												<td>---</td>
												<td>---</td>
												@endif
												<td>{{$info->qty}}</td>
												<td>{{$info->last_modified()}}</td>
												@if($order->status == 'draft')
												<td>
													<a href="{{route('frontend.orders.remove_item',[$info->id,$info->transaction_id])}}" class="btn btn-outline-danger"><i class="fa fa-trash"></i> Remove Item</a>
												</td>
												@endif
											</tr>
											@endforeach
										</tbody>
									</table>
									<a href="{{route('frontend.orders.index')}}" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i> Back to the order list</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@stop

@section('page-modals')
<div id="confirm-create" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirmation</h5>
			</div>

			<div class="modal-body">
				<h5>Do you want to proceed?</h5>
	            <div class="alert alert-icon-right alert-info alert-dismissible fade in mb-2" role="alert">
	            	{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            		<span aria-hidden="true">×</span>
	            	</button> --}}
	            	<strong>Note :</strong> Upon confirming it will not be editable any further.
	            </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<a href="" class="btn btn-outline-info block-element" id="btn-confirm-create">Submit your order</a>
			</div>
		</div>
	</div>
</div>

<div id="mod" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Mode of Payment</h5>
			</div>
			<form action="{{route('frontend.orders.mod',[$order->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Choose a mode of payment</label>
						{!!Form::select("mode_of_payment", $modes, old('mode_of_payment',$order->mode_of_payment), ['id' => "select2-array", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 mod"]) !!}
					</div>
				</div>
				<div class="row payment_term ">
					<div class="col-md-12">
						<label>Choose payment term</label>
						{!!Form::select("payment_term", $terms, old('payment_term',$order->payment_term), ['id' => "select2-array ", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 "]) !!}
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Update</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div id="ups" class="modal fade">
	<div class="modal-dialog card">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Update Payment Status</h5>
			</div>
			<form action="{{route('frontend.orders.ups',[$order->id])}}" method="POST">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Payment Status</label>
						{!!Form::select("payment_status", $statuses, old('payment_status',$order->payment_status), ['id' => "select2-array", 'class' => "form-control select2 product  col-xs-12 col-sm-12 col-md-12 col-lg-5 mod"]) !!}
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-info block-element" >Update</button>
			</div>
			</form>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('frontend._includes.styles')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@stop

@section('page-scripts')
<script type="text/javascript" src="{{asset('frontend/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/js/scripts/tooltip/tooltip.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('frontend/app-assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/js/core/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script type="text/javascript" src="{{asset('frontend/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js')}}"></script>
<script src="{{asset('frontend/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/app-assets/js/scripts/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(".action-create").on("click",function(){
		var btn = $(this);
		$("#btn-confirm-create").attr({"href" : btn.data('url')});
	});

	$( ".product" ).change(function() {
		var val = $(this).val();
		<?php 
		foreach($product_lists as $index => $info){
		?>
			if(val == "<?php echo $info->id ;?>"){
				$("#userinput6").val("<?php echo $info->cost;?>");
				$("#userinput3").attr('max',"<?php echo $info->physical_count($info->id)->quantity;?>");
			}
		<?php
		}
		?>
	});
</script>

<script src="{{asset('frontend/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(".payment_term").ready(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});
	$(".mod").change(function(){
		var mod = $(".mod").val();
		if(mod != 'cash_on_delivery'){
			$(".payment_term").show(500);
		}else{
			$(".payment_term").hide(500);
		}
	});
</script>
@stop
