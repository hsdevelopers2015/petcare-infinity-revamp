<li class="nav-item {{Helper::is_active($routes['dashboard'])? 'active':NULL}}">
    <a class="nav-link" href="{{route('frontend.dashboard')}}"><i class="ft-home"></i><span>Dashboard</span></a>
</li>

<li class="nav-item {{Helper::is_active($routes['business'])? 'active':NULL}}">
	<a href="{{route('frontend.business.info')}}" class="nav-link" aria-expanded="true"><i class="ft-briefcase"></i><span>Business</span></a>
</li>

<li  data-menu="dropdown"  class="dropdown nav-item {{Helper::is_active($routes['branches'])? 'active':NULL}}">
	<a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link" aria-expanded="true"><i class="ft-map-pin"></i><span>Branches</span></a>
	<ul class="dropdown-menu">
		<li data-menu="">
			<a class="dropdown-item" href="{{route('frontend.branches.index')}}">Record Data</a>
		</li>
		<li data-menu="">
			<a class="dropdown-item" href="{{route('frontend.branches.create')}}">Create</a>
		</li>
	</ul>
</li>

<li class=" nav-item {{Helper::is_active($routes['invoices'])? 'active':NULL}}">
	<a href="{{route('frontend.invoices.index')}}" class=" nav-link" aria-expanded="true"><i class="fa fa-file-text-o"></i><span>Invoice</span></a>
</li>

<li class=" nav-item {{Helper::is_active($routes['orders'])? 'active':NULL}}">
	<a href="{{route('frontend.orders.index')}}" class=" nav-link" aria-expanded="true"><i class="icon-basket-loaded"></i><span>Orders</span></a>
</li>

@if($contracts_count > 0)
<li class=" nav-item {{Helper::is_active($routes['contracts'])? 'active':NULL}}">
	<a href="{{route('frontend.contracts.index')}}" class=" nav-link" aria-expanded="true"><i class="fa fa-file-o"></i><span>Contracts</span></a>
</li>
@endif


