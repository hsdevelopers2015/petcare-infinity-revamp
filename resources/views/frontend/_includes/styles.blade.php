<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/fonts/feather/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/vendors/css/extensions/pace.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/vendors/css/extensions/unslider.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/fonts/meteocons/style.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/vendors/css/extensions/toastr.css')}}">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/bootstrap-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/app.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/colors.min.css')}}">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/fonts/simple-line-icons/style.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/core/colors/palette-gradient.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/pages/timeline.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/app-assets/css/plugins/extensions/toastr.min.css')}}">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/style.css')}}">
    <!-- END Custom CSS-->