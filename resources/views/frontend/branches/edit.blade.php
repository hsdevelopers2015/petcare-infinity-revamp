@extends('frontend._layouts.main')

@section('content')
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">{{$page_title}}</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('frontend.dashboard')}}">Home</a>
					</li>
					<li class="breadcrumb-item"><a href="{{route('frontend.'.$route_file.'.index')}}">List</a>
					</li>
					<li class="breadcrumb-item active"><a href="#">Edit {{$page_title}}</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title" id="horz-layout-colored-controls">{{$page_title}} Form</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						<li><a data-action="close"><i class="ft-x"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						<p>{!!$page_description!!}</p>
					</div>
					<form class="form form-horizontal" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-body">
							<h4 class="form-section"><i class="ft-briefcase"></i>Branch Info</h4>
							<div class="row mt-3">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput31">Branch Name</label>
										<div class="col-md-9">
											<input type="text" name="branch_name" id="userinput31" class="form-control {{$errors->first('branch_name') ? 'border-danger' : NULL}}" placeholder="Branch Name" value="{{old('branch_name',$branch->branch_name)}}" data-toggle="tooltip" title="{{$errors->first('branch_name') ? $errors->first('branch_name') : NULL}}">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="col-md-3 label-control" for="userinput32">Branch Office Contact #</label>
										<div class="col-md-9">
											<input type="text" name="branch_contact" id="userinput32" class="form-control {{$errors->first('branch_contact') ? 'border-danger' : NULL}}" placeholder="Contact Number" value="{{old('branch_contact',$branch->branch_contact)}}" data-toggle="tooltip" title="{{$errors->first('branch_contact') ? $errors->first('branch_contact') : NULL}}">
										</div>
									</div>
								</div>
							</div>

							<div class="row mb-3">
								<label class="col-md-1 label-control">Establishment Picture</label>
								<div class="col-md-10 ml-1">
									<div class="col-md-10">
										<div class="col-md-6">
											<img src="{{asset($branch->directory.'/resized/'.$branch->filename)}}" class="height-150 img-responsive img-thumbnail" alt="Card image">
										</div>
										<div class="col-md-6">
											<h3 class="mt-3">Current Thumbnail</h3>
											<input type="file" name="file" class="mt-1" value="{{old('file')}}" data-toggle="tooltip" title="{{$errors->first('file') ? $errors->first('file') : NULL}}">
										</div>
										<label class="mt-1 ml-1">Select an image file if you want to <code>update</code> the business establishment picture.</label>
									</div>
								</div>
							</div>

							<div class="row mb-3">
								<label class="col-md-1 ml-3"></label>
								<div class="col-md-10">
									<label class="label-control" for="userinput35">Search the Branch Location</label>
									<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDxClplR-RGiqXBZdnGXA79IumthYXJ2wQ&libraries=places"></script>
									
									<input id="pac-input" class="controls form-control mt-1 {{$errors->first('branch_location') ? 'border-danger' : NULL}}" type="text" name="branch_location" value="{{old('branch_location',$branch->branch_location)}}" placeholder="Search Business Location" style="width: 30%;" data-toggle="tooltip" title="{{$errors->first('branch_location') ? $errors->first('branch_location') : NULL}}">
									<div class="container img-thumbnail" id="map-canvas" style="height:300px;"></div>
								</div>
							</div>
						</div>

						<div class="left">
						<hr>
							<a href="{{route('frontend.'.$route_file.'.index')}}" class="btn btn-danger mr-1">
								<i class="ft-x"></i> Cancel
							</a>
							<button type="submit" class="btn btn-primary overlay-unblock">
								<i class="fa fa-check-square-o"></i> Save
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-styles')
@include('frontend._includes.styles')
<style type="text/css">
	/*html,
	body,*/
	#map-canvas {
		margin: 0;
		padding: 0;
		height: 100%;
	}
</style>
@stop

@section('page-scripts')
<script type="text/javascript">
	 function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 12.9715987,
       lng: 77.59456269999998
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>
@stop
