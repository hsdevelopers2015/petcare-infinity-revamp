<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Client</td>
			<td>Business</td>
			<td>Branch</td>
			<td>Contact</td>
			<td>Location</td>
			<td>Establishment Picture</td>
			<td>Last Modified</td>
		</tr>
		@foreach($branches as $index => $info)
		<tr>
			<td>{{$info->user_info->fname.' '.$info->user_info->lname}}</td>
			<td>{{$info->business_info->business_name}}</td>
			<td>{{$info->branch_name}}</td>
			<td>{{$info->branch_contact}}</td>
			<td>{{$info->branch_location}}</td>
			<td>
				<a href="{{asset($info->directory.'/resized/'.$info->filename)}}" data-toggle="modal" data-target="#view-business-{{$info->id}}">{{asset($info->directory.'/resized/'.$info->filename)}}</a>
			</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>