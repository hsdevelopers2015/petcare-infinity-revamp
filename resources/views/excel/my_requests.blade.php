<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Request Code</td>
			<td>Description</td>
			<td>Status</td>
			<td>Last Modified</td>
		</tr>

		@foreach($requests as $index => $info)
		<tr>
			<td>{{$info->request_code}}</td>
			<td>{{$info->description}}</td>
			<td>{!!Helper::expense_status_badge($info->status)!!}</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>