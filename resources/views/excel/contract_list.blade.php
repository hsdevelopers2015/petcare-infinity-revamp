<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Contract Transaction Code</td>
			<td>Detail</td>
			<td>Last Modified</td>
		</tr>

		@foreach($contracts as $index => $info)
		<tr>
			<td>
				<a href="{{asset($info->directory.'/'.$info->filename)}}" download="{{$info->contract_code.'|Contract'}}" target="_blank" title="Download Contract" >
					{{$info->contract_code}}
				</a>
			</td> 
			<td>{{$info->detail}}</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>