<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Name</td>
			<td>Type</td>
			<td>Access</td>
			<td>Last Modified</td>
		</tr>
		@foreach($access_control as $index => $info)
		<tr>
			<td>{{$info->fname.' '.$info->lname}}</td> 
			<td>{{str_replace('_', ' ', Str::upper($info->type))}}</td>
			<td><strong class="{{$info->check_user_access($info->id)>0? 'success': 'primary'}}">{{$info->check_user_access($info->id)>0?'CUSTOM':'DEFAULT'}}</strong></td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>