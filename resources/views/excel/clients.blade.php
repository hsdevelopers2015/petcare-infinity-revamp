<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>SEGMENTATION</strong></td>
			<td><strong>BUSINESS LOCATION</strong></td>
			<td><strong>VETERENARY CLINIC/PET SUPPLY</strong></td>
			<td><strong>ADDRESS</strong></td>
			<td><strong>CONTACT PERSON</strong></td>
			<td><strong>CONTACT NO</strong></td>
			<td><strong>EMAIL</strong></td>
			<td><strong>HOME ADDRESS</strong></td>
			<td><strong>REMARKS</strong></td>
			<td><strong>BIRTHDATE</strong></td>
			<td><strong>PRC NUMBER</strong></td>
			<td><strong>ACCOUNT NUMBER</strong></td>
			<td><strong>ACCOUNT NAME</strong></td>
			<td><strong>BANK NAME</strong></td>
		</tr>

		@foreach($clients as $index => $info)
		<tr>
			<td>{{$info->segment}}</td>
			<td>{{$info->cluster($info->cluster_id)? Str::title($info->cluster($info->cluster_id)->island).' - Area '.$info->cluster($info->cluster_id)->area: NULL}}</td>
			<td>{{$info->business_info? $info->business_info->business_name : NULL}}</td>
			<td>{{$info->business_info? $info->business_info->business_location : NULL}}</td>
			<td>{{$info->fname}}</td>
			<td>{{$info->contact}}</td>
			<td>{{$info->email}}</td>
			<td>{{$info->address}}</td>
			<td>{{$info->remarks}}</td>
			<td>{{$info->birthdate}}</td>
			<td>{{$info->business_info? $info->business_info->prc_number : NULL}}</td>
			<td>{{$info->business_info? $info->business_info->purchaser_number : NULL}}</td>
			<td>{{$info->business_info? $info->business_info->puchaser : NULL}}</td>
			<td>{{$info->business_info? $info->business_info->bank_name : NULL}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>