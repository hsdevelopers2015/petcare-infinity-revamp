<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Type</td>

			@if(in_array($auth->type, ['admin','super_user','finance']))
			<td>Employee</td>
			@endif

			<td>Status</td>
			<td>Request Details</td>
			<td>Last Modified</td>
		</tr>

		@foreach($expenses as $index => $info)
		<tr>
			<td>{!!Helper::request_status_badge($info->type)!!}</td>

			@if(in_array($auth->type, ['admin','super_user','finance']))
			<td>{{$info->sa_info($info->sa_id)->fname}} {{$info->sa_info($info->sa_id)->lname}}</td>
			@endif
			<td>{!!Helper::expense_status_badge($info->status)!!}</td>
			<td>
				@if($info->type == "liquidation")
				Requested : <span class="text-primary">{{round($info->requested_amount,2)}}</span> <br>
				@endif
				@if($info->item_count($info->id)->count() > 0)
				Qty : <span class="text-success">{{$info->item_count($info->id)->count()}}</span><br>
				Total : <span class="text-success">{{round($info->total_amount,2)}}</span><br>
				@if($info->type == 'liquidation')
				Change : <span class="text-success">{{round($info->change,2)}}</span><br>
				Reimbursable : <span class="text-warning">{{round($info->reimbursement_amount,2)}}</span>
				@endif
				@endif
			</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>