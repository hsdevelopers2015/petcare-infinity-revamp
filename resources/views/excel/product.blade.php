<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>PRODUCT NAME</strong></td>
			<td><strong>CATEGORY</strong></td>
			<td><strong>PRICE</strong></td>
			<td><strong>UNIT</strong></td>
			<td><strong>REPLENISH</strong></td>
			<td><strong>CONDITION</strong></td>
			<td><strong>INCENTIVE</strong></td>
		</tr>

		@foreach($products as $index => $info)
		<tr>
			<td>{{$info->product_name}}</td>
			<td>{{$info->category}}</td>
			<td>{{$info->cost}}</td>
			<td>{{$info->unit}}</td>
			<td>{{$info->replenish}}</td>
			<td>{{$info->deal_condition}}</td>
			<td>{{$info->deal_incentive}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>