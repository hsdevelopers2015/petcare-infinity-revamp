<html>
<body>
	<table>
		<tr style="background-color: green;">
			<th style="width: 30%">Transaction Code</th>
			<th style="width: 40%">Client Info</th>
			<th style="width: 30%">Status</th>
			<th style="width: 30%">Total Qty</th>
			<th style="width: 30%">Last Modified</th>
		</tr>
		@foreach($deliveries as $index => $info)
		<tr>
			<td>
				@if($info->status=='for_delivery' OR $info->status=="posted_cancelled")
				<a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}">
				@else
				<a href="{{route('backoffice.sales.add',[$info->transaction_code])}}">
				@endif
					{{$info->transaction_code}}
				</a>
			</td>
			<td><strong>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</strong><br>
				Area {{$info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->area:'---'}}<br>
				Location: {{Str::title($info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->island:'---')}}<br>
				City: {{$info->cluster_info($info->client_id)?$info->cluster_info($info->client_id)->city:'---'}}<br>
				<strong>{{$info->client_info($info->client_id)->business_info->business_name}}</strong>
			</td>
			<td>{!!Helper::sales_badge($info->status)!!}
				@if($info->status == "for_delivery")
				<br>
				{{$info->updated_at->diffForHumans()}}
				@endif
			</td>
			<td style="text-align: center">{{number_format($info->total_qty)}}</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>