<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>PRODUCT CODE</strong></td>
			<td><strong>PRODUCT</strong></td>
			<td><strong>CATEGORY</strong></td>
			<td><strong>QUANTITY</strong></td>
		</tr>
		@foreach($physical_count as $index => $info)
		<tr>
			<td>{{$info->product->product_code}}</td>
			<td>{{$info->product->product_name}}</td>
			<td>{{$info->product->category}}</td>
			<td>{{$info->quantity}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>