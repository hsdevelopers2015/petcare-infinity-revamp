<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Contract Transaction Code</td>
			<td>Detail</td>
			<td>Client</td>
			@if($auth->type != "sales_agent")
			<td>Sales Agent</td>
			@endif
			<td>Sent at</td>
		</tr>

		@foreach($sent_contracts as $index => $info)
		<tr>
			<td>
				<a href="{{asset($info->contract_info($info->contract_id)->directory.'/'.$info->contract_info($info->contract_id)->filename)}}" download="{{$info->contract_info($info->contract_id)->contract_code.'|Contract'}}" target="_blank" title="Download Contract" >
					{{$info->contract_info($info->contract_id)->contract_code}}
				</a>
			</td> 
			<td>{{$info->contract_info($info->contract_id)->detail}}</td>
			<td>{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}</td>
			@if($auth->type != "sales_agent")
			<td>{{$info->sa_info($info->sa_id)->fname}} {{$info->client_info($info->sa_id)->lname}}</td>
			@endif
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>