<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>Transaction Code </strong></td>
			<td><strong>Product Info</strong></td>
			<td><strong>Expiration Date</strong></td>
			<td><strong>Supplier Name</strong></td>
			<td><strong>Last Modified</strong></td>
		</tr>
		@foreach($product_returns as $index => $info)
		<tr>
			<td>{{$info->transaction_code}}</td>
			<td>
				@if($info->stock_in->product)
				{{$info->stock_in->product->product_code? : '---'}}<br>
				@else
				(the product is deleted)<br>
				@endif
				<strong>{{$info->stock_in->transaction_code? : '---'}}</strong><br>
				Returned Qty: {{$info->qty}}
			</td>
			<td>{{Helper::date_format($info->expiration_date,'F d, Y')}}</td>
			<td>{{$info->stock_in->supplier_name? : '---'}}</td>
			<td>{{$info->last_modified()}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>