<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>TRANSACTION CODE</strong></td>
			<td><strong>PRODUCT</strong></td>
			<td><strong>ACQUISITION COST</strong></td>
			<td><strong>QUANTITY</strong></td>
			<td><strong>SUPPLIER NAME</strong></td>
			<td><strong>STATUS</strong></td>
			<td><strong>EXPIRATION DATE</strong></td>
			<td><strong>ADDED AT</strong></td>
		</tr>
		@foreach($stock_in as $index => $info)
		<tr>
			<td>{{$info->transaction_code}}</td>
			<td>{{$info->product->product_name}}</td>
			<td>{{$info->acquisition_cost}}</td>
			<td>{{$info->qty}}</td>
			<td>{{$info->supplier_name}}</td>
			<td>{{$info->status}}</td>
			<td>{{Helper::date_format($info->expiration_date,'F d, Y')}}</td>
			<td>{{Helper::date_format($info->created_at,'F d, Y')}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>