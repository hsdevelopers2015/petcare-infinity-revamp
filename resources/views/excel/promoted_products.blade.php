<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td><strong>PRODUCT CODE</strong></td>
			<td><strong>PRODUCT NAME</strong></td>
			<td><strong>DESCRIPTION</strong></td>
		</tr>

		@foreach($products as $index => $info)
		<tr>
			<td>{{$info->product_info($info->product_id)->product_code}}</td>
			<td>{{$info->product_info($info->product_id)->product_name}}</td>
			<td>{{$info->description}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>