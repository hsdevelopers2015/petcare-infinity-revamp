<html>
<body>
	<table>
		<tr style="background-color: green;">
			<td>Transaction Code</td>
			<td>Establishment Name</td>
			<td>Client Name</td>
			<td>Transaction Details</td>
		</tr>
		@foreach($stock_out as $index => $info)
		<tr>
			<td>
				@if($info->status=='posted' OR $info->status=="posted_cancelled")
				<a target="_blank" href="{{route('backoffice.sales.invoice',[$info->id.'-'.Str::slug($info->transaction_code)])}}">
				@else
				<a href="{{route('backoffice.sales.add',[$info->transaction_code])}}">
				@endif
					{{$info->transaction_code}}
				</a>
			</td>
			<td>
				<strong>{{$info->client_info($info->client_id)->business_info->business_name}}</strong>
			</td>
			<td>
				{{$info->client_info($info->client_id)->fname}} {{$info->client_info($info->client_id)->lname}}
			</td>
			<td>
				<strong>Qty:</strong> {{number_format($info->total_qty)}} ( {{$info->total_amount}} )
				@if($info->mode_of_payment!='cash_on_delivery')
				<br><strong>Payment Term:</strong> {{str_replace('_', ' ', $info->payment_term)}}
				@endif
				<br><strong>Payment Status:</strong><br> {!!Helper::payment_status_badge($info->payment_status)!!}
				@if($info->payment_status == "partially_paid" AND $info->balance > 0)
				<br><strong>Balance: </strong>{!!$info->balance!!}
				@endif
				<br><strong>Mode of Payment:</strong><br> {!!Helper::sales_status_badge($info->mode_of_payment)!!}
				@if($info->payment_status != 'fully_paid' AND $info->status == 'received')
				<br><span style="float: right">{{ $info->aging }}%</span>
				@if($info->aging == 0)
				<span>Over Due</span>
				<progress title="{{ $info->aging }}%" class="progress progress-sm progress-danger mb-1" value="{{ $info->aging }}" max="100"></progress>
				@elseif($info->aging <= 10)
				<span> Need to Collect</span>
				<progress title="{{ $info->aging }}%" class="progress progress-sm progress-danger mb-1" value="{{ $info->aging }}" max="100"></progress>
				@elseif($info->aging <= 50)
				<span>Needs Attention</span>
				<progress title="{{ $info->aging }}%" class="progress progress-sm progress-warning mb-1" value="{{ $info->aging }}" max="100"></progress>
				@else
				<span>Collectible</span>
				<progress title="{{ $info->aging }}%" class="progress progress-sm progress-success mb-1" value="{{ $info->aging }}" max="100"></progress>
				@endif
				@endif
				<br><strong>Status: </strong>{!!Helper::sales_badge($info->status)!!}
			</td>
		</tr>
		@endforeach
	</table>
</body>
</html>